$(function () {

    loadJSPlugins();

    	/*
    	@event		: on form insert/edit submit
    	@function	: To open dialog box/confirmation on modal helper
    	 */
	$(document).on('click', 'form.crud button.submit', function () {
		if ($(this).val() == 'delete')
	  		return;        

	  	if($(this).val() != ""){
	  		$('#modal1 a').attr('value',$(this).val())
	  	}

		$('#modal1').openModal();
		return false;
	});

    	/*
    	@event		: on click 'yes/ok' button (positive response) on modal/dialog box
    	@function	: To commit inserting/editing data
    	@Note 	: rel confirm submit is an attribute on modal button
    	 */
	$(document).on('click', 'a[rel=confirm-submit]', function () {
		var close_form = ((typeof($(this).attr('value')) == 'undefined') || $(this).attr('value') == "") ? true : false;
		var serialized_formdata = $('#form_data').serialize();

		if(close_form == false){
			serialized_formdata = serialized_formdata + '&current_path=' + $(this).attr('value') + '&frame=view-content';
		}

		do_submit(serialized_formdata, $('#form_data').attr('action'), close_form);
	});

    	/*
    	@event		: on click cancel/back button
    	@function	: To redirect to specific url
    	@Note		: write the url destination on value attribute
    	 */
	$(document).on('click', 'a[rel=redirect-to-other-page]', function () {
		remove_notifications();
		var fetch_url = $(this).attr('value');

		$('#container').slideUp('normal', function () {
			$('#container').load(fetch_url, function (data, status, xhr) {
			    $(this).slideDown('normal');
			    loadJSPlugins();
			});
		});
	});

    	/*
    	@event		: on form submit
    	@function	: To redirect to specific url
    	@Note		: write the url destination on value attribute
    	 */
	$(document).on('submit', 'form[rel=redirect-to-other-page]', function () {
		remove_notifications();
		var fetch_url = $(this).attr('value');

		$('#container').slideUp('normal', function () {
			$.post(BASE_URL + CURRENT + fetch_url, function (data) {
				$('#container').html(data).slideDown('normal');
				loadJSPlugins();
			});
		});
		return false;
	});

    	/*
    	@event		: on each edit-button on grid row click
    	@function	: To show confirm modal, and redirect to edit form
    	@Note		: copy href in 'a' element to yes-button 
    	 */
	$(document).on('click', 'a[rel=edit]', function () {
		var fetch_url 	= $(this).attr('href');
		var attrTitle 	= $(this).attr('data-tooltip')
		var n 		= attrTitle.indexOf('`');
		var title 	= attrTitle.substring(n, (attrTitle.length));

		$('#modal-icon').addClass("mdi-alert-error yellow-text medium");
		$('#modal-title').text(attrTitle);
		$('#modal-message').text(CONFIRM_FORM_EDIT.replace('%s', title));
		$('#yes-button').attr("href", fetch_url);
		$('#yes-button').attr("rel", 'ajax');
		$('#modal2').openModal();

		return false;
	});

    	/*
    	@event		: on each delete-button on grid row click
    	@function	: To post record to delete and to redirect to specific method
    	@Note		: write the url destination on value attribute
    	 */
	$(document).on('click', 'a[rel=delete]', function () {
		var key 		= $(this).attr('id');
		var attrTitle 		= $(this).attr('data-tooltip')
		var n 			= attrTitle.indexOf('`');
		var title 		= attrTitle.substring(n, (attrTitle.length));

		$('#modal-icon').addClass("mdi-alert-error red-text medium");
		$('#modal-title').text(attrTitle);
		$('#modal-message').text(CONFIRM_DELETE.replace('%s', title));
		$('#yes-button').attr("href", BASE_URL + CURRENT + '/delete/' + key);
		$('#yes-button').attr("rel", 'confirm-delete');
		$('#modal2').openModal();

		return false;
	});

    	/*
    	@event		: on each delete-button on grid row click
    	@function	: To post record to delete and to redirect to specific method
    	@Note		: destination method write on id tag
    	 */
	$(document).on('click', 'a[rel=custom_delete]', function () {
		var key 		= $(this).attr('id');
		var attrTitle 		= $(this).attr('data-tooltip')
		var n 			= attrTitle.indexOf('`');
		var title 		= attrTitle.substring(n, (attrTitle.length));

		$('#modal-icon').addClass("mdi-alert-error red-text medium");
		$('#modal-title').text(attrTitle);
		$('#modal-message').text(CONFIRM_DELETE.replace('%s', title));
		$('#yes-button').attr("href", BASE_URL + CURRENT + '/' + key);
		$('#yes-button').attr("rel", 'confirm-delete');
		$('#modal2').openModal();

		return false;
	});

    	/*
    	@event		: on modal confirm delete click
    	@function	: To confirm delete process
    	@Note		: do_submit has 2 type redirect (to index or to specific method)
    	 */
	$(document).on('click', 'a[rel=confirm-delete]', function () {
		var fetch_url = $(this).attr('href');
		remove_notifications();
		do_submit(null, fetch_url, true);
		return false;
	});

    	/*
    	@event		: on each status-button on grid row click
    	@function	: To post request change status to related method
    	@Note		: copy href in 'a' element to yes-button 
    	 */
	$(document).on('click', 'a[rel=status]', function () {
		var fetch_url 	= $(this).attr('href');
		var attrTitle 	= $(this).attr('data-tooltip')
		var n 		= attrTitle.indexOf('`');
		var title 	= attrTitle.substring(n, (attrTitle.length));

		$('#modal-icon').addClass("mdi-alert-error yellow-text medium");
		$('#modal-title').text(attrTitle);
		$('#modal-message').text(CONFIRM_FORM_EDIT.replace('%s', title));
		$('#yes-button').attr("href", fetch_url);
		$('#yes-button').attr("rel", 'confirm-delete');
		$('#modal2').openModal();

		return false;
	});

    	/*
    	@event		: on every navigation that need to scrolling effect
    	@function	: To navigate to other method
    	 */
	$(document).on('click', 'a[rel=ajax]', function () {
		var fetch_url = $(this).attr('href');
		remove_notifications();

		$('#container').slideUp('normal', function () {
			$('#container').load(fetch_url, function (data, status, xhr) {
			    $(this).slideDown('normal');
			    loadJSPlugins();
			});
		});
		return false;
	});

    	/*
    	@event		: on each view-button on grid row click
    	@function	: To redirect to view-page
    	 */
	$(document).on('click', 'a[rel=view]', function () {
		var fetch_url = $(this).attr('href');
		remove_notifications();

		$('#container').slideUp('normal', function () {
			$('#container').load(fetch_url, function (data, status, xhr) {
				$(this).slideDown('normal');
				loadJSPlugins();
			});
		});
		return false;
	});

    	/*
    	@event		: on click cancel or back button
    	@function	: To back to previous page
    	 */
	$(document).on('click', 'a.button[value=cancel]', function () {
		remove_notifications();
		$('#container').slideUp('normal', function () {
			load_list();
		});

		return false;
	});

    	/*
    	@event		: on click check box on table header
    	@function	: To select all check box in each table row
    	 */
	$(document).on('click', '#check_all', function () {
		var checked = $('#check_all').is(':checked');
		for (i = 1; i <= $('.check_to').length; i++) {
			$('#data' + i).prop('checked', checked);
		}
	});

    	/*
    	@event		: on click delete selected record button
    	@function	: To open confirm modal for delete selected records in the grid
    	 */
	$(document).on('click', 'button.delete-selected', function () {
		remove_notifications();
		if ($('input[type="checkbox"]:checked').length == 0) {
			$('#alert-icon').addClass("mdi-alert-error yellow-text medium");
			$('#alert_modal').openModal();
		}else{
			var attrTitle 	= $(this).attr('data-tooltip')
			$('#modal-icon').addClass("mdi-alert-error red-text medium");
			$('#modal-title').text(attrTitle);
			$('#modal-message').text(CONFIRM_DELETE_SELECTED);
			$('#yes-button').attr("rel", 'active-has-been-selected');
			$('#modal2').openModal();
		}		
		return false;
	});

    	/*
    	@event		: on confirm delete button in modal click
    	@function	: To confirm delete process for selected records in the grid
    	@Note		: form_url depend on tag value values on form
    	 */
	$(document).on('click', 'a[rel=active-has-been-selected]', function () {
		var form = $('form[name=datas_table]');
		var form_url = typeof(form.attr('value')) !== 'undefined' ? form.attr('value') : form.attr('action');
		do_submit(form.serialize(), form_url + '/delete', true);
	});

    	/*
    	@event		: on pagination click
    	@function	: To provide pagination in separate grid
    	@Note		: input[name=additional_id] is object on form.php that containt current_page parameter
    	 */
	$(document).on('click', '.pagination a', function () {
		if (! $('.exceptions').length) {
			remove_notifications();

			var fetch_url = $(this).attr('href');
			var array_url = fetch_url.split('/');
			var filtered = {};
			filtered['current_page'] = array_url[array_url.length - 1];
			var per_page = $('#per_page').val();
			filtered['per_page'] = per_page;
			var keywords = $('input[name="f_keywords"]').val() == $('input[name="f_keywords"]').attr('default') ? '' : $('input[name="f_keywords"]').val();
			filtered['f_keywords'] = keywords;

			if ($('form[name=form_filter]').length) {
				var serialize = $('form[name=form_filter]').serializeArray();
				for (var i = 0; i < serialize.length; i++) {
					filtered[serialize[i].name] = serialize[i].value;
				}
			}

			var additional_url = "";

			if(typeof($("input[name=additional_id]").val()) !== 'undefined'){
				additional_url = "/detail/" + filtered['current_page'];
			}

			$('#container').slideUp('normal', function () {
				$.post(BASE_URL + CURRENT + additional_url, filtered, function (data) {
					$('#container').html(data).slideDown('normal');
					loadJSPlugins();
				});
			});
		}

		return false;
	});

	$(document).on('change', '#per_page', function () {
		remove_notifications();

		var filtered = {};
		var url = $(this).parents('form').attr('action');
		var keywords = $('input[name="f_keywords"]').val() == $('input[name="f_keywords"]').attr('default') ? '' : $('input[name="f_keywords"]').val();
		filtered['f_keywords'] = keywords;
		var per_page = $(this).val();
		filtered['per_page'] = per_page;

		if ($('form[name=form_filter]').length) {
			var serialize = $('form[name=form_filter]').serializeArray();
			for (var i = 0; i < serialize.length; i++) {
				filtered[serialize[i].name] = serialize[i].value;
			}
		}

		$('#container').slideUp('normal', function () {
			$.post(url, filtered, function (data) {
				$('#container').html(data).slideDown('normal');
				loadJSPlugins();
			});
		});

		return false;
	});

	$(document).on('submit', 'form[name=form_filter]', function () {
		remove_notifications();
		var form = $(this);

		$('#container').slideUp('normal', function () {
			$.post(BASE_URL + CURRENT, form.serialize(), function (data) {
				$('#container').html(data).slideDown('normal');
				var nFiltered = 0;
				var filtered = form.serializeArray();
				for (var i = 0; i < filtered.length; i++) {
					if (filtered[i].value != '') {
						++nFiltered;
					}
				}
				if (nFiltered) {
					$('#container').find('.collapsible-header').addClass('active');
				}
				loadJSPlugins();
			});
		});
		return false;
	});

	$(document).on('submit', 'form[name=form_filter_second_page]', function () {
		remove_notifications();
		var form = $(this);
		var id =$(this).attr('id');

		$('#container').slideUp('normal', function () {
			$.post(BASE_URL + CURRENT + '/detail/' + id, form.serialize(), function (data) {
				$('#container').html(data).slideDown('normal');
				var nFiltered = 0;
				var filtered = form.serializeArray();
				for (var i = 0; i < filtered.length; i++) {
					if (filtered[i].value != '') {
						++nFiltered;
					}
				}
				if (nFiltered) {
					$('#container').find('.collapsible-header').addClass('active');
				}
				loadJSPlugins();
			});
		});
		return false;
	});

	$(document).on('click', 'form[name=form_filter] button[type="reset"]', function () {
		if ($('form[name=form_filter]').find('select').length) {
			$('form[name=form_filter]').find('select').material_select('destroy')
		}

		$('form[name=form_filter]').find(':input').not(':submit, :reset, :hidden').val('');
		$('form[name=form_filter]').find(':radio, :checkbox, select').removeAttr('checked').removeAttr('selected');
		$('form[name=form_filter]').find('select').val($("select option:first").val());

		if ($('form[name=form_filter]').find('select').length) {
			$('form[name=form_filter]').find('select').material_select()
		}
		return false;
	});

	$(document).on('click', 'form[name=form_filter_second_page] button[type="reset"]', function () {
		if ($('form[name=form_filter_second_page]').find('select').length) {
			$('form[name=form_filter_second_page]').find('select').material_select('destroy')
		}

		$('form[name=form_filter_second_page]').find(':input').not(':submit, :reset, :hidden').val('');
		$('form[name=form_filter_second_page]').find(':radio, :checkbox, select').removeAttr('checked').removeAttr('selected');
		$('form[name=form_filter_second_page]').find('select').val($("select option:first").val());

		if ($('form[name=form_filter_second_page]').find('select').length) {
			$('form[name=form_filter_second_page]').find('select').material_select()
		}
		return false;
	});

	//=======================================================================


    // $(document).on('click', '.table tr th a', function () {
    //     remove_notifications();

    //     var field = $(this).attr('name');
    //     var dir = $(this).attr('dir');

    //     dir = dir == '' ? 'DESC' : dir;

    //     if (field != '') {
    //         $('#container').slideUp('normal', function () {
    //             $.post(BASE_URL + CURRENT, {sortfield: field, sortdir: dir}, function (data) {
    //                 $('#container').html(data).slideDown('normal');
    //                 loadJSPlugins();
    //             });
    //         });
    //     }

    //     return false;
    // });

    // $(document).on('click', 'a[rel=check]', function () {
    //     var fetch_url = $(this).attr('href');
    //     var attrTitle = $(this).attr('title')
    //     var n = attrTitle.indexOf('`');
    //     var title = attrTitle.substring(n, (attrTitle.length));

    //     if (confirm(CONFIRM_FORM_CHECK.replace('%s', title))) {
    //         remove_notifications();

    //         $('#container').slideUp('normal', function () {
    //             $('#container').load(fetch_url, function (data, status, xhr) {
    //                 $(this).slideDown('normal');
    //                 loadJSPlugins();
    //             });
    //         });
    //     }

    //     return false;
    // });

    // $(document).on('click', 'a[rel=delete_x]', function () {
    //     remove_notifications();

        // var attrTitle = $(this).attr('title')
        // var n = attrTitle.indexOf('`');
        // var title = attrTitle.substring(n, (attrTitle.length));

        // if (confirm(CONFIRM_DELETE.replace('%s', title))) {
            // var fetch_url = $(this).attr('href');
            // do_submit(null, fetch_url, true);
        // }

        // return false;
    // });

    // $(document).on('click', 'a[rel=post]', function () {
    //     remove_notifications();

        /*var attrTitle = $(this).attr('title')
        var n = attrTitle.indexOf('`');
        var title = attrTitle.substring(n, (attrTitle.length));*/

        //if (confirm(CONFIRM_COMMON.replace('%s', title))) {
            // var fetch_url = $(this).attr('href');
            // do_submit(null, fetch_url, true);
        //}

    //     return false;
    // });
    
    // $(document).on('click', 'button.active-selected', function () {
    //     remove_notifications();

    //     if ($('input[type="checkbox"]:checked').length == 0) {
    //         $('#modal4').openModal();
    //         return false;
    //     }else{
    //         $('#modal2').openModal();
    //     }
        /*if (confirm(CONFIRM_ACTIVE_SELECTED)) {
            do_submit(form.serialize(), form.attr('action') + '/delete', true);
        }*/

    //     return false;
    // });
    
    // $(document).on('click', 'a[rel=active-has-been-selected]', function () {
    //     var form = $('form[name=datas_table]');
    //     do_submit(form.serialize(), form.attr('action') + '/delete', true);
    // });


    
    // $(document).on('click', 'a[rel=delete-has-been-selected]', function () {
    //     var form = $('form[name=datas_table]');
    //     do_submit(form.serialize(), form.attr('action') + '/delete', true);
    // });


    // $(document).on('keyup', 'input[type="text"].search', function (e) {
    //     remove_notifications();

    //     if (e.keyCode == '13') {
    //         var me = $(this);
    //         var per_page = $('#per_page').val();

    //         $('#container').slideUp('normal', function () {
    //             $.post(BASE_URL + CURRENT, {f_keywords: me.context.value, per_page: per_page}, function (data) {
    //                 $('#container').html(data).slideDown('normal');
    //                 loadJSPlugins();
    //             });
    //         });
    //     }

    //     return false;
    // });

    // $(document).on('click', 'a.button[value=cancel]', function () {
    //     remove_notifications();
    //     $('#container').slideUp('normal', function () {
    //         load_list();
    //     });

    //     return false;
    // });


    
    // $(document).on('click', 'a[rel=has-been-submit]', function () {
    //     var data_submit;
    //     if (CURRENT === 'manual_input') {
    //         data_submit = '';
    //         $('.manual_input').each(function (i) {
    //             if ($(this).attr('id')) {
    //                 if ($(this).attr('id') === 'nominal') {
    //                     data_submit += $(this).attr('name') + '=' + $(this).autoNumeric('get') + '&';
    //                 } else if ($(this).hasClass('with-gap')) {
    //                     if ($(this).is(':checked')) {
    //                         data_submit += $(this).attr('name') + '=' + $(this).val() + '&';
    //                     }
    //                 } else {
    //                     data_submit += $(this).attr('name') + '=' + $(this).val() + '&';
    //                 }
    //             }
    //         });
    //         data_submit = data_submit.slice(0, -1);
    //     } else {
    //       data_submit = $('#form_data').serialize();
    //     }
    //     do_submit(data_submit, $('#form_data').attr('action'), true);
    // });
    


    // $(document).on('click', 'a.cancel-data-eq', function () {
    //     remove_notifications();

    //     $('#container').slideUp('normal', function () {
    //         $('#loading').fadeIn();
    //         $.ajax({
    //             type: 'POST',
    //             url: CURRENT_URL+'/cancelTrxNostro',
    //             dataType: 'json',
    //             success: function (data) {
    //                 var notification = '<div id="card-alert" class="alert card light-blue">\n\
    //                 <div class="card-content white-text">\n\
    //                     <p>' + data.message + '</p>\n\
    //                 </div>\n\
    //                     <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">\n\
    //                         <span aria-hidden="true">×</span>\n\
    //                     </button>\n\
    //                 </div>';

    //                 $('#container').before(notification);

    //                 load_list();

    //                 $('.alert').fadeIn('normal');
    //                 $('html, body').animate({ scrollTop: 0 }, 'fast');
    //             }
    //         });
    //     });
    //     return false;
    // });
});

function loadJSPlugins() {

	$("#slide-out").removeAttr("style");
	$('.collapsible').collapsible();
	$('.numeric').autoNumeric('init', {aSep: '.', aDec: ','});
	$('select').material_select();

    $('.modal-trigger').closeModal();
	$('.modal-trigger').leanModal({
		complete: function () {
			$('.lean-overlay').remove();
		}
	});

	$('.timepicker').pickatime({
		default: 'now',
		twelvehour: false, // change to 12 hour AM/PM clock from 24 hour
		donetext: 'OK',
		autoclose: false,
		vibrate: true // vibrate the device when
	})

	$('.pickadate').pickadate({
		selectMonths: !0,
		selectYears: 15,
		format: 'yyyy-mm-dd',
		onSet: function (context) {
			if(context.select){
				this.close();
			}
		}
	});

	$('.tooltipped').tooltip({
		delay:50,
		position:'bottom'
	});

	$("option").each(function(){
		if ($(this).val().toLowerCase() == "") {
			$(this).attr("disabled", "disabled");
		}
	});    

	$(document).on('click', '#card-alert .close', function () {
		$(this).closest("#card-alert").fadeOut("slow");
	});

	$('.ip_input').mask('099.099.099.099');

	if ($('#form_upload').length) {
		$('#form_upload').ajaxForm({
			uploadProgress: function() {
				$('#container').slideUp('normal');
				$('#loading').fadeIn('normal');
			},
			success: function() {
				$('#loading').fadeOut('normal');
			},
			complete: function(response) {
				var data = JSON.parse(response.responseText);
				var notification = '<div id="card-alert" class="alert card light-blue">\n\
				<div class="card-content white-text">\n\
				<p>' + data.message + '</p>\n\
				</div>\n\
				<button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">\n\
				<span aria-hidden="true">×</span>\n\
				</button>\n\
				</div>';

				var URL = (((typeof(data.path) !== 'undefined') && (data.path != '')) ? CURRENT + '/' + data.path : CURRENT);
				
				$('#container').before(notification);
				if (data.status == 'success') {
					$('.alert').fadeIn('normal', function () {
						if (data.reload) {
							window.location.reload();
						} else {
							if((typeof(data.path) !== 'undefined') && (data.path != '')){
								load_list_url(URL);
							}else{
                           						 load_list();
							}
							// load_list();
							$('.alert').fadeIn('normal');
							$('html, body').animate({ scrollTop: 0 }, 'fast');
						}
					});
				} else {
					$('#loading').fadeOut();
					$('#container').slideDown('normal');
				}
			}
		});
	}

	$('.dropdown-button').dropdown({
		belowOrigin: true,
	});

	if ($('#pymt_tgl_dari').length && $('#pymt_tgl_sampai').length) {		
        rangeDate();
    } 



}

function rangeDate()
{
    $('#pymt_tgl_dari').pickadate({
        selectMonths: !0,
        selectYears: 15,
        format: 'yyyy-mm-dd',
        closeOnSelect: true,
        onSet: function (context) {
            var date = new Date(context.select);
            $('#due_date').pickadate({
                min: new Date(date.getFullYear(), date.getDate(), date.getMonth())
            });
        }
    });

    $('#pymt_tgl_sampai').pickadate({
        selectMonths: !0,
        selectYears: 15,
        format: 'yyyy-mm-dd',
        closeOnSelect: true,
        onOpen: function () {
            var pStart = new Date($('#pymt_tgl_dari').val());
            var $input = $('#pymt_tgl_sampai').pickadate();
            var picker = $input.pickadate('picker');
            picker.set('min', [pStart.getFullYear(), pStart.getMonth(), pStart.getDate()]);
        }
    });
}


function load_list() {
    $('#container').load(CURRENT, function (data, status, xhr) {
        $('#loading').fadeOut();
        $(this).slideDown('normal');
        loadJSPlugins();
    });
}

function load_list_url(url) {
    $('#container').load(url, function (data, status, xhr) {

        $('#loading').fadeOut();
        $(this).slideDown('normal');
        $('.lean-overlay').remove();
        loadJSPlugins();
    });
}

function remove_notifications() {
    $('.alert').fadeOut('normal', function () {
        $(this).remove();
    });
}

function do_submit(form_data, post_url, close_form) {
	remove_notifications();
	var param_url = form_data.split("&");
	var method_param = param_url[param_url.length-2];
	var current_method_name = (method_param.split("="))[1];

	var frame_param = param_url[param_url.length-1];
	var frame_name = (frame_param.split("="))[1];

	var slideSection = frame_name == 'view-content' ? '#view-content' : '#container';

	console.log(frame_name);

	$(slideSection).slideUp('normal', function () {
		$('#loading').fadeIn();
		$.ajax({
			type: 'POST',
			url: post_url,
			data: form_data,
			dataType: 'json',
			success: function (data, status, xhr) {
				var notification = '<div id="card-alert" class="alert card light-blue">\n\
				<div class="card-content white-text">\n\
				<p>' + data.message + '</p>\n\
				</div>\n\
				<button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">\n\
				<span aria-hidden="true">×</span>\n\
				</button>\n\
				</div>';
				var URL = (((typeof(data.path) !== 'undefined') && (data.path != '')) ? CURRENT + '/' + data.path : CURRENT);

				$(slideSection).before(notification);
				if (data.status == 'success') {
					// console.log('111');
					$('.alert').fadeIn('normal', function () {
						// console.log('222');
						if (data.reload) {
							// console.log('333');
							window.location.reload();
						} else {
							// console.log('444');
							if (close_form) {
								// console.log('555');
								if((typeof(data.path) !== 'undefined') && (data.path != '')){
									// console.log('666');
									load_list_url(URL);
								}else{
									// console.log('777');
                                   						 load_list();
								}
							} else {
								if(frame_name == 'view-content'){
									load_list_url_partial(CURRENT + "/" + current_method_name);
								}else{
									load_list_url(CURRENT + "/" + current_method_name);
								}
								
							}
						}
					});
					return;
				} else {
					// console.log('999');
					$('#loading').fadeOut();
					$(slideSection).slideDown('normal');
				}
				$('.alert').fadeIn('normal');
				$('html, body').animate({ scrollTop: 0 }, 'fast');
			}
		});
	});
}

// function do_submit_and_redirect(form_data, post_url, close_form,id) {
// 	remove_notifications();

// 	$('#container').slideUp('normal', function () {
// 		$('#loading').fadeIn();
// 		$.ajax({
// 			type: 'POST',
// 			url: post_url,
// 			data: form_data,
// 			dataType: 'json',
// 			success: function (data, status, xhr,id) {
// 				var notification = '<div id="card-alert" class="alert card light-blue">\n\
// 				<div class="card-content white-text">\n\
// 				<p>' + data.message + '</p>\n\
// 				</div>\n\
// 				<button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">\n\
// 				<span aria-hidden="true">×</span>\n\
// 				</button>\n\
// 				</div>';

// 				$('#container').before(notification);
// 				if (data.status == 'success') {
// 					$('.alert').fadeIn('normal', function () {
// 						if (data.reload) {
// 							window.location.reload();
// 						} else {
// 							if (close_form) {
// 								load_list();
// 							} else {
// 								$('#container').load(BASE_URL + CURRENT + '/edit/' + id, function (data, status, xhr) {
// 									$(this).slideDown('normal');
// 									loadJSPlugins();
// 								});
// 							}
// 						}
// 					});
// 					return;
// 				} else {
// 					$('#loading').fadeOut();
// 					$('#container').slideDown('normal');
// 				}

// 				$('.alert').fadeIn('normal');
// 				$('html, body').animate({ scrollTop: 0 }, 'fast');
// 			}
// 		});
// 	});
// }
