$(function () {

    loadJS();

    $(document).on('click', '#check_all', function () {alert('1');
        // var checked = $('#check_all').is(':checked');
        // for (i = 1; i <= $('.minimal').length; i++) {
        //     $('#data' + i).prop('checked', checked);
        // }
        // $('.check_to').attr('checked', checked);
    });

    $(document).on('click', 'a[rel=ajaxx]', function () {
        var fetch_url = $(this).attr('href').trim();
        remove_notifications();
        $('#content').slideUp('normal', function () {
            $('#content').load(fetch_url, function (data, status, xhr) {
                // $(this).slideDown('normal');
                $(this).slideDown('slow');
                loadJS();
            });
        });
        return false;
    });

    // $(document).on('click', 'a[rel=ajax]', function () {
    //     var fetch_url = $(this).attr('href').trim();

    //     remove_notifications();

    //     $('#main-content').slideUp('normal', function () {
    //         $('#main-content').load(fetch_url, function (data, status, xhr) {
    //             // $(this).slideDown('normal');
    //             $(this).slideDown('slow');
    //             loadJS();
    //         });
    //     });

    //     return false;
    // });

    $(document).on('click', 'a[rel=ajax]', function () {
        var fetch_url = $(this).attr('href').trim();

        remove_notifications();

        $('#content').slideUp('normal', function () {
            $('#content').load(fetch_url, function (data, status, xhr) {
                // $(this).slideDown('normal');
                $(this).slideDown('slow');
                loadJS();
            });
        });

        return false;
    });

    $(document).on('click', 'a[rel=status]', function () {
        remove_notifications();

        var fetch_url = $(this).attr('href');
        do_submit(null, fetch_url, true);
        return false;
    });

    $(document).on('click', 'a[rel=openpartial]', function () {
        var fetch_url = $(this).attr('href').trim();

        remove_notifications();

        $('#child-main-content').slideUp('normal', function () {
            $('#child-main-content').load(fetch_url, function (data, status, xhr) {
                $(this).slideDown('normal');
                loadJS();
            });
        });

        return false;
    });

    $(document).on('click', 'a[rel=edit]', function () {
        var fetch_url = $(this).attr('href').trim();
        var message = $(this).attr('data-original-title')
        var n = message.indexOf('`');
        var title = message.substring(n, (message.length));

        if (confirm(CONFIRM_FORM_EDIT.replace('%s', title))) {
            remove_notifications();

            $('#content').slideUp('normal', function () {
                $('#content').load(fetch_url, function (data, status, xhr) {
                    $(this).slideDown('normal');
                    loadJS();
                });
            });
        } else {
            $(this).tooltip('hide');
        }

        return false;
    });

    $(document).on('click', 'a[rel=edit-openpartial]', function () {
        var fetch_url = $(this).attr('href').trim();
        var message = $(this).attr('data-original-title')
        var n = message.indexOf('`');
        var title = message.substring(n, (message.length));

        if (confirm(CONFIRM_FORM_EDIT.replace('%s', title))) {
            remove_notifications();

            $('#child-main-content').slideUp('normal', function () {
                $('#child-main-content').load(fetch_url, function (data, status, xhr) {
                    $(this).slideDown('normal');
                    loadJS();
                });
            });
        } else {
            $(this).tooltip('hide');
        }

        return false;
    });

    $(document).on('click', 'a[rel=view]', function () {
        var fetch_url = $(this).attr('href');

        remove_notifications();

        $('#content').slideUp('normal', function () {
            $('#content').load(fetch_url, function (data, status, xhr) {
                $(this).slideDown('normal');
                loadJS();
            });
        });

        return false;
    });

    $(document).on('click', 'a[rel=view-openpartial]', function () {
        var fetch_url = $(this).attr('href');

        remove_notifications();

        $('#child-main-content').slideUp('normal', function () {
            $('#child-main-content').load(fetch_url, function (data, status, xhr) {
                $(this).slideDown('normal');
                loadJS();
            });
        });

        return false;
    });

    // $(document).on('click', 'a[rel=delete_redirect]', function () {
    //     if (confirm(CONFIRM_DELETE.replace('%s', title))) {
    //         return true;
    //     }
    //     return false;
    // });

    $(document).on('click', 'a[rel=delete]', function () {
        remove_notifications();

        var message = $(this).attr('data-original-title');
        var n = message.indexOf('`');
        var title = message.substring(n, (message.length));
        // var triger_id = $(this).attr('id');

        if (confirm(CONFIRM_DELETE.replace('%s', title))) {
            var fetch_url = $(this).attr('href').trim();
            do_submit(null, fetch_url);
            // console.log(triger_id);
             // $.ajax({
             //     url : $('input[name=session-url]').val(),
             //     method : 'post',
             //     dataType : 'json',
             //     data : {},
             //     success : function(response){ 
             //        console.log(response);
             //         if(response == 0){
             //             $('#chart-alert').text('');
             //         }else{
             //             $('#chart-alert').text('1');
             //         }
             //     }
             // });
        } else {
            $(this).tooltip('hide');
        }

        return false;
    });

    $(document).on('click', 'a[rel=render]', function () {
        remove_notifications();

        var message = $(this).attr('data-original-title');
        // var n = message.indexOf('`');
        // var title = message.substring(n, (message.length));

        if (confirm(CONFIRM_COMMON.replace('%s', message))) {
            var fetch_url = $(this).attr('href').trim();
            do_submit(null, fetch_url);
        } else {
            $(this).tooltip('hide');
        }

        return false;
    });

    $(document).on('click', 'a[rel=post]', function () {
        remove_notifications();

        var message = $(this).attr('data-original-title');

        if (confirm(CONFIRM_COMMON.replace('%s', message))) {
            var fetch_url = $(this).attr('href').trim();
            do_submit(null, fetch_url);
        } else {
            $(this).tooltip('hide');
        }

        return false;
    });

    $(document).on('click', 'button.delete_selected', function () {
        remove_notifications();

        var form = $('form[name=form_table]');

        if ($('input[type="checkbox"]:checked').length == 0) {
            alert(DELETE_NO_SELECTED);
            return false;
        }

        if (confirm(CONFIRM_DELETE_SELECTED)) {
            // do_submit(form.serialize(), form.attr('action') + '/delete', true);
            // alert($('#form_table').attr('action')+'/delete');
            do_submit($('#form_table').serialize(), $('#form_table').attr('action') + '/delete');
        }

        return false;
    });

    $(document).on('click', '.pagination a', function () {
        remove_notifications();

        var fetch_url = $(this).attr('href').trim();
        var array_url = fetch_url.split('/');
        var filtered = {};

        filtered['current_page'] = array_url[array_url.length - 1];
        var per_page = $('#per_page').val();
        filtered['per_page'] = per_page;

        if ($('#form_filter').length) {
            var serialize = $('#form_filter').serializeArray();
            for (var i = 0; i < serialize.length; i++) {
                filtered[serialize[i].name] = serialize[i].value;
            }
        }

        $('#content').slideUp('normal', function () {
            $.post(CURRENT, filtered, function (data) {
                $('#content').html(data).slideDown('normal');
                loadJS();
            });
        });
        return false;
    });

    $(document).on('change', '#per_page', function () {
        remove_notifications();

        var filtered = {};

        var url = $('#form_filter').attr('action').trim();

        var per_page = $(this).val();
        filtered['per_page'] = per_page;

        if ($('#form_filter').length) {
            var serialize = $('#form_filter').serializeArray();
            for (var i = 0; i < serialize.length; i++) {
                filtered[serialize[i].name] = serialize[i].value;
            }
        }


        $('#content').slideUp('normal', function () {
            $.post(url, filtered, function (data) {
                $('#content').html(data).slideDown('normal');
                loadJS();
            });
        });

        return false;
    });

    $(document).on('click', 'button.cancel', function () {
        remove_notifications();
        $('#content').slideUp('normal', function () {
            if ($('textarea.tinymce').length) {
                tinymce.remove('textarea.tinymce');
            }
            load_list();
        });
        return false;
    });

    $(document).on('click', 'button.cancelx', function () {
        remove_notifications();
        $('#content').slideUp('normal', function () {
            if ($('textarea.tinymce').length) {
                tinymce.remove('textarea.tinymce');
            }
            load_list();
        });
        return false;
    });

    $(document).on('click', 'button.cancel-openpartial', function () {
        remove_notifications();

        $('#child-main-content').slideUp('normal', function () {            
            if ($('textarea.tinymce').length) {
                tinymce.remove('textarea.tinymce');
            }
            load_list_openpartial();
        });
        return false;
    });

    $(document).on('click', 'form.login-form button.submit', function () {
        console.log('999');
        remove_notifications();
    });

    $(document).on('click', 'form.crud button.submit', function () {
        if ($(this).val() == 'delete')
            return;
// console.log($('#form_data').serialize());
// console.log($('#form_data').attr('action'));
        if (confirm(CONFIRM_FORM) === true) {
            do_submit($('#form_data').serialize(), $('#form_data').attr('action').trim());
            // do_submit($('#form_data').serialize(), $('#form_data').attr('action'));
        }

        return false;
    });

    // $(document).on('click', 'form.crud_upload button.submit', function () {
    //     if ($(this).val() == 'delete')
    //         return;

    //     if (confirm(CONFIRM_FORM) === true) {
    //         // do_submit($('#form_upload').serialize(), $('#form_upload').attr('action').trim());
    //         do_submit($('#form_data').serialize(), $('#form_data').attr('action'));
    //     }

    //     return false;
    // });

    $(document).on('click', 'form.crud button.approve', function () {
        var input = $("<input>")
               .attr("type", "hidden")
               .attr("name", "response").val($(this).val());
        $('#form_data').append($(input));
        if ($(this).val() == 'delete')
            return;

        if (confirm(CONFIRM_FORM) === true) {
            do_submit($('#form_data').serialize(), $('#form_data').attr('action').trim());
        }

        return false;
    });

    $(document).on('submit', '#form_filter', function () {
        remove_notifications();

        var form = $(this);

        $('#content').slideUp('normal', function () {
            $.post(CURRENT, form.serialize(), function (data) {
                $('#content').html(data).slideDown('normal');
                loadJS();
            });
        });

        return false;
    });

    $(document).on('click', '#reset', function () {
        $('#form_filter').find('input, select').val('');
        return false;
    });

});

function loadJS() {
    if ($('.datepicker').length) {
        $('.datepicker').datepicker({
            autoclose:true,
            format:'yyyy-mm-dd'
        });
    }
    // $("[data-mask]").inputmask();
    $('input[type="checkbox"].minimal').on("ifChanged", function(){//alert(this.id);
        if($('#txt_'+this.id).length){
            if(this.checked){            
                    document.getElementById('txt_'+this.id).disabled = false;      
            }else{

                document.getElementById('txt_'+this.id).disabled = true;
            }
        }
    });



    $('input[type="checkbox"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });

    $('a').tooltip();

    if (typeof reindex === 'function') {
        reindex();
    }

    if ($('#date-popup').length) {
        $('#date-popup').datepicker();
    }

    if ($('textarea.tinymce').length) {
        tinymce.init({
            selector: "textarea.tinymce",
            theme: "modern",
            height:300,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "save table contextmenu directionality emoticons template paste textcolor"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",
            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
            setup: function (editor) {
                editor.on('change', function () {
                    editor.save();
                })
            }
        });
    }

    if ($('#form_upload').length) {
        $('#form_upload').ajaxForm({
            beforeSubmit: function() {
                if (confirm(CONFIRM_FORM) === true) {
                    return true;
                } else {
                    return false;
                }
           },
           uploadProgress: function() {
               $('#content').slideUp('normal');
               $('#loading').fadeIn('normal');
           },
           success: function() {
               $('#loading').fadeOut('normal');
           },
           complete: function(response) {console.log(response);
               var data = JSON.parse(response.responseText);
               var notification = '<div class="alert alert-info alert-dismissible" style="margin-bottom:0">' + data.message + '</div>';

            $('#alert').html(notification);

               if (data.status == 'success') {
                    $('.alert').fadeIn('normal', function () {                        
                            if (data.reload) {
                                window.location.reload();
                            } else {
                                load_list();
                            }
                        // });
                    });
                    return;
                }

                $('#content').slideDown('normal');
                $('.alert').fadeIn('normal');
                $('html, body').animate({ scrollTop: 0 }, 'fast');
           }
        });
    }
}

function load_list() {
    $('#content').load(CURRENT, function (data, status, xhr) {
        $(this).slideDown('normal');
        loadJS();
    });
}

function load_list_openpartial() {
    var current_url = window.location.href ;
    $('#child-main-content').load(current_url +' #child-main-content', function (data, status, xhr) {
        $(this).slideDown('normal');
        //loadJS();
    });
}

function remove_notifications() {
    $('a').tooltip('hide');
    $('.alert').fadeOut('normal', function () {
        $(this).remove();
    });
}

function do_submit(form_data, post_url) {
    remove_notifications();
    $('#content').slideUp('normal', function () {
        // $('#loading').fadeIn();
        $.ajax({
            type: 'POST',
            url: post_url,
            data: form_data,
            dataType: 'json',
            success: function (data, status, xhr) {
                var notification = '<div class="alert alert-info alert-dismissible" style="margin-bottom:0">' + data.message + '</div>';
                // $('#main-content').before(notification);
                $('#alert').html(notification);
                // $('#loading').fadeOut('normal');

                if (data.status == 'success') {
                    $('.alert').fadeIn('normal', function () {                        
                            if (data.reload) {
                                window.location.reload();
                            } else {
                                load_list();
                            }
                        // });
                    });
                    return;
                }

                $('#content').slideDown('normal');
                $('.alert').fadeIn('normal');
                $('html, body').animate({ scrollTop: 0 }, 'fast');
            }
        });
    });
}
