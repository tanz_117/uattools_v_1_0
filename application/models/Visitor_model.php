<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Visitor_model extends Base_Model
{

    protected $_table = '';
    protected $_pk = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function getActiveConfiguration()
    {
        $this->_table = 'configurations';
        $this->_pk = 'configuration_id';

        return parent::rows(array(
            'where' => array('configuration_status' => 1)
        ));
    }

    public function logUserActivity($input)
    {
        parent::query("EXEC audit_trail_insert ?, ?, ?, ?, ?, ?", $input);
        return true;
    }

}
