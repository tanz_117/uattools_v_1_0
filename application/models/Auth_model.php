<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends Base_Model
{

	protected $_table = '';
	protected $_pk = '';
	private $_uim_fields = array('user_name', 'user_npk', 'user_id', 'user_cabang',
	    'user_kodeinduk', 'user_namacabang', 'user_namainduk', 'user_kodekanwil',
	    'user_namakanwil', 'user_nama_jabatan', 'user_nama_posisi_penempatan',
	    'user_phone', 'user_email', 'user_kode_grade', 'user_nama_grade',
	    'user_penempatan_hris', 'user_password', 'user_isactive', 'user_pass_expired',
	    'user_pass_blocked', 'user_level', 'user_fungsitambahan', 'user_limit_d',
	    'user_limit_k', 'user_spv');

	public function __construct()
	{
		parent::__construct();
	}

	public function authenticate($credentials)
	{
		$this->_table = 'users';
		$isAuth = parent::row(array(
			// 'select' => 'users.user_id,users.user_identity,users.user_name,users.user_registerdate,users.user_role_id, roles.role_level, roles.role_active, roles.role_name,anggota.anggota_photo',
            // 'where' => array('users.user_identity' => $credentials['identity'],'users.user_password' => $credentials['password']),
            'where' => array('users.id' => $credentials['identity']),
			'join' => array(
                array('roles', 'users.role_id = roles.role_id'),
                // array('anggota', 'anggota.user_id = users.user_id','LEFT')
            )
		));

        if($isAuth){
            $isAuth = json_decode(json_encode($isAuth), True);
            $isAuth['access_menus'] = json_encode($this->getAccessedMenu($isAuth['role_level']));                        
        }else{
            $isAuth=false;
        }

		if ($isAuth) return $isAuth;
		return false;
	}

    public function parse($response) {
        $value = explode('|', $response);

        if (count($value) > 1) {
            $no = 0;
            foreach ($this->_uim_fields as $key) {
                $user[$key] = array_key_exists($no, $value) ? $value[$no] : '';
                ++$no;
            }
        } else {
            switch ($value[0]) {
                case ':p':
                    $error = 'server_not_registered';
                    break;
                case 'salah':
                    $error = 'wrong_id';
                    break;
                case 'tidak ada':
                    $error = 'not_registered';
                    break;
                default:
                    $error = 'not_exist';
                    break;
            }
            $user['error'] = $error;
        }

        return $user;
    }

    // public function authenticate($credentials)
    // {
    //     $this->_table = 'users';
    //     $isAuth = parent::row(array(
    //         'select' => 'users.identity, roles.role_id, roles.role_level, roles.role_active, roles.role_name',
    //         'where' => array('users.identity' => $credentials['identity']),
    //         'join' => array(array('roles', 'users.role_id = roles.role_id'))
    //     ));
    //
    //     if ($isAuth) return $isAuth;
    //
    //     return false;
    // }

	public function getAccessedMenu($level)
	{
		$this->_table = 'role_menus';
		return $this->_createMenuStructure(parent::rows(array(
			'select' => "menus.menu_id, menus.menu_name, menus.menu_icon, menus.menu_url, menus.menu_parent",
			'where' => array('roles.role_level' => $level, 'menus.menu_active' => 1),
			'join' => array(
				array('menus', 'role_menus.menu_id = menus.menu_id'),
				array('roles', 'role_menus.role_id = roles.role_id')
			),
			'order_by' => array(
				array('menus.menu_parent', 'ASC'),
				array('menus.menu_order', 'ASC'),
				array('menus.menu_id', 'ASC'),
			)
		)));
	}

    private function _createMenuStructure($rows)
    {
        $menuStructures = array();

        foreach ($rows as $row) {
            $structure = array(
                'menu_id' => $row->menu_id,
                'menu_name' => $row->menu_name,
                'menu_url' => $row->menu_url ? base_url($row->menu_url) : 'javascript:;',
                'menu_icon' => $row->menu_icon,
                'menu_childs' => array()
            );

            if ($row->menu_parent > 0) {
                for ($i = 0; $i < count($menuStructures); $i++) {
                    if ($menuStructures[$i]['menu_id'] === $row->menu_parent) {
                        $menuStructure = $menuStructures[$i]['menu_childs'];
                        $menuStructure[] = $structure;

                        $menuStructures[$i]['menu_childs'] = $menuStructure;

                        break;
                    }
                }
            } else {
                $menuStructures[] = $structure;
            }
        }

        return $menuStructures;
    }
    
	public function login($data)
	{
		$mistery = array();
		for($i = 0; $i < count($data) ; $i++){ $mistery[] = "?"; }
		$mistery = implode(", ", $mistery);
		$str_query = "CALL log_insert ('".$data['id_status']."','" .$data['userid']. "','" .$data['ip_address']. "','" .$data['browser']."','" .$data['status']. "','" .$data['method_name']. "','" .$data['module_name']. "','" .$data['historydetail']." ')";
		$result = parent::query($str_query);
		$return_value = $result[0]['jml'] > 0 ? true : false;
		return $return_value;
	}
}
