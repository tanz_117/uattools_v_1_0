<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Visitor extends Visitor_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('main_model', '_mainModel');
		$this->load->library(array('authentication', 'form_validation'));
	}

	public function index()
	{
		$this->load->model('products/Product_model', '_productModel');
		$this->data->datas = $this->_productModel->rows(array(
			'select' => "produk_photo,produk_id,produk_identity,produk_nama,produk_harga,produk_stok,produk_pointorder",
			'order_by' => array(array('produk_status', 'DESC'),array('produk_nama','ASC')),
			'limit' => array('8')
		));

		$this->load->model('sliders/slider_model', '_sliderModel');
		$this->data->pictures = $this->_sliderModel->rows();
		$this->template
			->title(lang('dashboard'))
			->build(config_item('default_theme') . '/visitor',$this->data);
	}

	public function signup()
	{
		$validation_rules = array(
			array(
				'field' => 'user_identity',
				'label' => db_lang('user_identity'),
				'rules' => 'required|trim|min_length[4]|callback__check_availablity'
			),
			array(
				'field' => 'anggota_nama',
				'label' => db_lang('anggota_nama'),
				'rules' => 'required|trim'
			),
			array(
				'field' => 'anggota_tmplahir',
				'label' => db_lang('anggota_tmplahir'),
				'rules' => 'required|trim'
			),
			array(
				'field' => 'anggota_tgllahir',
				'label' => db_lang('anggota_tgllahir'),
				'rules' => 'required|trim'
			),
			array(
				'field' => 'anggota_alamat',
				'label' => db_lang('anggota_alamat'),
				'rules' => 'required|trim'
			),
			array(
				'field' => 'anggota_jnskelamin',
				'label' => db_lang('anggota_jnskelamin'),
				'rules' => 'required|trim'
			),
			array(
				'field' => 'anggota_notlp',
				'label' => db_lang('anggota_notlp'),
				'rules' => 'required|trim|numeric'
			),
			array(
				'field' => 'anggota_email',
				'label' => db_lang('anggota_email'),
				'rules' => 'required|trim|valid_email'
			),
			array(
				'field' => 'anggota_noidentitas',
				'label' => db_lang('anggota_noidentitas'),
				'rules' => 'required|trim'
			),
			array(
				'field' => 'anggota_jnsidentitas',
				'label' => db_lang('anggota_jnsidentitas'),
				'rules' => 'required|trim'
			),
			array(
				'field' => 'anggota_scanidentitas',
				'label' => db_lang('anggota_scanidentitas'),
				'rules' => 'callback__checkfileidentitas'
			),
			array(
				'field' => 'anggota_persetujuan',
				'label' => db_lang('anggota_persetujuan'),
				'rules' => 'required|trim'
			),
			array(
				'field' => 'anggota_photo',
				'label' => db_lang('anggota_photo'),
				'rules' => 'callback__checkfilephoto'
			),
			array(
				'field' => 'user_password',
				'label' => db_lang('user_password'),
				'rules' => 'required|min_length[5]|trim|callback__check_password'
			),
			array(
				'field' => 'user_password_confirm',
				'label' => db_lang('user_password_confirm'),
				'rules' => 'required|trim'
			),
			array(
				'field' => 'anggota_area',
				'label' => db_lang('anggota_area'),
				'rules' => 'required|trim'
			)
		);
		$this->form_validation->set_rules($validation_rules);
		if ($this->form_validation->run()) {;
			$status_upload_id = true;
			$status_upload_photo = true;
			$this->load->model('reqanggotas/reqanggota_model', '_reqAnggota_model');

			if(isset($_FILES['anggota_scanidentitas']))
			{
				$nama_file_id = $_FILES['anggota_scanidentitas']['name'];
				$nama_file_id = str_replace(" ","_",$nama_file_id);
				$_FILES['anggota_scanidentitas']['name'] = rand(0, 99999).'_'.$nama_file_id;
				$nama_file_id = $_FILES['anggota_scanidentitas']['name'];

				$uploadPath = './assets/uploads/anggota/scanidentitas';
				$config['upload_path'] = $uploadPath;
				$config['allowed_types'] = 'jpg';

				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload('anggota_scanidentitas')){
					$_POST['anggota_scanidentitas'] = $nama_file_id;
					$status_upload_id = true;
				}
				else{
					$status_upload_id = false;
					$message_upload_id = $this->upload->display_errors();
				}
			}

			if(isset($_FILES['anggota_photo']))
			{
				$nama_file_photo = $_FILES['anggota_photo']['name'];
				$nama_file_photo = str_replace(" ","_",$nama_file_photo);
				$_FILES['anggota_photo']['name'] = rand(0, 99999).'_'.$nama_file_photo;
				$nama_file_photo = $_FILES['anggota_photo']['name'];

				$uploadPath = './assets/uploads/anggota/photo';
				$config['upload_path'] = $uploadPath;
				$config['allowed_types'] = 'jpg';

				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload('anggota_photo')){
					$_POST['anggota_photo'] = $nama_file_photo;
					$status_upload_photo = true;
				}
				else{
					$status_upload_photo = false;
					$message_upload_photo = $this->upload->display_errors();
				}
			}

			$_POST['user_salt'] = $this->_reqAnggota_model->generateSalt();
			$_POST['user_password'] = hash('sha256', $_POST['user_password'].''.$_POST['user_salt']);
			$_POST['user_password_confirm'] = hash('sha256', $_POST['user_password_confirm']);
			$compare = set_before_after($_POST);
			$post = set_audit_history($_POST,$this->module, $this->method, $compare);
			$post['userid'] = $_POST['user_identity'];
			$post['module_name'] = 'signup';

			if($status_upload_id && $status_upload_photo){
				// $this->load->model('reqanggotas/Reqanggota_model', '_reqAnggota_model');
				if ($this->_reqAnggota_model->insert($post)) {
					$message = sprintf(lang('thanks_for_register'),$post['anggota_nama']);
					$status = 'success';
				} else {
					$message = sprintf(lang('sorry_for_register'),$post['anggota_nama']);
					$status = 'error';
				}
			}else{
					$message = $message_upload_id .' tes '. $message_upload_photo;
					$status = 'error';
			}


			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}
		} elseif ($this->input->post()) {
			$message = $this->form_validation->error_string('', '');
			$status = 'error';
			$row = (object)$this->input->post();
		} else {
			$row = new stdClass;
			foreach ($this->validation_rules as $rule) {
				$row->{$rule['field']} = set_value($rule['field']);
			}
		}

		$this->data->row = $row;
		$this->data->areaList = $this->_getAreaList();
		$this->data->jnsIdentitas = $this->_getJenisIdentitasList(config_item('groupParametersId_jnsIdentitas'));
		$this->template
			->title(lang('signup'))
			->build(config_item('default_theme') . '/visitorsignup',$this->data);
	}

	private function _getAreaList($id)
	{
		$this->load->model('areas/area_model', '_areaModel');
		$row = $this->_areaModel->rows();
		return array_for_select($row, 'area_id', 'area_nama');
	}

	private function _getJenisIdentitasList($id)
	{
		$this->load->model('parameters/Parameter_model', '_parameterModel');
		$row = $this->_parameterModel->getParameterByParameterGroup($id);
		return array_for_select($row, 'configuration_id', 'configuration_value');
	}


	public function _checkfileidentitas(){
		$needCheck = true;
		if($this->method == 'edit'){
			$this->load->model('anggotas/anggota_model', '_anggota_model');
			$current_data = $this->_anggota_model->row(array(
				'select' => "anggota.*,  users.user_identity",
				'join' => array(
					array('users','anggota.user_id = users.user_id')
				),
				'where' => array('anggota.user_id' => $_POST['user_id'])
			));

			$needCheck = $current_data->user_identity == $_POST['user_identity'] ? false : true;
		}
		if($this->method == 'add' || $needCheck == true){
			// var_dump($_FILES['anggota_scanidentitas']['size']);
			if($_FILES['anggota_scanidentitas']['size'] == 0){
				// var_dump(db_lang('anggota_scanidentitas'));
				$this->form_validation->set_message('_checkfileidentitas', sprintf(lang('attachment_null'),db_lang('anggota_scanidentitas')));
				return false;
			}
		}
		return true;
	}

	public function _checkfilephoto(){
		$needCheck = true;
		if($this->method == 'edit'){
			// $current_data = $this->_anggota_model->row(array('where' => array('user_id' => $_POST['user_id'])));
			$current_data = $this->_anggota_model->row(array(
				'select' => "anggota.*,  users.user_identity",
				'join' => array(
					array('users','anggota.user_id = users.user_id')
				),
				'where' => array('anggota.user_id' => $_POST['user_id'])
			));

			$needCheck = $current_data->user_identity == $_POST['user_identity'] ? false : true;
		}
		if($this->method == 'add' || $needCheck == true){
			if($_FILES['anggota_photo']['size'] == 0){

				$this->form_validation->set_message('_checkfilephoto', sprintf(lang('attachment_null'),db_lang('anggota_photo')));
				return false;
			}
		}
		return true;
	}

	public function _check_availablity($user_identity)
	{
		$this->load->model('users/user_model', '_user_model');
		$this->load->model('reqanggotas/reqanggota_model', '_reqAnggota_model');
		$existing_data = $this->_user_model->count(array('where' => array('user_identity' => $user_identity,'user_status' => '1')));;
		$existing_req_data = $this->_reqAnggota_model->count(array('where' => array('user_identity' => $user_identity,'user_status' => '1')));

		if($existing_data > 0 || $existing_req_data > 0){
			$this->form_validation->set_message('_check_availablity', sprintf(lang('user_id_not_available'),$user_identity));
			return false;
		}
		return true;
	}

	public function _check_password(){
		if($_POST['user_password'] === $_POST['user_password_confirm']){
			return true;
		}
		$this->form_validation->set_message('_check_password', lang('user_password_not_match'));
		return false;
	}

	public function login()
	{
		$rules_2 = array(
			array('field' => 'userid','label' => lang('login.user_id'), 'rules' => 'required|callback__check_authentication'),
			array('field' => 'userpwd','label' => lang('login.password'),'rules' => 'required')
		);
// $this->form_validation->unset_rules();
		$this->form_validation->set_rules($rules_2);
		if ($this->form_validation->run() or $this->authentication->isLoggedIn()) {
			// var_dump($this->form_validation->run());
			// exit;
			$compare = set_before_after(array('uid' => $this->session->userdata('user_id'), 'logged_in' => true), array('uid' => NULL, 'logged_in' => false));
			$input = set_audit_history(array('id_status' => 'login'), 'visitor', $this->method, $compare);

			$this->load->model('Auth_model', '_authModel');
			$this->_authModel->login($input);

			// $this->session->set_flashdata('message', 'Login.'); 
			redirect('visitor','refresh');
		} elseif ($this->input->post()) {
			$message = $this->form_validation->error_string('', '');
			$status = 'error';
			$row = (object)$this->input->post();
		} else {
			$row = new stdClass;
			foreach ($this->validation_rules as $rule) {
				$row->{$rule['field']} = set_value($rule['field']);
			}
		}

		// var_dump($_POST);exit;

		$this->template
			->title('Login')
			->build(config_item('default_theme') . '/visitorlogin');
	}

	public function logout()
	{
		log_activity(array('identity' => NULL, 'logged_in' => false), array('identity' => userdata('identity'), 'logged_in' => true), NULL, 'auth', 'logout');

		$this->authentication->loggedOut();
		redirect('visitor');
	}

	public function _check_authentication($identity)
	{
		$this->load->model('users/user_model', '_userModel');
		$users = $this->_userModel->row(array('select' => 'user_id,user_identity,user_role_id,user_name,user_registerdate,user_salt','where' => array('user_identity' => $identity)));
		if (!$this->authentication->isAuthenticated(sanitize_input(strtoupper($identity)),$_POST['userpwd'].''.$users->user_salt)) {
			// $this->form_validation->set_message('_check_authentication', $this->authentication->errors());
			$this->form_validation->set_message('_check_authentication', lang('auth.login_unsuccessful'));
			// $this->form_validation->set_message('_check_authentication', sprintf(lang('user_id_not_available'),$user_identity));
			return false;
		}
		return true;
	}

}
