<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends Admin_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('main_model', '_mainModel');
		$this->load->library(array('authentication', 'form_validation'));

		$rules = array(
			array('field' => 'userid', 'rules' => 'required|max_length[25]|min_length[4]|alpha_numeric|callback__check_authentication'),
			array('field' => 'userpwd','label' => lang('login.password'),'rules' => 'required')
		);
		$this->form_validation->set_rules($rules);
	}

	public function index()
	{
		$this->template
			->title(lang('dashboard'))
			->build(config_item('default_theme') . '/main',$this->data);
	}

	public function login()
	{
		if ($this->form_validation->run() or $this->authentication->isLoggedIn()) {
			$compare = set_before_after(array('uid' => $this->session->userdata('id'), 'logged_in' => true), array('uid' => NULL, 'logged_in' => false));
			$input = set_audit_history(array('id_status' => 'login'), 'main', $this->method, $compare);

			$this->load->model('Auth_model', '_authModel');
			$this->_authModel->login($input);

			redirect('main');
		}

		$this->template
			->title('Login')
			->build(config_item('default_theme') . '/login');
	}

	public function logout()
	{
		log_activity(array('identity' => NULL, 'logged_in' => false), array('identity' => userdata('identity'), 'logged_in' => true), NULL, 'auth', 'logout');

		$this->authentication->loggedOut();
		redirect('main/login');
	}

	public function _check_authentication($identity)
	{
		$this->load->model('users/user_model', '_userModel');
		$users = $this->_userModel->row(array('where' => array('id' => $identity)));

		// if($users->user_role_id != '1'){
			// $this->form_validation->set_message('_check_authentication', lang('auth.user_access_denied'));
			// return false;
		// }else 
		if (!$this->authentication->isAuthenticated(sanitize_input(strtoupper($identity)),$_POST['userpwd'])) {
			$this->form_validation->set_message('_check_authentication', $this->authentication->errors());
			return false;
		}
		return true;
	}

}
