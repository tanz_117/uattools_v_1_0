<h2><?php //echo build_page_header('<i class="glyphicon %s"></i> %s') ?></h2>
<div class="breadcrumb-wrapper">
	<div class="row">
		<div class="col-sm-2">
			<h3 style="margin:0;padding-left:10px;padding-top:3px;"><?php echo build_page_header('<i class="glyphicon %s"></i> %s') ?></h3>
		</div>
		<div class="col-sm-10">
			<ol class="breadcrumb text-right" style="margin-bottom: 0">
				<?php echo build_breadcrumbs('<li%s>%s</li>') ?>
			</ol>
		</div>
	</div>
</div>