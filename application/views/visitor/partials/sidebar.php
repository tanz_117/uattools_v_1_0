<div class="user-panel">
	<div class="pull-left image">
		<?php echo image('100x100.jpg','_theme_',array('class' => 'img-circle', 'alt' => 'User Image')) ?>
	</div>
	<div class="pull-left info">
		<p><?php echo $this->session->userdata('user_name');?></p>
		<a href="#"><i class="fa fa-circle text-info"></i> Online</a>
	</div>
</div>
<ul class="sidebar-menu">
	<li class="header"><?php echo db_lang('page_title.menu_header') ?></li>
	<li class="treeview"><a href="main"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
	<?php echo build_menu('<li%s>%s</li>', '<ul class="treeview-menu" style="display:%s">%s</ul>'); ?>
	<?php //echo build_sidebar('<li%s>%s</li>', '<ul class="treeview-menu" style="display:%s">%s</ul>'); ?>
</ul>
