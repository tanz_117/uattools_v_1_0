<a href="index2.html" class="logo">
	<span class="logo-mini"><b>{/}</b></span>
	<span class="logo-lg pull-right" style="font-size:30px;letter-spacing:-2px"><b>PesanBelanjaan</b></span>
</a>
<nav class="navbar navbar-static-top">
	<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
		<span class="sr-only">{nav.}</span>
	</a>
	<div class="navbar-custom-menu">
		<ul class="nav navbar-nav">
			<li class="dropdown user user-menu">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<?php echo image('100x100.jpg','_theme_',array('class' => 'user-image', 'alt' => 'User Image')) ?>
					<span class="hidden-xs"><?php echo $this->session->userdata('user_name'); ?></span>
				</a>
				<ul class="dropdown-menu">
					<li class="user-header">
						<?php echo image('100x100.jpg','_theme_',array('class' => 'img-circle', 'alt' => 'User Image')) ?>
						<p>
							<?php echo $this->session->userdata('user_name'); ?>
							<small>Member since <?php echo date('m-Y',strtotime($this->session->userdata('user_registerdate'))); ?></small>
						</p>
					</li>
					<li class="user-footer">
						<div class="pull-right">
							<a href="main\logout" class="btn btn-default btn-flat">Sign out</a>
						</div>
					</li>
				</ul>
			</li>
<!-- 			<li>
				<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
			</li> -->
		</ul>
	</div>
</nav>
