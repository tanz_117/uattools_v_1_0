<?php if ($this->session->flashdata('error')): ?>
    <div class="alert alert-danger alert-fill alert-close alert-dismissible fade in" role="alert">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
<?php endif; ?>

<?php if (validation_errors()): ?>
    <div class="alert alert-danger alert-fill alert-close alert-dismissible fade in" role="alert">
        <?php echo validation_errors(); ?>
    </div>
<?php endif; ?>

<?php if (!empty($messages['error'])): ?>
    <div class="alert alert-danger alert-fill alert-close alert-dismissible fade in" role="alert">
        <?php echo $messages['error']; ?>
    </div>
<?php endif; ?>

<?php if ($this->session->flashdata('warning')): ?>
    <div class="alert alert-warning alert-fill alert-close alert-dismissible fade in" role="alert">
        <?php echo $this->session->flashdata('warning'); ?>
    </div>
<?php endif; ?>

<?php if (!empty($messages['warning'])): ?>
    <div class="alert alert-warning alert-fill alert-close alert-dismissible fade in" role="alert">
        <?php echo $messages['warning']; ?>
    </div>
<?php endif; ?>

<?php if ($this->session->flashdata('notice')): ?>
    <div class="alert alert-info alert-fill alert-close alert-dismissible fade in" role="alert">
        <?php echo $this->session->flashdata('notice'); ?>
    </div>
<?php endif; ?>

<?php if (!empty($messages['notice'])): ?>
    <div class="alert alert-info alert-fill alert-close alert-dismissible fade in" role="alert">
        <?php echo $messages['notice']; ?>
    </div>
<?php endif; ?>

<?php if ($this->session->flashdata('success')): ?>
    <div class="alert alert-success alert-fill alert-close alert-dismissible fade in" role="alert">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
<?php endif; ?>

<?php if (!empty($messages['success'])): ?>
    <div class="alert alert-success alert-fill alert-close alert-dismissible fade in" role="alert">
        <?php echo $messages['success']; ?>
    </div>
<?php endif; ?>
