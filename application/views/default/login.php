<?php echo $this->load->view(config_item('default_theme') . '/partials/notices') ?>
<?php echo form_open(BASE_URL().'main/login', array('class' => 'login-form', 'id' => 'validate-form')); ?>
    <div class="row">
        <div class="input-field col s12 center">
            <p class="center login-form-text" style="font-size: 18pt">{apps:helper:lang line="application_name"}</p>
        </div>
    </div>
    <div class="row margin">
        <div class="input-field col s12">
            <i class="mdi-social-person-outline prefix"></i>
            <?php echo form_input('userid', '', 'id="userid"') ?>
            <label for="username" class="center-align">{apps:helper:lang line="login.username"}</label>
        </div>
    </div>
    <div class="row margin">
        <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix"></i>
            <?php echo form_password('userpwd', '', 'id="password"') ?>
            <label for="password">{apps:helper:lang line="login.password"}</label>
        </div>
    </div>
    <div class="row">
        <div class="input-field col s12">
            <button type="submit" class="btn waves-effect waves-light col s12">{apps:helper:lang line="login.submit"}</button>
        </div>
    </div>
<?php echo form_close(); ?>
