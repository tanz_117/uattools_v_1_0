<aside id="left-sidebar-nav">
    <ul id="slide-out" class="side-nav fixed leftside-navigation">
        <li class="user-details cyan darken-2">
            <div class="row" style="margin-left: -17px">
                <div class="col s12 m12 l12">
                    <p class="user-roal white-text profile-btn" style="margin-bottom: -20px"><?php echo $this->session->userdata('user_id'); ?></p>
                    <p class="user-roal white-text" style="margin-bottom: -20px"><?php echo $this->session->userdata('user_name'); ?></p>
                    <p class="user-roal white-text" style="margin-bottom: -20px"><?php echo $this->session->userdata('user_namacabang'); ?></p>
                    <p class="user-roal white-text"><?php echo $this->session->userdata('user_role_name'); ?></p>
                </div>
            </div>
        </li>

        <li class="bold <?php echo (!$this->module > '' ? 'active' : '')?>">
            <a href="{apps:baseurl}" class="waves-effect waves-cyan" style="font-size:11px"><i class="mdi-action-dashboard"></i> <?php echo lang('dashboard'); ?></a>
        </li>
        <?php echo build_sidebar('<ul class="collapsible collapsible-accordion">%s</ul>', '<li%s>%s</li>'); ?>

    </ul>
</aside>
