<header id="header" class="page-topbar">
    <div class="navbar-fixed">
        <nav class="navbar-color">
            <div class="nav-wrapper">
                <ul class="left">
                    <li>
                        <span class="logo-wrapper">
                            <a href="" class="brand-logo darken-1">
                                <span class="logo-text">
                                    <div style="margin-top:-15px">{apps:helper:lang line="application_name"}</div>
                                </span>
                            </a>
                        </span>
                    </li>
                </ul>
                <ul class="right hide-on-med-and-down">                   
                    
                    <li>
                        <a href="{apps:baseurl}main/logout" class="waves-effect waves-block waves-light">
                            <i class="mdi-hardware-keyboard-tab"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>

</header>
