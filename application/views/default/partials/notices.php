<?php if ($this->session->flashdata('error')): ?>    
    <div id="card-alert" class="card red">
        <div class="card-content white-text">
            <p><?php echo $this->session->flashdata('error'); ?></p>
        </div>
        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
<?php endif; ?>

<?php if (validation_errors()): ?>    
    <div id="card-alert" class="card red">
        <div class="card-content white-text">
            <p><?php echo validation_errors(); ?></p>
        </div>
        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
<?php endif; ?>

<?php if (!empty($messages['error'])): ?>
    <div id="card-alert" class="card red">
        <div class="card-content white-text ">
            <p><?php echo $messages['error']; ?></p>
        </div>
        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
<?php endif; ?>

<?php if ($this->session->flashdata('warning')): ?>
    <div id="card-alert" class="card light-blue">
        <div class="card-content white-text">
            <p><?php echo $this->session->flashdata('warning'); ?></p>
        </div>
        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
<?php endif; ?>

<?php if (!empty($messages['warning'])): ?>
    <div id="card-alert" class="card light-blue">
        <div class="card-content white-text">
            <p><?php echo $messages['warning']; ?></p>
        </div>
        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
<?php endif; ?>


<?php if ($this->session->flashdata('notice')): ?>
    <div id="card-alert" class="card light-blue">
        <div class="card-content white-text">
            <p><?php echo $this->session->flashdata('notice'); ?></p>
        </div>
        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
<?php endif; ?>

<?php if (!empty($messages['notice'])): ?>
    <div id="card-alert" class="card light-blue">
        <div class="card-content white-text">
            <p><?php echo $messages['notice']; ?></p>
        </div>
        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
<?php endif; ?>

<?php if ($this->session->flashdata('success')): ?>
    <div id="card-alert" class="card green">
        <div class="card-content white-text">
            <p><?php echo $this->session->flashdata('success'); ?></p>
        </div>
        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
<?php endif; ?>

<?php if (!empty($messages['success'])): ?>
    <div id="card-alert" class="card green">
        <div class="card-content white-text">
            <p><?php echo $messages['success']; ?></p>
        </div>
        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
<?php endif; ?>
