<h2><?php echo build_page_header('<i class="fa %s"></i> %s') ?></h2>
<div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
        <?php echo build_breadcrumbs('<li%s>%s</li>') ?>
    </ol>
</div>
