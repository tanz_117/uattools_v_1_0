<?php $breadcrumbs = build_breadcrumbs(); ?>
<?php if (!empty($breadcrumbs)): ?>
<div id="breadcrumbs-wrapper">
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">{apps:template:title}</h5>
                <ol class="breadcrumbs" style="margin-top:0">
                    <?php foreach ($breadcrumbs as $link => $title) : ?>
                    <li><a href="<?php echo $link; ?>"><?php echo $title; ?></a></li>
                    <?php endforeach; ?>
                </ol>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>