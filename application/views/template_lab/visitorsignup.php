<section class="content"  id="main-content" styles="min-height: 180px;">
<h3><p class="login-box-msg text-olive"><?php echo lang('signup.registration') ?></p></h3>
<?php echo $this->load->view(config_item('default_theme') . '/partials/notices') ?>
<?php echo form_open_multipart(BASE_URL().'visitor/signup', array('class' => 'login-form', 'id' => 'validate-form','onsubmit' => "return confirm('Do you really want to submit the form?');")); ?>
<?php //echo form_open_multipart(BASE_URL().'visitor/signup', 'id="form_upload" class="crud_upload" autocomplete="off" ') ?>
	<div class="box-body">
		<div class="row">
			<div class="col-sm-6">
				<div class="row form-group">
					<label class="col-sm-12 form-control-label semibold"><?php echo db_lang('user_identity') ?></label>
					<div class="col-sm-12">
						<?php 
							echo form_input('user_identity', $row->user_identity, 'class="form-control" placeholder="'. db_lang('user_identity') .'"');
						?>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="row form-group">
					<label class="col-sm-12 form-control-label semibold"><?php echo db_lang('anggota_nama') ?></label>
					<div class="col-sm-12">
						<?php echo form_input('anggota_nama', $row->anggota_nama, 'class="form-control" placeholder="'. db_lang('anggota_nama') .'"') ?>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="row form-group">
					<label class="col-sm-12 form-control-label semibold"><?php echo db_lang('anggota_tmplahir') ?></label>
					<div class="col-sm-12">
						<?php echo form_input('anggota_tmplahir', $row->anggota_tmplahir, 'class="form-control" placeholder="'. db_lang('anggota_tmplahir') .'"') ?>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="row form-group">
					<label class="col-sm-12 form-control-label semibold"><?php echo db_lang('anggota_tgllahir') ?></label>
					<div class="col-sm-12">
						<?php echo form_input('anggota_tgllahir', $row->anggota_tgllahir, 'id="datepicker" class="form-control  pull-right"') ?>
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="row form-group">
					<label class="col-sm-12 form-control-label semibold"><?php echo db_lang('anggota_alamat') ?></label>
					<div class="col-sm-12">
						<?php echo form_input('anggota_alamat', $row->anggota_alamat, 'class="form-control" placeholder="'. db_lang('anggota_alamat') .'"') ?>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="row form-group">
					<label class="col-sm-12 form-control-label semibold"><?php echo db_lang('anggota_area') ?></label>
					<div class="col-sm-12">
						<?php
							$items = array('' => lang('select.pick')) + $areaList;
							echo form_dropdown('anggota_area', $items, $row->anggota_area, 'class="form-control"');
						?>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="row form-group">
					<label class="col-sm-12 form-control-label semibold"><?php echo db_lang('anggota_jnskelamin') ?></label>
					<div class="col-sm-12">
						<?php
							$items = array('' => lang('select.pick'),'L' =>lang('label.lakilaki'),'P' => lang('label.perempuan'));
							echo form_dropdown('anggota_jnskelamin', $items, $row->anggota_jnskelamin, 'class="form-control"');
						?>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="row form-group">
					<label class="col-sm-12 form-control-label semibold"><?php echo db_lang('anggota_notlp') ?></label>
					<div class="col-sm-12">
						<?php echo form_input('anggota_notlp', $row->anggota_notlp, 'class="form-control" placeholder="'. db_lang('anggota_notlp') .'"') ?>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="row form-group">
					<label class="col-sm-12 form-control-label semibold"><?php echo db_lang('anggota_email') ?></label>
					<div class="col-sm-12">
						<?php echo form_input('anggota_email', $row->anggota_email, 'class="form-control" placeholder="'. db_lang('anggota_email') .'"') ?>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
	 			<div class="row form-group">
					<label class="col-sm-12 form-control-label semibold"><?php echo db_lang('anggota_jnsidentitas') ?></label>
					<div class="col-sm-12">
						<?php
							$items = array('' => lang('select.pick')) + $jnsIdentitas;
							echo form_dropdown('anggota_jnsidentitas', $items, $row->anggota_jnsidentitas, 'class="form-control"');
						?>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="row form-group">
					<label class="col-sm-12 form-control-label semibold"><?php echo db_lang('anggota_noidentitas') ?></label>
					<div class="col-sm-12">
						<?php echo form_input('anggota_noidentitas', $row->anggota_noidentitas, 'class="form-control" placeholder="'. db_lang('anggota_noidentitas') .'"') ?>
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="row form-group">
					<label class="col-sm-12 form-control-label semibold"><?php echo db_lang('anggota_scanidentitas') ?></label>
					<div class="col-sm-12">
						<?php echo form_upload('anggota_scanidentitas', $row->anggota_scanidentitas, 'class="form-control" placeholder=" '. db_lang('anggota_scanidentitas') .' " ');?>
						<p class="help-block"><?php echo $row->anggota_scanidentitas ?></p>
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="row form-group">
					<label class="col-sm-12 form-control-label semibold"><?php echo db_lang('anggota_photo') ?></label>
					<div class="col-sm-12">
						<?php echo form_upload('anggota_photo', $row->anggota_photo, 'class="form-control" placeholder=" '. db_lang('anggota_photo') .' " ');?>
						<p class="help-block"><?php echo $row->anggota_photo ?></p>
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="row form-group">
					<label class="col-sm-12 form-control-label semibold"><?php echo db_lang('user_password') ?></label>
					<div class="col-sm-12">
						<?php echo form_password('user_password', '', 'class="form-control" placeholder="'. db_lang('user_password') .'"') ?>
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="row form-group">
					<label class="col-sm-12 form-control-label semibold"><?php echo db_lang('user_password_confirm') ?></label>
					<div class="col-sm-12">
						<?php echo form_password('user_password_confirm', '', 'class="form-control" placeholder="'. db_lang('user_password_confirm') .'"') ?>
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="row form-group">
					<label class="col-sm-12 form-control-label semibold"></label>
					<div class="col-sm-12">
						<div class="checkbox">
							<label>
								<?php echo form_checkbox('anggota_persetujuan', '1', $row->anggota_persetujuan == '1' ? TRUE : FALSE,'class="minimal"'); ?>
								<?php echo sprintf(lang('approve_terms_statement'),base_url('fhowto')); ?>
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="box-footer">
		<button type="submit" class="btn bg-olive btn-flat submit"><i class="glyphicon glyphicon-ok"></i>&nbsp;<?php echo lang('buttons.save') ?></button>
		<a href="<?php echo base_url('visitor') ?>" class="btn bg-maroon btn-flat" data-toggle="tooltip" title="">
			<i class="glyphicon glyphicon-remove"></i>&nbsp;<?php echo lang('buttons.cancel') ?>
		</a>
	</div>
</section>
<?php echo form_close(); ?>
