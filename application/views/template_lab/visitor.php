<div id="content" style="padding-top: 0">
		<?php if(userdata('user_id')): ?>
			<div  class="row">
				<div class="col-sm-12">
				<a style="margin:0" href="<?php echo base_url('Fproducts') ?>" class="btn bg-maroon btn-app col-xs-6" style="margin-right: 3px"><i class="fa fa-briefcase"></i>&nbsp;<?php echo db_lang('page_title.products') ?></a>
				<a style="margin:0" href="<?php echo base_url('Fsaldos') ?>" class="btn bg-olive btn-app col-xs-6" style="margin-right: 3px"><i class="fa fa-money"></i>&nbsp;<?php echo db_lang('page_title.saldos') ?></a>
				</div>
			</div>
		<?php endif ?>
		<div class="col-md-12" style="padding:0">
			<div class="box box-solid">
				<div class="box-body">
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<?php $counter = 0; ?>
							<?php foreach ($pictures as $key => $value): ?>
								<?php if($counter == 0): ?>
									<li data-target="#carousel-example-generic" data-slide-to="<?php echo $counter ?>" class="active"></li>
								<?php else: ?>
									<li data-target="#carousel-example-generic" data-slide-to="<?php echo $counter ?>" class=""></li>
								<?php endif ?>
								<?php $counter++ ?>
							<?php endforeach ?>
						</ol>
						<div class="carousel-inner">
							<?php $counter = 0; ?>
							<?php foreach ($pictures as $key => $value): ?>
								<?php if($counter == 0): ?>
									<div class="item active" style="height: 500px">
										<img style="width:100%" src="<?php echo base_url('assets/uploads/slider/'.$value->slider_name) ?>" alt="Second slide">
										<div class="carousel-caption">
											<?php echo $value->slider_name ?>
										</div>
									</div>
								<?php else: ?>
									<div class="item" style="height: 500px">
										<img style="width:100%" src="<?php echo base_url('assets/uploads/slider/'.$value->slider_name) ?>" alt="Second slide">
										<div class="carousel-caption">
											<?php echo $value->slider_name ?>
										</div>
									</div>
								<?php endif ?>
								<?php $counter++ ?>
							<?php endforeach ?>
						</div>
						<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
							<span class="fa fa-angle-left"></span>
						</a>
						<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
							<span class="fa fa-angle-right"></span>
						</a>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<section class="box-typical box-typical-padding" id="default">
			<div class="row" style="margin-bottom: 2%">
				<div class="col-sm-12 text-center">
					<h2 style="margin-bottom: 0">- <?php echo lang('label.ourProduct') ?> -</h2>
					<!-- <?php //if(userdata('user_id')): ?> -->
						<span><a href="<?php echo base_url('Fproducts/index') ?>"><?php echo lang('label.seeMore') ?></a></span>
<!-- 					<?php //else: ?>
						<span><a href="visitor/login"><?php //echo lang('label.seeMore') ?></a></span>
					<?php //endif ?> -->
				</div>
			</div>
			<?php echo form_open(base_url('Fproducts/addtochart'), 'name="datas_table" id="form_table"'); ?>
				<?php if (!empty($datas)): ?>
					<?php echo form_hidden('product_id_temp', '','class="form-control"');?>
					<?php foreach ($datas as $data): ?>
						<div class="col-sm-3">
							<div class="box box-solid">	
								<div class="box-body table-responsive no-padding">					
									<div class="col-sm-12" style="padding:0">
										<img class="img-responsive pad" src="<?php echo base_url('assets/uploads/produk/'.$data->produk_photo) ?>" width="100%" alt="<?php echo $data->produk_nama ?>">
									</div>
									<div class="col-sm-12 text-center" style="padding-bottom: 10px">
										<b><?php echo $data->produk_nama ?></b><br />
										<i><?php echo 'Rp. '.number_format($data->produk_harga) ?></i>
									</div>
								</div>
								<!-- <div class="box-footer"> -->
<!-- 									<?php //if(userdata('user_id')): ?>
										<button type="button" class="btn bg-olive btn-sm pull-right" onClick="addChart('<?php //echo $data->produk_id ?>')" style="margin-left: 3px">
											<i class="glyphicon glyphicon-shopping-cart"></i>&nbsp;<?php //echo lang('buttons.order') ?>
										</button>
									<?php //else: ?>
										<a href="visitor/login" class="btn bg-olive btn-sm pull-right" style="margin-left: 3px">
											<i class="glyphicon glyphicon-shopping-cart"></i>&nbsp;<?php //echo lang('buttons.order') ?>
										</a>
									<?php //endif ?> -->
<!-- 									<a href="<?php echo site_url('Fproducts/view/' .$data->produk_id) ?>" class="btn bg-maroon btn-sm pull-right" rel="view" data-toggle="tooltip" title="<?php echo lang('label.view') . ' `' . $data->produk_nama .'`' ?>">
										<i class="glyphicon glyphicon-info-sign"></i>&nbsp;<?php echo lang('buttons.detail') ?>
									</a> -->
								<!-- </div> -->
							</div>
						</div>
					<?php endforeach ?>
				<?php endif ?>
			<?php echo form_close() ?>
		</section>
		<div class="row text-right">
			<div class="col-sm-12">
				<?php //echo $pagination['links'] ?>
			</div>
		</div>
</div>
