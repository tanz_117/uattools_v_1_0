<p class="login-box-msg">Log In</p>
<?php echo $this->load->view(config_item('default_theme') . '/partials/notices') ?>
<?php echo form_open(BASE_URL().'main/login', array('class' => 'login-form', 'id' => 'validate-form')); ?>
	<div class="form-group has-feedback">
		<?php echo form_input('userid', '', 'id="userid" class="form-control" placeholder="{apps:helper:lang line="login.username"}"') ?>
		<span class="glyphicon glyphicon-user form-control-feedback"></span>
	</div>
	<div class="form-group has-feedback">
		<?php echo form_password('userpwd', '', 'id="password" class="form-control" placeholder="{apps:helper:lang line="login.password"}"') ?>
		<span class="glyphicon glyphicon-info-sign form-control-feedback"></span>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<button type="submit" class="btn waves-effect waves-light bg-olive btn-block btn-flat">{apps:helper:lang line="login.submit"}</button>
		</div>
	</div>
<?php echo form_close(); ?>
