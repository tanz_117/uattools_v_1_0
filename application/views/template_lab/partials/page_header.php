<h2><?php //echo build_page_header('<i class="glyphicon %s"></i> %s') ?></h2>
<div class="breadcrumb-wrapper">
	<div class="row">
		<div class="col-sm-3">
			<h3 style="margin:0;padding-left:10px;padding-top:3px;"><?php echo build_page_header('<i style="float:left" class="text-olive glyphicon %s"></i> <div style="float:left;margin-left:5px;font-size:25px"><span class="lead text-olive-active color-palette"">%s</span></div>') ?></h3>
		</div>
		<div class="col-sm-9">
			<ol class="breadcrumb text-right" style="margin-bottom: 0">
				<?php echo build_breadcrumbs('<li%s>%s</li>') ?>
			</ol>
		</div>
	</div>
</div>