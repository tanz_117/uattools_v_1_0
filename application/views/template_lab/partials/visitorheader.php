<nav class="navbar navbar-static-top" style="background-color: #f9ec00">
	<div class="container">
		<div class="navbar-header">
<!-- 			<div class="col-sm-12"> -->
				<!--<a href="<?php echo base_url() ?>" class="navbar-brand"><img src="<?php echo base_url('themes/template_lab/img/logo_header.png') ?>" width="25%"  alt="<?php echo $row->anggota_nama ?>"></a>-->
			<!-- </div> -->
			<a href="<?php echo base_url() ?>" class="navbar-brand text-olive"><b>Pesan</b>Belanjaan</a>
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
			<i class="fa fa-bars"></i>
			</button>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse pull-left text-olive" id="navbar-collapse">
			<ul class="nav navbar-nav">
				<?php echo build_visitormenu() ?>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
		<!-- Navbar Right Menu -->
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<?php if(userdata('user_id')): ?>
					<li class="dropdown notifications-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" id="btn-chart">
							<i class="glyphicon glyphicon-shopping-cart text-olive"></i>
							<?php if(userdata('trxpesanan_invoiceid')): ?>
								<span id="chart-alert" class="label label-warning">1</span>
							<?php endif ?>
						</a>
						<ul class="dropdown-menu">
							<li>
								<!-- inner menu: contains the actual data -->
								<div id="chart-list" class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto;">
									<ul class="menu" style="overflow: hidden; width: 100%;">
										<?php if(userdata('trxpesanan_invoiceid')): ?>
											<li>
												<a href="<?php echo base_url('Fcharts') ?>">
													<i class="glyphicon glyphicon-shopping-cart text-olive"></i><?php echo lang('label.id_pesanan').' : '.userdata('trxpesanan_invoiceid'); ?>
												</a>
											</li>
										<?php else: ?>
											<li>
												<a href="#">
													<i class="glyphicon glyphicon-shopping-cart text-aqua"></i><?php echo sprintf(lang('label.empty_grid'),'') ?>
												</a>
											</li>
										<?php endif ?>
									</ul>
								</div>
							</li>
						</ul>
					</li>
					<li class="dropdown user user-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<?php //echo image('100x100.jpg','_theme_',array('class' => 'user-image', 'alt' => 'User Image')) ?>
							<img src="<?php echo base_url('assets/uploads/anggota/photo/'.userdata('anggota_photo')) ?>"  alt="<?php userdata('user_name') ?>" class = "user-image">
							<span class="hidden-xs text-olive"><?php echo userdata('user_name'); ?></span>
						</a>
						<ul class="dropdown-menu">
							<li class="user-header bg-olive">
								<?php //echo image('tes.jpg','base_url("assets/")',array('class' => 'img-circle', 'alt' => 'User Image')) ?>
								<img src="<?php echo base_url('assets/uploads/anggota/photo/'.userdata('anggota_photo')) ?>"  alt="<?php userdata('user_name') ?>" class = "img-circle">
								<p>
									<?php echo $this->session->userdata('user_name'); ?>
									<small>Member since <?php echo date('m-Y',strtotime(userdata('user_registerdate'))); ?></small>
								</p>
							</li>
							<li class="user-footer">
								<div class="pull-right">
									<a href="visitor\logout" class="btn btn-default btn-flat">Sign out</a>
								</div>
							</li>
						</ul>
					</li>
				<?php else: ?>
					<li><a href="<?php echo base_url('visitor/login') ?>" class="text-olive"><?php echo lang('label.login') ?></a></li>
					<li><a href="<?php echo base_url('visitor/signup') ?>" class="text-olive"><?php echo lang('label.signup') ?></a></li>
				<?php endif ?>
			</ul>
		</div>
		<!-- /.navbar-custom-menu -->
	</div>
	<!-- /.container-fluid -->
</nav>