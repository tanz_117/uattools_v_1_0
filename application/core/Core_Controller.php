<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Core_Controller extends CI_Controller
{

    public $module;
    public $controller;
    public $method;
    public $data;
    public $configuration;

    public function __construct()
    {
        parent::__construct();

        $this->module = $this->router->fetch_module();
        $this->controller = $this->router->fetch_class();
        $this->method = $this->router->fetch_method();
        $this->data = new stdClass;

        $this->hooks =& $GLOBALS['EXT'];
        $this->hooks->call_hook('post_core_controller_constructor');

        $apps['baseurl'] = BASE_URL;
        $apps['currenturl'] = current_url();

        if (userdata('languages') != '') {
            $this->load->model('languages/language_model', '_languageModel');
            $languages = json_encode($this->_languageModel->getDefaultLanguage());
            $this->session->set_userdata('languages', gzdeflate($languages, 9));
        }

        $this->load->model('main_model', '_mainModel');
        $configs = $this->_mainModel->getActiveConfiguration();
        foreach ($configs as $conf) {
            $this->configuration[$conf->configuration_key] = $conf->configuration_value;
            $apps[$conf->configuration_key] = $conf->configuration_value;
        }

        $this->bahasa->load();

        $langs = config_item('supported_languages');
        $apps['lang'] = $langs[CURRENT_LANGUAGE];
        $apps['lang']['code'] = CURRENT_LANGUAGE;

        $this->load->vars($apps);
    }

    protected function is_ajax()
    {
        return $this->input->server('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest';
    }

}
