<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Base_model extends CI_Model
{
	protected $_table;
	protected $_pk;

	public function __construct()
	{
		parent::__construct();
	}

	public function get_table(){
		return $this->_table;
	}
	
	public function get_pk(){
		return $this->_pk;
	}

	public function lastId()
	{
		$this->db->limit(1);
		$this->db->order_by($this->_pk, 'DESC');
		$lastId = $this->db->select($this->_pk)->get($this->_table)->row();
		return $lastId->{$this->_pk};
	}

	// public function query($syntax, $parameters = NULL)
	// {
	// 	$query = $this->db->query($syntax, $parameters);
	// 	var_dump($query);exit;
	// 	if (!is_bool($query)) {
	// 		$result = $query->result();
	// 		$query->free_result();
	// 		return $result;
	// 	}
	// 	return $query;
	// }

	public function query($syntax)
	{
		$query = $this->db->query($syntax);
		if (!is_bool($query)) {
			$result = $query->result_array();
			$query->free_result();
			return $result;
		}
		return $query;
	}

	public function procedure($syntax, $parameters = NULL)
	{
		return $this->db->query($syntax, $parameters);
	}

	public function rows($attributes = array())
	{
		if (isset($attributes)) {
			foreach ($attributes as $key => $value) {
				// if (is_array($value) and isset($value[0])) {
				// 	if (is_array($value[0])) {
				// 		foreach ($value as $val) {
				// 			$this->db->{$key}($val[0], (isset($val[1]) ? $val[1] : ''), (isset($val[2]) ? $val[2] : ''));
				// 		}
				// 	} else {
				// 		if (count($value) < 3) {
				// 			$this->db->{$key}($value[0], (isset($value[1]) ? $value[1] : ''), (isset($value[2]) ? $value[2] : ''));
				// 		} else {
				// 			$this->db->{$key}($value);
				// 		}
				// 	}
				// } else {
				// 	$this->db->{$key}($value);
				// }
				if (is_array($value) and isset($value[0])) {
					if (is_array($value[0])) {
						foreach ($value as $val) {
							$this->db->{$key}($val[0], (isset($val[1]) ? $val[1] : ''), (isset($val[2]) ? $val[2] : ''));
						}
					} else {
						$this->db->{$key}($value[0], (isset($value[1]) ? $value[1] : ''), (isset($value[2]) ? $value[2] : ''));
					}
				} else {
					$this->db->{$key}($value);
				}
			}
		}
		// $query_result = $this->db->get($this->_table)->result();
		// $return_value = $query_result;
		// $query_result->free_result();
		// return $return_value;
		return $this->db->get($this->_table)->result();
	}

	public function row($attributes = array())
	{
		if (isset($attributes)) {
			foreach ($attributes as $key => $value) {
				// if (is_array($value) and isset($value[0])) {
				// 	if (is_array($value[0])) {
				// 		foreach ($value as $val) {
				// 			$this->db->{$key}($val[0], (isset($val[1]) ? $val[1] : ''), (isset($val[2]) ? $val[2] : ''));
				// 		}
				// 	} else {
				// 		if (count($value) > 3) {
				// 			$this->db->{$key}($value[0], (isset($value[1]) ? $value[1] : ''), (isset($value[2]) ? $value[2] : ''));
				// 		} else {
				// 			$this->db->{$key}($value);
				// 		}
				// 	}
				// } else {
				// 	$this->db->{$key}($value);
				// }
				if (is_array($value) and isset($value[0])) {
                    if (is_array($value[0])) {
                        foreach ($value as $val) {
                            $this->db->{$key}($val[0], (isset($val[1]) ? $val[1] : ''), (isset($val[2]) ? $val[2] : ''));
                        }
                    } else {
                        $this->db->{$key}($value[0], (isset($value[1]) ? $value[1] : ''), (isset($value[2]) ? $value[2] : ''));
                    }
                } else {
                    $this->db->{$key}($value);
                }
			}
		}
		return $this->db->get($this->_table)->row();
	}

	public function count($attributes = array())
	{
		if (isset($attributes)) {
			foreach ($attributes as $key => $value) {
				if (is_array($value) and isset($value[0])) {
					if (is_array($value[0])) {
						foreach ($value as $val) {
							$this->db->{$key}($val[0], (isset($val[1]) ? $val[1] : ''), (isset($val[2]) ? $val[2] : ''));
						}
					} else {
						$this->db->{$key}($value[0], (isset($value[1]) ? $value[1] : ''), (isset($value[2]) ? $value[2] : ''));
					}
				} else {
					$this->db->{$key}($value);
				}
			}
		}
		$this->db->from($this->_table);
		return $this->db->count_all_results();
	}

	public function where_in($key,$value){
		$this->db->where_in($key,$value);
		return $this->db->get($this->_table)->result();
	}

	public function where_not_in($key,$value){
		$this->db->where_not_in($key,$value);
		return $this->db->get($this->_table)->result();
	}

	public function rowById($id,$attributes = array())
	{
		if (isset($attributes)) {
			foreach ($attributes as $key => $value) {
				if (is_array($value) and isset($value[0])) {
					if (is_array($value[0])) {
						foreach ($value as $val) {
							$this->db->{$key}($val[0], (isset($val[1]) ? $val[1] : ''), (isset($val[2]) ? $val[2] : ''));
						}
					} else {
						$this->db->{$key}($value[0], (isset($value[1]) ? $value[1] : ''), (isset($value[2]) ? $value[2] : ''));
					}
				} else {
					$this->db->{$key}($value);
				}
			}
		}
		$result =  $this->db->get_where($this->_table, array($this->_pk => html_decode($id)))->row();
		return $this->unsanitizedResult($result);
	}

	public function insert($input)
	{
		$result = false;
		// $this->db->trans_start();
		$item_id = $this->generate_uuid();

		if(!in_array($this->_pk,$input)){
			$input = $input + array($this->_pk => $item_id);
		}else{
			$input = $input + array($this->_pk => $input->{$this->_pk});
		}
		
		$this->db->insert($this->_table, $input);
		// $historical_id = $this->insert_historical(html_decode($param['historydetail']));
		// $this->insert_audittrial($historical_id,$item_id,$input);
		// $result = true;
		// $this->db->trans_complete();
		return $result;
	}

	private function insert_historical($history_detail)
	{
		if($history_detail){
			$history_id = $this->generate_uuid();
			$insert_param = array('history_id' => $history_id,'history_detail' => $history_detail,'history_active' => '1');
			$a = $this->db->insert('histories', $insert_param);
			return $history_id;
		}
	}
	
	private function insert_audittrial($history_id,$item_id,$param){
		if($param){
			$audit_trail_id = $this->generate_uuid();
			$insert_param = array(
				'audit_trail_id' => $audit_trail_id,
				'audit_trail_user_id' => $param['userid'],
				'audit_trail_ip_address' => $param['ip_address'],
				'audit_trail_browser_ua' => $param['browser'],
				'audit_trail_history_id' => $history_id,
				'audit_trail_status' => $param['status'],
				'audit_trail_action_id' => $param['method_name'],
				'audit_trail_module_id' => $param['module_name'],
				'audit_trail_record_id' => $item_id
			);
			$this->db->insert('audit_trails', $insert_param);
		}
	}

	public function update($input, $id)
	{
		$result = false;
		$this->db->trans_start();
		$update_param = array();
		$param = $this->unsanitizedParameter($input);
		$insert_field_list = $this->mergeInput();
		foreach ($insert_field_list as $item) {
			if(in_array($item,array_keys($param))){
				$update_param = $update_param + array($item => $param[$item]);
			}
		}
		$this->db->where($this->_pk, $id);
		$this->db->update($this->_table, $update_param);
		$historical_id = $this->insert_historical(html_decode($param['historydetail']));
		$this->insert_audittrial($historical_id,$id,$input);
		$result = true;
		$this->db->trans_complete();
		return $result;
	}

	public function delete($input)
	{
		$result = false;
		$this->db->trans_start();
		$param = $this->unsanitizedParameter($input);
		$this->db->where($this->_pk, $param['id']);
		$this->db->delete($this->_table);
		$historical_id = $this->insert_historical(html_decode($param['historydetail']));
		$this->insert_audittrial($historical_id,$param['id'],$input);
		$result = true;
		$this->db->trans_complete();
		return $result;
	}

	protected function unsanitizedParameter($param = array()){
		$param_result = array();
		foreach ($param as $key => $value) {
			$param_result = $param_result + array($key => html_decode($value));
		}
		return $param_result;
	}

	private function mergeInput(){
		$field_on_form = $this->get_fields_on_form() ? $this->get_fields_on_form() : array();
		$field_on_session = $this->get_fields_on_session() ? $this->get_fields_on_session() : array();
		return array_merge($field_on_form,$field_on_session);
	}

	private function unsanitizedResult($param){
		foreach ($param as $key => $value) {
			$param->{$key} = html_decode($value);
		}
		return $param;
	}

	private function generate_uuid()
	{
		return strtoupper(sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
			// 32 bits for "time_low"
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
			// 16 bits for "time_mid"
			mt_rand( 0, 0xffff ),
			// 16 bits for "time_hi_and_version",
			// four most significant bits holds version number 4
			mt_rand( 0, 0x0fff ) | 0x4000,
			// 16 bits, 8 bits for "clk_seq_hi_res",
			// 8 bits for "clk_seq_low",
			// two most significant bits holds zero and one for variant DCE1.1
			mt_rand( 0, 0x3fff ) | 0x8000,
			// 48 bits for "node"
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
		));
	}
}