<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Input extends CI_Input
{

    protected $_ci;

    public function __construct()
    {
        parent::__construct();
    }

    public function post_sanitized($input = NULL, $xss_clean = TRUE)
    {
        $output = NULL;
        if (! empty($input)) {
            if (is_array($input)) {
                foreach ($input as $index) {
                    $output[$index] = sanitize_input($this->post($index, $xss_clean));
                }
            } else {
                $output = sanitize_input($this->post($input, $xss_clean));
            }
        } else {
            $output = sanitize_input($this->post(NULL, $xss_clean));
        }

        return $output;
    }

}
