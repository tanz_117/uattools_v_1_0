<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Controller extends Core_Controller
{

    protected $_ignored_pages = array('main/login', 'main/logout');
    protected $_current_page;    
    protected $_total_notification = 0;

	public function __construct()
	{
		parent::__construct();
		$this->_current_page = $this->uri->segment(1, '') . '/' . $this->uri->segment(2, 'index');

		$this->lang->load('admin');
		$this->load->helper('pagination');
		$this->load->library('authentication');

		if (!self::_checkLogin()) {
			if (userdata('force_logout')) {
				$this->session->set_flashdata('warning', lang('auth.user_force_logged_out'));
				$this->session->unset_userdata('force_logout');
			} else {
				$this->session->set_flashdata('error', lang('auth.user_must_login'));
			}
			redirect('main/login');
			exit;
		}

		if (!self::_checkAccess()) {
			$this->session->set_flashdata('error', lang('auth.user_access_denied'));
			redirect('');
			exit;
		}

		$apps['current_page'] = $this->module ? $this->module : $this->uri->segment(1, '');
		$apps['app_name'] = config_item('application_name');
		$apps['default_theme'] = config_item('default_theme');

		$this->load->vars($apps);
		$this->asset->set_theme(config_item('default_theme'));
		$this->template->set_theme(config_item('default_theme'));
		//materializetemplate
		// $this->template
		// 	->append_styles(css('materialize.min.css', '_theme_', array('media' => 'screen,projection')))
		// 	->append_styles(css('style.min.css', '_theme_', array('media' => 'screen,projection')))
		// 	->append_styles(css('custom/custom.min.css', '_theme_', array('media' => 'screen,projection')))
		// 	->append_styles(css('plugins/perfect-scrollbar/perfect-scrollbar.css', '_theme_', array('media' => 'screen,projection')))
		// 	->append_styles(css('plugins/prism/prism.css', '_theme_', array('media' => 'screen,projection')))
		// 	->append_styles(css('materialize.clockpicker.css', '_theme_', array('media' => 'screen,projection')))
		// 	->append_styles(css('lib/animate-css/animate.css', '_theme_', array('media' => 'screen,projection')))
		// 	->append_styles(meta_plugins('timeline/style.css', '_theme_'))
		// 	->append_scripts(js('plugins/jquery-1.11.2.min.js', '_theme_'))
		// 	->append_scripts(js('materialize.min.js', '_theme_'))
		// 	->append_scripts(js('plugins/perfect-scrollbar/perfect-scrollbar.min.js', '_theme_'))
		// 	->append_scripts(js('plugins/sparkline/jquery.sparkline.min.js', '_theme_'))
		// 	->append_scripts(js('plugins/sparkline/sparkline-script.js', '_theme_'))
		// 	->append_scripts(js('jquery.form.min.js', '_theme_'))
		// 	->append_scripts(js('plugins/autoNumeric.js', '_theme_'))
		// 	->append_scripts(js('plugins/formatter/jquery.formatter.min.js', '_theme_'))
		// 	->append_scripts(js('plugins/jquery-mask/jquery.mask.js', '_theme_'))
		// 	->append_scripts(js('plugins.min.js', '_theme_'))
		// 	->append_scripts(js('custom-script.js', '_theme_'))
		// 	->append_scripts(js('plugins/prism/prism.js', '_theme_'))
		// 	->append_scripts(js('plugins/ckeditor/ckeditor.js', '_theme_'))
		// 	->append_scripts(js('plugins/ckeditor/build-config.js', '_theme_'))
		// 	->append_scripts(js('plugins/ckeditor/config.js', '_theme_'))
		// 	->append_scripts(js('plugins/ckeditor/styles.js', '_theme_'))
		// 	->append_scripts(js('materialize.clockpicker.js', '_theme_'))
		// 	->append_scripts(js('scripts.js', '_theme_'));
		// 	
		$this->template
			->append_styles(css('lib/bootstrap/css/bootstrap.min.css', '_theme_'))
			->append_styles(css('lib/font-awesome/font-awesome.min.css', '_theme_'))
			->append_styles(css('lib/ionicons.min.css', '_theme_'))
			->append_styles(css('lib/plugins/iCheck/all.css', '_theme_'))
			->append_styles(css('lib/plugins/select2/select2.min.css', '_theme_'))
			->append_styles(css('lib/dist/css/AdminLTE.min.css', '_theme_'))
			->append_styles(css('lib/plugins/datepicker/datepicker3.css', '_theme_'))

			// ->append_scripts(js('lib/Plugins/jquery.min.js', '_theme_'))
			->append_scripts(js('lib/Plugins/jQuery/jquery-2.2.3.min.js', '_theme_'))
			->append_scripts(js('lib/bootstrap/js/bootstrap.min.js', '_theme_'))
			->append_scripts(js('lib/Plugins/select2/select2.full.min.js', '_theme_'))
			->append_scripts(js('lib/Plugins/iCheck/icheck.min.js', '_theme_'))
			->append_scripts(js('scripts.js', '_theme_'))
			->append_scripts(js('promosi.js', '_theme_'))
			->append_scripts(js('jquery.form.min.js', '_theme_'))
			->append_scripts(js('lib/Plugins/datepicker/bootstrap-datepicker.js', '_theme_'))
			;

		if (in_array($this->_current_page, $this->_ignored_pages)) {

			$this->template
				// ->append_styles(css('layouts/page-center.css', '_theme_', array('media' => 'screen,projection')))
				->append_styles(css('lib/Plugins/iCheck/square/blue.css', '_theme_'))
				->set_layout('login.html');
		} else {
			$apps['total_notification'] = $this->_total_notification;
			$apps['list_notification'] = NULL;

			$this->load->vars($apps);
			$this->template
				// ->append_styles(meta_plugins('js/plugins/pace/pace.css', '_theme_'))
				// ->append_scripts(js('plugins/pace/pace.min.js', '_theme_', 'js'))
				->append_styles(css('lib/dist/css/skins/_all-skins.min.css', '_theme_'))
				->append_scripts(js('lib/dist/js/app.min.js', '_theme_'))
				->append_scripts(js('lib/dist/js/demo.js', '_theme_'))
				->append_scripts('<script type="text/javascript">var BASE_URL = " '. BASE_URL .'"; var CURRENT_URL = "'. current_url() .'"; var CURRENT = "' . $this->module . '"; var DELETE_NO_SELECTED = "'. lang('dialog.delete_no_selected') .'"; var CONFIRM_FORM = "'. lang('dialog.confirm_form') .'"; var CONFIRM_FORM_EDIT = "'. lang('dialog.confirm_form_edit') .'"; var CONFIRM_DELETE = "'. lang('dialog.confirm_delete') .'"; var CONFIRM_COMMON = "'. lang('dialog.confirm_common') .'"; var CONFIRM_DELETE_SELECTED = "'. lang('dialog.confirm_delete_selected') .'"; var CONFIRM_FORM_APPROVE = "'. lang('dialog.confirm_form_approve') .'"; var CONFIRM_FORM_REJECT = "'. lang('dialog.confirm_form_reject') .'"; var CONFIRM_ACTIVE_SELECTED = "'. lang('dialog.confirm_active_selected') .'"; var ACTIVE_NO_SELECTED = "'. lang('dialog.active_no_selected') .'"; var CONFIRM_REPLACE_DTF = "'. lang('dialog.confirm_replace_dtf') .'"; var CONFIRM_REPLACE_EXCEL = "'. lang('dialog.confirm_replace_excel') .'"; var CONFIRM_CANCEL_NOSTRO_EQ = "'. lang('dialog.confirm_cancel_nostro_eq') .'"; var CONFIRM_SAVE_NOSTRO_EQ = "'. lang('dialog.confirm_save_nostro_eq') .'";</script>')
				->set_layout('default.html')
				->set_partial('sidebar', config_item('default_theme') . '/partials/sidebar')
				->set_partial('notices', config_item('default_theme') . '/partials/notices')
				->set_partial('header', config_item('default_theme') . '/partials/header')
				->set_partial('footer', config_item('default_theme') . '/partials/footer')
				->set_partial('loader', config_item('default_theme') . '/partials/loader')
				->set_partial('page_header', config_item('default_theme') . '/partials/page_header')
				->set_partial('breadcrumbs', config_item('default_theme') . '/partials/breadcrumbs');
		}
	}

	private function _checkLogin() {			
		if (in_array($this->_current_page, $this->_ignored_pages)) {
			return TRUE;
		} elseif ($this->authentication->isLoggedIn()) {
			return TRUE;
		}
		return FALSE;
	}

	private function _checkAccess($access_menus = FALSE) {
		if (in_array($this->_current_page, array_merge($this->_ignored_pages, array('/index', 'main/index', 'checks/index')))) {
			return TRUE;
		}

		$access_menus = $access_menus ? $access_menus : json_decode(gzipDecompress(userdata('access_menus')));
		foreach ($access_menus as $access_menu) {
			if ($access_menu->menu_name === $this->module) {
				return TRUE;
			} elseif (count($access_menu->menu_childs) > 0) {
				if ($this->_checkAccess($access_menu->menu_childs)) {
					return TRUE;
				}
			}
		}
		return FALSE;
	}

}
