<?php  defined('BASEPATH') OR exit('No direct script access allowed');

$config['application_name'] = 'Pesanbelanjaan v1.0';
$config['default_theme'] = 'template_lab';
$config['visitor_theme'] = 'visitor';

$config['per_page_options']     = array(
    '5' => '5',
    '10' => '10',
    '20' => '20',
    '50' => '50',
    '100' => '100',
    '9999999' => 'All',
);

$config['per_page_options_tabular']     = array(
    '8' => '8',
    '16' => '16',
    '24' => '24',
    '32' => '32',
    '40' => '40',
    '9999999' => 'All',
);

$config['status_activeinactive'] = array('0' => array('key' => '1', 'value' => 'Active'), '1' => array('key' => '2', 'value'  => 'Inactive'));

$config['db_driver'] = 'sqlsrv';
$config['write_log_user'] = false;

$config['groupParametersId_jnsIdentitas'] = '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F';
$config['groupParametersId_statusanggota'] = '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6G';
$config['groupParametersId_paymentmethode'] = '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6I';
$config['MIN_POINT_ORDER'] = '26EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F';
$config['groupParametersId_pembayaranstatus'] = '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6J';
$config['groupParametersId_pesananstatus'] = '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6K';
