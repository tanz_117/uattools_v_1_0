<?php defined('BASEPATH') OR exit('No direct script access allowed');

if (! function_exists('create_module_folder')) {
    function create_module_folder($destination)
    {
        $dest = APPPATH . 'modules/' . $destination;
        mkdir($dest);
        xcopy('.' . config_item('asset_dir') . '/temp_modules', $dest);
        rename_associated_module($destination);
    }
}

if (! function_exists('rename_associated_module')) {
    function rename_associated_module($destination)
    {
        $controllerDir = APPPATH . 'modules/' . $destination . '/controllers';
        $modelDir = APPPATH . 'modules/' . $destination . '/models';
        rename($controllerDir . '/Controller.php' , $controllerDir . '/'. ucfirst($destination) .'.php');
        $modelName = substr($destination, 0, strlen($destination) - 1);
        rename($modelDir . '/Model.php' , $modelDir . '/'. ucfirst($modelName) .'_model.php');
    }
}

if (! function_exists('rename_module')) {
    function rename_module($newModule, $oldModule)
    {
        $module = APPPATH . 'modules/';
        if (is_dir($module . $oldModule) and file_exists($module . $oldModule)) {
            // rename folder first
            rename($module . $oldModule, $module . $newModule);
            // rename controller name
            $controller = $module . $newModule . 'controllers/';
            if (is_file($controller . $oldModule .'.php') and file_exists($controller . $oldModule .'.php')) {
                rename($controller . $oldModule .'.php', $controller . $newModule .'.php');
                // rename model name
                $model = $module . $newModule . 'models/';
                $oldModelName = substr($oldModule, 0, strlen($oldModule) - 1);
                if (is_file($model . $oldModelName . '_model.php') and file_exists($model . $oldModelName . '_model.php')) {
                    rename($model . $oldModelName . '_model.php', $model . $newModule . '_model.php');
                }
            }
        }
    }
}

if (! function_exists('xcopy')) {
    function xcopy($source, $destination, $permission = 0755)
    {
        if (is_link($source))
            return symlink(readlink($source), $destination);

        if (is_file($source))
            return copy($source, $destination);

        if (! is_dir($destination))
            mkdir($destination, $permission);

        $dir = dir($source);
        while (false !== $entry = $dir->read()) {
            if ($entry == '.' || $entry == '..')
                continue;

            xcopy("$source/$entry", "$destination/$entry", $permission);
        }

        $dir->close();

        return true;
    }
}
