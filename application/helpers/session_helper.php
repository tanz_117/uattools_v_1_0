<?php defined('BASEPATH') OR exit('No direct script access allowed');

if (! function_exists('userdata')) {
    function userdata($key)
    {
        $CI =& get_instance();
        return $CI->session->userdata($key);
    }
}

if (! function_exists('module_method_userdata')) {
    function module_method_userdata($key)
    {
    	$CI =& get_instance();
        $session_key = $CI->method == 'index' ? (array_key_exists($CI->module .'.'. $CI->method .'.'. $key, $CI->session->all_userdata()) ? $CI->module .'.'. $CI->method .'.'. $key : $CI->module .'.'. $key) : $CI->module .'.'. $CI->method .'.'. $key;

    	return $CI->session->userdata($session_key) !== NULL ? $CI->session->userdata($session_key) : FALSE;
    }
}
