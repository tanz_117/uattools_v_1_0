<?php defined('BASEPATH') OR exit('No direct script access allowed');

if (! function_exists('table_sorting')) {
    function table_sorting($field, $caption = '') {
        if (!$field) {
            return $caption;
        }

        $CI =& get_instance();
        $CI->load->helper('session');

        if (module_method_userdata('sort.field') == $field) {
            $dir = module_method_userdata('sort.dir') === 'ASC' ? '' : 'ASC';
            $class = module_method_userdata('sort.dir') === 'ASC' ? 'headerSortUp' : 'headerSortDown';
        } else {
            $dir = 'ASC';
            $class = '';
        }

        return '<a href="javascript:void(0)" name="' . $field . '" dir="' . $dir . '" class="' . $class . '">' . $caption . '</a>';
    }
}

if (! function_exists('table_per_page_options')) {
    function table_per_page_options() {
        $CI =& get_instance();
        $CI->load->helper('session');

        $sess = module_method_userdata('per_page');
        $per_page_options = config_item('per_page_options');

        return form_dropdown('per_page', $per_page_options, $sess !== FALSE ? $sess : key($per_page_options), 'id="per_page"');
    }
}

if (! function_exists('table_per_page_options_tabular')) {
    function table_per_page_options_tabular() {
        $CI =& get_instance();
        $CI->load->helper('session');

        $sess = module_method_userdata('per_page');
        $per_page_options = config_item('per_page_options_tabular');

        return form_dropdown('per_page', $per_page_options, $sess !== FALSE ? $sess : key($per_page_options), 'id="per_page"');
    }
}