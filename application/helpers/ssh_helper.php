<?php defined('BASEPATH') OR exit('No direct script access allowed');

if (! function_exists('check_running_service')) {
    function check_running_service($service)
    {
        $CI =& get_instance();

        $CI->load->model('simulators/simulator_model', '_simulatorModel');
        $row = $CI->_simulatorModel->selectByName(sanitize_input($service));

        $CI->load->library('SSH_Communication');
        $output = $CI->ssh_communication->run($row->simulator_ip, $row->simulator_ssh_port, $row->simulator_ssh_username, $row->simulator_ssh_password, 'cat /var/run/' . $service . '.pid');

        return intval($output) > 0 ? 'RUNNING' : 'STOP';
    }
}

if (! function_exists('maintain_service')) {
    function maintain_service($service, $command)
    {
        $CI =& get_instance();

        $CI->load->model('simulators/simulator_model', '_simulatorModel');
        $row = $CI->_simulatorModel->selectByName(sanitize_input($service));

        $CI->load->library('SSH_Communication');
        $output = $sOutput = '';
        $output = $CI->ssh_communication->run($row->simulator_ip, $row->simulator_ssh_port, $row->simulator_ssh_username, $row->simulator_ssh_password, 'service ' . $service . ' ' . $command);
        return $output;
        if (! empty($output)) {
          $output = explode('${,}', $output);
          $n = count($output);
          for ($i = 0; $i < $n; $i++) {$sOutput .= $output[$i] . '<br>';}
        } else {
          $sOutput = 'Unrecognize Service ' . $service;
        }

        return $sOutput;
    }
}
