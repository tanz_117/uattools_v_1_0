<?php defined('BASEPATH') OR exit('No direct script access allowed');

if (! function_exists('log_activity')) {
    function log_activity($after, $before = NULL, $id = NULL, $module = FALSE, $method = FALSE)
    {
        $CI =& get_instance();
        $CI->load->model('main_model', '_mainModel');
        $writeLog = config_item('write_log_user');

        $module = $module ? $module : $CI->module;
        $method = $method ? $method : $CI->method;

        if (is_array($id)) {
            $CI->load->model($id[0], $id[1]);
            $id = $CI->{$id[1]}->lastId();
        }
        
        $compare = set_before_after($after, $before);
        $input = set_audit_history(array('identifier' => $id), $module, $method, $compare);
            
        if ($writeLog):
            $CI->_mainModel->logUserActivity(sanitize_input($input));
        endif;
    }
}
