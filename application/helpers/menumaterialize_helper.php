<?php defined('BASEPATH') OR exit('No direct script access allowed');

if (! function_exists('build_menu')) {
    function build_menu($menuWrapper, $menuFormat, $access_menus = FALSE, $isChild = FALSE)
    {
        $CI =& get_instance();

        $isUseWrapper = $access_menus !== FALSE;
        $access_menus = $isUseWrapper ? $access_menus : json_decode(gzipDecompress(userdata('access_menus')));
        $current_url = base_url() . uri_string();

        $menuHTML = '';

        foreach ($access_menus as $access_menu):
            $menu = '';
            $nChilds = count($access_menu->menu_childs);
            if ($nChilds > 0):
                $menu = build_menu($menuWrapper, $menuFormat, $access_menu->menu_childs, TRUE);
                $isMenuActive = ($CI->module === $access_menu->menu_name || $current_url === $access_menu->menu_url) || strpos($menu, 'opened') !== FALSE;
                $menu = sprintf($menuFormat, $isMenuActive ? 'block' : 'none', $menu);
            endif;

            $isMenuActive = ($CI->module === $access_menu->menu_name || $current_url === $access_menu->menu_url) || strpos($menu, 'opened') !== FALSE;

            $name = '<span class="lbl">' . db_lang('page_title.' . $access_menu->menu_name) . '</span>';

            $openElement = $nChilds > 0 ? '<span>' : '<a href="' . $access_menu->menu_url . '">';
            $closeElement = $nChilds > 0 ? '</span>' : '</a>';
            $icon = !$isChild ? '<i class="font-icon '. (strpos($access_menu->menu_icon, 'glyphicon') !== false ? 'glyphicon ' : '') . $access_menu->menu_icon . '"></i>' : '';
            $menu = $openElement . $icon . $name . $closeElement  . $menu;

            $attr = NULL;
            $attr[] = count($access_menu->menu_childs) > 0 ? ' with-sub' : '';
            $attr[] = $isMenuActive ? 'opened' : '';
            $menuHTML .= sprintf($menuWrapper, ' class="blue'. implode(' ', $attr) .'"', $menu);
        endforeach;

        return $menuHTML;
    }
}

if (! function_exists('build_page_header')) {
    function build_page_header($format, $access_menus = FALSE)
    {
        $isUseWrapper = $access_menus !== FALSE;
        $access_menus = $isUseWrapper ? $access_menus : json_decode(gzipDecompress(userdata('access_menus')));
        $current_url = base_url() . uri_string();

        $page_header = '';
        foreach ($access_menus as $access_menu):
            if (! empty($access_menu->menu_childs)):
                $page_header = build_page_header($format, $access_menu->menu_childs);
            else:
                if ($access_menu->menu_url === $current_url):
                    $page_header = sprintf($format, $access_menu->menu_icon, db_lang('page_title.'.$access_menu->menu_name));
                endif;
            endif;
        endforeach;

        if (empty($page_header)) {$page_header = sprintf($format, 'fa-home', lang('dashboard'));}

        return $page_header;
    }
}

if (! function_exists('build_sidebar')) {
    function build_sidebar($menuWrapper, $menuFormat, $access_menus = FALSE, $isChild = FALSE, $parent_menu = array()) {
        $CI = & get_instance();

        $isUseWrapper = $access_menus !== FALSE;
        $access_menus = $isUseWrapper ? $access_menus :$CI->session->userdata('access_menus');
        // $access_menus = $isUseWrapper ? $access_menus : json_decode(gzipDecompress($CI->session->userdata('access_menus')));
        $current_url = base_url() . uri_string();

        $menuHTML = '';
        $parentActive = FALSE;
var_dump($access_menus);exit;
        foreach ($access_menus as $access_menu):
            $menu = '';

            $isMenuActive = ($CI->module === $access_menu->menu_name || $current_url === $access_menu->menu_url) || strpos($menu, 'opened') !== FALSE;
            if ($isMenuActive):
                $parentActive = $isMenuActive;
            endif;

            $title = $access_menu->menu_name;

            $menuChild = count($access_menu->menu_childs);

            if ($menuChild > 0):
                $menu = build_sidebar($menuWrapper, $menuFormat, $access_menu->menu_childs, TRUE, $access_menu);
            else:
    //            if ($isChild):
    //                $menu = '<a href="' . $access_menu->menu_url . '"> ' . db_lang('nav.title.' . $title) . '</a>';
    //            else:
                $menu = '<a href="' . $access_menu->menu_url . '" class="waves-effect waves-cyan" style="font-size:11px"><i class="' . $access_menu->menu_icon . '"></i> ' . db_lang('nav.title.' . $title) . '</a>';
    //            endif;
            endif;

            if ($isChild):
                $menuHTML .= sprintf($menuFormat, ' style="margin-left:-25px;"' . ($isMenuActive ? ' class="active"' : ''), $menu);
            else:
                $menuHTML .= sprintf($menuFormat, $menuChild > 0 ? ' class="no-padding"' : ' class="bold '.($isMenuActive ? 'active' : '').'"', $menu);
            endif;

        endforeach;

        if ($isChild):
            $childHTML = '<li class="bold ' . ($parentActive ? 'active' : '') . '">'
                    . '<a class="collapsible-header waves-effect waves-cyan ' . ($parentActive ? 'active' : '') . '" style="font-size:11px"><i class="' . $parent_menu->menu_icon . '"></i> ' . db_lang('nav.title.' . $parent_menu->menu_name) . '</a>'
                    . '<div class="collapsible-body">'
                    . '<ul>' . $menuHTML . '</ul>'
                    . '</div>'
                    . '</li>';
            return $isUseWrapper ? sprintf($menuWrapper, $childHTML) : $childHTML;
        else:
            return $isUseWrapper ? sprintf($menuWrapper, $menuHTML) : $menuHTML;
        endif;
    }
}

if (! function_exists('build_breadcrumbs')) {
    function build_breadcrumbs($access_menus = FALSE, &$arr_breadcrumbs = array(), $parent_menu = array()) {
        $CI = & get_instance();

        $isUseWrapper = $access_menus !== FALSE;
        $access_menus = $isUseWrapper ? $access_menus : json_decode(gzipDecompress($CI->session->userdata('access_menus')));
        $current_url = base_url() . uri_string();

        $arr_breadcrumbs[BASE_URI] = lang('dashboard');

        foreach ($access_menus as $access_menu):
            if (!empty($access_menu->menu_childs)):
                build_breadcrumbs($access_menu->menu_childs, $arr_breadcrumbs, $access_menu);
            else:
                if ($access_menu->menu_url === $current_url):
                    if(!empty($parent_menu)):
                        $arr_breadcrumbs[$parent_menu->menu_url] = db_lang('nav.title.' . $parent_menu->menu_name);
                    endif;

                    $arr_breadcrumbs[$access_menu->menu_url] = db_lang('nav.title.' . $access_menu->menu_name);
                endif;
            endif;
        endforeach;

        return $arr_breadcrumbs;
    }
}
