<?php defined('BASEPATH') OR exit('No direct script access allowed.');

if (! function_exists('create_pagination')) {
    function create_pagination($uri, $total_rows, $limit = NULL, $uri_segment = 0) {
        $CI = & get_instance();
        $CI->load->library('pagination');
        $CI->load->helper('session');

        $current_page = $uri_segment == 0 ? ($CI->input->post('current_page') ? $CI->input->post('current_page') : module_method_userdata('current_page')) : $CI->uri->segment($uri_segment, 0);

        if (in_array(config_item('db_driver'), array('sqlsrv', 'mssql'))) {
            $per_page = key(config_item('per_page_options'));
        }

        if (! is_numeric($CI->input->post('current_page')) || ! is_numeric(module_method_userdata('current_page'))) {
            $current_page = 0;
        }

        // Initialize pagination
        $config['base_url'] = site_url($uri);
        $config['cur_page'] = $current_page;
        $config['total_rows'] = $total_rows; // count all records
        $config['per_page'] = $limit === NULL ? (array_key_exists('per_page', $_POST) ? $CI->input->post('per_page') : (module_method_userdata('per_page') !== FALSE ? module_method_userdata('per_page') : key(config_item('per_page_options')))) : $limit;
        $config['uri_segment'] = $uri_segment;
        $config['page_query_string'] = FALSE;

        $config['num_links'] = 4;

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['first_link'] = '<i class="glyphicon glyphicon-fast-backward"></i>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';

        $config['prev_link'] = '<i class="glyphicon glyphicon-backward"></i>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="waves-effect">';
        $config['num_tag_close'] = '</li>';

        $config['next_link'] = '<i class="glyphicon glyphicon-forward"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['last_link'] = '<i class="glyphicon glyphicon-fast-forward"></i>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $CI->pagination->initialize($config); // initialize pagination

        return array(
            'current_page' => $current_page,
            'per_page' => $config['per_page'],
            'limit' => array($config['per_page'], $current_page),
            'links' => $CI->pagination->render_pagination_links()
        );
    }
}
