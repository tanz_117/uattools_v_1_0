<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class hoapdf{
	private $report_table = array();
	private $report_data = array();
	private $report_header = array();
	private $report_name = '';

	public function __construct(){
	}

	public function addTable($table = array()){
		$this->report_table = $table;
	}

	public function setDataTable($data = array()){
		$this->report_data = $data;
	}

	public function setReportName($name = ''){
		$this->report_name = $name;
	}

	public function addReportHeader($header = array()){
		$this->report_header = $header;
	}

	public function downloadReport(){
		$reportType = 'DATA ';
		$CI = & get_instance();
		$CI->template->set_layout(false);

		ini_set('max_execution_time', 3600);
		ini_set('max_input_time', 120);
		ini_set('memory_limit', '128M');

		$CI->load->helper('table');
		$CI->load->library('tcpdf');

		ob_clean();

		$pdf = new TCPDF('L', PDF_UNIT, array(215, 430), true, 'UTF-8', false);
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
		$pdf->setFontSubsetting(false);
		$pdf->SetFont('helvetica', '', 10);
		$pdf->SetCompression(true);
		$pdf->setPrintHeader(false);
		$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
			require_once dirname(__FILE__) . '/lang/eng.php';
			$pdf->setLanguageArray($l);
		}

		$pdf->AddPage();
		$pdf->SetFont('helvetica', 'N', 10);

		$pdf->SetXY(10, 8.5);
		$txt_report = '<table border="0" cellspacing="0" cellpadding="2" style="width:100%">
						<tr>
							<td style="width:195px;text-align:center;"><img src="./application/libraries/tcpdf/examples/images/logo_transparent.png" width="100" height="50" rel="stylesheet" /></td>
						</tr>
					</table>';
		$pdf->writeHTML($txt_report, true, false, true, false, '');
		$pdf->SetXY(170, 7.5);
		$txt_report = '<table border="0" cellspacing="0" cellpadding="2" style="width:100%">
						<tr>
							<td style="width:300px;text-align:center;vertical-align:middle"><b><span style="font-size:14px"><br />LAPORAN <br /> ' . db_lang("page_title." . $this->report_name) . ' <br /> Tanggal Cetak : ' . date("Y/m/d") . '</span></b> </td>
						</tr>
					</table>';
		$pdf->writeHTML($txt_report, true, false, true, false, '');

		$row_coordinate = 30;
		foreach ($this->report_header as $header) {
			$pdf->SetXY(10, $row_coordinate);
			$pdf->writeHTML($header['label'], true, false, true, false, '');

			$pdf->SetXY(50, $row_coordinate);
			$pdf->writeHTML(': '.$header['value'], true, false, true, false, '');
			$row_coordinate = $row_coordinate + 5;
		}

		$pdf->SetXY(10, $row_coordinate + 5);
		$columns = $this->report_table;

		$txt_report = '<table border="1" cellspacing="0" cellpadding="2" style="width:100%"><tr style="line-height:30px">';
		foreach ($columns as $item) {
			$txt_report .= '<td style="width:'.$item['width'].'%;text-align:'.$item['align'].'">'.$item['text'].'</td>';
		}
		$txt_report .= '</tr>';
		$report_data = $this->report_data;
		$count_data = count($this->report_data);

		for($i = 0;$i<$count_data;$i++){
			$txt_report .= '<tr>';
			foreach ($columns as $item) {
				$txt_report .= '<td style="width:'.$item['width'].'%;text-align:'.$item['align'].'">'.$report_data[$i][$item['name']].'</td>';
			}
			$txt_report .= '</tr>';
		}

		$txt_report .= '</table>';
		$pdf->writeHTML($txt_report, true, false, true, false, '');
		$filename = db_lang("page_title." . $this->report_name).'_'. date('YmdHis');
		$pdf->Output($filename . '.pdf', 'I');
		ob_flush();
	}
}

class hoapdf_table{
	private $return_table = array();

	public function __construct(){
	}

	public function addColumn($column = array()){
		array_push($this->return_table,$column);
	}

	public function getColumn(){
		$count_columns = count($this->return_table);
		$used_width = 0;
		$undefine_width = 0;

		foreach ($this->return_table as $item) {
			if(!$item['width']){
				++$undefine_width;
			}else{
				$used_width = $used_width + intval($item['width']);
			}
		}
		$default_width = (100-$used_width)/$undefine_width;

		for($i=0;$i<$count_columns;$i++){
			if(!$this->return_table[$i]['width']){
				$this->return_table[$i]['width'] = strval($default_width);
			}
		}

		return $this->return_table;
	}
}

class hoapdf_column{
	private $col_name = '';
	private $col_text = '';
	private $col_width = '';
	private $col_align = 'C';

	public function __construct(){
	}

	public function setColumnName($col_name){
		$this->col_name = $col_name;
	}

	public function setColumnWidth($col_width){
		$this->col_width = $col_width;
	}

	public function setColumnAlign($col_align){
		$this->col_align = $col_align;
	}

	public function getColumn(){
		return array(
				'name' => $this->col_name,
				'text' => db_lang($this->col_name),
				'width' => $this->col_width,
				'align' => ($this->col_align == 'C' ? 'center' : ($this->col_align == 'L' ? 'left' : 'right'))
			);
	}
}

class hoapdf_header{
	private $header_label = '';
	private $header_value ='';
	private $header_group = array();

	public function __construct(){
	}

	public function setHeaderLabel($header_label){
		$this->header_label = $header_label;
	}

	public function setHeaderValue($header_value){
		$this->header_value = $header_value;
	}

	public function appendHeaderRow(){
		array_push($this->header_group,array('label' => $this->header_label,'value' => $this->header_value));
	}

	public function addHeaderRow(){
		return $this->header_group;
	}
}