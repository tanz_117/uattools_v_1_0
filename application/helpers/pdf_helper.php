<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

function pdf_tabular($data = array()){
	$reportType = 'DATA ';

	$CI = & get_instance();
	$CI->template->set_layout(false);

	ini_set('max_execution_time', 3600);
	ini_set('max_input_time', 120);
	ini_set('memory_limit', '128M');

	$CI->load->helper('table');
	$CI->load->library('tcpdf');

	ob_clean();

	$pdf = new TCPDF('L', PDF_UNIT, array(215, 430), true, 'UTF-8', false);
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
	$pdf->setFontSubsetting(false);
	$pdf->SetFont('helvetica', '', 10);
	$pdf->SetCompression(true);
	$pdf->setPrintHeader(false);
	$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
		require_once dirname(__FILE__) . '/lang/eng.php';
		$pdf->setLanguageArray($l);
	}

	$pdf->AddPage();
	$pdf->SetFont('helvetica', 'N', 10);
	$pdf->SetXY(10, 10);
	$txt_report = '<table border="1" cellspacing="0" cellpadding="2" style="width:100%"><tr>';
	foreach ($data[0] as $key => $value) {
		$txt_report .= '<td>'.$key.'</td>';
	}
	$txt_report .= '</tr>';

	$i = 1;
	foreach ($data as $report) {
		$txt_report .= '<tr>';
		foreach ($report as $key => $value) {			
			$txt_report .= '<td>'.$value.'</td>';			
		}
		$txt_report .= '</tr>';
		$i++;
	}

	$txt_report .= '</table>';
	$pdf->writeHTML($txt_report, true, false, true, false, '');
	$filename = $reportType . date('YmdHis');
	$pdf->Output($filename . '.pdf', 'D');
	ob_flush();
}