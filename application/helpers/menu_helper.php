<?php defined('BASEPATH') OR exit('No direct script access allowed');

if (! function_exists('build_menu')) {
	function build_menu($menuWrapper, $menuFormat, $access_menus = FALSE, $isChild = FALSE)
	{
		$CI =& get_instance();
		$isUseWrapper = $access_menus !== FALSE;
		$access_menus = $isUseWrapper ? $access_menus : json_decode(userdata('access_menus'));
		$current_url = base_url() . uri_string();
		$menuHTML = '';

		foreach ($access_menus as $access_menu):
			$menu = '';
			$nChilds = count($access_menu->menu_childs);

			if ($nChilds > 0):
				$menu = build_menu($menuWrapper, $menuFormat, $access_menu->menu_childs, TRUE);
				$isMenuActive = ($CI->module === $access_menu->menu_name || $current_url === $access_menu->menu_url) || strpos($menu, 'opened') !== FALSE;
				$menu = sprintf($menuFormat, $isMenuActive ? 'block' : 'none', $menu);
			endif;

			$isMenuActive = ($CI->module === $access_menu->menu_name || $current_url === $access_menu->menu_url) || strpos($menu, 'opened') !== FALSE;

			$openName = '<span>';
			$closeName = '</span>';
			$name = $openName . db_lang('page_title.' . $access_menu->menu_name) . $closeName;
			
			$openElement = $nChilds > 0 ? '<a href="#" >' : '<a href="' . $access_menu->menu_url . '" >';
				$submenu_sign = $nChilds > 0 ? '<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>' : '';
			$closeElement = '</a>';
			
			$icon = !$isChild ? '<i class="'. (strpos($access_menu->menu_icon, 'glyphicon') !== false ? 'glyphicon ' : '') . $access_menu->menu_icon . '"></i>' : '';
			$menu = $openElement . $icon . $name. $submenu_sign. $closeElement  . $menu;
			$attr = NULL;
			
			$attr[] = $isMenuActive ? 'opened' : '';
			$menuHTML .= sprintf($menuWrapper, ' class="treeview'. implode(' ', $attr) .'"', $menu);
		endforeach;
// print_r($menuHTML);exit;
		return $menuHTML;
	}
}

	if (! function_exists('build_page_header')) {
		function build_page_header($format, $access_menus = FALSE,$init_build = 0)
		{
			$isUseWrapper = $access_menus !== FALSE;
			$access_menus = $isUseWrapper ? $access_menus : json_decode(gzipDecompress(userdata('access_menus')));
			$current_url = base_url() . uri_string();
			$page_header = '';

			foreach ($access_menus as $access_menu):
				if (! empty($access_menu->menu_childs)):
					$page_header .= build_page_header($format, $access_menu->menu_childs,1);
				else:
					if ($access_menu->menu_url === $current_url):
						$page_header .= sprintf($format, $access_menu->menu_icon, db_lang('page_title.'.$access_menu->menu_name));
					endif;
				endif;
			endforeach;

			if ($init_build == 0 && empty($page_header)) {$page_header = sprintf($format, 'glyphicon-home', lang('dashboard'));}

			return $page_header;
		}
	}

	if (! function_exists('build_breadcrumbs')) {
		function build_breadcrumbs($format, $access_menus = FALSE, $init_build = 0, $breadcrumbs = '', $parent_menu = array())
		{
			// var_dump($format);
			// var_dump($access_menus);
			// var_dump($init_build);
			// var_dump($breadcrumbs);
			// var_dump($parent_menu);exit;
			$isUseWrapper = $access_menus !== FALSE;
			$access_menus = $isUseWrapper ? $access_menus : json_decode(gzipDecompress(userdata('access_menus')));
			$current_url = base_url() . uri_string();
			$tempInit = '';

			if ($init_build == 0):
				$breadcrumbs = $tempInit = sprintf($format, '', '<a href="{apps:baseurl}main">'.config_item('application_name').'</a>');
			endif;
// var_dump($access_menus);
			foreach ($access_menus as $access_menu):
				if (! empty($access_menu->menu_childs)):
					$temp = build_breadcrumbs($format, $access_menu->menu_childs, 1, '', $access_menu);//var_dump($temp);var_dump($access_menu->menu_name);
					$breadcrumbs .= ! empty($temp) ? $temp : '';
				else:
					if ($access_menu->menu_url === $current_url):
						$breadcrumbs .= sprintf($format, '', '<a href="{apps:baseurl}main">'.lang('dashboard').'</a>');
						//var_dump($breadcrumbs);exit;
						if (! empty($parent_menu)):
							$breadcrumbs .= sprintf($format, '', '<a href="'.$parent_menu->menu_url.'">'.db_lang('page_title.'.$parent_menu->menu_name).'</a>');
						endif;
						$breadcrumbs .= sprintf($format, ' class="active"', db_lang('page_title.'.$access_menu->menu_name));
					endif;
				endif;
			endforeach;

			// 	var_dump(strcmp($breadcrumbs, $tempInit));
			// 	var_dump($tempInit);
			// if (strcmp($breadcrumbs, $tempInit) == 0):
			// 	$breadcrumbs .= sprintf($format, ' class="active"', lang('dashboard'));
			// endif;

			return $breadcrumbs;
		}
	}

	if (! function_exists('build_visitormenu')) {
		function build_visitormenu($menuWrapper, $menuFormat, $access_menus = FALSE, $isChild = FALSE)
		{
			$CI =& get_instance();
			$isUseWrapper = $access_menus !== FALSE;
			$access_menus = $isUseWrapper ? $access_menus : json_decode(userdata('access_menus'));
			$current_url = base_url() . uri_string();
			$menuHTML = '';

			foreach ($access_menus as $access_menu):

				$nChilds = count($access_menu->menu_childs);

				if ($nChilds > 0):
					$menuHTML .= '<li class="dropdown">';
					$menuHTML .= '<a href="#" class="dropdown-toggle text-olive" data-toggle="dropdown">'.db_lang('page_title.' . $access_menu->menu_name).'<span class="caret"></span></a>';
					$menuHTML .= '<ul class="dropdown-menu" role="menu">';
					foreach ($access_menu->menu_childs as $item) {
						$menuHTML .= '<li><a class="text-olive" href="'.$item->menu_url.'">'.db_lang('page_title.' . $item->menu_name).'</a></li>';
					}
					$menuHTML .= '</ul>';
					$menuHTML .= '</li>';
				else:
					$menuHTML .= '<li><a class="text-olive" href="'.$access_menu->menu_url.'">'.db_lang('page_title.' . $access_menu->menu_name).'</a></li>';
				endif;
			endforeach;
			return $menuHTML;
		}
	}

// if (! function_exists('get_module_caption')) {
//     function get_module_caption($caption = null)
//     {
//         return db_lang
//     }
// }
