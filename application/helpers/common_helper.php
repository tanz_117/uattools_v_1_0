<?php defined('BASEPATH') OR exit('No direct script access allowed');

if (! function_exists('stream_date')) {
    function stream_date()
    {
        $t = microtime(true);
        $u = sprintf("%06d",($t - floor($t)) * 1000000);
        return date('Y-m-d H:i:s.'. $u);
    }
}

if (! function_exists('set_datetime_format')) {
    function set_datetime_format($format, $datetime)
    {
        if (strpos($datetime, '/') !== false) {
            $datetime = str_replace('/', '-', $datetime);
        }
		
        $datetime = !empty($datetime) ? $datetime : '0000-00-00 00:00:00';
        return date($format, strtotime($datetime));
    }
}

if (! function_exists('set_datetime_format2')) {
    function set_datetime_format2($format, $datetime)
    {
		$datetime = str_replace(array('Mei', 'Agu', 'Okt', 'Nop', 'Des'), array('May', 'Aug', 'Oct', 'Nov', 'Dec'), $datetime);
		$date = date_create(substr($datetime, 0, 20));
		
		return date_format($date, $format);
    }
}

if (! function_exists('db_lang')) {
	function db_lang($line, $id = '')
	{
		$CI =& get_instance();
		$line = $CI->bahasa->line($line);

		if ($id != '') {
			$line = '<label for="'. $id .'">'. $line .'</label>';
		}

		return $line;
	}
}

if (!function_exists('gzipDecompress')) {
    function gzipDecompress($compressed)
    {
        $decompressed = gzinflate($compressed);

        if (false !== $decompressed)
            return $decompressed;

        $decompressed = gzuncompress($compressed);

        if (false !== $decompressed)
            return $decompressed;

        if (function_exists('gzdecode')) {
            $decompressed = gzdecode($compressed);

            if (false !== $decompressed)
                return $decompressed;
        }

        return $compressed;
    }
}

if (! function_exists('set_before_after')) {
    function set_before_after($after = NULL, $before = array())
    {
        if (!empty($before) && (is_object($before) || is_array($before))) {
            $before = (array) $before;
            foreach($before as $key => $val) {
                if (is_array($val) || is_object($val)) {
                    $val = (array) $val;
                    $x[strtolower($key)] = $val;
                } else {
                    $x[strtolower($key)] = $val;
                }
            }

            $before = $x;
        }

        if (isset($after) && !empty($before)) {
            $res = array();
            foreach ($after as $key => $val) {
                if (is_array($val) || is_object($val)) {
                    $val = (array) $val;
                    $x = array_diff_assoc($val, $before[$key]);
                    if (is_array($x)) {
                        $z = NULL;
                        foreach ($x as $a => $b) {
                            is_numeric($a) ? $z[] = $b : $z[$a] = $b;
                        }
                        $x = $z;
                    }
                    $res[$key] = !empty($x) ? $x : $after[$key];
                } else {
                    $res[$key] = isset($before[$key]) ? $val === $before[$key] ? $val : $after[$key] : $after[$key];
                }
            }
        } else {
            $res = $after;
        }

    	$json = json_encode(
    		array(
        		array('before' => $before),
        		array('after' => $res)
    		)
    	);

    	return $json;
    }
}

if (! function_exists('set_audit_history')) {
    function set_audit_history($post, $module, $method, $compare) {
        $ci =& get_instance(); 
        
        $post['userid'] = $ci->session->userdata('id');
        $post['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $post['browser'] = $_SERVER['HTTP_USER_AGENT'];
        $post['status'] = '0000';
        $post['method_name'] = $method;
        $post['module_name'] = $module;
        // $post['data'] = $compare;
        $post['historydetail'] = $compare;
                
        return $post;
    }
}

if (! function_exists('array_for_select')) {
    function array_for_select() {
        $args = func_get_args();

        $return = array();

        switch (count($args)) {
            case 5:
                foreach ($args[0] as $itteration):
                    if (is_object($itteration))
                        $itteration = (array) $itteration;
                    $return[$itteration[$args[1]]] = $args[3]($args[4] . $itteration[$args[2]]);
                endforeach;
                break;

            case 4:
                foreach ($args[0] as $itteration):
                    if (is_object($itteration))
                        $itteration = (array) $itteration;
                    $return[$itteration[$args[1]]] = $args[3]($itteration[$args[2]]);
                endforeach;
                break;

            case 3:
                foreach ($args[0] as $itteration):
                    if (is_object($itteration))
                        $itteration = (array) $itteration;
                    $return[$itteration[$args[1]]] = $itteration[$args[2]];
                endforeach;
                break;

            case 2:
                foreach ($args[0] as $key => $itteration):
                    if (is_object($itteration))
                        $itteration = (array) $itteration;
                    $return[$key] = $itteration[$args[1]];
                endforeach;
                break;

            case 1:
                foreach ($args[0] as $itteration):
                    $return[$itteration] = $itteration;
                endforeach;
                break;

            default:
                return FALSE;
        }

        return $return;
    }
}
