<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
* Code Igniter
*
* An open source application development framework for PHP 4.3.2 or newer
*
* @package		CodeIgniter
* @author		Rick Ellis
* @copyright	Copyright (c) 2006, pMachine, Inc.
* @license		http://www.codeignitor.com/user_guide/license.html
* @link			http://www.codeigniter.com
* @since        Version 1.0
* @filesource
*/
/*
----- BACA INI DULU WOI ----
harus ada :
href="#modal1" class="modal-trigger"
pada waktu di klik
*/

function confirm_modal_multipart($attribute = array()){
	$icon 		= isset($attribute['icon']) ? $attribute["icon"] : 'mdi-alert-error';
	$title 		= isset($attribute['title']) ? $attribute['title'] : lang('dialog.confirm_title');
	$content 	= isset($attribute['content']) ? $attribute['content'] : lang('dialog.confirm_form');
	$url 		= isset($attribute['url']) ? $attribute['url'] : 'javascript:void(0)';
	$rel 		= isset($attribute['rel']) ? $attribute['rel'] : 'ajax';

	echo 
	'<div id="modal1" class="modal animated swing" style="overflow:hidden; width:30%;">
		<div class="modal-content">
			<div class="col s12 m12 l12">
				<h1><i class="'.$icon.' cyan-text"></i>&nbsp;'.$title.'</h1>
			</div>
		</div>
		<div class="modal-content">
			<p>'.$content.'</p>
		</div>
		<div class="modal-footer">
			<a href="#!" class="btn waves-effect waves-red btn-flat modal-action modal-close">'.lang('dialog.no').'</a>
		          <button type="submit" class="btn waves-effect waves-green btn-flat modal-action modal-close">'.lang('dialog.yes').'</button>
		</div>
	</div>';
}

function confirm_modal($attribute = array()){
	$icon 		= isset($attribute['icon']) ? $attribute["icon"] : 'mdi-alert-error';
	$title 		= isset($attribute['title']) ? $attribute['title'] : lang('dialog.confirm_title');
	$content 	= isset($attribute['content']) ? $attribute['content'] : lang('dialog.confirm_form');
	$url 		= isset($attribute['url']) ? $attribute['url'] : 'javascript:void(0)';
	$rel 		= isset($attribute['rel']) ? $attribute['rel'] : 'ajax';

	echo 
	'<div id="modal1" class="modal animated swing" style="overflow:hidden; width:30%;">
		<div class="modal-content">
			<div class="col s12 m12 l12">
				<h1><i class="'.$icon.' cyan-text"></i>&nbsp;'.$title.'</h1>
			</div>
		</div>
		<div class="modal-content">
			<p>'.$content.'</p>
		</div>
		<div class="modal-footer">
			<a href="#!" class="btn waves-effect waves-red btn-flat modal-action modal-close">'.lang('dialog.no').'</a>
			<a href="'.$url.'" rel="'.$rel.'" class="btn waves-effect waves-green btn-flat modal-action modal-close">'.lang('dialog.yes').'</a>
		</div>
	</div>';
}

function custom_confirm_modal(){
	echo 
	'<div id="modal2" class="modal animated swing" style="overflow:hidden; width:40%;">
		<div class="modal-content" style="padding-bottom:0">
			<div class="row">			
				<div class="col s2 m2 l2 valign" >
					<i id="modal-icon" style="margin-top:-15px"></i>
				</div>
				<div class="col s10 m10 l10">				
					<div class="row">
						<div class="col s12 m12 l12">
							<p><h5 id="modal-title"></h5></p>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m12 l12">
							<p id="modal-message"></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<a href="#!" class="btn waves-effect waves-red btn-flat modal-action modal-close">'.lang('dialog.no').'</a>
			<a id="yes-button" class="btn waves-effect waves-green btn-flat modal-action modal-close">'.lang('dialog.yes').'</a>
		</div>	
	</div>';
}

function alert_modal($attribute = array()){
	$title 		= isset($attribute['title']) ? $attribute['title'] : lang('dialog.confirm_title');
	$content 	= isset($attribute['content']) ? $attribute['content'] : lang('dialog.confirm_form');

	echo 
	'<div id="alert_modal" class="modal animated swing" style="overflow:hidden; width:40%;">
		<div class="modal-content" style="padding-bottom:0">
			<div class="row">			
				<div class="col s2 m2 l2 valign" >
					<i id="alert-icon" style="margin-top:-15px"></i>
				</div>
				<div class="col s10 m10 l10">				
					<div class="row">
						<div class="col s12 m12 l12">
							<p><h5 id="alert-title">'.$title.'</h5></p>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m12 l12">
							<p id="alert-message">'.$content.'</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<a class="waves-effect waves-red btn-flat modal-action modal-close">' . lang('dialog.close') . '</a>
		</div>	
	</div>';
}

function confirm_modal_2($judul='Konfirmasi',$id='modal1',$url = '#',$content = '',$class_a = '',$rel = 'ajax',$background = 'green',$text = 'white-text')
{
	echo'
	<div id="'.$id.'" class="modal" style="overflow:hidden; width:30%;">
		<ul id="task-card" class="collection with-header" style="margin:-1px;">
			<li class="collection-header '.$background.' '.$text.'">
				<h5 class="task-card-title">'.$judul.'</h5>
			</li>
			<li class="collection-item dismissable" style="min-height:100px; max-height:340px;">
				<p >'.$content.'</p>
			</li>
			<div class="modal-footer green lighten-4">
				<a href="#!" class="waves-effect waves-red btn-flat modal-action modal-close">'.lang('konfirm.modal.batal').'</a>
				<a href="'.$url.'" rel="'.$rel.'" class="btn waves-effect waves-green btn-flat modal-action modal-close '.$class_a.'">'.lang('konfirm.modal.ok').'</a>
			</div>
		</div>
		';
}
	function confirm_modal_submit($judul='Konfirmasi',$id='modal1',$content = '',$class_a = '',$rel = 'ajax',$background = 'green',$text = 'white-text')
	{
	echo'
	<div id="'.$id.'" class="modal" style="overflow:hidden; width:30%;">
		<ul id="task-card" class="collection with-header" style="margin:-1px;">
			<li class="collection-header '.$background.' '.$text.'">
				<h5 class="task-card-title">'.$judul.'</h5>
			</li>
			<li class="collection-item dismissable" style="min-height:100px; max-height:340px;">
				<p >'.$content.'</p>
			</li>
			<div class="modal-footer green lighten-4">
				<a href="#!" class="waves-effect waves-red btn-flat modal-action modal-close">'.lang('konfirm.modal.batal').'</a>
				<button type="submit" class="btn waves-effect waves-green btn-flat modal-action modal-close '.$class_a.'">
				<i class="mdi-content-send"></i> '.lang('konfirm.modal.ok').'
				</button>
			</div>
		</div>
		';
		}
		// function alert_modal($judul = 'Konfirmasi', $id = 'modal1', $url = '#', $content = '', $class_a = '', $rel = 'ajax', $background = 'green', $text = 'white-text') {
		// echo'
		// <div id="' . $id . '" class="modal" style="overflow:hidden; width:30%;">
		// 	<ul id="task-card" class="collection with-header" style="margin:-1px;">
		// 		<li class="collection-header ' . $background . ' ' . $text . '">
		// 			<h5 class="task-card-title">' . $judul . '</h5>
		// 		</li>
		// 		<li class="collection-item dismissable" style="min-height:100px; max-height:340px;">
		// 			<p >' . $content . '</p>
		// 		</li>
		// 		<div class="modal-footer green lighten-4">
		// 			<a href="#!" class="waves-effect waves-red btn-flat modal-action modal-close">' . lang('konfirm.modal.batal') . '</a>
		// 		</div>
		// 	</div>
		// 	';
		// 	}