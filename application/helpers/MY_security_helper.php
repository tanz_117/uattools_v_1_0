<?php defined('BASEPATH') OR exit('No direct script access allowed');

if (! function_exists('sanitize_input')) {
    function sanitize_input($input = NULL)
    {
        $output = NULL;
        if (! empty($input)) {
            if (is_array($input)) {
                foreach($input as $key => $value) {
                    if (is_array($value)) {$output[$key] = sanitize_input($value);}
                    else {$output[$key] = clean_input($value);}
                }
            } else {
                $output = clean_input($input);
            }
        }

        return $output;
    }
}

if (! function_exists('clean_input')) {
    function clean_input($input)
    {
        // built-in function
        $input = trim($input);
        $input = stripslashes($input);
        $input = htmlspecialchars($input, ENT_QUOTES /*| ENT_HTML401 | ENT_XML1 | ENT_XHTML | ENT_HTML5*/);
        $input = htmlentities($input, ENT_QUOTES /*| ENT_HTML401 | ENT_XML1 | ENT_XHTML | ENT_HTML5*/);
        // CI function
        $input = xss_clean($input);
        $input = strip_image_tags($input);

        // if (is_int($input) || is_numeric($input) || filter_var($input, FILTER_VALIDATE_IP))
        if (is_int($input) || is_numeric($input))
            $input = filter_var($input, FILTER_SANITIZE_NUMBER_INT);
        if (is_float($input) || is_double($input))
            $input = filter_var($input, FILTER_SANITIZE_NUMBER_FLOAT);
        if (filter_var($input, FILTER_VALIDATE_EMAIL))
            $input = filter_var($input, FILTER_SANITIZE_EMAIL);
        if (is_string($input)) {
            if (filter_var($input, FILTER_VALIDATE_URL))
                $input = filter_var($input, FILTER_SANITIZE_URL);
            $input = filter_var($input, FILTER_SANITIZE_STRING);
        }

        return $input;
    }
}

if (! function_exists('html_decode')) {
    function html_decode($input)
    {
        return html_entity_decode($input, ENT_QUOTES /*| ENT_HTML401 | ENT_XML1 | ENT_XHTML | ENT_HTML5*/);
    }
}
