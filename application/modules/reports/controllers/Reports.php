<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Reports extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->module . '/Report_model', '_reportModel');
		$this->load->helper('modal');
		$this->load->library('form_validation');

		$this->validation_rules = array(
			array(
				'field' => 'bank_nama',
				'label' => db_lang('bank_nama'),
				'rules' => 'required|trim'
			)
		);

		$this->form_validation->set_rules($this->validation_rules);
		// $this->template->append_scripts(js($this->module . '.js', $this->module));
		$this->_is_ajax = $this->is_ajax() || $this->input->post('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest';
		$this->_is_ajax ? $this->template->set_layout(FALSE) : '';
	}

	public function index()
	{
		$this->load->helper('table');
		$ref_array = explode('/', array_key_exists('HTTP_REFERER', $_SERVER) ? $_SERVER['HTTP_REFERER'] : '');
		if (!in_array($this->module, $ref_array)) {
			$this->session->set_userdata($this->module . '.sort.field', '');
			$this->session->set_userdata($this->module . '.sort.dir', '');
			$this->session->set_userdata($this->module . '.current_page', '');
			$this->session->set_userdata($this->module . '.bank_nama', '');
		}

		$baseCondition = $condition = array();
		if ($this->input->post()) {
			$post = $this->input->post_sanitized();
			foreach($post as $key => $val) {
				if (!in_array($key, array('sortfield', 'sortdir', 'current_page', 'per_page'))){
					$this->session->set_userdata($this->module . '.search.' . $key, $val);
					$condition = $condition + array($key => userdata($this->module . '.search.'.$key));
				}
			}
			$baseCondition = $baseCondition + array('like' => $condition);
		}

		$base_order = array();
		if ($this->input->post('sortfield')) {
			$this->session->set_userdata($this->module . '.sort.field', $this->input->post('sortfield'));
			$this->session->set_userdata($this->module . '.sort.dir', $this->input->post('sortdir'));
		}

        		$base_order = $this->session->userdata($this->module . '.sort.field') ? $base_order + array(trim($this->session->userdata($this->module . '.sort.field') . ' ' . $this->session->userdata($this->module . '.sort.dir'))) : $base_order;

		if (array_key_exists('current_page', $_POST)) {
			// set paging by user
			$this->session->set_userdata($this->module . '.current_page', $this->input->post('current_page'));
		}

		if (array_key_exists('per_page', $_POST)) {
			$this->session->set_userdata($this->module . '.' . $this->method . '.per_page', $this->input->post('per_page'));
		}

		// $pagination = create_pagination($this->module, $this->_reportModel->count($baseCondition));
		
		if ($this->input->post('sortfield')) {
			$this->session->set_userdata($this->module . '.sort.field', $this->input->post_sanitized('sortfield'));
			$this->session->set_userdata($this->module . '.sort.dir', $this->input->post_sanitized('sortdir'));
			$baseCondition = $this->session->set_userdata($this->module . '.sort.field', $this->input->post('sortfield')) ? $baseCondition + array('order_by' => array(userdata($this->module . '.sort.field'), userdata($this->module . '.sort.dir'))) : $baseCondition;
		}

		// $datas = $this->_reportModel->rows(array(
		// 	'limit' => $pagination['limit']
		// ) + $baseCondition);
		$jnsReport = array('1' => lang('select.laptrxpesanans'),'2' => lang('select.laptrxdetailpesanans'));

		$this->template
			// ->set('pagination', $pagination)
			// ->set('datas', $datas)
			->set('jnsReport',$jnsReport)
			->title(db_lang('page_title.' . $this->module))
			->build('default', array());
	}

	public function pdf()
	{	
		$allowPdf = false; 
		$parameter = array();
		if($_POST['jns_report'] == '1'){
			$report_name = 'Laporan_Pemesanan';
			$this->load->model('parameters/Parameter_model', '_parameterModel');
			$min_point_order =  $this->_parameterModel->row(array('where' => array('configuration_id' => config_item('MIN_POINT_ORDER'))));

			$allowPdf = ($_POST['tgl_awal'] != '' && $_POST['tgl_akhir'] != '') ? true : false;

			if($allowPdf){
				$parameter = array('where' => array('trxpesanan_tgl >=' => $_POST['tgl_awal'].' 00:00:00','trxpesanan_tgl <=' => $_POST['tgl_akhir'].' 23:59:59'));
				$reports = $this->_reportModel->rows(array(
					'join' => array(
							array('anggota b','trxpesanan.trxpesanan_userid = b.user_id'),
							array('area c','b.anggota_areaid = c.area_id','LEFT')
						),
				) + $parameter);

				$this->load->model('trxpesanan_details/Trxpesanandetail_model', '_trxpesanandetail_model');

				$counter = 0;
				foreach ($reports as $report) {
					$data_summary = $this->_trxpesanandetail_model->row(array(
						'select' => 'SUM(b.produk_harga * trxpesanan_qty) AS totalbayar,(SUM(FLOOR(trxpesanan_qty/b.produk_pointorder)))AS totalpointorder',
						'join' => array('produk b','trxpesanan_detail.trxpesanan_produkid = b.produk_id','LEFT'),
						'where' => array('trxpesanan_id' =>  $report->trxpesanan_id)
					));
					$reports[$counter]->totalbayar = $data_summary->totalbayar;
					$reports[$counter]->totalpointorder = $data_summary->totalpointorder;
					$reports[$counter]->area_biayakirim = ($data_summary->totalpointorder >= $min_point_order->configuration_value) ? '0' : $reports[$counter]->area_biayakirim;
					$counter++;
				}
			}

		}else if($_POST['jns_report'] == '2'){
			$report_name = 'Laporan_Detail_Pemesanan';
			$allowPdf = ($_POST['trxpesanan_invoiceid'] != '') ? true : false;

			$this->load->model('parameters/Parameter_model', '_parameterModel');
			$min_point_order =  $this->_parameterModel->row(array('where' => array('configuration_id' => config_item('MIN_POINT_ORDER'))));


			if($allowPdf){
				$header_data = $this->_reportModel->row(array(
					'join' => array(
							array('anggota b','trxpesanan.trxpesanan_userid = b.user_id'),
							array('area c','b.anggota_areaid = c.area_id')
						),
					'where' => array('trxpesanan_invoiceid' => $_POST['trxpesanan_invoiceid'])
				));

				$trxpesanan = $this->_reportModel->row(array('where' => array('trxpesanan_invoiceid' => $_POST['trxpesanan_invoiceid'])));
				$this->load->model('trxpesanan_details/Trxpesanandetail_model', '_trxpesanandetail_model');
				$reports = $this->_trxpesanandetail_model->rows(
					array(
						'select' => 'trxpesanan_id,trxpesanan_produkid,produk_id,produk_identity,produk_nama,trxpesanan_qty,produk_harga,(produk_harga * trxpesanan_qty) as subtotal',
						'join' => array('produk','trxpesanan_detail.trxpesanan_produkid = produk.produk_id'),
						'where' => array('trxpesanan_id' => $trxpesanan->trxpesanan_id)
					)
				);

				$data_summary = $this->_trxpesanandetail_model->row(array(
					'select' => 'SUM(b.produk_harga * trxpesanan_qty) AS totalbayar,(SUM(FLOOR(trxpesanan_qty/b.produk_pointorder)))AS totalpointorder',
					'join' => array('produk b','trxpesanan_detail.trxpesanan_produkid = b.produk_id','LEFT'),
					'where' => array('trxpesanan_id' =>  $trxpesanan->trxpesanan_id)
				));

			}
		}

		if($allowPdf){			
			$this->load->helper('hoapdf');
			$table = new hoapdf_table();

			if($_POST['jns_report'] == '1'){
				$column = new hoapdf_column();
				$column->setColumnName(lang('lbl_number'));
				$column->setColumnWidth('5');
				$table->addColumn($column->getColumn());

				$column = new hoapdf_column();
				$column->setColumnName(db_lang('trxpesanan_invoiceid'));
				$table->addColumn($column->getColumn());

				$column = new hoapdf_column();
				$column->setColumnName(db_lang('trxpesanan_tgl'));
				$table->addColumn($column->getColumn());

				$column = new hoapdf_column();
				$column->setColumnName(db_lang('anggota_nama'));
				$table->addColumn($column->getColumn());

				$column = new hoapdf_column();
				$column->setColumnName(db_lang('area_biayakirim'));
				$table->addColumn($column->getColumn());

				$column = new hoapdf_column();
				$column->setColumnName(db_lang('totalbayar'));
				$table->addColumn($column->getColumn());

				$column = new hoapdf_column();
				$column->setColumnName(db_lang('totalpointorder'));
				$table->addColumn($column->getColumn());

				$column = new hoapdf_column();
				$column->setColumnName(db_lang('grandtotal'));
				$table->addColumn($column->getColumn());
			}else if($_POST['jns_report'] == '2'){
				$column = new hoapdf_column();
				$column->setColumnName(lang('lbl_number'));
				$column->setColumnWidth('5');
				$table->addColumn($column->getColumn());

				$column = new hoapdf_column();
				$column->setColumnName(db_lang('produk_identity'));
				$table->addColumn($column->getColumn());

				$column = new hoapdf_column();
				$column->setColumnName(db_lang('produk_nama'));
				$table->addColumn($column->getColumn());

				$column = new hoapdf_column();
				$column->setColumnName(db_lang('trxpesanan_qty'));
				$table->addColumn($column->getColumn());

				$column = new hoapdf_column();
				$column->setColumnName(db_lang('produk_harga'));
				$table->addColumn($column->getColumn());

				$column = new hoapdf_column();
				$column->setColumnName(db_lang('subtotal'));
				$table->addColumn($column->getColumn());
			}

			// $reports = $this->_reportModel->rows($parameter);
			$param = array();
			$i = 1;
			if($_POST['jns_report'] == '1'){
				foreach ($reports as $report) {
					$item = array(
						lang('lbl_number') => $i,
						db_lang('trxpesanan_invoiceid') => $report->trxpesanan_invoiceid,
						db_lang('trxpesanan_tgl') => $report->trxpesanan_tgl,
						db_lang('anggota_nama') => $report->anggota_nama,
						db_lang('area_biayakirim') => 'Rp. '.number_format($report->area_biayakirim),
						db_lang('totalbayar') => 'Rp. '.number_format($report->totalbayar),
						db_lang('totalpointorder') => $report->totalpointorder,
						db_lang('grandtotal') => 'Rp. '.number_format($report->totalbayar + $report->area_biayakirim)
					);
				    $i++;
				    array_push($param, $item);
				}
			}else if($_POST['jns_report'] == '2'){
				foreach ($reports as $report) {
					$item = array(
						lang('lbl_number') => $i,
						db_lang('produk_identity') => $report->produk_identity,
						db_lang('produk_nama') => $report->produk_nama,
						db_lang('trxpesanan_qty') => $report->trxpesanan_qty,
						db_lang('produk_harga') => 'Rp. '.number_format($report->produk_harga),
						db_lang('subtotal') => 'Rp. '.number_format($report->subtotal)
					);
				    $i++;
				    array_push($param, $item);
				}
			}

			$hoapdf = new hoapdf();
			if($_POST['jns_report'] == '1'){
				$header = new hoapdf_header();
				$header->setHeaderLabel(db_lang('tgl_awal'));
				$header->setHeaderValue($_POST['tgl_awal']);
				$header->appendHeaderRow();

				$header->setHeaderLabel(db_lang('tgl_akhir'));
				$header->setHeaderValue($_POST['tgl_akhir']);
				$header->appendHeaderRow();

				$hoapdf->addReportHeader($header->addHeaderRow());

			}else if($_POST['jns_report'] == '2'){
				$header = new hoapdf_header();
				$header->setHeaderLabel(db_lang('trxpesanan_invoiceid'));
				$header->setHeaderValue($header_data->trxpesanan_invoiceid);
				$header->appendHeaderRow();

				$header->setHeaderLabel(db_lang('trxpesanan_tgl'));
				$header->setHeaderValue($header_data->trxpesanan_tgl);
				$header->appendHeaderRow();

				$header->setHeaderLabel(db_lang('anggota_nama'));
				$header->setHeaderValue($header_data->anggota_nama);
				$header->appendHeaderRow();

				$header->setHeaderLabel(db_lang('anggota_alamat'));
				$header->setHeaderValue($header_data->anggota_alamat);
				$header->appendHeaderRow();

				$header->setHeaderLabel(db_lang('totalbayar'));
				$header->setHeaderValue('Rp. '.number_format($data_summary->totalbayar));
				$header->appendHeaderRow();

				$header->setHeaderLabel(db_lang('totalpointorder'));
				$header->setHeaderValue($data_summary->totalpointorder);
				$header->appendHeaderRow();

				$header->setHeaderLabel(db_lang('area_biayakirim'));
				$header->setHeaderValue($header_data->area_nama.' - '.'Rp. '.number_format($header_data->area_biayakirim));
				$header->appendHeaderRow();

				$header->setHeaderLabel(db_lang('grandtotal'));
				$header->setHeaderValue('Rp. '.number_format($data_summary->totalbayar + (($data_summary->totalpointorder >= $min_point_order->configuration_value) ? '0' : $reports[$counter]->area_biayakirim)));
				$header->appendHeaderRow();

				$hoapdf->addReportHeader($header->addHeaderRow());
			}

			$hoapdf->addTable($table->getColumn());
			$hoapdf->setDataTable($param);
			$hoapdf->setReportName($report_name);
			$hoapdf->downloadReport();
		}else{
			$this->session->set_flashdata('notice', lang('parameter_report_incomplete'));
			redirect($this->module);
		}
	}
}