<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Report_model extends Base_Model
{
	protected $_table = 'trxpesanan';
	protected $_pk = 'trxpesanan_id';

	public function __construct()
	{
		parent::__construct();
	}

	public function insert($data)
	{
		$result = parent::query("CALL sp_bank_insert('{$data['bank_nama']}','{$data['userid']}','{$data['ip_address']}','{$data['browser']}','{$data['status']}','{$data['method_name']}','{$data['module_name']}','{$data['historydetail']}')");
		
		return $result[0]['jml'] == 1 ? true : false;
	}

	public function update($data)
	{
		$result = parent::query("CALL sp_bank_update('{$data['bank_id']}','{$data['bank_nama']}','{$data['userid']}','{$data['ip_address']}','{$data['browser']}','{$data['status']}','{$data['method_name']}','{$data['module_name']}','{$data['historydetail']}')");
		
		return $result[0]['jml'] == 1 ? true : false;
	}
}