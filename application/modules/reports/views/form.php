<div class="box-typical box-typical-padding">
	<div class="box box-solid">
		<div class="box-header bg-maroon with-border">
			<h3 class="m-t-lg with-border"><?php echo lang('label.' . $this->method) . ' ' . db_lang('page_title.' . $this->module) ?></h3>
		</div>
		<?php echo form_open_multipart(uri_string(), 'id="form_upload" class="crud_upload" autocomplete="off" ') ?>
		<div class="box-body">
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('bank_nama') ?></label>
				<div class="col-sm-9">
					<?php 
						if($this->method == 'edit'){
							echo form_hidden('bank_id', $row->bank_id, 'class="form-control"');
						}
						echo form_input('bank_nama', $row->bank_nama, 'class="form-control" placeholder="'. db_lang('bank_nama') .'"') 
					?>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-sm-9 col-sm-push-2">
					<button type="submit" class="btn bg-olive btn-flat submit"><i class="glyphicon glyphicon-ok"></i>&nbsp;<?php echo lang('buttons.save') ?></button>
					<button class="btn bg-maroon btn-flat cancel"><i class="glyphicon glyphicon-remove"></i>&nbsp;<?php echo lang('buttons.cancel') ?></button>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close() ?>
</div>