<?php $column_names = array('bank_nama'); ?>
<div id="content">
	<div class="box box-solid">
		<div class="box-header bg-olive disabled color-palette">
			<h3 class="box-title"><?php echo db_lang('search') ?></h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Expand" style="color:white">
				<i class="fa fa-minus"></i>
				</button>
			</div>
		</div>
		<?php echo form_open(base_url($this->module.'/pdf'), 'id="form_data" target="_blank" autocomplete="off"') ?>
		<div class="box-body">
			<div class="row">
				<div class="col-sm-12">
					<fieldset class="form-group">
						<label class="form-label semibold"><?php echo db_lang('jns_report') ?></label>
						<?php 
							$items = array('' => lang('select.pick')) + $jnsReport;
							echo form_dropdown('jns_report', $items, $row->jns_report, 'id="jns_report" onChange="jnsReportOnChange()" class="form-control"'); 
						?>
					</fieldset>
				</div>
				<div class="col-sm-6">
					<fieldset class="form-group">
						<label class="form-label semibold"><?php echo db_lang('tgl_awal') ?></label>
						<?php echo form_input('tgl_awal', $row->tgl_awal, 'id="tgl_awal" class="form-control datepicker" placeholder="'. db_lang('tgl_awal') .' " disabled') ?>
					</fieldset>
				</div>
				<div class="col-sm-6">
					<fieldset class="form-group">
						<label class="form-label semibold"><?php echo db_lang('tgl_akhir') ?></label>
						<?php echo form_input('tgl_akhir', $row->tgl_akhir, 'id="tgl_akhir" class="form-control datepicker" placeholder="'. db_lang('tgl_akhir') .' " disabled') ?>
					</fieldset>
				</div>
				<div class="col-sm-12">
					<fieldset class="form-group">
						<label class="form-label semibold"><?php echo db_lang('trxpesanan_invoiceid') ?></label>
						<?php echo form_input('trxpesanan_invoiceid', $row->trxpesanan_invoiceid, 'id="trxpesanan_invoiceid" class="form-control" placeholder="'. db_lang('trxpesanan_invoiceid') .' " disabled') ?>
					</fieldset>
				</div>
				<div class="col-sm-12">
					<div class="btn-group pull-right">
						<button type="submit" class="btn bg-maroon color-palette btn-flat" style="margin-right: 3px"><i class="glyphicon glyphicon-search"></i>&nbsp;<?php echo lang('buttons.search') ?></button>
						<button class="btn bg-maroon-active color-palette btn-flat" id="reset"><i class="glyphicon glyphicon-repeat"></i>&nbsp;<?php echo lang('buttons.reset') ?></button>
					</div>
				</div>
			</div>
		</div>
		<?php echo form_close() ?>
	</div>
</div>
<script type="text/javascript">
	function jnsReportOnChange(){
		if($("#jns_report").val() == 1){
			$("#tgl_awal").prop("disabled", false);
			$("#tgl_akhir").prop("disabled", false);
			$("#trxpesanan_invoiceid").attr('disabled','disabled');
		}else if($("#jns_report").val() == 2){
			$("#tgl_awal").attr('disabled','disabled');
			$("#tgl_akhir").attr('disabled','disabled');
			$("#trxpesanan_invoiceid").prop("disabled", false);
		}
	};
</script>