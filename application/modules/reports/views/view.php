<div class="box-typical box-typical-padding">
	<div class="box box-primary box-solid">
		<div class="box-header with-border">
			<h3 class="m-t-lg with-border"><?php echo lang('label.' . $this->method) . ' ' . db_lang('page_title.' . $this->module) ?></h3>
		</div>
		<?php echo form_open(uri_string(), 'id="form_data" class="crud" autocomplete="off"') ?>
		<div class="box-body">
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('produk_identity') ?></label>
				<div class="col-sm-9">
					<?php echo form_input('produk_identity', $row->produk_identity, 'class="form-control" placeholder="'. db_lang('produk_identity') .'" disabled') ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('produk_nama') ?></label>
				<div class="col-sm-9">
					<?php echo form_input('produk_nama', $row->produk_nama, 'class="form-control" placeholder="'. db_lang('produk_nama') .'" disabled') ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('produk_harga') ?></label>
				<div class="col-sm-9">
					<?php echo form_input('produk_harga', $row->produk_harga, 'class="form-control" placeholder="'. db_lang('produk_harga') .'" disabled') ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('produk_stok') ?></label>
				<div class="col-sm-9">
					<?php echo form_input('produk_stok', $row->produk_stok, 'class="form-control" placeholder="'. db_lang('produk_stok') .'" disabled') ?>
				</div>
			</div>

			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('produk_satuan') ?></label>
				<div class="col-sm-9">
					<?php echo form_input('produk_satuan', $row->produk_satuan, 'class="form-control" placeholder="'. db_lang('produk_satuan') .'" disabled') ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('produk_photo') ?></label>
				<div class="col-sm-9">
					<?php echo form_input('produk_photo', $row->produk_photo, 'class="form-control" placeholder="'. db_lang('produk_photo') .'" disabled') ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('produk_description') ?></label>
				<div class="col-sm-9">
					<?php echo form_input('produk_description', $row->produk_description, 'class="form-control" placeholder="'. db_lang('produk_description') .'" disabled') ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('produk_status') ?></label>
				<div class="col-sm-9">
					<?php
						$items = array('' => lang('select.pick'),'1' =>lang('select.active'),'0' => lang('select.no_active'));
						echo form_dropdown('produk_status', $items, $row->produk_status, 'class="form-control" disabled');
					?>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-sm-9 col-sm-push-2">
					<button type="submit" class="btn btn-primary submit"><?php echo lang('buttons.save') ?></button>
					<button type="reset" class="btn btn-warning"><?php echo lang('buttons.reset') ?></button>
					<button class="btn btn-danger cancel"><?php echo lang('buttons.cancel') ?></button>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close() ?>
</div>