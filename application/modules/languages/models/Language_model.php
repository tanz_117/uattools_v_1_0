<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Language_model extends Base_Model
{
	protected $_table = 'languages';
	protected $_pk = 'language_id';

	public function __construct()
	{
		parent::__construct();
	}

	public function getDefaultLanguage()
	{
		return parent::row(array(
			'where' => array('language_default' => 1)
		));
	}

	public function insert($data)
	{
		parent::query("EXEC languages_insert ?, ?, ?, ?", $data);
		return true;
	}

	public function delete($id)
	{
		$this->db->where($this->_pk, html_decode($id));
		return $this->db->delete($this->_table);
	}
	
	public function update($input, $id)
	{
		$this->db->where($this->_pk, html_decode($id));
		return $this->db->update($this->_table, $input);
	}
}