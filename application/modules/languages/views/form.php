<div class="section">
	<div class="row">
		<div class="col s12 m12 l12">
			<div class="card-panel">
				<div class="row">
					<?php echo form_open(uri_string(), 'class="col s12 crud" id="form_data" autocomplete="off"'); ?>
					<div class="row">
						<div class="input-field col s12">
							<?php echo form_input('language_id', $row->language_id, 'id="language_id"'); ?>
							<label class="active"><?php echo db_lang('language_id'); ?></label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<?php echo form_input('language_name', $row->language_name, 'id="language_name"'); ?>
							<label class="active"><?php echo db_lang('language_name'); ?></label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<?php
							$select = array('' => lang('select.pick'), 1 => lang('dialog.yes'), 0 => lang('dialog.no'));
							echo form_dropdown('language_default', $select, $row->language_default);
							?>
							<label><?php echo db_lang('language_default'); ?></label>
						</div>
					</div>
					<?php if ($this->method === 'add'): ?>
					<div class="row">
						<div class="input-field col s12">
							<?php
							$status = array('' => lang('select.pick'), '1' => lang('select.active'), '0' => lang('select.no_active'));
							echo form_dropdown('language_active', $status, $row->language_active);
							?>
							<label><?php echo db_lang('language_active'); ?></label>
						</div>
					</div>
					<?php endif; ?>
					<div class="row">
						<div class="input-field col s12">
							<button type="submit" class="submit btn cyan waves-effect waves-light">
							<i class="mdi-content-send left"></i> <?php echo lang('buttons.save') ?>
							</button>
							<?php confirm_modal(array('rel' => 'confirm-submit')); ?>
							<a href="javascript:void(0);" class="button btn orange waves-effect waves-light" value="cancel">
								<i class="mdi-av-replay left"></i> <?php echo lang('buttons.cancel') ?>
							</a>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>