<div class="section">
	<!-- search detail -->
	<div class="row">
		<div class="col s12 m12 l12">
			<ul class="collapsible collapsible-accordion" data-collapsible="accordion">
				<li>
					<div class="collapsible-header light-blue light-blue-text text-lighten-5">
						<i class="mdi-action-explore"></i> <?php echo lang('search_detail.label') ?>
					</div>
					<div class="collapsible-body row" style="padding:10px">
						<div class="col s12 m12 l12">
							<?php echo form_open(uri_string(), 'name="form_filter" autocomplete="off"'); ?>
							<div class="input-field col s4">
								<?php echo form_input('name', userdata($this->module . '.search.name')) ?>
								<label class="active"><?php echo db_lang('language_name'); ?></label>
							</div>
							<div class="input-field col s4 right">
								<?php
								$select = array(' ' => lang('select.pick'), 1 => lang('dialog.yes'), 0 => lang('dialog.no'));
								echo form_dropdown('default', $select, userdata($this->module . '.search.default'));
								?>
								<label><?php echo db_lang('language_default'); ?></label>
							</div>
							<div class="input-field col s4 right">
								<?php
								$select = array(' ' => lang('select.pick'), 1 => lang('select.active'), 0 => lang('select.no_active'));
								echo form_dropdown('active', $select, userdata($this->module . '.search.active'));
								?>
								<label><?php echo db_lang('language_active'); ?></label>
							</div>
							<div class="right">
								<div class="input-field col s12">
									<button type="submit" class="filter btn cyan waves-effect waves-light">
									<i class="mdi-content-send"></i> <?php echo lang('buttons.search') ?>
									</button>
									<button type="reset" class="btn orange waves-effect waves-light" id="reset">
									<i class="mdi-action-autorenew"></i> <?php echo lang('buttons.reset') ?>
									</button>
								</div>
							</div>
							<?php echo form_close(); ?>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<!-- display data -->
	<div class="card-panel">
		<?php echo form_open(uri_string(), 'name="datas_table"'); ?>
		<div class="row" style="margin-bottom: 20px">
			<div class="col s12 m12 l12">
				<div class="col s2">
					<?php echo sprintf(lang('table.per_page'), table_per_page_options()) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s12 m12 l12">
				<table class="striped table">
					<thead>
						<tr>
							<th>
								<?php echo form_checkbox(array('name' => 'action_to_all', 'id' => 'check_all')); ?>
								<label for="check_all">&nbsp;</label>
							</th>
							<th><?php echo db_lang('language_id') ?></th>
							<th><?php echo db_lang('language_name') ?></th>
							<th><?php echo db_lang('language_default') ?></th>
							<th><?php echo db_lang('language_active') ?></th>
							<th><?php echo lang('label.action') ?></th>
						</tr>
					</thead>
					<tbody>
						<?php if (!empty($datas)): ?>
						<?php $i = 1; ?>
						<?php foreach ($datas as $data): ?>
						<tr>
							<td>
								<?php echo form_checkbox(array('name' => 'action_to[]', 'value' => $data->language_id, 'class' => 'check_to', 'id' => 'data' . $i)); ?>
								<label for="data<?php echo $i; ?>">&nbsp;</label>
							</td>
							<td><?php echo $data->language_id ?></td>
							<td><?php echo $data->language_name ?></td>
							<td><?php echo $data->defaults ?></td>
							<td><?php echo $data->status ?></td>
							<td>
								<a href="<?php echo site_url($this->module . '/status/' .$data->language_id) ?>" rel="status" class="tooltipped" data-tooltip="<?php echo str_replace("%s", '`'.$data->language_name.'`', lang('dialog.confirm_active')); ?>">
									<i class="mdi-navigation-refresh"></i>
								</a>
								<a href="<?php echo site_url($this->module . '/edit/' .$data->language_id) ?>" rel="edit" class="tooltipped" data-tooltip="<?php echo lang('label.edit').' `'.$data->language_id.' `' ?>">
									<i class="mdi-content-create"></i>
								</a>
								<a id="<?php echo $data->language_id ?>" rel="delete" class="tooltipped" data-tooltip="<?php echo lang('label.delete').' `'.$data->language_id.' `' ?>">
									<i class="mdi-content-clear"></i>
								</a>
							</td>
						</tr>
						<?php ++$i; ?>
						<?php endforeach; ?>
						<?php else: ?>
						<tr>
							<td colspan="7"><?php echo sprintf(lang('label.empty_grid'), db_lang('page_title.' . $this->module)) ?></td>
						</tr>
						<?php endif; ?>
					</tbody>
				</table>
				<div class="row" style="margin-top: 20px">
					<div class="col s6">
						<?php echo $pagination['links'] ?>
					</div>
					<div class="col s6 right" style="margin-top: 15px">
						<a href="<?php echo site_url($this->module . '/add'); ?>" rel="ajax" class="btn waves-effect waves-light cyan darken-2 right">
							<?php echo lang('label.add'); ?>
						</a>
						<button type="submit" class="submit delete-selected btn waves-effect waves-light red darken-4 right tooltipped" style="margin-right: 5px" data-tooltip="<?php echo db_lang('page_title.'.$this->module) ?>">
						<?php echo lang('buttons.delete_selected') ?>
						</button>
					</div>
				</div>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>
<?php
	custom_confirm_modal();
	alert_modal(array('title' => db_lang('page_title.'.$this->module) ,'content' => lang('dialog.delete_no_selected')));
?>