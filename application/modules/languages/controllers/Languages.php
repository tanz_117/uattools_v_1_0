<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Languages extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->module . '/language_model', '_languageModel');
		$this->load->helper('modal');
		$this->load->library('form_validation');
		$this->validation_rules = array(
			array(
				'field' => 'language_id',
				'label' => db_lang('language_id'),
				'rules' => 'required|trim|min_length[2]|alpha|callback__is_lang_id_exist['. $this->uri->segment(3) .']'
			),
			array(
				'field' => 'language_name',
				'label' => db_lang('language_name'),
				'rules' => 'required|trim|min_length[5]|alpha_numeric_spaces|callback__is_lang_name_exist['. $this->uri->segment(3) .']'
			),
			array(
				'field' => 'language_default',
				'label' => db_lang('language_default'),
				'rules' => 'required|trim|integer'
			)
		);
		$this->form_validation->set_rules($this->validation_rules);
		$this->_is_ajax = $this->is_ajax() || $this->input->post('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest';
		$this->_is_ajax ? $this->template->set_layout(FALSE) : '';
	}

	public function index()
	{
		$this->load->helper('table');
		$ref_array = explode('/', array_key_exists('HTTP_REFERER', $_SERVER) ? $_SERVER['HTTP_REFERER'] : '');
		if (!in_array($this->module, $ref_array)) {
			$this->session->set_userdata($this->module . '.sort.field', '');
			$this->session->set_userdata($this->module . '.sort.dir', '');
			$this->session->set_userdata($this->module . '.current_page', '');
			$this->session->set_userdata($this->module . '.search.name', '');
			$this->session->set_userdata($this->module . '.search.default', '');
			$this->session->set_userdata($this->module . '.search.active', '');
		}

		$baseCondition = array();
		if ($this->input->post()) {
			$post = $this->input->post_sanitized();var_dump($post);
			foreach($post as $key => $val) {
				if (!in_array($key, array('sortfield', 'sortdir', 'current_page', 'per_page')))
					$this->session->set_userdata($this->module . '.search.' . $key, $val);
			}

			$baseCondition = array(
				'like' => array('language_name' => userdata($this->module . '.search.name'))
			);

			$conditions = array();
			if (trim($this->input->post('default')) != '')
				$conditions = $conditions + array('language_default' => userdata($this->module . '.search.default'));

			if (trim($this->input->post('active')) != '')
				$conditions = $conditions + array('language_active' => userdata($this->module . '.search.active'));

			$baseCondition = $baseCondition + array('where' => $conditions);
		}

		if ($this->input->post('current_page')) {
			$this->session->set_userdata($this->module . '.current_page', $this->input->post_sanitized('current_page'));
		}

		if ($this->input->post('per_page')) {
			$this->session->set_userdata($this->module . '.' . $this->method . '.per_page', $this->input->post_sanitized('per_page'));
		}

		$pagination = create_pagination($this->module, $this->_languageModel->count($baseCondition));
		if ($this->input->post('sortfield')) {
			$this->session->set_userdata($this->module . '.sort.field', $this->input->post_sanitized('sortfield'));
			$this->session->set_userdata($this->module . '.sort.dir', $this->input->post_sanitized('sortdir'));
			$baseCondition = $this->session->set_userdata($this->module . '.sort.field', $this->input->post('sortfield')) ? $baseCondition + array('order_by' => array(userdata($this->module . '.sort.field'), userdata($this->module . '.sort.dir'))) : $baseCondition;
		}

		$datas = $this->_languageModel->rows(array(
			'select' => "*, (CASE WHEN language_default = 1 THEN '". lang('dialog.yes') ."' ELSE '". lang('dialog.no') ."' END) AS defaults, (CASE WHEN language_active = 1 THEN '". lang('select.active') ."' ELSE '". lang('select.no_active') ."' END) AS status",
			'order_by' => array('language_default', 'ASC'),
			'limit' => $pagination['limit']
		) + $baseCondition);

		$this->template
			->set('pagination', $pagination)
			->set('datas', $datas)
			->title(db_lang('page_title.' . $this->module))
			->build('default', array());
	}

	public function add()
	{
		if (!$this->_is_ajax) {
			$this->session->set_flashdata('notice', lang('auth.no_direct_link'));
			redirect($this->module);
		}

		$this->validation_rules[] = array(
			'field' => 'language_active',
			'label' => db_lang('language_active'),
			'rules' => 'required|trim|numeric'
		);

		$this->form_validation->set_rules($this->validation_rules);
		if ($this->form_validation->run()) {
			$post = $this->input->post_sanitized();

			if ($this->_languageModel->insert($post)) {
				log_activity($post, NULL, array($this->module . '/language_model', '_languageModel'));
				$message = sprintf(lang('notice.add_success'), $post['language_name']);
				$status = 'success';
			} else {
				$message = sprintf(lang('notice.add_error'), $post['language_name']);
				$status = 'error';
			}

			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}
		} elseif ($this->input->post()) {
			$message = $this->form_validation->error_string('', '');
			$status = 'error';
			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}
		} else {
			$row = new stdClass;
			foreach ($this->validation_rules as $rule) {
				$row->{$rule['field']} = set_value($rule['field']);
			}
		}

		$this->data->row = $row;
		if ($this->_is_ajax && $this->input->post()) {
			echo json_encode(array('status' => 'error', 'message' => validation_errors()));
		} else {
			$this->template->build('form', $this->data);
		}
	}

	public function edit($id = '')
	{
		if (!$this->_is_ajax) {
			$this->session->set_flashdata('notice', lang('auth.no_direct_link'));
			redirect($this->module);
		}

		if (!$id) {
			redirect($this->module);
		}

		$id = sanitize_input($id);
		$row = $this->_languageModel->rowById($id);
		if ($this->form_validation->run()) {
			$post = $this->input->post_sanitized();
			if ($this->_languageModel->update($post, $id)) {
				log_activity($post, $row, $id);
				$message = sprintf(lang('notice.edit_success'), $post['language_name']);
				$status = 'success';
			} else {
				$message = sprintf(lang('notice.edit_error'), $post['language_name']);
				$status = 'error';
			}

			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}
		} elseif ($this->input->post()) {
			$message = $this->form_validation->error_string('', '');
			$status = 'error';
			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}
		} else {
			if ($this->input->post()) {
				$row = new stdClass;
				foreach ($this->validation_rules as $rule) {
					$row->{$rule['field']} = set_value($rule['field']);
				}
			}
		}
		$this->data->row = $row;
		if ($this->_is_ajax && $this->input->post()) {
			echo json_encode(array('status' => 'error', 'message' => validation_errors()));
		} else {
			$this->template->build('form', $this->data);
		}
	}

	public function delete($id = '')
	{
		if (!$this->_is_ajax) {
			$this->session->set_flashdata('notice', lang('auth.no_direct_link'));
			redirect($this->module);
		}

		$ids = (!empty($id)) ? array($id) : $this->input->post('action_to');
		if (!empty($ids)) {
			$deleted = 0;
			$to_delete = 0;
			foreach ($ids as $id) {
				if ($this->_languageModel->delete(sanitize_input($id))) {
					log_activity(array('deleted' => true), array('deleted' => false), $id);
					$deleted++;
				} else {
					$status = 'error';
					$message = sprintf(lang('notice.delete_error'), $id);
				}
				$to_delete++;
			}

			if ($deleted > 0) {
				$status = 'success';
				$message = sprintf(lang('notice.delete_success'), $deleted, $to_delete);
			}
		} else {
			$status = 'notice';
			$message = lang('notice.no_select_error');
		}
		
		if ($this->_is_ajax) {
			echo json_encode(array('status' => $status, 'message' => $message));
			return;
		} else {
			$this->session->set_flashdata($status, $message);
			redirect($this->module);
		}
	}

	public function status($id = '')
	{
		if (!$this->_is_ajax) {
			$this->session->set_flashdata('notice', lang('auth.no_direct_link'));
			redirect($this->module);
		}

		if (!$id) {
			redirect($this->module);
		}

		$id = sanitize_input($id);
		$row = $this->_languageModel->rowById($id);
		if ($this->_languageModel->procedure('EXEC languages_set_status ?', array($id))) {
			log_activity(array('language_active' => ($row->language_active ? 0 : 1)), array('language_active' => $row->language_active), $id);
			$message = sprintf(lang('notice.status_success'), db_lang('page_title.' . $this->module) .' '. $id);
			$status = 'success';
		} else {
			$message = sprintf(lang('notice.status_error'), db_lang('page_title.' . $this->module) .' '. $id);
			$status = 'error';
		}

		if ($this->_is_ajax) {
			echo json_encode(array('status' => $status, 'message' => $message));
			return;
		} else {
			$this->session->set_flashdata($status, $message);
			redirect($this->module);
		}
	}

	public function default_lang($id = '')
	{
		if (!$this->_is_ajax) {
			$this->session->set_flashdata('notice', lang('auth.no_direct_link'));
			redirect($this->module);
		}

		if (!$id) {
			redirect($this->module);
		}

		$id = sanitize_input($id);
		$row = $this->_languageModel->rowById($id);
		if ($this->_languageModel->procedure('EXEC languages_set_default_lang ?', array($id))) {
			log_activity(array('language_default' => ($row->language_default ? 0 : 1)), array('language_default' => $row->language_default), $id);
			$message = sprintf(lang('notice.default_lang_success'), db_lang('page_title.' . $this->module) .' '. $id);
			$status = 'success';
		} else {
			$message = sprintf(lang('notice.default_lang_error'), db_lang('page_title.' . $this->module) .' '. $id);
			$status = 'error';
		}

		if ($this->_is_ajax) {
			echo json_encode(array('status' => $status, 'message' => $message));
			return;
		} else {
			$this->session->set_flashdata($status, $message);
			redirect($this->module);
		}
	}

	public function _is_lang_id_exist($input, $id = '')
	{
		$condition['like'] = array('language_id' => $input);
		if (! empty($id))
			$condition['where'] = array('language_id <>' => $id);
		
		if ($this->_languageModel->count($condition) > 0) {
			$this->form_validation->set_message('_is_lang_id_exist', sprintf(lang('is_already_exist'), $input));
			return false;
		}
		return true;
	}
	
	public function _is_lang_name_exist($input, $id = '')
	{
		$condition['like'] = array('language_name' => $input);
		if (! empty($id))
			$condition['where'] = array('language_id <>' => $id);
		
		if ($this->_languageModel->count($condition) > 0) {
			$this->form_validation->set_message('_is_lang_name_exist', sprintf(lang('is_already_exist'), $input));
			return false;
		}
		return true;
	}
}