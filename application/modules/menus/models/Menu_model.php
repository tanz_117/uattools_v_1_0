<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Menu_model extends Base_Model
{
	protected $_table = 'menus';
	protected $_pk = 'menu_id';

	public function __construct()
	{
		parent::__construct();
	}

	public function select($condition = array(), $limit = NULL)
	{
		$result = $this->db->query("CALL sp_menu_list()");
		return $result->result();
	}

	public function getIconList(){
		$result = $this->db->query("CALL sp_icon_list()");
		return $result->result();
	}

	public function update($data)
	{
		$result = parent::query("CALL sp_menu_update('{$data['menu_id']}','{$data['menu_icon']}','{$data['menu_active']}','{$data['userid']}','{$data['ip_address']}','{$data['browser']}','{$data['status']}','{$data['method_name']}','{$data['module_name']}','{$data['historydetail']}')");
		return $result[0]['jml'] == 1 ? true : false;
	}

	public function getMenuOrder($id)
	{
		$this->db->select('MAX(menu_order) + 1 AS new_order');
		$this->db->from('menus');
		$this->db->where('menu_parent', $id );
		$query = $this->db->get();
		return empty($query->result()[0]->new_order) ? 1 : $query->result()[0]->new_order;
	}
}