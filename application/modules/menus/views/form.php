<div class="box-typical box-typical-padding">
	<div class="box box-solid">
		<div class="box-header bg-olive with-border">
			<h3 class="m-t-lg with-border"><?php echo lang('label.' . $this->method) . ' ' . db_lang('page_title.' . $this->module) ?></h3>
		</div>
		<?php echo form_open(uri_string(), 'id="form_data" class="crud" autocomplete="off"') ?>
		<div class="box-body">
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('menu_name') ?></label>
				<div class="col-sm-9">
					<?php echo form_input('menu_name', $row->menu_name, 'class="form-control" placeholder="'. db_lang('menu_name') .'"') ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('menu_url') ?></label>
				<div class="col-sm-9">
					<?php echo form_input('menu_url', $row->menu_url, 'class="form-control" placeholder="'. db_lang('menu_url') .'"') ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('menu_parent') ?></label>
				<div class="col-sm-9">
					<?php echo form_input('menu_parent', $row->menu_parent, 'class="form-control" placeholder="'. db_lang('menu_parent') .'"') ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('menu_order') ?></label>
				<div class="col-sm-9">
					<?php echo form_input('menu_order', $row->menu_order, 'class="form-control" placeholder="'. db_lang('menu_order') .'"') ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('menu_icon') ?></label>
				<div class="col-sm-7">
					<?php echo form_input('menu_icon', $row->menu_icon, 'id="menu_icon" class="form-control" placeholder="'. db_lang('menu_icon') .'" readonly') ?>
				</div>
				<div class="col-sm-2">
					<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">
						<?php echo lang('buttons.icons'); ?>
					</button>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('menu_active') ?></label>
				<div class="col-sm-9">
					<?php
					$items = array('' => lang('select.pick'),'1' =>lang('select.active'),'0' => lang('select.no_active'));
					echo form_dropdown('menu_active', $items, $row->menu_active, 'class="form-control"');
					?>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-sm-9 col-sm-push-2">
					<button type="submit" class="btn bg-olive btn-flat submit"><i class="glyphicon glyphicon-ok"></i>&nbsp;<?php echo lang('buttons.save') ?></button>
					<button class="btn bg-maroon btn-flat cancel"><i class="glyphicon glyphicon-remove"></i>&nbsp;<?php echo lang('buttons.cancel') ?></button>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close() ?>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document" style="width: 80%">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><?php echo lang('buttons.icons'); ?></h4>
			</div>
			<div class="modal-body" style="margin:0 10px 0 10px">
				<div class="row">
					<?php foreach ($icons as $item): ?>					
						<div class="col-sm-2 block-icon" style="border:1px solid gray;height: 85px;" onClick="addIcon('<?php echo $item->icon_name; ?>')" data-toggle="modal" data-target="#myModal">
							<div class="text-center" style="padding-top:5px">
								<div class="row">
									<i class="<?php echo $item->icon_name; ?>"></i>
								</div>
								<div class="row" style="width:100%;margin:0">
									<?php echo $item->icon_name; ?>
								</div>
							</div>
						</div>
					<?php endforeach ?>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<style type="text/css">
	.block-icon:hover{
		background-color: #F1FFF1;
		cursor: pointer;
	};
</style>
<script type="text/javascript">
	function addIcon(id){
		$('#menu_icon').val(id);
	}
</script>