<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Menus extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->module . '/menu_model', '_menuModel');
		$this->load->helper('modal');
		$this->load->library('form_validation');

		$this->validation_rules = array(
			array(
				'field' => 'menu_icon',
				'label' => db_lang('menu_icon'),
				'rules' => 'required|trim'
			)
		);

		$this->form_validation->set_rules($this->validation_rules);
		$this->template->append_scripts(js($this->module . '.js', $this->module));
		$this->_is_ajax = $this->is_ajax() || $this->input->post('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest';
		$this->_is_ajax ? $this->template->set_layout(FALSE) : '';
	}

	public function index()
	{
		$this->load->helper('table');
		$ref_array = explode('/', array_key_exists('HTTP_REFERER', $_SERVER) ? $_SERVER['HTTP_REFERER'] : '');
		if (!in_array($this->module, $ref_array)) {
			$this->session->set_userdata($this->module . '.sort.field', '');
			$this->session->set_userdata($this->module . '.sort.dir', '');
			$this->session->set_userdata($this->module . '.current_page', '');
			$this->session->set_userdata($this->module . '.search.menu_name', '');
		}

		$baseCondition = $condition = array();
		if ($this->input->post()) {
			$post = $this->input->post_sanitized();
			foreach($post as $key => $val) {
				if (!in_array($key, array('sortfield', 'sortdir', 'current_page', 'per_page'))){
					$this->session->set_userdata($this->module . '.search.' . $key, $val);
					if($key == 'menu_name'){
						$condition = $condition + array('menus.'.$key => userdata($this->module . '.search.'.$key));
					}else{
						$condition = $condition + array($key => userdata($this->module . '.search.'.$key));
					}
				}
			}
			$baseCondition = $baseCondition + array('like' => $condition);
		}

		$base_order = array();
		if ($this->input->post('sortfield')) {
			$this->session->set_userdata($this->module . '.sort.field', $this->input->post('sortfield'));
			$this->session->set_userdata($this->module . '.sort.dir', $this->input->post('sortdir'));
		}

        		$base_order = $this->session->userdata($this->module . '.sort.field') ? $base_order + array(trim($this->session->userdata($this->module . '.sort.field') . ' ' . $this->session->userdata($this->module . '.sort.dir'))) : $base_order;

		if (array_key_exists('current_page', $_POST)) {
			// set paging by user
			$this->session->set_userdata($this->module . '.current_page', $this->input->post('current_page'));
		}

		if (array_key_exists('per_page', $_POST)) {
			$this->session->set_userdata($this->module . '.' . $this->method . '.per_page', $this->input->post('per_page'));
		}

		$pagination = create_pagination($this->module, $this->_menuModel->count($baseCondition));
		
		if ($this->input->post('sortfield')) {
			$this->session->set_userdata($this->module . '.sort.field', $this->input->post_sanitized('sortfield'));
			$this->session->set_userdata($this->module . '.sort.dir', $this->input->post_sanitized('sortdir'));
			$baseCondition = $this->session->set_userdata($this->module . '.sort.field', $this->input->post('sortfield')) ? $baseCondition + array('order_by' => array(userdata($this->module . '.sort.field'), userdata($this->module . '.sort.dir'))) : $baseCondition;
		}

		$datas = $this->_menuModel->rows(array(
			'select' => "menus.menu_id,menus.menu_name,menus.menu_url,menus.menu_icon,menus.menu_parent,menus.menu_order,menus.menu_active,(CASE WHEN menus.menu_active = '1' THEN 'active' ELSE 'inactive' END) AS menu_active_label,
				(CASE WHEN b.menu_name IS NULL THEN 'PARENT' ELSE b.menu_name END) AS menus",
			'join' => array('menus b','menus.menu_parent = b.menu_id','LEFT'),
			'limit' => $pagination['limit']
		) + $baseCondition);
		
		$this->template
			->set('pagination', $pagination)
			->set('datas', $datas)
			->title(db_lang('page_title.' . $this->module))
			->build('default', array());
	}

	public function add()
	{
		if (!$this->_is_ajax) {
			$this->session->set_flashdata('notice', lang('auth.no_direct_link'));
			redirect($this->module);
		}

		$this->validation_rules[] = array(
			'field' => 'menu_active',
			'label' => db_lang('menu_active'),
			'rules' => 'required|trim|numeric'
		);

		$this->form_validation->set_rules($this->validation_rules);
		if ($this->form_validation->run()) {
			$_POST['menu_url'] = $_POST['menu_parent'] == '0' ? '#' : $_POST['menu_url'];
			$post = $this->input->post_sanitized();
			if ($this->_menuModel->insert($post)) {
				log_activity($post, NULL, array($this->module . '/menu_model', '_menuModel'));
				if (isset($post['menu_create_module']) && $post['menu_create_module']) {
					$this->load->helper('file');
				}

				$message = sprintf(lang('notice.add_success'), $post['menu_name']);
				$status = 'success';
			} else {
				$message = sprintf(lang('notice.add_error'), $post['menu_name']);
				$status = 'error';
			}

			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}
		} elseif ($this->input->post()) {
			$message = $this->form_validation->error_string('', '');
			$status = 'error';
			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}
		} else {
			$row = new stdClass;
			foreach ($this->validation_rules as $rule) {
				$row->{$rule['field']} = set_value($rule['field']);
			}
		}

		$this->data->row = $row;
		$this->data->parents = $this->_buildMenuParentOption();

		if ($this->_is_ajax && $this->input->post()) {
			echo json_encode(array('status' => 'error', 'message' => validation_errors()));
		} else {
			$this->template->build('form', $this->data);
		}
	}

	public function edit($id = '')
	{
		if (!$this->_is_ajax) {
			$this->session->set_flashdata('notice', lang('auth.no_direct_link'));
			redirect($this->module);
		}

		if (!$id) {
			redirect($this->module);
		}

		if($_POST && $id){
			$_POST['menu_id'] = $id;
		}

		$row = $this->_menuModel->row(array('where' => array('menu_id' => $id)));
		// $menus = $this->_roleModel->selectRoleMenus(array('where' => array('role_id' => $id)));
		
		if ($this->form_validation->run()) {
			$menu_before = array();
			foreach ($menus as $menu) {
				array_push($menu_before, $menu->menu_id);
			}

			$before = array('menu_id' => $id,'menu_icon' => $_POST['menu_icon'],'menu_active' => $_POST['menu_active']);
			$compare = set_before_after($_POST,$before);
			$post = set_audit_history($_POST,$this->module, $this->method, $compare);

			if ($this->_menuModel->update($post)) {
				$message = sprintf(lang('notice.edit_success'), $row->menu_name);
				$status = 'success';
			} else {
				$message = sprintf(lang('notice.edit_error'), $row->menu_name);
				$status = 'error';
			}

			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}
		} elseif ($this->input->post()) {
			$message = $this->form_validation->error_string('', '');
			$status = 'error';
			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}
		} else {
			if ($this->input->post()) {
				$row = new stdClass;
				foreach ($this->validation_rules as $rule) {
					$row->{$rule['field']} = set_value($rule['field']);
				}
			}
		}

		
		// foreach ($menus as $menu) {
		// 	$selectedMenu[$menu->menu_id] = $menu->menu_id;
		// }
		// $this->data->selected = $selectedMenu;
		// var_dump('asdf');exit;
		$this->data->icons = $this->_menuModel->getIconList();
		// $this->data->menus = $this->_buildMenusOption();
		$this->data->row = $row;
		// var_dump(css_path('lib/bootstrap/css/bootstrap.min.css', '_theme_'));exit;
		// var_dump(base_url('themes/template_lab/css/lib/bootstrap/css/bootstrap.min.css'));exit;
		// $files = file(css_path('lib/bootstrap/css/bootstrap.min.css', '_theme_'));
		// var_dump($files);exit;
		if ($this->_is_ajax && $this->input->post()) {
			echo json_encode(array('status' => 'error', 'message' => validation_errors()));
		} else {
			$this->template->build('form', $this->data);
		}
	}


	public function menuOrder() {
		$id = intval($this->input->post_sanitized('id'));
		$order = $this->_menuModel->getMenuOrder($id);
		echo $order;
		exit;
	}

	public function _is_already_exist($input, $id = '')
	{
		$condition['like'] = array('menu_name' => $input);
		if (! empty($id))
			$condition['where'] = array('menu_id <>' => $id);
		
		if ($this->_menuModel->count($condition) > 0) {
			$this->form_validation->set_message('_is_already_exist', sprintf(lang('is_already_exist'), $input));
			return false;
		}
		return true;
	}
	
	private function _buildMenuParentOption()
	{
		$rows = $this->_menuModel->rows(array(
			'select' => 'menu_id, menu_name',
			'where' => array('menu_active' => 1)
		));
		return array_for_select($rows, 'menu_id', 'menu_name');
	}
}