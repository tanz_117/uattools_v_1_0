<?php defined('BASEPATH') OR exit('No direct script access allowed');
class information_model extends Base_Model
{
	protected $_table = 'detail_informations';
	protected $_pk = 'id';

	public function __construct()
	{
		parent::__construct();
	}

	public function insert($data)
	{
		$result = parent::query("CALL sp_information_insert('{$data['label']}', '{$data['status_id']}', '{$data['group_detail_information_id']}', '{$data['parent']}', '{$data['role_id']}', '{$data['order']}','{$data['userid']}','{$data['ip_address']}','{$data['browser']}','{$data['status']}','{$data['method_name']}','{$data['module_name']}','{$data['historydetail']}')");		
		return $result[0]['jml'] == 1 ? true : false;
	}

	public function update($data)
	{
		$result = parent::query("CALL sp_information_update('{$data['id']}','{$data['label']}','{$data['status_id']}', '{$data['group_detail_information_id']}', '{$data['parent']}', '{$data['role_id']}', '{$data['order']}','{$data['userid']}','{$data['ip_address']}','{$data['browser']}','{$data['status']}','{$data['method_name']}','{$data['module_name']}','{$data['historydetail']}')");		
		return $result[0]['jml'] == 1 ? true : false;
	}
}