<div class="box-typical box-typical-padding">
	<div class="box box-solid">
		<div class="box-header bg-olive with-border">
			<h3 class="m-t-lg with-border"><?php echo lang('label.' . $this->method) . ' ' . db_lang('page_title.' . $this->module) ?></h3>
		</div>
		<?php echo form_open_multipart(uri_string(), 'id="form_upload" class="crud_upload" autocomplete="off" ') ?>
		<div class="box-body">
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('label') ?></label>
				<div class="col-sm-9">
					<?php 
						if($this->method == 'edit'){
							echo form_hidden('id', $row->id, 'class="form-control"');
						}
						echo form_input('label', $row->label, 'class="form-control" placeholder="'. db_lang('label') .'"') 
					?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('group_detail_information_id') ?></label>
				<div class="col-sm-9">
					<?php
					$items = array('' => lang('select.pick')) + $groupdetailinformationlist ;
					echo form_dropdown('group_detail_information_id', $items, $row->group_detail_information_id, 'class="form-control"');
					?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('parent') ?></label>
				<div class="col-sm-9">
					<?php
					$items = array('' => lang('select.pick')) + $parentlist ;
					echo form_dropdown('parent', $items, $row->parent, 'class="form-control"');
					?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('role_id') ?></label>
				<div class="col-sm-9">
					<?php
					$items = array('' => lang('select.pick')) + $rolelist ;
					echo form_dropdown('role_id', $items, $row->role_id, 'class="form-control"');
					?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('order') ?></label>
				<div class="col-sm-9">
					<?php echo form_input('order', $row->order, 'class="form-control" placeholder="'. db_lang('order') .'"') ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('status_id') ?></label>
				<div class="col-sm-9">
					<?php
					$items = array('' => lang('select.pick')) + $statuslist ;
					echo form_dropdown('status_id', $items, $row->status_id, 'class="form-control"');
					?>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-sm-9 col-sm-push-2">
					<button type="submit" class="btn bg-olive btn-flat submit"><i class="glyphicon glyphicon-ok"></i>&nbsp;<?php echo lang('buttons.save') ?></button>
					<button class="btn bg-maroon btn-flat cancel"><i class="glyphicon glyphicon-remove"></i>&nbsp;<?php echo lang('buttons.cancel') ?></button>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close() ?>
</div>