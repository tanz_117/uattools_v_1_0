<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Informations extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->module . '/information_model', '_informationModel');
		$this->load->helper('modal');
		$this->load->library('form_validation');

		$this->validation_rules = array(
			array(
				'field' => 'label',
				'label' => db_lang('label'),
				'rules' => 'required|trim'
			),
			array(
				'field' => 'status_id',
				'label' => db_lang('status_id'),
				'rules' => 'required|trim|numeric'
			),
			array(
				'field' => 'group_detail_information_id',
				'label' => db_lang('group_detail_information_id'),
				'rules' => 'required|trim|numeric'
			),
			array(
				'field' => 'parent',
				'label' => db_lang('parent'),
				'rules' => 'trim|numeric'
			),
			array(
				'field' => 'role_id',
				'label' => db_lang('role_id'),
				'rules' => 'required|trim|numeric'
			),
			array(
				'field' => 'order',
				'label' => db_lang('order'),
				'rules' => 'required|trim|numeric'
			)
		);

		$this->form_validation->set_rules($this->validation_rules);
		$this->_is_ajax = $this->is_ajax() || $this->input->post('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest';
		$this->_is_ajax ? $this->template->set_layout(FALSE) : '';
	}

	public function index()
	{
		$this->load->helper('table');
		$ref_array = explode('/', array_key_exists('HTTP_REFERER', $_SERVER) ? $_SERVER['HTTP_REFERER'] : '');
		if (!in_array($this->module, $ref_array)) {
			$this->session->set_userdata($this->module . '.sort.field', '');
			$this->session->set_userdata($this->module . '.sort.dir', '');
			$this->session->set_userdata($this->module . '.current_page', '');
			$this->session->set_userdata($this->module . '.name', '');
		}

		$baseCondition = $condition = array();
		if ($this->input->post()) {
			$post = $this->input->post_sanitized();
			foreach($post as $key => $val) {
				if (!in_array($key, array('sortfield', 'sortdir', 'current_page', 'per_page'))){
					$this->session->set_userdata($this->module . '.search.' . $key, $val);
					$condition = $condition + array($key => userdata($this->module . '.search.'.$key));
				}
			}
			$baseCondition = $baseCondition + array('like' => $condition);
		}

		$base_order = array();
		if ($this->input->post('sortfield')) {
			$this->session->set_userdata($this->module . '.sort.field', $this->input->post('sortfield'));
			$this->session->set_userdata($this->module . '.sort.dir', $this->input->post('sortdir'));
		}

		$base_order = $this->session->userdata($this->module . '.sort.field') ? $base_order + array(trim($this->session->userdata($this->module . '.sort.field') . ' ' . $this->session->userdata($this->module . '.sort.dir'))) : $base_order;

		if (array_key_exists('current_page', $_POST)) {
			// set paging by user
			$this->session->set_userdata($this->module . '.current_page', $this->input->post('current_page'));
		}

		if (array_key_exists('per_page', $_POST)) {
			$this->session->set_userdata($this->module . '.' . $this->method . '.per_page', $this->input->post('per_page'));
		}

		$pagination = create_pagination($this->module, $this->_informationModel->count($baseCondition));
		
		if ($this->input->post('sortfield')) {
			$this->session->set_userdata($this->module . '.sort.field', $this->input->post_sanitized('sortfield'));
			$this->session->set_userdata($this->module . '.sort.dir', $this->input->post_sanitized('sortdir'));
			$baseCondition = $this->session->set_userdata($this->module . '.sort.field', $this->input->post('sortfield')) ? $baseCondition + array('order_by' => array(userdata($this->module . '.sort.field'), userdata($this->module . '.sort.dir'))) : $baseCondition;
		}

		$datas = $this->_informationModel->rows(array(
			'select' => 'detail_informations.id,detail_informations.label,detail_informations.status_id',
			'join' => array(
				array('group_detail_informations g','g.id = detail_informations.group_detail_information_id','left'),
				array('detail_informations d','d.id = detail_informations.parent','left'),
				array('roles r','r.role_id = detail_informations.role_id','left'),
			),
			'order_by' => array('detail_informations.order', 'ASC'),
			'limit' => $pagination['limit']
		) + $baseCondition);
		
		$this->template
			->set('pagination', $pagination)
			->set('datas', $datas)
			->title(db_lang('page_title.' . $this->module))
			->build('default', array());
	}

	public function add()
	{
		if (!$this->_is_ajax) {
			$this->session->set_flashdata('notice', lang('auth.no_direct_link'));
			redirect($this->module);
		}

		if ($this->form_validation->run()) {
			if(!$_POST['parent']){
				$_POST['parent'] = '0';
			}

			$compare = set_before_after($_POST);
			$post = set_audit_history($_POST,$this->module, $this->method, $compare);
			
			if ($this->_informationModel->insert($post)) {
				$message = sprintf(lang('notice.add_success'), $post['label']);
				$status = 'success';
			} else {
				$message = sprintf(lang('notice.add_error'), $post['label']);
				$status = 'error';
			}

			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}
		} elseif ($this->input->post()) {
			$message = $this->form_validation->error_string('', '');
			$status = 'error';
			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}
		} else {
			$row = new stdClass;
			foreach ($this->validation_rules as $rule) {
				$row->{$rule['field']} = set_value($rule['field']);
			}
		}

		$this->data->statuslist = array_for_select(config_item('status_activeinactive'), 'key', 'value');
		$this->data->groupdetailinformationlist = $this->_getgroupinformationlist();
		$this->data->rolelist = $this->_getrolelist();
		$this->data->parentlist = $this->_getparentlist();

		$this->data->row = $row;
		if ($this->_is_ajax && $this->input->post()) {
			echo json_encode(array('status' => 'error', 'message' => validation_errors()));
		} else {
			$this->template->build('form', $this->data);
		}
	}

	public function edit($id = '')
	{
		if (!$this->_is_ajax) {
			$this->session->set_flashdata('notice', lang('auth.no_direct_link'));
			redirect($this->module);
		}

		if (!$id) {
			redirect($this->module);
		}

		$row = $this->_informationModel->row(array('where' => array('id' => $id)));

		if ($this->form_validation->run()) {
			$before = array(
				'id' => $id,
				'label' => $row->label,
				'group_detail_information_id' => $row->group_detail_information_id,
				'parent' => $row->parent,
				'role_id' => $row->role_id,
				'order' => $row->order,
				'status_id' => $row->status_id
			);

			$compare = set_before_after($_POST,$before);
			$post = set_audit_history($_POST,$this->module, $this->method, $compare);

			if ($this->_informationModel->update($post)) {
				$message = sprintf(lang('notice.edit_success'), $row->label);
				$status = 'success';
			} else {
				$message = sprintf(lang('notice.edit_error'), $row->label);
				$status = 'error';
			}

			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}
		} elseif ($this->input->post()) {
			$message = $this->form_validation->error_string('', '');
			$status = 'error';
			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}
		} else {
			if ($this->input->post()) {
				$row = new stdClass;
				foreach ($this->validation_rules as $rule) {
					$row->{$rule['field']} = set_value($rule['field']);
				}
			}
		}

		$this->data->statuslist = array_for_select(config_item('status_activeinactive'), 'key', 'value');
		$this->data->groupdetailinformationlist = $this->_getgroupinformationlist();
		$this->data->rolelist = $this->_getrolelist();
		$this->data->parentlist = $this->_getparentlist();

		$this->data->row = $row;

		if ($this->_is_ajax && $this->input->post()) {
			echo json_encode(array('status' => 'error', 'message' => validation_errors()));
		} else {
			$this->template->build('form', $this->data);
		}
	}

	public function view()
	{
		if (!$this->_is_ajax) {
			$this->session->set_flashdata('notice', lang('auth.no_direct_link'));
			redirect($this->module);
		}

		$row = $this->_productModel->row(array('select' => 'produk_identity,produk_nama,produk_harga,produk_stok,produk_satuan,produk_status,produk_photo,produk_description'));
		$this->data->row = $row;
		if ($this->_is_ajax && $this->input->post()) {
			echo json_encode(array('status' => 'error', 'message' => validation_errors()));
		} else {
			$this->template->build('view', $this->data);
		}
	}

	private function _getgroupinformationlist()
	{
		$this->load->model('groupinformations/groupinformation_model', '_groupinformation_model');
		$row = $this->_groupinformation_model->rows();
		return array_for_select($row, 'id', 'name');
	}

	private function _getparentlist()
	{
		$row = $this->_informationModel->rows();
		return array_for_select($row, 'id', 'label');
	}

	private function _getrolelist()
	{
		$this->load->model('roles/Role_model', '_role_model');
		$row = $this->_role_model->rows();
		return array_for_select($row, 'role_id', 'role_name');
	}

	public function _check_availability($produk_identity)
	{
		$needCheck = true;
		if($this->method == 'edit'){
			$current_data = $this->_productModel->row(array('where' => array('produk_id' => $_POST['produk_id'])));
			$needCheck = $current_data->produk_identity == $produk_identity ? false : true;
		}

		if($this->method == 'add' || $needCheck == true){
			$existing_data = $this->_productModel->count(array('where' => array('produk_identity' => $produk_identity)));
			if($existing_data > 0){
				$this->form_validation->set_message('_check_availability', sprintf(lang('user_id_not_available'),$user_identity));
				return false;
			}
		}
		return true;
	}
}