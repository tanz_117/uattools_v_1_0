<?php $column_names = array('role_name','role_level','role_active_label','roles'); ?>
<div id="content">
	<section class="box-typical box-typical-padding" id="default">
		<?php echo form_open(uri_string(), 'name="datas_table" id="form_table"'); ?>
		<div class="box box-solid">
			<div class="box-header bg-olive-active color-palette">
				<h3 class="box-title"><?php echo db_lang('page_title.' . $this->module) ?></h3>
<!-- 				<div class="box-tools" >
					<div class="form-control input-sm">
						<?php echo sprintf(lang('table.per_page'), table_per_page_options()) ?>
					</div>
				</div> -->
			</div>
			<div class="box-body table-responsive no-padding">
				<table class="table">
					<tbody>
						<tr>
							<?php foreach ($column_names as $column_name): ?>
								<th><?php echo db_lang($column_name) ?></th>
							<?php endforeach ?>
							<th style="width:10%"><?php echo lang('label.action') ?></th>
						</tr>
						<tr>
							<?php if (!empty($datas)): ?>
							<?php $i = 1; ?>
							<?php foreach ($datas as $data): ?>
							<tr>
								<?php foreach ($column_names as $column_name): ?>
									<?php if($column_name == 'role_active_label'): ?>
										<td>
											<small class="label bg-<?php echo $data->role_active == '1' ? 'olive' : 'maroon' ?>">
												<?php echo lang($data->role_active == '1' ? 'select.active' : 'select.no_active') ?>
											</small>
										</td>
									<?php else: ?>
										<td><?php echo $data->{$column_name} ?></td>
									<?php endif ?>
								<?php endforeach ?>
								<td>
									<a href="<?php echo site_url($this->module . '/edit/' .$data->role_id) ?>" class="btn bg-maroon btn-flat" rel="edit" data-toggle="tooltip" title="<?php echo lang('label.edit') . ' `' . $data->role_name .'`' ?>">
										<i class="fa fa-pencil-square-o"></i>
									</a>
								</td>
							</tr>
							<?php ++$i; ?>
							<?php endforeach; ?>
							<?php else: ?>
							<tr>
								<td colspan="4"><?php echo sprintf(lang('label.empty_grid'), db_lang('page_title.' . $this->module)) ?></td>
							</tr>
							<?php endif; ?>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="box-footer">
				<div class="col-sm-12">
					<div class="pull-left">
						<?php echo $pagination['links'] ?>
					</div>
				</div>
			</div>
		</div>
		<?php echo form_close() ?>
	</section>
</div>