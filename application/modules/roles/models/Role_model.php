<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Role_model extends Base_Model
{
	protected $_table = 'roles';
	protected $_pk = 'role_id';

	public function __construct()
	{
		parent::__construct();
	}

	public function select($condition = array(), $limit = NULL)
	{
		// $result = parent::query("CALL sp_role_list");
		$result = $this->db->query("CALL sp_role_list");//var_dump($result->result());exit;
		return $result->result();
	}

	public function selectRoleMenus($condition = array())
	{
		$this->_table = 'role_menus';
		$result = parent::rows(array('select' => 'menu_id') + $condition);
		return $result;
	}

	public function update($data)
	{
		$selected_menu_id = '';
		foreach ($data['menu_id'] as $key => $value) {
			$selected_menu_id = $selected_menu_id .','.$value;
		}
		$selected_menu_id = substr($selected_menu_id, 1);
		
		$result = parent::query("CALL sp_role_update('{$data['role_id']}','{$selected_menu_id}','{$data['userid']}','{$data['ip_address']}','{$data['browser']}','{$data['status']}','{$data['method_name']}','{$data['module_name']}','{$data['historydetail']}')");
		
		return $result[0]['jml'] == 1 ? true : false;
	}

}