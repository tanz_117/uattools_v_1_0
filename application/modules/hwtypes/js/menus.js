$(function () {
	$(document).on('keyup', '#menu_name', function () {
		$('#menu_url').val($(this).val().split(' ').join('_'));
	});

	$(document).on('keyup', '#menu_url', function () {
		$(this).val($(this).val().split(' ').join('_'));
	});

	$(document).on('change', '#menu_parent', function () {
		if ($(this).val() != '') {
			$.post(CURRENT + '/menuOrder', {id: $(this).val()}, function (data) {
				$('#menu_order').prop('readonly', false).val(data);
			})
		} else {
			$('#menu_order').prop('readonly', false).val('');
		}
	});

	$(document).on('show.bs.modal', '#iconList', function () {
		$('.modal .modal-body').css('overflow-y', 'auto');
		$('.modal .modal-body').css('max-height', $(window).height() * 0.7);
	});
});

function fillIconField(ico) {
	$('#menu_icon').val(ico);
	$('#icoList').closeModal();
}
