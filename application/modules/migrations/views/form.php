<div class="nav-tabs-custom">
	<ul class="nav nav-tabs pull-right">
		<li class=""><a href="#tab_remark" data-toggle="tab" aria-expanded="false">Remark</a></li>
		<li class=""><a href="#tab_information" data-toggle="tab" aria-expanded="false">Information</a></li>
		<li class=""><a href="#tab_hardware" data-toggle="tab" aria-expanded="false">Hardware</a></li>
		<li class="active"><a href="#tab_user" data-toggle="tab" aria-expanded="true">User</a></li>
		<li class="pull-left header"><i class="fa fa-th"></i> Migration document</li>
	</ul>
	<?php echo form_open_multipart(uri_string(), 'id="form_upload" class="crud_upload" autocomplete="off" ') ?>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_user">
			<div class="box-body">
				<div class="row form-group">
					<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('user_id') ?></label>
					<div class="col-sm-9">
						<?php echo form_input('user_id', $row->user_id, 'class="form-control" placeholder="'. db_lang('user_id') .'"') ?>
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('name') ?></label>
					<div class="col-sm-9">
						<?php echo form_input('name', $row->name, 'class="form-control" placeholder="'. db_lang('name') .'"') ?>
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('company_id') ?></label>
					<div class="col-sm-9">
						<?php echo form_input('company_id', $row->company_id, 'class="form-control" placeholder="'. db_lang('company_id') .'"') ?>
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('phone') ?></label>
					<div class="col-sm-9">
						<?php echo form_input('phone', $row->phone, 'class="form-control" placeholder="'. db_lang('phone') .'"') ?>
					</div>
				</div>
			</div>
		</div>
		<!-- /.tab-pane -->
		<div class="tab-pane" id="tab_hardware">
			<div class="box-body">
				<div class="row form-group">
					<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('asset_tag') ?></label>
					<div class="col-sm-9">
						<?php echo form_input('asset_tag', $row->asset_tag, 'class="form-control" placeholder="'. db_lang('asset_tag') .'"') ?>
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('serial_number') ?></label>
					<div class="col-sm-9">
						<?php echo form_input('serial_number', $row->serial_number, 'class="form-control" placeholder="'. db_lang('serial_number') .'"') ?>
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('hostname_id') ?></label>
					<div class="col-sm-9">
						<?php echo form_input('hostname_id', $row->hostname_id, 'class="form-control" placeholder="'. db_lang('hostname_id') .'"') ?>
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('ipaddress') ?></label>
					<div class="col-sm-9">
						<?php echo form_input('ipaddress', $row->ipaddress, 'class="form-control" placeholder="'. db_lang('ipaddress') .'"') ?>
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('serial_number') ?></label>
					<div class="col-sm-9">
						<?php echo form_input('serial_number', $row->serial_number, 'class="form-control" placeholder="'. db_lang('serial_number') .'"') ?>
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('asset_tag') ?></label>
					<div class="col-sm-9">
						<?php echo form_input('asset_tag', $row->asset_tag, 'class="form-control" placeholder="'. db_lang('asset_tag') .'"') ?>
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('hardware_name') ?></label>
					<div class="col-sm-9">
						<?php echo form_input('hardware_name', $row->hardware_name, 'class="form-control" placeholder="'. db_lang('hardware_name') .'"') ?>
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('hw_type_name') ?></label>
					<div class="col-sm-9">
						<?php echo form_input('hw_type_name', $row->hw_type_name, 'class="form-control" placeholder="'. db_lang('hw_type_name') .'"') ?>
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('memmory') ?></label>
					<div class="col-sm-9">
						<?php echo form_input('memmory', $row->memmory, 'class="form-control" placeholder="'. db_lang('memmory') .'"') ?>
					</div>
				</div>
				<div class="row form-group">
					<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('hdd') ?></label>
					<div class="col-sm-9">
						<?php echo form_input('hdd', $row->hdd, 'class="form-control" placeholder="'. db_lang('hdd') .'"') ?>
					</div>
				</div>
			</div>
		</div>
		<!-- /.tab-pane -->
		<div class="tab-pane" id="tab_information">
			<div class="box-body">
				<table>
					<thead>
						<tr>
							<th><?php echo lang('information') ?></th>
							<th><?php echo lang('value_before') ?></th>
							<th><?php echo lang('note_before') ?></th>
							<th><?php echo lang('value_after') ?></th>
							<th><?php echo lang('note_after') ?></th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
		<!-- /.tab-pane -->
		<div class="tab-pane" id="tab_remark">
			<div class="box-body">
				<div class="row form-group">
					<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('remark') ?></label>
					<div class="col-sm-9">
						<?php echo form_input('remark', $row->remark, 'class="form-control" placeholder="'. db_lang('remark') .'"') ?>
					</div>
				</div>
			</div>
		</div>
		<!-- /.tab-pane -->
	</div>
	<!-- /.tab-content -->
	<?php echo form_close() ?>
</div>