<?php defined('BASEPATH') OR exit('No direct script access allowed');
class migration_model extends Base_Model
{
	protected $_table = 'migrations';
	protected $_pk = 'id';

	public function __construct()
	{
		parent::__construct();
	}

	public function insert($data)
	{
		$result = parent::query("CALL sp_migration_insert('{$data['name']}','{$data['userid']}','{$data['ip_address']}','{$data['browser']}','{$data['status']}','{$data['method_name']}','{$data['module_name']}','{$data['historydetail']}')");		
		return $result[0]['jml'] == 1 ? true : false;
	}

	public function update($data)
	{
		$result = parent::query("CALL sp_migration_update('{$data['id']}','{$data['name']}','{$data['userid']}','{$data['ip_address']}','{$data['browser']}','{$data['status']}','{$data['method_name']}','{$data['module_name']}','{$data['historydetail']}')");		
		return $result[0]['jml'] == 1 ? true : false;
	}
}