<?php defined('BASEPATH') OR exit('No direct script access allowed');
class User_model extends Base_Model
{
	protected $_table = 'users';
	protected $_pk = 'id';

	public function __construct()
	{
		parent::__construct();
	}

	public function insert($data)
	{
		$result = parent::query("CALL sp_user_insert('{$data['id']}','{$data['name']}',{$data['company_id']},'{$data['phone']}','{$data['status_id']}','{$data['site_group_id']}','{$data['unit_id']}','{$data['role_id']}','{$data['userid']}','{$data['ip_address']}','{$data['browser']}','{$data['status']}','{$data['method_name']}','{$data['module_name']}','{$data['historydetail']}')");		
		return $result[0]['jml'] == 1 ? true : false;
	}
	
	public function delete($data)
	{
		$result = parent::query("CALL sp_user_delete('{$data['id']}','{$data['userid']}','{$data['ip_address']}','{$data['browser']}','{$data['status']}','{$data['method_name']}','{$data['module_name']}','{$data['historydetail']}')");
		return $result[0]['jml'] == 1 ? true : false;
	}

	public function update($data, $id)
	{
		$result = parent::query("CALL sp_user_update('{$id}','{$data['name']}',{$data['company_id']},'{$data['phone']}','{$data['status_id']}','{$data['site_group_id']}','{$data['unit_id']}','{$data['role_id']}','{$data['userid']}','{$data['ip_address']}','{$data['browser']}','{$data['status']}','{$data['method_name']}','{$data['module_name']}','{$data['historydetail']}')");
		return $result[0]['jml'] == 1 ? true : false;
	}

	public function status($data,$id)
	{
		$result = parent::query("CALL sp_users_status('{$id}',{$data['user_status']},'{$data['userid']}','{$data['ip_address']}','{$data['browser']}','{$data['status']}','{$data['method_name']}','{$data['module_name']}','{$data['historydetail']}')");
		return $result[0]['jml'] == 1 ? true : false;
	}
}