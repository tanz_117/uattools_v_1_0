<div class="box-typical box-typical-padding">
	<div class="box box-primary box-solid">
		<div class="box-header bg-olive with-border">
			<h3 class="m-t-lg with-border"><?php echo lang('label.' . $this->method) . ' ' . db_lang('page_title.' . $this->module) ?></h3>
		</div>
		<?php echo form_open(uri_string(), 'id="form_data" class="crud" autocomplete="off"') ?>
		<div class="box-body">
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('user_identity') ?></label>
				<div class="col-sm-9">
					<?php echo form_input('user_identity', $row->user_identity, 'class="form-control" placeholder="'. db_lang('user_identity') .'" disabled') ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('user_name') ?></label>
				<div class="col-sm-9">
					<?php echo form_input('user_name', $row->user_name, 'class="form-control" placeholder="'. db_lang('user_name') .'" disabled') ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('user_password') ?></label>
				<div class="col-sm-9">
					<?php echo form_password('user_password', '', 'class="form-control" placeholder="'. db_lang('user_password') .'"') ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('user_password_confirm') ?></label>
				<div class="col-sm-9">
					<?php echo form_password('user_password_confirm', '', 'class="form-control" placeholder="'. db_lang('user_password_confirm') .'"') ?>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-sm-9 col-sm-push-2">
					<button type="submit" class="btn bg-olive btn-flat submit"><i class="glyphicon glyphicon-ok"></i>&nbsp;<?php echo lang('buttons.save') ?></button>
					<button class="btn bg-maroon btn-flat cancel"><i class="glyphicon glyphicon-remove"></i>&nbsp;<?php echo lang('buttons.cancel') ?></button>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close() ?>
</div>