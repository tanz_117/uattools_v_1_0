<div class="box-typical box-typical-padding">
	<div class="box box-solid">
		<div class="box-header bg-olive with-border">
			<h3 class="m-t-lg with-border"><?php echo lang('label.' . $this->method) . ' ' . db_lang('page_title.' . $this->module) ?></h3>
		</div>
		<?php echo form_open(uri_string(), 'id="form_data" class="crud" autocomplete="off"') ?>
		<div class="box-body">
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('id') ?></label>
				<div class="col-sm-9">
					<?php echo form_input('id', $row->id, 'class="form-control" placeholder="'. db_lang('id') .'"') ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('name') ?></label>
				<div class="col-sm-9">
					<?php echo form_input('name', $row->name, 'class="form-control" placeholder="'. db_lang('name') .'"') ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('role_name') ?></label>
				<div class="col-sm-9">
					<?php
					$items = array('' => lang('select.pick')) + $rolelist ;
					echo form_dropdown('role_id', $items, $row->role_id, 'class="form-control"');
					?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('company_id') ?></label>
				<div class="col-sm-9">
					<?php
					$items = array('' => lang('select.pick')) + $companylist ;
					echo form_dropdown('company_id', $items, $row->company_id, 'class="form-control"');
					?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('status_id') ?></label>
				<div class="col-sm-9">
					<?php
					$items = array('' => lang('select.pick')) + $statuslist ;
					echo form_dropdown('status_id', $items, $row->status_id, 'class="form-control"');
					?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('site_group_id') ?></label>
				<div class="col-sm-9">
					<?php
					$items = array('' => lang('select.pick')) + $sitegrouplist ;
					echo form_dropdown('site_group_id', $items, $row->site_group_id, 'class="form-control"');
					?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('unit_id') ?></label>
				<div class="col-sm-9">
					<?php
					$items = array('' => lang('select.pick')) + $unitlist ;
					echo form_dropdown('unit_id', $items, $row->unit_id, 'class="form-control"');
					?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('phone') ?></label>
				<div class="col-sm-9">
					<?php echo form_input('phone', $row->phone, 'class="form-control" placeholder="'. db_lang('phone') .'"') ?>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-sm-9 col-sm-push-2">
					<button type="submit" class="btn bg-olive btn-flat submit"><i class="glyphicon glyphicon-ok"></i>&nbsp;<?php echo lang('buttons.save') ?></button>
					<button class="btn bg-maroon btn-flat cancel"><i class="glyphicon glyphicon-remove"></i>&nbsp;<?php echo lang('buttons.cancel') ?></button>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close() ?>
</div>