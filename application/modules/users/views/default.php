<?php $column_names = array('id','name','role_name','site_group_name','unit_name','status_id'); ?>
<div id="content">
	<div class="box box-solid collapsed-box">
		<div class="box-header bg-olive disabled color-palette">
			<h3 class="box-title"><?php echo db_lang('search') ?></h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Expand" style="color:white">
				<i class="fa fa-plus"></i>
				</button>
			</div>
		</div>
		<?php echo form_open(uri_string(), 'id="form_filter" autocomplete="off"') ?>
		<div class="box-body">
			<div class="row">
				<div class="col-sm-6">
					<fieldset class="form-group">
						<label class="form-label semibold"><?php echo db_lang('id') ?></label>
						<?php echo form_input('id', userdata($this->module . '.search.id'), 'class="form-control" placeholder="'. db_lang('id') .'"') ?>
					</fieldset>
				</div>
				<div class="col-sm-6">
					<fieldset class="form-group">
						<label class="form-label semibold"><?php echo db_lang('name') ?></label>
						<?php echo form_input('name', userdata($this->module . '.search.name'), 'class="form-control" placeholder="'. db_lang('name') .'"') ?>
					</fieldset>
				</div>
				<div class="col-sm-12">
					<div class="btn-group pull-right">
						<button type="submit" class="btn bg-aqua color-palette btn-flat" style="margin-right: 3px"><i class="glyphicon glyphicon-search"></i>&nbsp;<?php echo lang('buttons.search') ?></button>
						<button class="btn bg-maroon-active color-palette btn-flat" id="reset"><i class="glyphicon glyphicon-repeat"></i>&nbsp;<?php echo lang('buttons.reset') ?></button>
					</div>
				</div>
			</div>
		</div>
		<?php echo form_close() ?>
	</div>
	<section class="box-typical box-typical-padding" id="default">
		<?php echo form_open(uri_string(), 'name="datas_table" id="form_table"'); ?>
		<div class="box box-solid">
			<div class="box-header bg-olive-active color-palette">
				<h3 class="box-title"><?php echo db_lang('page_title.' . $this->module) ?></h3>
				<div class="box-tools" >
					<div class="form-control input-sm">
						<?php echo sprintf(lang('table.per_page'), table_per_page_options()) ?>
					</div>
				</div>
			</div>
			<div class="box-body table-responsive no-padding">
				<table class="table">
					<tbody>
						<tr>
							<th style="width:5%">
								<div>
									<?php echo form_checkbox(array('name' => 'action_to_all', 'id' => 'check_all', 'class' => 'minimal', )); ?>
								</div>
							</th>
							<?php foreach ($column_names as $column_name): ?>
								<th><?php echo db_lang($column_name) ?></th>
							<?php endforeach ?>
							<th style="width:20%"><?php echo lang('label.action') ?></th>
						</tr>
						<tr>
							<?php if (!empty($datas)): ?>
							<?php $i = 1; ?>
							<?php foreach ($datas as $data): ?>
							<tr>
								<td>
									<div>
										<label>
											<?php echo form_checkbox(array('name' => 'action_to[]', 'value' => $data->id, 'class' => 'minimal', 'id' => 'data'.$i)); ?>
										</label>
									</div>
								</td>
								<?php foreach ($column_names as $column_name): ?>
									<?php if($column_name == 'status_id'): ?>
										<td>
											<small class="label bg-<?php echo $data->{$column_name} == '1' ? 'olive' : 'maroon' ?>">
												<?php echo lang($data->{$column_name} == '1' ? 'select.active' : 'select.no_active') ?>
											</small>									
										</td>
									<?php else: ?>
										<td><?php echo $data->{$column_name} ?></td>
									<?php endif ?>
								<?php endforeach ?>
 								<td>
									<div class="btn-group pull-left" role="button">
										<a href="<?php echo site_url($this->module . '/view/' .$data->id) ?>" class="btn bg-purple color-palette btn-sm" rel="view" data-toggle="tooltip" data-placement="left" title="<?php echo lang('label.view') . ' `' . $data->id .'`' ?>">
											<i class="glyphicon glyphicon-info-sign"></i>
										</a>
										<a href="<?php echo site_url($this->module . '/edit/' .$data->id) ?>" class="btn bg-maroon-active color-palette btn-sm" rel="edit" data-toggle="tooltip" data-placement="left" title="<?php echo lang('label.edit') . ' `' . $data->id .'`' ?>">
											<i class="fa fa-pencil-square-o"></i>
										</a>
										<a href="<?php echo site_url($this->module . '/delete/' .$data->id) ?>" class="btn bg-purple btn-sm" rel="delete" data-toggle="tooltip" data-placement="left" title="<?php echo lang('label.delete') . '`'. $data->id .'`' ?>">
											<i class="fa fa-trash-o"></i>
										</a>
										<!-- <a href="<?php //echo site_url($this->module . '/status/' .$data->id) ?>" class="btn bg-maroon-active color-palette btn-sm" rel="status" data-toggle="tooltip" data-placement="right" title="<?php echo sprintf(lang('label.status'),$data->id,$data->user_status == 1 ? 'Inactive' : 'active') ?>">
											<i class="glyphicon glyphicon-off"></i>
										</a> -->
										<!-- <a href="<?php //echo site_url($this->module . '/resetpassword/' .$data->id) ?>" class="btn bg-purple btn-sm" rel="ajax" data-toggle="tooltip" data-placement="left" title="<?php echo lang('label.resetpassword') . '`'. $data->id .'`' ?>">
											<i class="glyphicon glyphicon-repeat"></i>
										</a> -->
									</div>
								</td>
							</tr>
							<?php ++$i; ?>
							<?php endforeach; ?>
							<?php else: ?>
							<tr>
								<td colspan="6"><?php echo sprintf(lang('label.empty_grid'), db_lang('page_title.' . $this->module)) ?></td>
							</tr>
							<?php endif; ?>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="box-footer">
				<div class="col-sm-12">
					<div class="pull-left">
						<?php echo $pagination['links'] ?>
					</div>
					<div class="pull-right">
						<a href="<?php echo site_url($this->module . '/add') ?>" class="btn bg-olive color-palette btn-flat"  rel="ajax" style="margin-right: 3px">
							<i class="fa fa-plus"></i> <?php echo lang('label.add'). ' ' . db_lang('page_title.' . $this->module) ?>
						</a>
						<button type="submit" class="delete_selected btn bg-maroon btn-flat pull-right">
						<i class="glyphicon glyphicon-trash"></i>&nbsp;<?php echo lang('buttons.delete_selected') ?>
						</button>
					</div>
				</div>
			</div>
		</div>
		<?php echo form_close() ?>
	</section>
</div>






