<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->module . '/User_model', '_user_model');
		$this->load->helper('modal');
		$this->load->library('form_validation');
		$this->_is_ajax = $this->is_ajax() || $this->input->post('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest';
		$this->_is_ajax ? $this->template->set_layout(FALSE) : '';
	}

	public function index()
	{
		$this->load->helper('table');
		$ref_array = explode('/', array_key_exists('HTTP_REFERER', $_SERVER) ? $_SERVER['HTTP_REFERER'] : '');
		if (!in_array($this->module, $ref_array)) {
			$this->session->set_userdata($this->module . '.sort.field', '');
			$this->session->set_userdata($this->module . '.sort.dir', '');
			$this->session->set_userdata($this->module . '.current_page', '');
			$this->session->set_userdata($this->module . '.search.id', '');
			$this->session->set_userdata($this->module . '.search.name', '');
		}

		$baseCondition = $condition = array();
		if ($this->input->post()) {
			$post = $this->input->post_sanitized();
			foreach($post as $key => $val) {
				if (!in_array($key, array('sortfield', 'sortdir', 'current_page', 'per_page'))){
					$this->session->set_userdata($this->module . '.search.' . $key, $val);
					$condition = $condition + array($key => userdata($this->module . '.search.'.$key));}
			}
			$baseCondition = $baseCondition + array('like' => $condition);
		}

		$base_order = array();
		if ($this->input->post('sortfield')) {
			$this->session->set_userdata($this->module . '.sort.field', $this->input->post('sortfield'));
			$this->session->set_userdata($this->module . '.sort.dir', $this->input->post('sortdir'));
		}

        		$base_order = $this->session->userdata($this->module . '.sort.field') ? $base_order + array(trim($this->session->userdata($this->module . '.sort.field') . ' ' . $this->session->userdata($this->module . '.sort.dir'))) : $base_order;

		if (array_key_exists('current_page', $_POST)) {
			// set paging by user
			$this->session->set_userdata($this->module . '.current_page', $this->input->post('current_page'));
		}

		if (array_key_exists('per_page', $_POST)) {
			$this->session->set_userdata($this->module . '.' . $this->method . '.per_page', $this->input->post('per_page'));
		}

		if($baseCondition){
			$baseCondition['like']['users.id'] = $baseCondition['like']['id'];
			unset($baseCondition['like']['id']);
			$baseCondition['like']['users.name'] = $baseCondition['like']['name'];
			unset($baseCondition['like']['name']);
		}

		$pagination = create_pagination($this->module, $this->_user_model->count($baseCondition));
		
		if ($this->input->post('sortfield')) {
			$this->session->set_userdata($this->module . '.sort.field', $this->input->post_sanitized('sortfield'));
			$this->session->set_userdata($this->module . '.sort.dir', $this->input->post_sanitized('sortdir'));
			$baseCondition = $this->session->set_userdata($this->module . '.sort.field', $this->input->post('sortfield')) ? $baseCondition + array('order_by' => array(userdata($this->module . '.sort.field'), userdata($this->module . '.sort.dir'))) : $baseCondition;
		}


		$datas = $this->_user_model->rows(array(
			'select' => 'users.id, users.name, company_id, companies.name as company_name, phone, status_id, site_group_id, site_groups.name as site_group_name, unit_id, units.name as unit_name, users.role_id, roles.role_name',
			'join' => array(
							array('roles','users.role_id = roles.role_id'),
							array('site_groups','users.site_group_id = site_groups.id'),
							array('units','users.unit_id = units.id'),
							array('companies','users.company_id = companies.id'),
						),
			'order_by' => array(array('status_id', 'DESC'),array('name','ASC')),
			'limit' => $pagination['limit']
		) + $baseCondition);

		$rolelist = $this->_getrolelist();

		$this->template
			->set('pagination', $pagination)
			->set('datas', $datas)
			->set('rolelist', $rolelist)
			->title(db_lang('page_title.' . $this->module))
			->build('default', array());
	}
	
	public function add()
	{
		if (!$this->_is_ajax) {
			$this->session->set_flashdata('notice', lang('auth.no_direct_link'));
			redirect($this->module);
		}

		$validation_rules = array(
			array(
				'field' => 'id',
				'label' => db_lang('id'),
				'rules' => 'required|trim|max_length[5]|min_length[4]|alpha_numeric|callback__check_availablity'
			),
			array(
				'field' => 'name',
				'label' => db_lang('name'),
				'rules' => 'required|trim'
			),
			array(
				'field' => 'phone',
				'label' => db_lang('phone'),
				'rules' => 'required|trim'
			),
			array(
				'field' => 'role_id',
				'label' => db_lang('role_id'),
				'rules' => 'required|trim|numeric'
			),
			array(
				'field' => 'company_id',
				'label' => db_lang('company_id'),
				'rules' => 'required|trim|numeric'
			),
			array(
				'field' => 'status_id',
				'label' => db_lang('status_id'),
				'rules' => 'required|trim|numeric'
			),
			array(
				'field' => 'site_group_id',
				'label' => db_lang('site_group_id'),
				'rules' => 'required|trim|numeric'
			),
			array(
				'field' => 'unit_id',
				'label' => db_lang('unit_id'),
				'rules' => 'required|trim|numeric'
			)
		);

		$this->form_validation->set_rules($validation_rules);
		if ($this->form_validation->run()) {		
			// $_POST['user_password'] = hash('sha256', $_POST['user_password']);	
			$compare = set_before_after($_POST);
			$post = set_audit_history($_POST,$this->module, $this->method, $compare);

			if ($this->_user_model->insert($post)) {
				$message = sprintf(lang('notice.add_success'), $post['name']);
				$status = 'success';
			} else {
				$message = sprintf(lang('notice.add_error'), $post['name']);
				$status = 'error';
			}

			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}
		} elseif ($this->input->post()) {
			$message = $this->form_validation->error_string('', '');
			$status = 'error';
			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}
		} else {
			$row = new stdClass;
			foreach ($this->validation_rules as $rule) {
				$row->{$rule['field']} = set_value($rule['field']);
			}
		}


		$this->data->rolelist = $this->_getrolelist();
		$this->data->companylist = $this->_getcompanylist();
		$this->data->statuslist = array_for_select(config_item('status_activeinactive'), 'key', 'value');
		$this->data->sitegrouplist = $this->_getsitegrouplist();
		$this->data->unitlist = $this->_getunitlist();

		$this->data->row = $row;
		if ($this->_is_ajax && $this->input->post()) {
			echo json_encode(array('status' => 'error', 'message' => validation_errors()));
		} else {
			$this->template->build('form', $this->data);
		}
	}

	public function edit($id = '')
	{
		if (!$this->_is_ajax) {
			$this->session->set_flashdata('notice', lang('auth.no_direct_link'));
			redirect($this->module);
		}

		if (!$id) {
			redirect($this->module);
		}

		if($_POST && $id){
			$_POST['id'] = $id;
		}

		$row = $this->_user_model->rowById($id);

		$validation_rules = array(
			array(
				'field' => 'id',
				'label' => db_lang('id'),
				'rules' => 'required|trim|max_length[5]|min_length[4]|alpha_numeric|callback__check_availablity'
			),
			array(
				'field' => 'name',
				'label' => db_lang('name'),
				'rules' => 'required|trim'
			),
			array(
				'field' => 'phone',
				'label' => db_lang('phone'),
				'rules' => 'required|trim'
			),
			array(
				'field' => 'role_id',
				'label' => db_lang('role_id'),
				'rules' => 'required|trim|numeric'
			),
			array(
				'field' => 'company_id',
				'label' => db_lang('company_id'),
				'rules' => 'required|trim|numeric'
			),
			array(
				'field' => 'status_id',
				'label' => db_lang('status_id'),
				'rules' => 'required|trim|numeric'
			),
			array(
				'field' => 'site_group_id',
				'label' => db_lang('site_group_id'),
				'rules' => 'required|trim|numeric'
			),
			array(
				'field' => 'unit_id',
				'label' => db_lang('unit_id'),
				'rules' => 'required|trim|numeric'
			)
		);
		$this->form_validation->set_rules($validation_rules);
		if ($this->form_validation->run()) {
			$before = array(
							'id' => $row->id,
							'name' => $row->name,
							'role_id' => $row->role_id,
							'company_id' => $row->company_id,
							'phone' => $row->phone,
							'status_id' => $row->status_id,
							'site_group_id' => $row->site_group_id,
							'unit_id' => $row->unit_id
						);
			$compare = set_before_after($_POST,$before);
			$post = set_audit_history($_POST,$this->module, $this->method, $compare);

			if ($this->_user_model->update($post, $id)) {
				$message = sprintf(lang('notice.edit_success'), $post['id']);
				$status = 'success';
			} else {
				$message = sprintf(lang('notice.edit_error'), $post['id']);
				$status = 'error';
			}

			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}
		} elseif ($this->input->post()) {
			$message = $this->form_validation->error_string('', '');
			$status = 'error';
			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}
		} else {
			if ($this->input->post()) {
				$row = new stdClass;
				foreach ($this->validation_rules as $rule) {
					$row->{$rule['field']} = set_value($rule['field']);
				}
			}
		}

		$this->data->rolelist = $this->_getrolelist();
		$this->data->companylist = $this->_getcompanylist();
		$this->data->statuslist = array_for_select(config_item('status_activeinactive'), 'key', 'value');
		$this->data->sitegrouplist = $this->_getsitegrouplist();
		$this->data->unitlist = $this->_getunitlist();

		$this->data->row = $row;

		if ($this->_is_ajax && $this->input->post()) {
			echo json_encode(array('status' => 'error', 'message' => validation_errors()));
		} else {
			$this->template->build('form', $this->data);
		}
	}

	public function delete($id = '')
	{
		if (!$this->_is_ajax) {
			$this->session->set_flashdata('notice', lang('auth.no_direct_link'));
			redirect($this->module);
		}

		$ids = (!empty($id)) ? array($id) : $this->input->post('action_to');
		if (!empty($ids)) {
			$deleted = 0;
			$to_delete = 0;
			foreach ($ids as $id) {
				$_POST['id'] = $id;
				$compare = set_before_after(array(),$_POST);
				$post = set_audit_history($_POST,$this->module, $this->method, $compare);
				if ($this->_user_model->delete($post)) {
					$deleted++;
				} else {
					$status = 'error';
					$message = sprintf(lang('notice.delete_error'), $id);
				}
				$to_delete++;
			}
			if ($deleted > 0) {
				$status = 'success';
				$message = sprintf(lang('notice.delete_success'), $deleted, $to_delete);
			}
		} else {
			$status = 'notice';
			$message = lang('notice.no_select_error');
		}

		if ($this->_is_ajax) {
			echo json_encode(array('status' => $status, 'message' => $message));
			return;
		} else {
			$this->session->set_flashdata($status, $message);
			redirect($this->module);
		}
	}

	public function view($id = '')
	{
		if (!$this->_is_ajax) {
			$this->session->set_flashdata('notice', lang('auth.no_direct_link'));
			redirect($this->module);
		}

		if (!$id) {
			redirect($this->module);
		}

		$this->data->rolelist = $this->_getrolelist();
		$this->data->companylist = $this->_getcompanylist();
		$this->data->statuslist = array_for_select(config_item('status_activeinactive'), 'key', 'value');
		$this->data->sitegrouplist = $this->_getsitegrouplist();
		$this->data->unitlist = $this->_getunitlist();

		$this->data->row = $this->_user_model->row(
			array(
				'select' => 'users.id, users.name, company_id, companies.name as company_name, phone, status_id, site_group_id, site_groups.name as site_group_name, unit_id, units.name as unit_name, users.role_id, roles.role_name,users.created_at,users.updated_at,users.deleted_at',
				'join' => array(
					array('roles','users.role_id = roles.role_id','left'),
					array('site_groups','users.site_group_id = site_groups.id','left'),
					array('units','users.unit_id = units.id','left'),
					array('companies','users.company_id = companies.id','left')
				),
				'where' => array('users.id' => $id)
			)
		);

		if ($this->_is_ajax && $this->input->post()) {
			echo json_encode(array('status' => 'error', 'message' => validation_errors()));
		} else {
			$this->template->build('view', $this->data);
		}
	}
	
	private function _getrolelist()
	{
		$this->load->model('roles/Role_model', '_role_model');
		$row = $this->_role_model->rows();
		return array_for_select($row, 'role_id', 'role_name');
	}

	private function _getcompanylist()
	{
		$this->load->model('companies/company_model', '_company_model');
		$row = $this->_company_model->rows();
		return array_for_select($row, 'id', 'name');
	}

	private function _getsitegrouplist()
	{
		$this->load->model('sitegroups/sitegroup_model', '_sitegroup_model');
		$row = $this->_sitegroup_model->rows();
		return array_for_select($row, 'id', 'name');
	}

	private function _getunitlist()
	{
		$this->load->model('units/unit_model', '_unit_model');
		$row = $this->_unit_model->rows();
		return array_for_select($row, 'id', 'name');
	}

	public function _check_availablity($user_identity)
	{
		$needCheck = true;
		if($this->method == 'edit'){
			$current_data = $this->_user_model->row(array('where' => array('id' => $_POST['id'])));
			$needCheck = $current_data->id == $user_identity ? false : true;
		}

		if($this->method == 'add' || $needCheck == true){
			$existing_data = $this->_user_model->count(array('where' => array('id' => $user_identity)));
			if($existing_data > 0){
				$this->form_validation->set_message('_check_availablity', sprintf(lang('user_id_not_available'),$user_identity));
				return false;
			}
		}
		return true;
	}
}