<?php $column_names = array('name', 'hostname_name', 'ipaddress', 'asset_tag', 'serial_number', 'hw_type_name'); ?>
<div id="content">
	<div class="box box-solid collapsed-box">
		<div class="box-header bg-olive disabled color-palette">
			<h3 class="box-title"><?php echo db_lang('search') ?></h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Expand" style="color:white">
				<i class="fa fa-plus"></i>
				</button>
			</div>
		</div>
		<?php echo form_open(uri_string(), 'id="form_filter" autocomplete="off"') ?>
		<div class="box-body">
			<div class="row">
				<div class="col-sm-12">
					<fieldset class="form-group">
						<label class="form-label semibold"><?php echo db_lang('name') ?></label>
						<?php echo form_input('name', userdata($this->module . '.search.name'), 'class="form-control" placeholder="'. db_lang('name') .'"') ?>
					</fieldset>
				</div>
				<div class="col-sm-12">
					<div class="btn-group pull-right">
						<button type="submit" class="btn bg-aqua color-palette btn-flat" style="margin-right: 3px"><i class="glyphicon glyphicon-search"></i>&nbsp;<?php echo lang('buttons.search') ?></button>
						<button class="btn bg-maroon-active color-palette btn-flat" id="reset"><i class="glyphicon glyphicon-repeat"></i>&nbsp;<?php echo lang('buttons.reset') ?></button>
					</div>
				</div>
			</div>
		</div>
		<?php echo form_close() ?>
	</div>
	<section class="box-typical box-typical-padding" id="default">
		<?php echo form_open(uri_string(), 'name="datas_table" id="form_table"'); ?>
		<div class="box box-solid">
			<div class="box-header bg-olive-active color-palette">
				<h3 class="box-title"><?php echo db_lang('page_title.' . $this->module) ?></h3>
			</div>
			<div class="box-body table-responsive no-padding">
				<table class="table">
					<tbody>
						<tr>
							<?php foreach ($column_names as $column_name): ?>
								<th><?php echo db_lang($column_name) ?></th>
							<?php endforeach ?>
							<th style="width:15%"><?php echo lang('label.action') ?></th>
						</tr>
						<tr>
							<?php if (!empty($datas)): ?>
							<?php $i = 1; ?>
							<?php foreach ($datas as $data): ?>
							<tr>
								<?php foreach ($column_names as $column_name): ?>
									<td><?php echo $data->{$column_name} ?></td>
								<?php endforeach ?>
 								<td>
									<div class="btn-group" role="button">
										<a href="<?php echo site_url($this->module . '/edit/' .$data->id) ?>" class="btn bg-purple btn-sm" rel="edit" data-toggle="tooltip" title="<?php echo lang('label.edit') . ' `' . $data->name .'`' ?>">
											<i class="fa fa-pencil-square-o"></i>
										</a>
									</div>
								</td>
							</tr>
							<?php ++$i; ?>
							<?php endforeach; ?>
							<?php else: ?>
							<tr>
								<td colspan="4"><?php echo sprintf(lang('label.empty_grid'), db_lang('page_title.' . $this->module)) ?></td>
							</tr>
							<?php endif; ?>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="box-footer">
				<div class="col-sm-12">
					<div class="pull-left">
						<?php echo $pagination['links'] ?>
					</div>
					<div class="btn-group pull-right">
						<a href="<?php echo site_url($this->module . '/add') ?>" class="btn bg-olive"  rel="ajax">
							<i class="fa fa-plus"></i> <?php echo lang('label.add'). ' ' . db_lang('page_title.' . $this->module) ?>
						</a>
					</div>
				</div>
			</div>
		</div>
		<?php echo form_close() ?>
	</section>
</div>