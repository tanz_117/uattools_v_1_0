<div class="box-typical box-typical-padding">
	<div class="box box-solid">
		<div class="box-header bg-olive with-border">
			<h3 class="m-t-lg with-border"><?php echo lang('label.' . $this->method) . ' ' . db_lang('page_title.' . $this->module) ?></h3>
		</div>
		<?php echo form_open_multipart(uri_string(), 'id="form_upload" class="crud_upload" autocomplete="off" ') ?>
		<div class="box-body">
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('name') ?></label>
				<div class="col-sm-9">
					<?php 
						if($this->method == 'edit'){
							echo form_hidden('id', $row->id, 'class="form-control"');
						}
						echo form_input('name', $row->name, 'class="form-control" placeholder="'. db_lang('name') .'"') 
					?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('hostname_id') ?></label>
				<div class="col-sm-9">
					<?php
					$items = array('' => lang('select.pick')) + $hostnamelist ;
					echo form_dropdown('hostname_id', $items, $row->hostname_id, 'class="form-control"');
					?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('ipaddress') ?></label>
				<div class="col-sm-9">
					<?php echo form_input('ipaddress', $row->ipaddress, 'class="form-control" placeholder="'. db_lang('ipaddress') .'"') ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('asset_tag') ?></label>
				<div class="col-sm-9">
					<?php echo form_input('asset_tag', $row->asset_tag, 'class="form-control" placeholder="'. db_lang('asset_tag') .'"') ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('serial_number') ?></label>
				<div class="col-sm-9">
					<?php echo form_input('serial_number', $row->serial_number, 'class="form-control" placeholder="'. db_lang('serial_number') .'"') ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('hw_type_id') ?></label>
				<div class="col-sm-9">
					<?php
					$items = array('' => lang('select.pick')) + $hwtypelist ;
					echo form_dropdown('hw_type_id', $items, $row->hw_type_id, 'class="form-control"');
					?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('memory') ?></label>
				<div class="col-sm-9">
					<?php echo form_input('memory', $row->memory, 'class="form-control" placeholder="'. db_lang('memory') .'"') ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('hdd') ?></label>
				<div class="col-sm-9">
					<?php echo form_input('hdd', $row->hdd, 'class="form-control" placeholder="'. db_lang('hdd') .'"') ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="col-sm-2 form-control-label semibold"><?php echo db_lang('status_id') ?></label>
				<div class="col-sm-9">
					<?php
					$items = array('' => lang('select.pick')) + $statuslist ;
					echo form_dropdown('status_id', $items, $row->status_id, 'class="form-control"');
					?>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-sm-9 col-sm-push-2">
					<button type="submit" class="btn bg-olive btn-flat submit"><i class="glyphicon glyphicon-ok"></i>&nbsp;<?php echo lang('buttons.save') ?></button>
					<button class="btn bg-maroon btn-flat cancel"><i class="glyphicon glyphicon-remove"></i>&nbsp;<?php echo lang('buttons.cancel') ?></button>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close() ?>
</div>