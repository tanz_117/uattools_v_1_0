<?php defined('BASEPATH') OR exit('No direct script access allowed');
class hardware_model extends Base_Model
{
	protected $_table = 'hardwares';
	protected $_pk = 'id';

	public function __construct()
	{
		parent::__construct();
	}

	public function insert($data)
	{
		$result = parent::query("CALL sp_hardware_insert('{$data['name']}', '{$data['hostname_id']}', '{$data['ipaddress']}', '{$data['asset_tag']}', '{$data['serial_number']}', '{$data['hw_type_id']}', '{$data['memory']}', '{$data['hdd']}', '{$data['status_id']}','{$data['userid']}','{$data['ip_address']}','{$data['browser']}','{$data['status']}','{$data['method_name']}','{$data['module_name']}','{$data['historydetail']}')");		
		return $result[0]['jml'] == 1 ? true : false;
	}

	public function update($data,$id)
	{
		$result = parent::query("CALL sp_hardware_update('{$data['id']}','{$data['name']}', '{$data['hostname_id']}', '{$data['ipaddress']}', '{$data['asset_tag']}', '{$data['serial_number']}', '{$data['hw_type_id']}', '{$data['memory']}', '{$data['hdd']}', '{$data['status_id']}','{$data['userid']}','{$data['ip_address']}','{$data['browser']}','{$data['status']}','{$data['method_name']}','{$data['module_name']}','{$data['historydetail']}')");		
		return $result[0]['jml'] == 1 ? true : false;
	}
}