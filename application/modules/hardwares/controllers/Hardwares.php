<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Hardwares extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->module . '/hardware_model', '_hardwareModel');
		$this->load->helper('modal');
		$this->load->library('form_validation');

		$this->validation_rules = array(
			array(
				'field' => 'name',
				'label' => db_lang('name'),
				'rules' => 'required|trim'
			),
			array(
				'field' => 'hostname_id',
				'label' => db_lang('hostname_id'),
				'rules' => 'required|trim|numeric'
			),
			array(
				'field' => 'ipaddress',
				'label' => db_lang('ipaddress'),
				'rules' => 'required|trim'
			),
			array(
				'field' => 'asset_tag',
				'label' => db_lang('asset_tag'),
				'rules' => 'required|trim'
			),
			array(
				'field' => 'serial_number',
				'label' => db_lang('serial_number'),
				'rules' => 'required|trim'
			),
			array(
				'field' => 'hw_type_id',
				'label' => db_lang('hw_type_id'),
				'rules' => 'required|trim|numeric'
			),
			array(
				'field' => 'memory',
				'label' => db_lang('memory'),
				'rules' => 'required|trim|numeric'
			),
			array(
				'field' => 'hdd',
				'label' => db_lang('hdd'),
				'rules' => 'required|trim|numeric'
			),
			array(
				'field' => 'status_id',
				'label' => db_lang('status_id'),
				'rules' => 'required|trim|numeric'
			)
		);

		$this->form_validation->set_rules($this->validation_rules);
		$this->_is_ajax = $this->is_ajax() || $this->input->post('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest';
		$this->_is_ajax ? $this->template->set_layout(FALSE) : '';
	}

	public function index()
	{
		$this->load->helper('table');
		$ref_array = explode('/', array_key_exists('HTTP_REFERER', $_SERVER) ? $_SERVER['HTTP_REFERER'] : '');
		if (!in_array($this->module, $ref_array)) {
			$this->session->set_userdata($this->module . '.sort.field', '');
			$this->session->set_userdata($this->module . '.sort.dir', '');
			$this->session->set_userdata($this->module . '.current_page', '');
			$this->session->set_userdata($this->module . '.name', '');
		}

		$baseCondition = $condition = array();
		if ($this->input->post()) {
			$post = $this->input->post_sanitized();
			foreach($post as $key => $val) {
				if (!in_array($key, array('sortfield', 'sortdir', 'current_page', 'per_page'))){
					$this->session->set_userdata($this->module . '.search.' . $key, $val);
					$condition = $condition + array($key => userdata($this->module . '.search.'.$key));
				}
			}
			$baseCondition = $baseCondition + array('like' => $condition);
		}

		$base_order = array();
		if ($this->input->post('sortfield')) {
			$this->session->set_userdata($this->module . '.sort.field', $this->input->post('sortfield'));
			$this->session->set_userdata($this->module . '.sort.dir', $this->input->post('sortdir'));
		}

		$base_order = $this->session->userdata($this->module . '.sort.field') ? $base_order + array(trim($this->session->userdata($this->module . '.sort.field') . ' ' . $this->session->userdata($this->module . '.sort.dir'))) : $base_order;

		if (array_key_exists('current_page', $_POST)) {
			// set paging by user
			$this->session->set_userdata($this->module . '.current_page', $this->input->post('current_page'));
		}

		if (array_key_exists('per_page', $_POST)) {
			$this->session->set_userdata($this->module . '.' . $this->method . '.per_page', $this->input->post('per_page'));
		}

		$pagination = create_pagination($this->module, $this->_hardwareModel->count($baseCondition));
		
		if ($this->input->post('sortfield')) {
			$this->session->set_userdata($this->module . '.sort.field', $this->input->post_sanitized('sortfield'));
			$this->session->set_userdata($this->module . '.sort.dir', $this->input->post_sanitized('sortdir'));
			$baseCondition = $this->session->set_userdata($this->module . '.sort.field', $this->input->post('sortfield')) ? $baseCondition + array('order_by' => array(userdata($this->module . '.sort.field'), userdata($this->module . '.sort.dir'))) : $baseCondition;
		}

		$datas = $this->_hardwareModel->rows(array(
			'select' => 'hardwares.id, hardwares.name, h.name as hostname_name, hardwares.ipaddress,hardwares.asset_tag,hardwares.serial_number,ht.name as hw_type_name',
			'join' => array(
				array('hostnames h','h.id = hardwares.hostname_id'),
				array('hw_types ht','ht.id = hardwares.hw_type_id'),
			),
			'order_by' => array('name', 'ASC'),
			'limit' => $pagination['limit']
		) + $baseCondition);
		
		$this->template
			->set('pagination', $pagination)
			->set('datas', $datas)
			->title(db_lang('page_title.' . $this->module))
			->build('default', array());
	}

	public function add()
	{
		if (!$this->_is_ajax) {
			$this->session->set_flashdata('notice', lang('auth.no_direct_link'));
			redirect($this->module);
		}

		if ($this->form_validation->run()) {
			$compare = set_before_after($_POST);
			$post = set_audit_history($_POST,$this->module, $this->method, $compare);

			if ($this->_hardwareModel->insert($post)) {
				$message = sprintf(lang('notice.add_success'), $post['name']);
				$status = 'success';
			} else {
				$message = sprintf(lang('notice.add_error'), $post['name']);
				$status = 'error';
			}

			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}
		} elseif ($this->input->post()) {
			$message = $this->form_validation->error_string('', '');
			$status = 'error';
			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}
		} else {
			$row = new stdClass;
			foreach ($this->validation_rules as $rule) {
				$row->{$rule['field']} = set_value($rule['field']);
			}
		}

		$this->data->statuslist = array_for_select(config_item('status_activeinactive'), 'key', 'value');;
		$this->data->hwtypelist = $this->_gethwtypeslist();
		$this->data->hostnamelist = $this->_gethostnamelist();

		$this->data->row = $row;
		if ($this->_is_ajax && $this->input->post()) {
			echo json_encode(array('status' => 'error', 'message' => validation_errors()));
		} else {
			$this->template->build('form', $this->data);
		}
	}


	public function edit($id = '')
	{
		if (!$this->_is_ajax) {
			$this->session->set_flashdata('notice', lang('auth.no_direct_link'));
			redirect($this->module);
		}

		if (!$id) {
			redirect($this->module);
		}

		$row = $this->_hardwareModel->row(array('where' => array('id' => $id)));

		if ($this->form_validation->run()) {
			$before = array(
				'id' => $id,
				'name' => $row->name
			);

			$compare = set_before_after($_POST,$before);
			$post = set_audit_history($_POST,$this->module, $this->method, $compare);

			if ($this->_hardwareModel->update($post,$id)) {
				$message = sprintf(lang('notice.edit_success'), $row->name);
				$status = 'success';
			} else {
				$message = sprintf(lang('notice.edit_error'), $row->name);
				$status = 'error';
			}

			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}
		} elseif ($this->input->post()) {
			$message = $this->form_validation->error_string('', '');
			$status = 'error';
			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}
		} else {
			if ($this->input->post()) {
				$row = new stdClass;
				foreach ($this->validation_rules as $rule) {
					$row->{$rule['field']} = set_value($rule['field']);
				}
			}
		}

		$this->data->statuslist = array_for_select(config_item('status_activeinactive'), 'key', 'value');;
		$this->data->hwtypelist = $this->_gethwtypeslist();
		$this->data->hostnamelist = $this->_gethostnamelist();

		$this->data->row = $row;
		if ($this->_is_ajax && $this->input->post()) {
			echo json_encode(array('status' => 'error', 'message' => validation_errors()));
		} else {
			$this->template->build('form', $this->data);
		}
	}

	public function view()
	{
		if (!$this->_is_ajax) {
			$this->session->set_flashdata('notice', lang('auth.no_direct_link'));
			redirect($this->module);
		}

		$row = $this->_productModel->row(array('select' => 'produk_identity,produk_nama,produk_harga,produk_stok,produk_satuan,produk_status,produk_photo,produk_description'));
		$this->data->row = $row;
		if ($this->_is_ajax && $this->input->post()) {
			echo json_encode(array('status' => 'error', 'message' => validation_errors()));
		} else {
			$this->template->build('view', $this->data);
		}
	}

	private function _gethwtypeslist()
	{
		$this->load->model('hwtypes/hwtypes_model', '_hwtypes_model');
		$row = $this->_hwtypes_model->rows();
		return array_for_select($row, 'id', 'name');
	}

	private function _gethostnamelist()
	{
		$this->load->model('hostnames/hostname_model', '_hostname_model');
		$row = $this->_hostname_model->rows();
		return array_for_select($row, 'id', 'name');
	}

	public function _check_availability($produk_identity)
	{
		$needCheck = true;
		if($this->method == 'edit'){
			$current_data = $this->_productModel->row(array('where' => array('produk_id' => $_POST['produk_id'])));
			$needCheck = $current_data->produk_identity == $produk_identity ? false : true;
		}

		if($this->method == 'add' || $needCheck == true){
			$existing_data = $this->_productModel->count(array('where' => array('produk_identity' => $produk_identity)));
			if($existing_data > 0){
				$this->form_validation->set_message('_check_availability', sprintf(lang('user_id_not_available'),$user_identity));
				return false;
			}
		}
		return true;
	}
}