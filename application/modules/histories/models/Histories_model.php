<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Histories_model extends Base_model
{
	protected $_table = 'surat';
	protected $_pk = 'surat_id';

	public function __construct()
	{
		parent::__construct();
	}

	public function insert($input){
		$input = parent::unsanitizedParameter($input);
		parent::query("EXEC sp_temp_starter_module_insert ?,?,?,?,?,?,?,?,?",$input);
		return true;
	}

	public function update($input,$id){
		$input = $input + array($this->_pk => $id);
		$input = parent::unsanitizedParameter($input);
		parent::query("EXEC sp_temp_starter_module_update ?,?,?,?,?,?,?,?,?,?",$input);
		return true;
	}

	public function delete($input){
		$input = parent::unsanitizedParameter($input);
		$input = parent::unsanitizedParameter($input);
		parent::query("EXEC sp_temp_starter_module_delete ?,?,?,?,?,?,?,?",$input);
		return true;
	}

	public function changestatus($input){
		$input = parent::unsanitizedParameter($input);
		$input = parent::unsanitizedParameter($input);
		parent::query("EXEC sp_temp_starter_module_changestatus ?,?,?,?,?,?,?,?",$input);
		return true;
	}
}