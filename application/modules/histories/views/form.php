<div class="section">
	<div class="row">
		<div class="col s12 m12 l12">
			<div class="card-panel">
				<div class="row">
					<?php echo form_open(uri_string(), 'class="col s12 crud" id="form_data" autocomplete="off"'); ?>
					<div class="row">
						<div class="input-field col s12">
							<?php echo form_input('field_2',  $row->field_2,'id="field_2"') ?>
							<label class="active"><?php echo db_lang('field_2'); ?></label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<?php
								$select = array('' => lang('select.pick')) + $dropdownlist_item;
								echo form_dropdown('field_3',$select, $row->field_3, ' id="field_3" ')
							?>
							<label><?php echo db_lang('field_3'); ?></label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<button type="submit" class="submit btn cyan waves-effect waves-light">
							<i class="mdi-content-send left"></i> <?php echo lang('buttons.save') ?>
							</button>
							<?php confirm_modal(array('rel' => 'confirm-submit')); ?>
							<a href="javascript:void(0);" class="button btn orange waves-effect waves-light" value="cancel">
								<i class="mdi-av-replay left"></i> <?php echo lang('buttons.cancel') ?>
							</a>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>