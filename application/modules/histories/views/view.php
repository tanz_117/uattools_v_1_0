<div class="row">
	<div class="col s12 m12 l12">
		<div class="card light-blue">
			<div class="card-content white-text">
				<span class="card-title"><?php echo $mail->surat_nomor ?></span>
				<div class="ultra-small"><?php echo $mail->surat_perihal ?></div>
			</div>
		</div>
	</div>
</div>

<div class="timeline">
	<?php if (!empty($datas)): ?>
	<?php $i = 1; $div = 1; $unit = 1; $peg = 1; ?>
	<?php foreach ($datas as $key => $data): ?>
	<?php
		if($data->disposisi_type_tujuan == 1) {
			$disposisi_type_tujuan = db_lang('pindiv');
			$color = 'red';
		}
		else if($data->disposisi_type_tujuan == 2) {
			$disposisi_type_tujuan = db_lang('group_head');
			$color = 'blue';
		}
		else{
			$disposisi_type_tujuan = db_lang('staff');
			$color = 'green';
		}
	?>
	<div class="timeline-event">
		<div class="card timeline-content">
			<div class="card-content">
				<span class="card-title activator grey-text text-darken-4"><?php echo $disposisi_type_tujuan ?><i class="material-icons right" style="font-size:14px;"><?php echo date('Y-m-d', strtotime($data->disposisi_tgl)) ?></i></span>
				<p><?php echo db_lang('dari') ?> <b class="material-icons right"><a href="#"><?php echo $key == 0 ? $data->pengirim_divisi : $data->dari; ?></a></b></p>
				<p><?php echo db_lang('disposisi_tujuan') ?> <b class="material-icons right"><a href="#"><?php echo $data->nama ?></a></b></p>
				<blockquote><?php echo $data->disposisi_instruksi ?></blockquote>
			</div>
		</div>
		<div class="timeline-badge <?php echo $color; ?> white-text"><i class="mdi-action-account-circle"></i></div>
	</div>
	<?php endforeach; ?>
	<?php endif; ?>
</div>