<div class="section">
	<div class="row">
		<div class="col s12 m12 l12">
			<ul class="collapsible collapsible-accordion" data-collapsible="accordion">
				<li>
					<div class="collapsible-header light-blue light-blue-text text-lighten-5">
						<i class="mdi-action-search"></i> <?php echo lang('search_detail.label') ?>
					</div>
					<div class="collapsible-body row" style="padding:10px">
						<div class="col s12 m12 l12">
							<?php echo form_open(uri_string(), 'name="form_filter" autocomplete="off"'); ?>
							<div class="input-field col s4">
								<?php echo form_input('surat_tgl', userdata($this->module . '.search.surat_tgl'), 'class="pickadate" placeholder="'.db_lang('surat_tgl').'"')?>
								<label><?php echo db_lang('surat_tgl') ?></label>
							</div>
							<div class="input-field col s4">
								<?php echo form_input('surat_nomor', userdata($this->module . '.search.surat_nomor'), 'placeholder="'.db_lang('surat_nomor').'"')?>
								<label><?php echo db_lang('surat_nomor') ?></label>
							</div>
							<div class="input-field col s4">
								<?php 
									$select = array('0' => lang('select.pick'), '1' => 'Memo', '2' => 'Surat');
									echo form_dropdown('surat_jenis',$select, userdata($this->module . '.search.surat_jenis')) 
								?>
								<label><?php echo db_lang('surat_jenis') ?></label>
							</div>
							<div class="right">
								<div class="input-field col s12">
									<button type="submit" class="filter btn cyan waves-effect waves-light">
										<i class="mdi-content-send"></i> <?php echo lang('buttons.search') ?>
									</button>
									<button type="reset" class="btn orange waves-effect waves-light" id="reset">
										<i class="mdi-action-autorenew"></i> <?php echo lang('buttons.reset') ?>
									</button>
								</div>
							</div>
							<?php echo form_close(); ?>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<div class="card-panel">
		<?php echo form_open(uri_string(), 'name="datas_table"'); ?>
		<div class="row" style="margin-bottom: 20px">
			<div class="col s12 m12 l12">
				<div class="col s2">
					<?php echo sprintf(lang('table.per_page'), table_per_page_options()) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s12 m12 l12">
				<table class="striped table">
					<thead>
						<tr>
							<th></th>
							<th><?php echo db_lang('surat_nomor') ?></th>
							<th><?php echo db_lang('surat_perihal') ?></th>
							<th width="10%"><?php echo db_lang('surat_received') ?></th>
							<th width="8%"><?php echo db_lang('surat_tgl') ?></th>
							<th><?php echo db_lang('surat_pengirim') ?></th>
							<th width="5%"><?php echo lang('label.action') ?></th>
						</tr>
					</thead>
					<tbody>
						<?php if (!empty($datas)): ?>
							<?php $i = 1; ?>
							<?php foreach ($datas as $data): ?>								
								<tr>
									<td>
										<div class="chip <?php echo $data->surat_jenis_label == 'M' ? 'pink' : 'cyan darken-1' ?> white-text tooltipped" data-tooltip="<?php echo $data->surat_jenis == '2' ? db_lang('memo') : db_lang('surat') ?>">
											<span class="ultra-small"><?php echo $data->surat_jenis_label ?></span>
										</div>
									</td>
									<td><?php echo $data->surat_nomor ?></td>
									<td><?php echo $data->surat_perihal ?></td>
									<td><?php echo date('Y-m-d', strtotime($data->surat_received)) ?></td>
									<td><?php echo $data->surat_tgl ?></td>
									<td><?php echo $data->divisi_nama ?></td>
									<td class="center-align">
										<a href="<?php echo site_url($this->module . '/view/' .$data->surat_id) ?>" rel="view" class="tooltipped" data-tooltip="<?php echo lang('label.view').' `'.$data->surat_nomor.' `' ?>">
											<i class="mdi-action-search"></i>
										</a>
									</td>
								</tr>
								<?php ++$i; ?>
							<?php endforeach; ?>
						<?php else: ?>
							<tr>
								<td colspan="7"><?php echo sprintf(lang('label.empty_grid'), db_lang('page_title.' . $this->module)) ?></td>
							</tr>
						<?php endif; ?>
					</tbody>
				</table>
				<div class="row" style="margin-top: 20px">
					<div class="col s6">
						<?php echo $pagination['links']; ?>
					</div>
				</div>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
</div>
<?php
	custom_confirm_modal();
	alert_modal(array('title' => db_lang('page_title.'.$this->module) ,'content' => lang('dialog.delete_no_selected')));
?>