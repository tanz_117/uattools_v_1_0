<?php defined('BASEPATH') OR exit('No direct script access allowed');

class histories extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->module . '/Histories_model', '_histories_model');		
		$this->load->helper('modal');
		$this->load->library('form_validation');
		$this->validation_rules = array(
			array('field' => 'field_2', 'label' => db_lang('field_2'), 'rules' => 'required'),
			array('field' => 'field_3', 'label' => db_lang('field_3'), 'rules' => 'required')
		);
		$this->form_validation->set_rules($this->validation_rules);
		$this->_is_ajax = $this->is_ajax() || $this->input->post('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest';
		$this->_is_ajax ? $this->template->set_layout(FALSE) : '';
	}

	public function index()
	{
		$this->load->helper('table');
		$ref_array = explode('/', array_key_exists('HTTP_REFERER', $_SERVER) ? $_SERVER['HTTP_REFERER'] : '');

		if (!in_array($this->module, $ref_array)) {
			$this->session->set_userdata($this->module . '.sort.field', '');
			$this->session->set_userdata($this->module . '.sort.dir', '');
			$this->session->set_userdata($this->module . '.current_page', '');
			$this->session->set_userdata($this->module . '.search.identity', '');
			$this->session->set_userdata($this->module . '.search.role', '');
			$this->session->set_userdata($this->module . '.search.surat_tgl', '');
			$this->session->set_userdata($this->module . '.search.surat_nomor', '');
			$this->session->set_userdata($this->module . '.search.surat_jenis', '');
		}

		$baseCondition = array();
		if ($this->input->post()) {
			$post = $this->input->post_sanitized();
			foreach($post as $key => $val) {
				if (!in_array($key, array('sortfield', 'sortdir', 'current_page', 'per_page')))
					$this->session->set_userdata($this->module . '.search.' . $key, $val);
			}

			$baseCondition = array(
				'like' => array(
					'surat_tgl' => userdata($this->module . '.search.surat_tgl'),
					'surat_nomor' => userdata($this->module . '.search.surat_nomor')
				),
			);
			$baseCondition += userdata($this->module . '.search.surat_jenis') == '0' ? array() : array('where' => array('surat_jenis' => userdata($this->module . '.search.surat_jenis')));
		}

		if ($this->input->post('current_page')) {
			$this->session->set_userdata($this->module . '.current_page', $this->input->post_sanitized('current_page'));
		}

		if ($this->input->post('per_page')) {
			$this->session->set_userdata($this->module . '.' . $this->method . '.per_page', $this->input->post_sanitized('per_page'));
		}
		
		
		if(userdata('user_level') == config_item('role_level')['Pimpinan Unit Kerja']){
			$baseCondition += array('where' => array('c.disposisi_tujuan' => userdata('user_cabang')));
		}
		else if(userdata('user_level') == config_item('role_level')['Staff']){
			$baseCondition += array('where' => array('c.disposisi_tujuan' => userdata('user_id')));
		}
		else if(userdata('user_level') == config_item('role_level')['Admin Unit Kerja']){
			$baseCondition += array('where' => array('c.disposisi_tujuan' => userdata('user_cabang')));
		}
		

		$pagination = create_pagination($this->module, $this->_histories_model->count(
				array(
					'DISTINCT' => array(),
					'select' => "surat_id,surat_nomor,surat_perihal,surat_received,surat_tgl,surat_jenis,CASE WHEN surat_jenis = 1 THEN 'M' ELSE 'S' END AS surat_jenis_label,divisi_nama,surat_createdby",
					'join' => array(array('divisi b','b.divisi_id = surat.surat_pengirim','left'), array('disposisi c','c.disposisi_surat_id = surat.surat_id','left'))
				) + $baseCondition
			));

		if ($this->input->post('sortfield')) {
			$this->session->set_userdata($this->module . '.sort.field', $this->input->post_sanitized('sortfield'));
			$this->session->set_userdata($this->module . '.sort.dir', $this->input->post_sanitized('sortdir'));
			$baseCondition = $this->session->set_userdata($this->module . '.sort.field', $this->input->post('sortfield')) ? $baseCondition + array('order_by' => array(userdata($this->module . '.sort.field'), userdata($this->module . '.sort.dir'))) : $baseCondition;
		}


		$datas = $this->_histories_model->rows(
			array(
				'DISTINCT' => array(),
				'select' => "surat_id,surat_nomor,surat_perihal,surat_received,surat_tgl,surat_jenis,CASE WHEN surat_jenis = 1 THEN 'M' ELSE 'S' END AS surat_jenis_label,CASE WHEN surat.surat_type_pengirim = 'E' THEN surat.surat_pengirim ELSE divisi_nama END AS divisi_nama,surat_createdby",
				'join' => array(array('divisi b','b.divisi_id = surat.surat_pengirim','left'), array('disposisi c','c.disposisi_surat_id = surat.surat_id','left')),				
				'order_by' => array('surat_createdby', 'ASC'), //MANDATORI BUAT PAGINATION!!!! CATEUT
				'limit' => $pagination['limit']
			) + $baseCondition
		);		

		$dropdownlist_item = array('item_1' => 'item_1','item_2' => 'item_2');

		$this->template
			->set('pagination', $pagination)
			->set('dropdownlist_item',$dropdownlist_item)
			->set('datas', $datas)
			->title(db_lang('page_title.' . $this->module))
			->build('default', array());
	}	

	public function view($id = '')
	{
		$this->load->helper('table');
		if (!$this->_is_ajax) {
			$this->session->set_flashdata('notice', lang('auth.no_direct_link'));
			redirect($this->module);
		}
		// Got ID?
		if (!$id) {
			redirect($this->module);
		}
		//---- WRITE UR CUSTOM CODE HERE ------------------
		//
		// $this->data->dropdownlist_item = array('item_1' => 'item_1','item_2' => 'item_2');		
		//
		//---- END OF CUSTOM CODE ------------------------
		$this->load->model('mails/Mails_model', '_mails_model');	
		$this->data->mail = $this->_mails_model->row(array('where' => array('surat_id' => $id)));
		$this->data->datas = $this->_histories_model->rows(
				array(
						'select' => array('*, 
									(
										SELECT
											CASE
												WHEN z.disposisi_type_tujuan = 1 THEN
													m.divisi_nama
												WHEN z.disposisi_type_tujuan = 2 THEN
													n.unitkerja_nama
												ELSE
													o.pegawai_nama
											END AS dari
										FROM
											disposisi AS z 
										LEFT JOIN "divisi" AS m ON "z"."disposisi_tujuan" = "m"."divisi_id"
										LEFT JOIN "unitkerja" AS n ON "z"."disposisi_tujuan" = "n"."unitkerja_id"
										LEFT JOIN "pegawai" AS o ON "z"."disposisi_tujuan" = "o"."pegawai_id"
										WHERE
											z.disposisi_id = disposisi.disposisi_related_disposisi_id
									) AS dari, 	
									(SELECT z.disposisi_tujuan FROM disposisi AS z WHERE z.disposisi_id = disposisi.disposisi_id) AS ke, 
									CASE 
									WHEN disposisi.disposisi_type_tujuan = 1 THEN divisi.divisi_nama
									WHEN disposisi.disposisi_type_tujuan = 2 THEN unitkerja.unitkerja_nama
									ELSE pegawai.pegawai_nama
									END AS nama,
									CASE WHEN surat_type_pengirim = \'I\' THEN
										(SELECT div.divisi_nama FROM divisi AS div WHERE div.divisi_id = surat_pengirim)
									ELSE
										surat_pengirim
									END AS pengirim_divisi', false),
						'join' => array(
								array('disposisi', 'surat.surat_id = disposisi.disposisi_surat_id', 'left'),
								array('divisi', 'disposisi.disposisi_tujuan = divisi.divisi_id', 'left'),
								array('unitkerja', 'disposisi.disposisi_tujuan = unitkerja.unitkerja_id', 'left'),
								array('pegawai', 'disposisi.disposisi_tujuan = pegawai.pegawai_id', 'left')
							),
						'where' => array('surat.surat_id', $id),
						'order_by' => array(array('disposisi_type_tujuan', 'ASC'),array('disposisi_tgl', 'ASC'))
					)
				);
		// var_dump($this->data->datas);exit;
		$this->template->build('view', $this->data);
	}
}