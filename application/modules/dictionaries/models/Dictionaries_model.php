<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dictionaries_model extends Base_Model
{
	protected $_table = 'dictionaries';
	protected $_pk = 'dictionary_id';

	public function __construct()
	{
		parent::__construct();
	}

	public function selectCount($condition = array())
	{
		$this->_table = 'dictionaries_list()';
		return parent::count(array(
			'select' => "dictionary_key",
			'distinct' => NULL,
		) + $condition);
	}

	public function select($condition = array(), $limit = NULL)
	{
		$this->_table = 'dictionaries_list()';
		return parent::rows(array(
			'select' => "dictionary_key, kamus",
			'order_by' => array('dictionary_key', 'ASC'),
			'group_by' => array('dictionary_key, kamus'),
			'distinct' => NULL,
			'limit' => $limit
		) + $condition);
	}

	public function getDictionariesByLanguage($language)
	{
		return parent::rows(array(
			'where' => array('dictionary_lang_id' => $language)
		));
	}

	public function insert($data)
	{
		$n = count($data['dictionary_lang_id']);
		for ($i = 0; $i < $n; $i++) {
			$datas = array(
				'dictionary_lang_id' => $data['dictionary_lang_id'][$i],
				'dictionary_key' => html_decode($data['dictionary_key']),
				'dictionary_value' => $data['dictionary_value'][$i]
			);
			parent::query("EXEC dictionaries_insert ?, ?, ?", $datas);
		}
		return true;
	}

	public function update($data, $id)
	{
		$n = count($data['dictionary_id']);
		for ($i = 0; $i < $n; $i++) {
			$data_update = array(
				'dictionary_lang_id' => $data['dictionary_lang_id'][$i],
				'dictionary_key' => html_decode($data['dictionary_key']),
				'dictionary_value' => $data['dictionary_value'][$i]
			);
			$this->db->where($this->_pk, $data['dictionary_id'][$i]);
			$this->db->update($this->_table, $data_update);
		}
		return true;
	}
	
	public function delete($id)
	{
		$this->_pk = 'dictionary_key';
		$this->db->where($this->_pk, html_decode($id));
		return $this->db->delete($this->_table);
	}
}