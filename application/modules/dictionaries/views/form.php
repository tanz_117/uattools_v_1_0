<div class="section">
	<div class="row">
		<div class="col s12 m12 l12">
			<div class="card-panel">
				<div class="row">
					<?php echo form_open(uri_string(), 'class="col s12 crud" id="form_data" autocomplete="off"'); ?>
					<div class="row">
						<div class="input-field col s12">
							<?php echo form_input('dictionary_key', $row[0]->dictionary_key, 'id="dictionary_key"'); ?>
							<label class="active"><?php echo db_lang('dictionary_key'); ?></label>
						</div>
					</div>
					<?php if ($this->method === 'add'): ?>
					<?php
					foreach ($languages as $language) {
						echo '<div class="row">';
							echo '<div class="input-field col s12">';
								echo form_hidden('dictionary_lang_id[]', $language->language_id);
								echo form_input('dictionary_value[]', '');
								echo '<label class="active">' . $language->language_name . '</label>';
							echo '</div>';
						echo '</div>';
					}
					?>
					<?php else: ?>
					<?php
					for ($i = 0; $i < count($languages); $i++) {
						echo '<div class="row">';
							echo '<div class="input-field col s12">';
								echo form_hidden('dictionary_id[]', $row[$i]->dictionary_id);
								echo form_hidden('dictionary_lang_id[]', $row[$i]->dictionary_lang_id);
								echo form_input('dictionary_value[]', $row[$i]->dictionary_value);
								echo '<label class="active">' . $languages[$i]->language_name . '</label>';
							echo '</div>';
						echo '</div>';
					}
					?>
					<?php endif; ?>
					<div class="row">
						<div class="input-field col s12">
							<button type="submit" class="submit btn cyan waves-effect waves-light">
							<i class="mdi-content-send"></i> <?php echo lang('buttons.save') ?>
							</button>
							<?php confirm_modal(array('rel' => 'confirm-submit')); ?>
							<a href="javascript:void(0);" class="button btn orange waves-effect waves-light" value="cancel">
								<i class="mdi-av-replay"></i> <?php echo lang('buttons.cancel') ?>
							</a>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>