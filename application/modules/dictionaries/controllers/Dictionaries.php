<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Dictionaries extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->module . '/dictionaries_model', '_dictionariesModel');
		$this->load->helper('modal');
		$this->load->library('form_validation');
		$this->validation_rules = array(
			array(
				'field' => 'dictionary_key',
				'label' => db_lang('dictionary_key'),
				'rules' => 'required|trim|regex_match[/^[a-zA-Z0-9._]*$/i]|callback__is_dictionary_key_exist['. $this->uri->segment(3) .']'
			),
			array(
				'field' => 'dictionary_value[]',
				'label' => db_lang('dictionary_value'),
				'rules' => 'required|trim|alpha_numeric_spaces'
			)
		);
		$this->form_validation->set_rules($this->validation_rules);
		$this->_is_ajax = $this->is_ajax() || $this->input->post('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest';
		$this->_is_ajax ? $this->template->set_layout(FALSE) : '';
	}

	public function index()
	{
		$this->load->helper('table');
		$ref_array = explode('/', array_key_exists('HTTP_REFERER', $_SERVER) ? $_SERVER['HTTP_REFERER'] : '');

		if (!in_array($this->module, $ref_array)) {
			$this->session->set_userdata($this->module . '.sort.field', '');
			$this->session->set_userdata($this->module . '.sort.dir', '');
			$this->session->set_userdata($this->module . '.current_page', '');
			$this->session->set_userdata($this->module . '.search.key', '');
			$this->session->set_userdata($this->module . '.search.value', '');
		}

		$baseCondition = array();

		if ($this->input->post()) {
			$post = $this->input->post_sanitized();
			foreach($post as $key => $val) {
				if (!in_array($key, array('sortfield', 'sortdir', 'current_page', 'per_page')))
					$this->session->set_userdata($this->module . '.search.' . $key, $val);
			}
			
			$baseCondition = array(
				'like' => array('dictionary_key' => userdata($this->module . '.search.key'), 'dictionary_value' => userdata($this->module . '.search.value'))
			);
		}

		if ($this->input->post('current_page')) {
			$this->session->set_userdata($this->module . '.current_page', $this->input->post_sanitized('current_page'));
		}

		if ($this->input->post('per_page')) {
			$this->session->set_userdata($this->module . '.' . $this->method . '.per_page', $this->input->post_sanitized('per_page'));
		}

		$pagination = create_pagination($this->module, $this->_dictionariesModel->selectCount($baseCondition));

		if ($this->input->post('sortfield')) {
			$this->session->set_userdata($this->module . '.sort.field', $this->input->post_sanitized('sortfield'));
			$this->session->set_userdata($this->module . '.sort.dir', $this->input->post_sanitized('sortdir'));
			$baseCondition = $this->session->set_userdata($this->module . '.sort.field', $this->input->post('sortfield')) ? $baseCondition + array('order_by' => array(userdata($this->module . '.sort.field'), userdata($this->module . '.sort.dir'))) : $baseCondition;
		}

		$datas = $this->_dictionariesModel->select($baseCondition, $pagination['limit']);

		$this->template
			->set('pagination', $pagination)
			->set('datas', $datas)
			->title(db_lang('page_title.' . $this->module))
			->build('default', array());
	}

	public function add()
	{
		if (!$this->_is_ajax) {
			$this->session->set_flashdata('notice', lang('auth.no_direct_link'));
			redirect($this->module);
		}

		if ($this->form_validation->run()) {
			$post = $this->input->post_sanitized();

			if ($this->_dictionariesModel->insert($post)) {
				for ($i = 0; $i < count($post['dictionary_lang_id']); $i++) {
					$input[] = array(
					'dictionary_lang_id' => $post['dictionary_lang_id'][$i],
					'dictionary_key' => $post['dictionary_key'],
					'dictionary_value' => $post['dictionary_value'][$i]
					);
				}

				log_activity($input, NULL, $post['dictionary_key']);
				$message = sprintf(lang('notice.add_success'), $post['dictionary_key']);
				$status = 'success';
			} else {
				$message = sprintf(lang('notice.add_error'), $post['dictionary_key']);
				$status = 'error';
			}

			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}

		} elseif ($this->input->post()) {
			$message = $this->form_validation->error_string('', '');
			$status = 'error';

			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}

		} else {
			$row[0] = new stdClass;

			foreach ($this->validation_rules as $rule) {
				$row[0]->{$rule['field']} = set_value($rule['field']);
			}
		}

		$this->data->row = $row;
		$this->data->languages = $this->_buildLanguagesOption();

		if ($this->_is_ajax && $this->input->post()) {
			echo json_encode(array('status' => 'error', 'message' => validation_errors()));
		} else {
			$this->template->build('form', $this->data);
		}

	}

	public function edit($id = '')
	{
		if (!$this->_is_ajax) {
			$this->session->set_flashdata('notice', lang('auth.no_direct_link'));
			redirect($this->module);
		}

		if (!$id) {
			redirect($this->module);
		}

		$id = sanitize_input($id);
		$row = $this->_dictionariesModel->rows(array('where' => array('dictionary_key' => html_decode($id))));

		if ($this->form_validation->run()) {
			$post = $this->input->post_sanitized();

			if ($this->_dictionariesModel->update($post, $id)) {
				for ($i = 0; $i < count($post['dictionary_id']); $i++) {
					$input[] = array(
						'dictionary_id' => $post['dictionary_id'][$i],
						'dictionary_lang_id' => $post['dictionary_lang_id'][$i],
						'dictionary_key' => $post['dictionary_key'],
						'dictionary_value' => $post['dictionary_value'][$i]
					);
				}

				log_activity($input, $row, $id);
				$message = sprintf(lang('notice.edit_success'), $post['dictionary_key']);
				$status = 'success';
			} else {
				$message = sprintf(lang('notice.edit_error'), $post['dictionary_key']);
				$status = 'error';
			}

			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}

		} elseif ($this->input->post()) {
			$message = $this->form_validation->error_string('', '');
			$status = 'error';

			if ($this->_is_ajax) {
				echo json_encode(array('status' => $status, 'message' => $message));
				return;
			} else {
				$this->session->set_flashdata($status, $message);
				redirect($this->module);
			}
		} else {
			if ($this->input->post()) {
				$row = new stdClass;

				foreach ($this->validation_rules as $rule) {
					$row->{$rule['field']} = set_value($rule['field']);
				}
			}
		}

		$this->data->row = $row;
		$this->data->languages = $this->_buildLanguagesOption();

		if ($this->_is_ajax && $this->input->post()) {
			echo json_encode(array('status' => 'error', 'message' => validation_errors()));
		} else {
			$this->template->build('form', $this->data);
		}
	}

	public function delete($id = '')
	{
		if (!$this->_is_ajax) {
			$this->session->set_flashdata('notice', lang('auth.no_direct_link'));
			redirect($this->module);
		}

		$ids = (!empty($id)) ? array($id) : $this->input->post('action_to');

		if (!empty($ids)) {
			$deleted = 0;
			$to_delete = 0;
			foreach ($ids as $id) {
				if ($this->_dictionariesModel->delete(sanitize_input($id))) {
					log_activity(array('deleted' => true), array('deleted' => false), $id);
					$deleted++;
				} else {
					$status = 'error';
					$message = sprintf(lang('notice.delete_error'), $id);
				}
				$to_delete++;
			}

			if ($deleted > 0) {
				$status = 'success';
				$message = sprintf(lang('notice.delete_success'), $deleted, $to_delete);
			}
		} else {
			$status = 'notice';
			$message = lang('notice.no_select_error');
		}

		if ($this->_is_ajax) {
			echo json_encode(array('status' => $status, 'message' => $message));
			return;
		} else {
			$this->session->set_flashdata($status, $message);
			redirect($this->module);
		}
	}

	public function _is_dictionary_key_exist($input, $id = '')
	{
		$condition['like'] = array('dictionary_key', $input, 'after');
		if (! empty($id))
			$condition['where'] = array('dictionary_key <>' => $id);

		if ($this->_dictionariesModel->count($condition) > 0) {
			$this->form_validation->set_message('_is_dictionary_key_exist', sprintf(lang('is_already_exist'), $input));
			return false;
		}

		return true;
	}

	public function _is_dictionary_value_exist($input, $id = '')
	{
		$condition['like'] = array('dictionary_value', $input, 'after');
		if (! empty($id))
			$condition['where'] = array('dictionary_key <>' => $id);
		
		if ($this->_dictionariesModel->count($condition) > 0) {
			$this->form_validation->set_message('_is_dictionary_value_exist', sprintf(lang('is_already_exist'), $input));
			return false;
		}
		return true;
	}
	
	private function _buildLanguagesOption()
	{
		$this->load->model('languages/language_model', '_languageModel');
		return $this->_languageModel->rows();
	}
}