<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['buttons.cancel'] = 'Batal';
$lang['buttons.save'] = 'Simpan';
$lang['buttons.edit'] = 'Ubah';
$lang['buttons.save_exit'] = 'Simpan & Tutup';
$lang['buttons.reset'] = 'Reset';
$lang['buttons.search'] = 'Cari';
$lang['buttons.close'] = 'Tutup';
$lang['buttons.back'] = 'Kembali';
$lang['buttons.icons'] = 'Daftar Icon';
$lang['buttons.delete_selected'] = 'Hapus yang dipilih';
$lang['buttons.active_selected'] = 'Non/Aktifkan yang dipilih';
$lang['buttons.download']	= 'Unduh';
$lang['buttons.upload']	= 'Unggah';
$lang['buttons.send']	= 'Kirim';
$lang['buttons.generate_password']	= 'Buat Password';
$lang['buttons.generate']	= 'Generate Pemenang';
$lang['buttons.uim'] = 'Ambil dari UIM';
$lang['buttons.pdf'] = 'PDF';
$lang['buttons.xls'] = 'EXCEL';
$lang['buttons.disposition'] = 'Disposisi';
$lang['buttons.disposition'] = 'Disposisi';
$lang['buttons.disposition.internal'] = 'Internal Grup';
$lang['buttons.disposition.other'] = 'Grup Lain';

$lang['konfirm.modal.batal'] = 'Close';
$lang['konfirm.modal.ok'] = 'Ok';

$lang['btn.summary'] = 'Summary';
$lang['btn.profile'] = 'Profil';
$lang['btn.pic'] = 'PIC';
$lang['btn.document'] = 'Dokumen';
$lang['btn.usermatrix'] = 'Matrix User';
$lang['btn.module'] = 'Module';
$lang['buttons.approve'] = 'Setuju';
$lang['buttons.reject'] = 'Tolak';
$lang['buttons.order'] = 'Pesan';
$lang['buttons.detail'] = 'Detail';
