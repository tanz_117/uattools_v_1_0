<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['application_name'] = 'HOA CataLoGue';

$lang['notification'] = 'Notifikasi';
$lang['dashboard'] = 'Dashboard';

$lang['label.action'] = 'Aksi';
$lang['label.add'] = 'Tambah';
$lang['label.get_from_eq'] = 'Get From EQ';
$lang['label.upload'] = 'Unggah';
$lang['label.edit'] = 'Ubah';
$lang['label.view'] = 'Lihat';
$lang['label.delete'] = 'Hapus';
$lang['label.deleted'] = 'telah dihapus';
$lang['label.status'] = 'Set status `%s` menjadi %s';
$lang['label.locked_status'] = 'Set akses `%s` menjadi %s';
$lang['label.detail'] = 'Lihat Detail';
$lang['label.search'] = 'Pencarian...';
$lang['label.search_detail'] = 'Pencarian Detail';
$lang['label.empty_grid'] = 'Data %s tidak tersedia';
$lang['label.recent_activity'] = 'Log Aktifitas';
$lang['label.dictionary_word'] = 'Kata %s';
$lang['label.default_lang'] = 'Set `%s` sebagai Bahasa Default';
$lang['label.expired'] = 'Set password `%s` menjadi %s';
$lang['label.add_table'] = 'Create Table';
$lang['label.add_field'] = 'Create Field';
$lang['label.add_project'] = 'Mulai Project Baru';
$lang['label.add_manual'] = 'Tambah data manual';
$lang['label.assign_programmers'] = 'Tugaskan Programmer';

$lang['table.per_page'] = 'Tampilkan %s per halaman';

$lang['text.logged_in_as'] = 'Login sebagai';
$lang['text.logout'] = 'Logout';
$lang['text.profile'] = 'Profile';
$lang['text.configuration'] = 'Konfigurasi';
$lang['text.expired'] = 'Kadaluarsa';
$lang['text.unexpired'] = 'Tidak Kadaluarsa';
$lang['text.unlocked'] = 'Buka Akses';
$lang['text.locked'] = 'Kunci Akses';

$lang['dialog.delete_no_selected'] = 'Harap pilih salah satu data yang akan dihapus';
$lang['dialog.active_no_selected'] = 'Harap pilih salah satu data yang akan di Non-aktifkan';
$lang['dialog.delete_message'] = 'Hapus data yang dipilih?';
$lang['dialog.confirm_title'] = 'Konfirmasi!';
$lang['dialog.confirm_form'] = 'Anda yakin untuk menyimpan data yang sudah diisi?';
$lang['dialog.confirm_form_edit'] = 'Anda yakin ingin mengubah %s?';
$lang['dialog.confirm_delete'] = 'Anda yakin ingin menghapus %s?';
$lang['dialog.confirm_active'] = 'Anda yakin ingin Non/Aktifkan %s?';
$lang['dialog.confirm_delete_selected'] = 'Hapus data yang dipilih?';
$lang['dialog.confirm_active_selected'] = 'Non/Aktifkan data yang dipilih?';
$lang['dialog.confirm_upload_dtf'] = 'Apakah anda yakin akan menambahkan data EQ dengan yang terbaru?';
$lang['dialog.confirm_replace_dtf'] = 'Apakah anda yakin akan mengganti data EQ dengan yang terbaru?';
$lang['dialog.confirm_cancel_nostro_eq'] = 'Apakan anda yakin untuk membatalkan data Nostro EQ ?';
$lang['dialog.confirm_save_nostro_eq'] = 'Apakan anda yakin untuk menyimpan data Nostro EQ ?';
$lang['dialog.confirm_cancel_nostro_excel'] = 'Apakan anda yakin untuk membatalkan data Nostro Blotter Excel ?';
$lang['dialog.confirm_save_nostro_excel'] = 'Apakan anda yakin untuk menyimpan data Nostro Blotter Excel ?';
$lang['dialog.confirm_replace_excel'] = 'Apakah anda yakin akan mengganti data Blotter Excel dengan yang terbaru?';
$lang['dialog.confirm_upload_excel'] = 'Apakah anda yakin akan menambahkan data Blotter Excel dengan yang terbaru?';


$lang['dialog.yes'] = 'Ya';
$lang['dialog.no'] = 'Tidak';
$lang['dialog.close'] = 'Tutup';

$lang['notice.add_success'] = '%s berhasil ditambahkan';
$lang['notice.add_error'] = '%s gagal ditambahkan';
$lang['notice.add_file_incomplete'] = '%s dibutuhkan';
$lang['notice.edit_success'] = '%s berhasil diubah';
$lang['notice.multiedit_success'] = '%s dari %s data berhasil diubah';
$lang['notice.edit_error'] = '%s gagal diubah';
$lang['notice.single_delete_success'] = '%s berhasil dihapus';
$lang['notice.delete_success'] = '%s dari %s data berhasil dihapus';
$lang['notice.singledelete_success'] = '%s  berhasil dihapus';
$lang['notice.delete_error'] = 'Data %s gagal dihapus';
$lang['notice.status_success'] = 'Status %s berhasil diubah';
$lang['notice.status_error'] = 'Status %s gagal diubah';
$lang['notice.expired_success'] = 'Status Password Kadaluarsa %s berhasil diubah';
$lang['notice.expired_error'] = 'Status Password Kadaluarsa %s gagal diubah';
$lang['notice.locked_success'] = 'Status Akses %s berhasil diubah';
$lang['notice.locked_error'] = 'Status Akses %s gagal diubah';
$lang['notice.default_lang_success'] = 'Default %s berhasil diubah';
$lang['notice.default_lang_error'] = 'Default %s gagal diubah';
$lang['notice.no_select_error'] = 'Anda harus memilih data terlebih dahulu';
$lang['notice.already_exist_error'] = '%s sudah ada';
$lang['notice.permit_char_name'] = '%s mengandung karakter yang tidak diperbolehkan (%s)';
$lang['notice.upload_success'] = '%s berhasil';
$lang['notice.add_error_exist'] = 'Provider dan periode pada program %s sudah ada';
$lang['notice.batch_add_success'] = '%s dari %s data berhasil ditambahkan';
$lang['notice.role_id_is_exist'] = 'Role ID %s sudah ada';

$lang['select.pick'] = '- Pilih salah satu -';
$lang['select.all'] = 'Tampilkan Semua';
$lang['select.active'] = 'Aktif';
$lang['select.no_active'] = 'Tidak Aktif';

$lang['label.januari'] = 'Januari';
$lang['label.februari'] = 'Februari';
$lang['label.maret'] = 'Maret';
$lang['label.april'] = 'April';
$lang['label.mei'] = 'Mei';
$lang['label.juni'] = 'Juni';
$lang['label.juli'] = 'Juli';
$lang['label.agustus'] = 'Agustus';
$lang['label.september'] = 'September';
$lang['label.oktober'] = 'Oktober';
$lang['label.november'] = 'November';
$lang['label.desember'] = 'Desember';

$lang['userlevel.pemimpin_divisi'] = 'Pemimpin Divisi';
$lang['userlevel.pemimpin_bagian'] = 'Pemimpin Bagian';
$lang['userlevel.staff'] = 'Staff';
$lang['userlevel.sekum'] = 'Sekum';

$lang['mails.inactivebysekum'] = 'Inactive by Sekum';

$lang['applevel.low'] = 'Low';
$lang['applevel.medium'] = 'Medium';
$lang['applevel.high'] = 'High';
$lang['applevel.critical'] = 'Critical';

$lang['appstatus.init'] = 'Initialization';
$lang['appstatus.development'] = 'Development';
$lang['appstatus.live'] = 'Live';
$lang['appstatus.rfc'] = 'RFC';
$lang['appstatus.close'] = 'Close';

$lang['parameter.notavailable'] = 'Not Available';
$lang['app_type.desktop'] = 'Desktop Application';
$lang['app_type.web'] = 'Web Application';

$lang['dashboard.apptotal'] = 'Total Aplikasi';
$lang['dashboard.desktopapp'] = 'Desktop App';
$lang['dashboard.webapp'] = 'Web App';
$lang['dashboard.app_live'] = 'Production';
$lang['dashboard.app_on_development'] = 'Development';
$lang['dashboard.app_on_init'] = 'Tahap Analisa';
$lang['attachment'] = 'Lampiran';
$lang['assign_to'] = 'Tugaskan %s';
$lang['select_hoa_members'] = 'Tugaskan satu atau beberapa member HOA untuk project ini';
$lang['member.count_current_project'] = 'Sedang mengerjakan %s projek';
$lang['label.empty_assigned_employee'] = 'Data pegawai belum ditentukan';
$lang['label.upload'] = 'Upload document %s';
$lang['label.add_user_matrix'] = 'Tambah user matrix';
$lang['label.add_module'] = 'Tambah module';
$lang['label.rbbtype_nrbb'] = 'Non RBB';
$lang['label.rbbtype_rbb'] = 'RBB';

$lang['label.lakilaki'] = 'Laki-laki';
$lang['label.perempuan'] = 'Perempuan';

$lang['label.tambahsaldo'] = 'Tambah saldo';
$lang['label.resetpassword'] = 'Reset Password';
$lang['label.waiting'] = 'Menunggu';
$lang['label.reject_reason'] = 'Alasan penolakan';
$lang['label.resetpassword'] = 'Reset password';
$lang['label.approve'] = 'Disetujui';
$lang['label.reject'] = 'Ditolak';
$lang['label.packing'] = 'Packing';
$lang['label.ready_to_send'] = 'Siap kirim';
$lang['point_order_desc'] = 'Point order adalah minimum item dibeli untuk 1 point order';
$lang['label.checkout'] = 'CheckOut';
$lang['label.trxpembayaran'] = 'Pembayaran';
$lang['label.trxpemesanan'] = 'Pemesanan';
$lang['label.anggota_profile'] = 'Profile Anggota';
$lang['label.minimum_order_point'] = 'Gratis biaya kirim jika telah memiliki %s point order';
$lang['label.login'] = 'Masuk';
$lang['label.signup'] = 'Daftar';
$lang['label.anggota_aktif'] = 'Anggota Aktif';
$lang['label.anggota_nonaktif'] = 'Anggota Non-Aktif';
$lang['label.pengajuan_anggota_waiting'] = 'Menunggu persetujuan';
$lang['label.pengajuan_anggota_approve'] = 'Anggota disetujui ';
$lang['label.pengajuan_anggota_reject'] = 'Anggota ditolak';
$lang['label.trxsaldo_waiting'] = 'Pengajuan saldo';
$lang['label.trxsaldo_approve'] = 'Pengajuan saldo disetujui';
$lang['label.trxsaldo_reject'] = 'Pengajuan saldo ditolak';
$lang['label.trxsaldo_methode_1'] = 'Transfer Bank';
$lang['label.trxsaldo_methode_2'] = 'Tunai';
$lang['label.highest_saldo'] = 'Saldo Tertinggi';
$lang['label.lowest_saldo'] = 'Saldo Terendah';
$lang['label.id_pesanan'] = 'ID Pesanan';
$lang['label.trxpesanan_total'] = 'Total Pesanan';
$lang['label.trxpesanan_waiting'] = 'Pesanan menunggu';
$lang['label.trxpesanan_approve'] = 'Pesanan disetujui';
$lang['label.trxpesanan_reject'] = 'Pesanan ditolak';
$lang['label.trxpesanan_packing'] = 'Pesanan packing';
$lang['label.trxpesanan_readytosend'] = 'Pesanan siap dikirim';
$lang['label.trxpesanan_sent'] = 'Pesanan Terkirim';
$lang['label.product'] = 'Total Product';

$lang['select.laptrxpesanans'] = 'Laporan Daftar Pesanan';
$lang['select.laptrxdetailpesanans'] = 'Laporan Detail Pesanan';

$lang['parameter_report_incomplete'] = 'SIlahkan isi parameter laporan';
$lang['lbl_number'] = 'No.';
$lang['label.ourProduct'] = 'Produk Kita';
$lang['label.seeMore'] = 'Selengkapnya >>';
