<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['is_already_exist'] = 'Isian dengan nama yang mengandung kata/kalimat <strong>`%s`</strong> sudah ada';
$lang['upload_required'] = 'Field untuk %s dibutuhkan';
$lang['upload_checked_excel_cur'] = 'File excel field %s mohon untuk di cek kembali, sesuaikan dengan currency module nostro currency pair';
$lang['user_id_not_available'] = 'User ID %s telah digunakan';
$lang['approve_terms_statement'] = 'Dengan ini saya telah mengetahui dan menyetujui <a href="%s">ketentuan pesanbelanjaan.com</a>';
$lang['attachment_null'] = 'Lampiran %s belum diisi';
$lang['user_password_not_match'] = 'Kolom Password tidak sama dengan Confirm Password';
$lang['product_id_not_available'] = 'ID Produk %s telah digunakan';
$lang['old_password_wrong'] = 'Password lama salah';
$lang['there_are_null_field'] = 'Terdapat kolom belum terisi';
$lang['add_error'] = 'Input data gagal';
$lang['stock_out_of_count'] = 'Stock tidak mencukupi';
$lang['thanks_for_register'] = 'Terimakasih %s telah mendaftarkan diri anda, kami akan proses pengajuan anda secepat mungkin, silahkan coba LogIn berkala untuk memastikan proses pendaftaran anda telah kami proses';
$lang['sorry_for_register'] = 'Maaf %s karena alasan teknis permohonan anda belum bisa kami proses';
$lang['nominal_lower_than_total'] = 'Nominal yang anda inputkan kurang';
$lang['low_saldo'] = 'Saldo tidak mencukupi';