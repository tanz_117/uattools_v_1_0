<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['buttons.cancel'] = 'Batal';
$lang['buttons.save'] = 'Simpan';
$lang['buttons.save_exit'] = 'Simpan & Tutup';
$lang['buttons.reset'] = 'Reset';
$lang['buttons.search'] = 'Cari';
$lang['buttons.close'] = 'Tutup';
$lang['buttons.back'] = 'Kembali';
$lang['buttons.icons'] = 'Daftar Icon';
$lang['buttons.delete_selected'] = 'Hapus yang dipilih';
$lang['buttons.download']	= 'Unduh';
$lang['buttons.upload']	= 'Unggah';
$lang['buttons.send']	= 'Kirim';
$lang['buttons.generate_password']	= 'Buat Password';
$lang['buttons.generate']	= 'Generate Pemenang';
$lang['buttons.approved'] = 'Terima';
$lang['buttons.rejected'] = 'Tolak';
$lang['buttons.pdf'] = 'PDF';
$lang['buttons.xls'] = 'EXCEL';

$lang['konfirm.modal.batal'] = 'Close';
$lang['konfirm.modal.ok'] = 'Ok';
