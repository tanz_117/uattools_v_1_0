<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['application_name'] = 'bjb Nostro';

$lang['dashboard'] = 'Dashboard';

$lang['label.action'] = 'Aksi';
$lang['label.add'] = 'Tambah';
$lang['label.upload'] = 'Upload';
$lang['label.edit'] = 'Ubah';
$lang['label.delete'] = 'Hapus';
$lang['label.status'] = 'Set status `%s` menjadi %s';
$lang['label.blocked'] = 'Blokir';
$lang['label.unblocked'] = 'Tidak di Blokir';
$lang['label.block'] = 'Blokir';
$lang['label.unblock'] = 'Buka Blokir';
$lang['label.detail'] = 'Lihat Detail';
$lang['label.search'] = 'Pencarian...';
$lang['label.search_detail'] = 'Pencarian Detail';
$lang['label.empty_grid'] = 'Data %s tidak tersedia';
$lang['label.recent_activity'] = 'Log Aktifitas';
$lang['label.dictionary_word'] = 'Kata %s';
$lang['label.default_lang'] = 'Set `%s` sebagai Bahasa Default';
$lang['label.to'] = 's.d';
$lang['label.verifikasi'] = 'Verifikasi';
$lang['label.waiting'] = 'Menunggu Verifikasi';
$lang['label.approved'] = 'Permintaan Diterima';
$lang['label.rejected'] = 'Permintaan Ditolak';
$lang['label.report'] = 'Cetak Laporan';
$lang['label.activity'] = 'Aktifitas';

$lang['profile.personal_info'] = 'Data Pribadi';
$lang['profile.user_type'] = 'Akses';
$lang['profile.code'] = 'Kode Kantor';
$lang['profile.address'] = 'Alamat';
$lang['profile.phone_number'] = 'No. Telepon';
$lang['profile.fax'] = 'No. Fax';

$lang['table.per_page'] = 'Tampilkan %s per halaman';

$lang['text.logged_in_as'] = 'Login sebagai';

$lang['dialog.delete_no_selected'] = 'Harap pilih salah satu data yang akan dihapus';
$lang['dialog.delete_message'] = 'Hapus data yang dipilih?';
$lang['dialog.confirm_form'] = 'Anda yakin untuk menyimpan data yang sudah diisi?';
$lang['dialog.confirm_form_edit'] = 'Anda yakin ingin mengubah %s?';
$lang['dialog.confirm_delete'] = 'Anda yakin ingin menghapus %s?';
$lang['dialog.confirm_delete_selected'] = 'Hapus data yang dipilih?';
$lang['dialog.confirm_common'] = 'Anda yakin untuk %s?';
$lang['dialog.confirm_form_approve'] = 'Anda yakin untuk menyetujui pengajuan tersebut?';
$lang['dialog.confirm_form_reject'] = 'Anda yakin untuk menolak pengajuan tersebut?';

$lang['dialog.yes'] = 'Ya';
$lang['dialog.no'] = 'Tidak';

$lang['notice.add_success'] = '%s berhasil ditambahkan';
$lang['notice.add_error'] = '%s gagal ditambahkan';
$lang['notice.edit_success'] = '%s berhasil diubah';
$lang['notice.edit_error'] = '%s gagal diubah';
$lang['notice.delete_success'] = '%s dari %s data berhasil dihapus';
$lang['notice.delete_error'] = 'Data %s gagal dihapus';
$lang['notice.status_success'] = 'Status %s berhasil diubah';
$lang['notice.status_error'] = 'Status %s gagal diubah';
$lang['notice.default_lang_success'] = 'Default %s berhasil diubah';
$lang['notice.default_lang_error'] = 'Default %s gagal diubah';
$lang['notice.block_success'] = '%s berhasil di blokir';
$lang['notice.block_error'] = '%s gagal di blokir';
$lang['notice.no_select_error'] = 'Anda harus memilih data terlebih dahulu';
$lang['notice.already_exist_error'] = '%s sudah ada';
$lang['notice.permit_char_name'] = '%s mengandung karakter yang tidak diperbolehkan (%s)';

$lang['select.pick'] = '- Pilih salah satu -';
$lang['select.active'] = 'Aktif';
$lang['select.no_active'] = 'Tidak Aktif';

$lang['select.pick'] = '- Pilih salah satu -';
$lang['select.approved'] = 'Di Setujui';
$lang['select.waiting'] = 'Menunggu';
$lang['select.rejected'] = 'Di Tolak';

$lang['new_request'] = 'Permohonan Baru';

$lang['label.januari'] = 'Januari';
$lang['label.februari'] = 'Februari';
$lang['label.maret'] = 'Maret';
$lang['label.april'] = 'April';
$lang['label.mei'] = 'Mei';
$lang['label.juni'] = 'Juni';
$lang['label.juli'] = 'Juli';
$lang['label.agustus'] = 'Agustus';
$lang['label.september'] = 'September';
$lang['label.oktober'] = 'Oktober';
$lang['label.november'] = 'November';
$lang['label.desember'] = 'Desember';
