<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['login.login_text'] = 'Login';
$lang['login.username'] = 'Username';
$lang['login.password'] = 'Password';
$lang['login.branch'] = 'Kantor';
$lang['login.code'] = 'Kode Kantor';
$lang['login.submit'] = 'Login';
$lang['login.remember'] = 'Ingat saya';
$lang['login.forgot'] = 'Lupa password';
$lang['login.login_as_cabang'] = 'Login Cabang';
$lang['login.login_as_user'] = 'Login User';


$lang['forgot.forgot_placeholder'] = 'Masukkan email yang didaftarkan di aplikasi';
$lang['forgot.forgot_text'] = 'Reset Password';
$lang['forgot.forgot_message'] = 'Password akan dikirim ke email anda';
$lang['forgot.success_reset'] = 'Password anda berhasil direset.<br>Silahkan cek email anda';
$lang['forgot.error_reset'] = 'Password anda gagal direset.<br>Silahkan coba sekali lagi';

$lang['auth.login_unsuccessful'] = 'Username atau password salah !!';
$lang['auth.user_not_active'] = 'User tidak aktif';
$lang['auth.user_is_blocked'] = 'User di blokir';
$lang['auth.user_logged_out'] = 'Anda telah logout dari sistem';
$lang['auth.user_must_login'] = 'Anda harus login terlebih dahulu';
$lang['auth.user_access_denied'] = 'Anda tidak memiliki hak akses terhadap menu';
$lang['auth.no_direct_link'] = 'Anda tidak dapat mengakses halaman ini dari link secara langsung';
