<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Bahasa
{
    private $_ci;
    private $_lang;

    public function __construct()
    {
        $this->_ci =& get_instance();
        $this->_ci->load->model('dictionaries/dictionaries_model', '_dictionariesModel');
    }

    public function load($module = '')
    {
        $rows = $this->_ci->_dictionariesModel->getDictionariesByLanguage(sanitize_input(config_item('default_language')));

        foreach ($rows as $row) {
            $this->_lang[$row->dictionary_key] = $row->dictionary_value;
        }
    }

    public function line($line = '', $module = false)
    {
        $module = $module ? $module : $this->_ci->module;

        $base = 'base.'. $line;
        $key = $module . '.' . $line;
        return isset($this->_lang[$key]) ? $this->_lang[$key] : (isset($this->_lang[$base]) ? $this->_lang[$base] : $line);
    }

}
