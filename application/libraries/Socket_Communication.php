<?php defined('BASEPATH') OR exit('No direct script access allowed');

include './application/third_party/phpseclib/Crypt/AES.php';

class Socket_Communication {

  public function send($address = '', $port = '', $key= '', $encrypt= FALSE, $request)
  {
    $socket = fsockopen($address, $port, $errnum, $errstr, 60);
    $request = ($encrypt ? $this->encrypt($request, $key) : $request);
    fwrite($socket, $request);
    fwrite($socket, chr(-1));

    $response = '';
    while (!feof($socket) && ($partial = fread($socket, 1)) != chr(-1)) {
      if ($partial !== '') {
        $response .= $partial;
      }
    }

    fclose($socket);

    $response = ($encrypt ? $this->decrypt($response, $key) : $response);
    return $response;
  }

  private function encrypt($str, $key)
  {
      $aes = new Crypt_AES(CRYPT_AES_MODE_ECB);
      $aes->setKeyLength(128);
      $aes->enablePadding();
      $aes->setKey($key);
      return base64_encode($aes->encrypt($str));
  }

  private function decrypt($str, $key)
  {
      $aes = new Crypt_AES(CRYPT_AES_MODE_ECB);
      $aes->setKeyLength(128);
      $aes->enablePadding();
      $aes->setKey($key);
      return $aes->decrypt(base64_decode($str));
  }

  public function generateSTAN() {
      list($usec, $sec) = explode(' ', microtime());
      mt_srand((10000000000 * (float) $usec) ^ (float) $sec);
      return mt_rand();
  }

}
