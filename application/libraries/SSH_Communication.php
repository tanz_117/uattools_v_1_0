<?php defined('BASEPATH') OR exit('No direct script access allowed');

include './application/third_party/phpseclib/Net/SSH2.php';

class SSH_Communication {

  public function run($address = '', $ssh_port = 22, $username = '', $password = '', $command)
  {
    $ssh = new Net_SSH2($address, $ssh_port);

    $username = empty($username) ? $this->_byPassUser : $username;
    $password = empty($password) ? $this->_byPassPwd : $password;
    if (! $ssh->login($username, $password)) {
      return false;
    }

    $output = $ssh->exec($command);
    return $output;
  }

}
