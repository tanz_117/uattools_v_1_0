<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication
{
	private $_ci;

	protected $messages;
	protected $errors = array();
	protected $error_start_delimiter;
	protected $error_end_delimiter;

	public function __construct()
	{
		$this->_ci =& get_instance();
		$this->_ci->load->model('auth_model', '_authModel');

		$this->messages = array();
		$this->errors = array();
		$this->message_start_delimiter  = '';
		$this->message_end_delimiter    = '';
		$this->error_start_delimiter    = '';
		$this->error_end_delimiter      = '';
	}

	public function isLoggedIn()
	{
		return $this->_ci->session->userdata('id') ? true : false;
	}

	public function isAuthenticated($identity, $password)
	{
		$isAuth = false;

		$credentials['identity'] = $identity;
		$credentials['password'] = hash('sha256', $password);
		$isAuth = $this->_ci->_authModel->authenticate($credentials);

		if ($isAuth) {
			if (!array_key_exists('error', $isAuth)) {
				$this->_ci->session->set_userdata($isAuth);
				// log_activity(array('identity' => userdata('user_id'), 'logged_in' => true), array('identity' => NULL, 'logged_in' => false), NULL, 'auth', 'login');
			} else {
				$this->setError('uim_auth.' . $isAuth['error']);
				$isAuth = false;
			}
		} else {
			$isAuth = false;
			$this->setError(lang('auth.login_unsuccessful'));
		}

		return $isAuth;
	}

    public function generateAccessMenus($level)
    {
        $this->_ci->session->set_userdata('access_menus', gzdeflate(json_encode($this->_ci->_authModel->getAccessedMenu(sanitize_input($level))), 9));
    }

    public function loggedOut()
    {
        $this->_ci->session->sess_destroy();
    }

    public function setMessageDelimiters($start_delimiter, $end_delimiter)
    {
        $this->message_start_delimiter = $start_delimiter;
        $this->message_end_delimiter   = $end_delimiter;

        return true;
    }

    public function setErrorDelimiters($start_delimiter, $end_delimiter)
    {
        $this->error_start_delimiter = $start_delimiter;
        $this->error_end_delimiter   = $end_delimiter;

        return true;
    }

    public function setMessage($message)
    {
        $this->messages[] = $message;

        return $message;
    }

    public function messages()
    {
        $_output = '';
        foreach ($this->messages as $message) {
            $_output .= $this->message_start_delimiter . $message . $this->message_end_delimiter;
        }

        return $_output;
    }

    public function setError($error)
    {
        $this->errors[] = $error;

        return $error;
    }

    public function errors()
    {
        $_output = '';
        foreach ($this->errors as $error) {
            $_output .= $this->error_start_delimiter . $this->_ci->lang->line($error) . $this->error_end_delimiter;
        }

        return $_output;
    }

}
