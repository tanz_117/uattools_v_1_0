/*
Navicat MySQL Data Transfer

Source Server         : localhost_mySQL
Source Server Version : 50626
Source Host           : localhost:3306
Source Database       : onlinestore_1

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2017-10-24 10:21:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for slider
-- ----------------------------
DROP TABLE IF EXISTS `slider`;
CREATE TABLE `slider` (
  `slider_id` varchar(36) NOT NULL,
  `slider_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`slider_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of slider
-- ----------------------------
INSERT INTO `slider` VALUES ('12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'Chrysanthemum.jpg');
INSERT INTO `slider` VALUES ('13EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'Desert.jpg');
INSERT INTO `slider` VALUES ('14EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'Hydrangeas.jpg');
INSERT INTO `slider` VALUES ('15EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'Jellyfish.jpg');
INSERT INTO `slider` VALUES ('16EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'Koala.jpg');
INSERT INTO `slider` VALUES ('17EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'Lighthouse.jpg');
INSERT INTO `slider` VALUES ('18EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'Penguins.jpg');
INSERT INTO `slider` VALUES ('19EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'Tulips.jpg');
