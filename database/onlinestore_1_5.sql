/*
Navicat MySQL Data Transfer

Source Server         : localhost_mySQL
Source Server Version : 50626
Source Host           : localhost:3306
Source Database       : onlinestore_1

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2017-10-08 08:18:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for anggota
-- ----------------------------
DROP TABLE IF EXISTS `anggota`;
CREATE TABLE `anggota` (
  `user_id` varchar(36) NOT NULL,
  `anggota_nama` varchar(100) DEFAULT NULL,
  `anggota_tmplahir` varchar(100) DEFAULT NULL,
  `anggota_tgllahir` date DEFAULT NULL,
  `anggota_alamat` text,
  `anggota_jnskelamin` varchar(1) DEFAULT NULL,
  `anggota_tglregistrasi` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `anggota_notlp` varchar(50) DEFAULT NULL,
  `anggota_email` varchar(50) DEFAULT NULL,
  `anggota_noidentitas` varchar(50) DEFAULT NULL,
  `anggota_jnsidentitas` varchar(36) DEFAULT NULL,
  `anggota_scanidentitas` text,
  `anggota_approvedby` varchar(36) DEFAULT NULL,
  `anggota_tglapprove` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `anggota_persetujuan` bit(1) DEFAULT NULL,
  `anggota_photo` text,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of anggota
-- ----------------------------
INSERT INTO `anggota` VALUES ('2b6cd002-a48e-11e7-8af8-54650c0f9c19', 'administrator 01', 'bandung', '2017-10-01', 'green city view', 'L', '2017-10-01 17:01:17', '089898798', 'saya@mail.com', '1234567890', 'KTP', 'tes', 'adm1', '2017-10-01 17:01:54', '', 'tes1');
INSERT INTO `anggota` VALUES ('ede4108c-a7fd-11e7-bbff-54650c0f9c19', 'testing 2', 'sdf', '2017-10-11', 'sadf', 'L', '2017-10-03 12:44:32', '098', 'lkjl', '0987', '12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '3121_Jellyfish.jpg', 'adm1', '2017-10-03 12:44:32', '', '17028_Hydrangeas.jpg');

-- ----------------------------
-- Table structure for audit_trails
-- ----------------------------
DROP TABLE IF EXISTS `audit_trails`;
CREATE TABLE `audit_trails` (
  `audit_trail_id` varchar(36) DEFAULT NULL,
  `audit_trail_action_date` datetime DEFAULT NULL,
  `audit_trail_user_id` varchar(36) DEFAULT NULL,
  `audit_trail_ip_address` varchar(16) DEFAULT NULL,
  `audit_trail_browser_ua` text,
  `audit_trail_history_id` varchar(36) DEFAULT NULL,
  `audit_trail_status` varchar(4) DEFAULT NULL,
  `audit_trail_action_id` varchar(25) DEFAULT NULL,
  `audit_trail_module_id` varchar(32) DEFAULT NULL,
  `audit_trail_record_id` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of audit_trails
-- ----------------------------
INSERT INTO `audit_trails` VALUES ('2da203ad-a197-11e7-82fb-54650c0f9c19', '2017-09-25 09:14:10', '  ', '  ', '  ', '2d8d4047-a197-11e7-82fb-54650c0f9c19', '  ', '  ', '  ', '  ');
INSERT INTO `audit_trails` VALUES ('43ae2ac9-a198-11e7-82fb-54650c0f9c19', '2017-09-25 09:21:57', '', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '43a065bb-a198-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('76d632d1-a198-11e7-82fb-54650c0f9c19', '2017-09-25 09:23:22', '', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '76ac0620-a198-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('ad681659-a198-11e7-82fb-54650c0f9c19', '2017-09-25 09:24:54', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'ad5208ae-a198-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('1f85b271-a19b-11e7-82fb-54650c0f9c19', '2017-09-25 09:42:24', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '1f72262e-a19b-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('36b31d3b-a19b-11e7-82fb-54650c0f9c19', '2017-09-25 09:43:03', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '36a38f7a-a19b-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('7e30316e-a19b-11e7-82fb-54650c0f9c19', '2017-09-25 09:45:03', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '7e199ad7-a19b-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('995abe65-a19b-11e7-82fb-54650c0f9c19', '2017-09-25 09:45:49', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '994bfd2e-a19b-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('1bb41212-a19d-11e7-82fb-54650c0f9c19', '2017-09-25 09:56:37', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '1ba56384-a19d-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('29ef3bed-a19d-11e7-82fb-54650c0f9c19', '2017-09-25 09:57:01', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '29d9a420-a19d-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('46fc5b4e-a19e-11e7-82fb-54650c0f9c19', '2017-09-25 10:04:59', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '46ef2bcb-a19e-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('4ec9a5fe-a19e-11e7-82fb-54650c0f9c19', '2017-09-25 10:05:12', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '4eb32108-a19e-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('7cf9a0a7-a19e-11e7-82fb-54650c0f9c19', '2017-09-25 10:06:30', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '7cd8bbf1-a19e-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('98c1c1dc-a19e-11e7-82fb-54650c0f9c19', '2017-09-25 10:07:16', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '98b4ba37-a19e-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('debbdf71-a19f-11e7-82fb-54650c0f9c19', '2017-09-25 10:16:23', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'de7ec82b-a19f-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('e6e9cd7f-a19f-11e7-82fb-54650c0f9c19', '2017-09-25 10:16:37', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'e6dbb52d-a19f-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('17c6802f-a1a1-11e7-82fb-54650c0f9c19', '2017-09-25 10:25:08', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '17b80710-a1a1-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('d03d63af-a1a3-11e7-82fb-54650c0f9c19', '2017-09-25 10:44:37', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'd0233878-a1a3-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('de4bf0f5-a1a3-11e7-82fb-54650c0f9c19', '2017-09-25 10:45:01', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'de3bdbc7-a1a3-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('3e07f649-a1a6-11e7-82fb-54650c0f9c19', '2017-09-25 11:02:00', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '3df6c7b9-a1a6-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('4fff648b-a1a6-11e7-82fb-54650c0f9c19', '2017-09-25 11:02:30', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '4ff591ae-a1a6-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('5488bbd9-a1a6-11e7-82fb-54650c0f9c19', '2017-09-25 11:02:38', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '547924bd-a1a6-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('594c39c3-a1a6-11e7-82fb-54650c0f9c19', '2017-09-25 11:02:46', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '59419377-a1a6-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('cb95a247-a1a6-11e7-82fb-54650c0f9c19', '2017-09-25 11:05:58', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'cb8a5c63-a1a6-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('e7af3b53-a1a6-11e7-82fb-54650c0f9c19', '2017-09-25 11:06:45', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'e7a23b02-a1a6-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('f1a21079-a1aa-11e7-82fb-54650c0f9c19', '2017-09-25 11:35:39', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'f1863bb2-a1aa-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('40e2e7ee-a1ad-11e7-82fb-54650c0f9c19', '2017-09-25 11:52:11', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '40d890d5-a1ad-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('e6ac144b-a1b0-11e7-82fb-54650c0f9c19', '2017-09-25 12:18:18', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'e698747c-a1b0-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('ec4c73cd-a1b0-11e7-82fb-54650c0f9c19', '2017-09-25 12:18:28', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'ec4233ca-a1b0-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('63f298c0-a1ba-11e7-82fb-54650c0f9c19', '2017-09-25 13:26:14', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '63deb379-a1ba-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('e24d1061-a1bc-11e7-82fb-54650c0f9c19', '2017-09-25 13:44:06', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'e24472b7-a1bc-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('eb46ec30-a1bc-11e7-82fb-54650c0f9c19', '2017-09-25 13:44:21', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'eb3e5ec1-a1bc-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('eeab8363-a1bc-11e7-82fb-54650c0f9c19', '2017-09-25 13:44:26', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'ee9e1acd-a1bc-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('f79b5a06-a1bc-11e7-82fb-54650c0f9c19', '2017-09-25 13:44:41', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'f78e590b-a1bc-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('179e20e1-a1bd-11e7-82fb-54650c0f9c19', '2017-09-25 13:45:35', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '178f037f-a1bd-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('2a432706-a1bd-11e7-82fb-54650c0f9c19', '2017-09-25 13:46:06', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '2a314af6-a1bd-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('da4e7c35-a1be-11e7-82fb-54650c0f9c19', '2017-09-25 13:58:11', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'da46e229-a1be-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('e0e39ec9-a1be-11e7-82fb-54650c0f9c19', '2017-09-25 13:58:22', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'e0d78e53-a1be-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('ef6cbd0e-a1be-11e7-82fb-54650c0f9c19', '2017-09-25 13:58:47', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'ef5d55c3-a1be-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('06de7561-a1bf-11e7-82fb-54650c0f9c19', '2017-09-25 13:59:26', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '06d2d58f-a1bf-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('1ba9e21c-a1bf-11e7-82fb-54650c0f9c19', '2017-09-25 14:00:01', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '1b9d7daa-a1bf-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('b8adfab4-a1bf-11e7-82fb-54650c0f9c19', '2017-09-25 14:04:24', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'b89f0747-a1bf-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('c40f6688-a1bf-11e7-82fb-54650c0f9c19', '2017-09-25 14:04:43', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'c402ddb7-a1bf-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('d844ce3d-a1bf-11e7-82fb-54650c0f9c19', '2017-09-25 14:05:17', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'd839f258-a1bf-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('e75fcb9c-a1bf-11e7-82fb-54650c0f9c19', '2017-09-25 14:05:43', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'e7571eef-a1bf-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('f6c20280-a1bf-11e7-82fb-54650c0f9c19', '2017-09-25 14:06:08', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'f6b7bfc1-a1bf-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('0976ec86-a1c0-11e7-82fb-54650c0f9c19', '2017-09-25 14:06:40', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '096ad7c0-a1c0-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('15a03d89-a1c0-11e7-82fb-54650c0f9c19', '2017-09-25 14:07:00', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '158f27de-a1c0-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('2385ffff-a1c0-11e7-82fb-54650c0f9c19', '2017-09-25 14:07:23', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '237bca06-a1c0-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('05aae81a-a1c1-11e7-82fb-54650c0f9c19', '2017-09-25 14:13:43', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '059e5f73-a1c1-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('571dfc54-a1c1-11e7-82fb-54650c0f9c19', '2017-09-25 14:16:00', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '5713f117-a1c1-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('628ab737-a1c1-11e7-82fb-54650c0f9c19', '2017-09-25 14:16:19', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '627cbcc8-a1c1-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('71ea7d1f-a1c1-11e7-82fb-54650c0f9c19', '2017-09-25 14:16:45', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '71df9980-a1c1-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('7c803136-a1c1-11e7-82fb-54650c0f9c19', '2017-09-25 14:17:02', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '7c72f98d-a1c1-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('aa15b028-a1c1-11e7-82fb-54650c0f9c19', '2017-09-25 14:18:19', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'aa04afe4-a1c1-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('c103414f-a1c1-11e7-82fb-54650c0f9c19', '2017-09-25 14:18:57', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'c0f4760f-a1c1-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('ca23419c-a1c1-11e7-82fb-54650c0f9c19', '2017-09-25 14:19:13', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'ca115bfb-a1c1-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('ea41f048-a1c1-11e7-82fb-54650c0f9c19', '2017-09-25 14:20:06', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'ea2cd741-a1c1-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('2193eb96-a1c2-11e7-82fb-54650c0f9c19', '2017-09-25 14:21:39', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '2182bd50-a1c2-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('02b4460e-a1c3-11e7-82fb-54650c0f9c19', '2017-09-25 14:27:57', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '02a563a1-a1c3-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('19b71508-a1d2-11e7-82fb-54650c0f9c19', '2017-09-25 16:15:58', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '19a6abc5-a1d2-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('7271e0b8-a22a-11e7-82fb-54650c0f9c19', '2017-09-26 02:48:28', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '724f1537-a22a-11e7-82fb-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('032c31c6-a25d-11e7-9707-54650c0f9c19', '2017-09-26 08:50:02', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '03211b11-a25d-11e7-9707-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('a4d4fbf4-a260-11e7-9707-54650c0f9c19', '2017-09-26 09:16:02', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'a4ca707d-a260-11e7-9707-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('870d659f-a261-11e7-9707-54650c0f9c19', '2017-09-26 09:22:22', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '8702d272-a261-11e7-9707-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('081436f5-a262-11e7-9707-54650c0f9c19', '2017-09-26 09:25:58', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '0801923f-a262-11e7-9707-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('ec41aaa2-a262-11e7-9707-54650c0f9c19', '2017-09-26 09:32:21', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'ec35c0e9-a262-11e7-9707-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('2ca4a426-a28f-11e7-9707-54650c0f9c19', '2017-09-26 14:49:07', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '2c7b9a85-a28f-11e7-9707-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('875f27e9-a28f-11e7-9707-54650c0f9c19', '2017-09-26 14:51:39', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '8756ad13-a28f-11e7-9707-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('d5416589-a28f-11e7-9707-54650c0f9c19', '2017-09-26 14:53:50', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'd532b932-a28f-11e7-9707-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('46bc7f8e-a290-11e7-9707-54650c0f9c19', '2017-09-26 14:57:00', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '46b40901-a290-11e7-9707-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('66063365-a299-11e7-9707-54650c0f9c19', '2017-09-26 16:02:18', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '65f841a5-a299-11e7-9707-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('3ef63109-a2cc-11e7-8fb8-54650c0f9c19', '2017-09-26 22:06:17', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '3eda9d4e-a2cc-11e7-8fb8-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('933ccdd6-a2cf-11e7-8fb8-54650c0f9c19', '2017-09-26 22:30:07', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '93316e63-a2cf-11e7-8fb8-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('1d1f805e-a324-11e7-8261-54650c0f9c19', '2017-09-27 08:35:16', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '1cfd324c-a324-11e7-8261-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('1225a63c-a335-11e7-8261-54650c0f9c19', '2017-09-27 10:36:39', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '12032a5c-a335-11e7-8261-54650c0f9c19', '0000', 'add', 'users', '11f43519-a335-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('48c16084-a335-11e7-8261-54650c0f9c19', '2017-09-27 10:38:10', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '48b8518d-a335-11e7-8261-54650c0f9c19', '0000', 'add', 'users', '48aef523-a335-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('9604fa02-a335-11e7-8261-54650c0f9c19', '2017-09-27 10:40:20', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '95fd8c4e-a335-11e7-8261-54650c0f9c19', '0000', 'edit', 'users', '123');
INSERT INTO `audit_trails` VALUES ('f05882a0-a335-11e7-8261-54650c0f9c19', '2017-09-27 10:42:52', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'f0502eb8-a335-11e7-8261-54650c0f9c19', '0000', 'edit', 'users', '48aef523-a335-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('4f3e0b78-a336-11e7-8261-54650c0f9c19', '2017-09-27 10:45:31', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '4f36e47b-a336-11e7-8261-54650c0f9c19', '0000', 'edit', 'users', '11f43519-a335-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('ef9a5043-a337-11e7-8261-54650c0f9c19', '2017-09-27 10:57:09', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'ef8e1354-a337-11e7-8261-54650c0f9c19', '0000', 'edit', 'users', '98501005-a32f-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('3f0734bf-a338-11e7-8261-54650c0f9c19', '2017-09-27 10:59:23', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '3efb1542-a338-11e7-8261-54650c0f9c19', '0000', 'edit', 'users', '98501005-a32f-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('4e1001e8-a339-11e7-8261-54650c0f9c19', '2017-09-27 11:06:57', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '4df937b5-a339-11e7-8261-54650c0f9c19', '0000', 'delete', 'users', '123');
INSERT INTO `audit_trails` VALUES ('4d8283ba-a33b-11e7-8261-54650c0f9c19', '2017-09-27 11:21:15', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '4d7f4bfc-a33b-11e7-8261-54650c0f9c19', '0000', 'delete', 'users', '48aef523-a335-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('6b9995cb-a33b-11e7-8261-54650c0f9c19', '2017-09-27 11:22:06', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '6b83207e-a33b-11e7-8261-54650c0f9c19', '0000', 'delete', 'users', '11f43519-a335-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('9c3ea141-a33c-11e7-8261-54650c0f9c19', '2017-09-27 11:30:37', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '9c32e3a2-a33c-11e7-8261-54650c0f9c19', '0000', 'add', 'users', '9c244b70-a33c-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('adc66d52-a33c-11e7-8261-54650c0f9c19', '2017-09-27 11:31:06', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'adaf41bd-a33c-11e7-8261-54650c0f9c19', '0000', 'add', 'users', 'ad9a9abe-a33c-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('b3e8bb57-a33c-11e7-8261-54650c0f9c19', '2017-09-27 11:31:17', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'b3d6f1e0-a33c-11e7-8261-54650c0f9c19', '0000', 'delete', 'users', '9c244b70-a33c-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('beb48a54-a33c-11e7-8261-54650c0f9c19', '2017-09-27 11:31:35', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'be8b8325-a33c-11e7-8261-54650c0f9c19', '0000', 'add', 'users', 'be797ed7-a33c-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('c97f39a7-a33c-11e7-8261-54650c0f9c19', '2017-09-27 11:31:53', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'c9743a94-a33c-11e7-8261-54650c0f9c19', '0000', 'delete', 'users', 'ad9a9abe-a33c-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('168e400e-a33d-11e7-8261-54650c0f9c19', '2017-09-27 11:34:02', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '1677fff3-a33d-11e7-8261-54650c0f9c19', '0000', 'add', 'users', '1669765e-a33d-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('23bbe852-a33d-11e7-8261-54650c0f9c19', '2017-09-27 11:34:24', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '23affaa2-a33d-11e7-8261-54650c0f9c19', '0000', 'delete', 'users', '1669765e-a33d-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('49dc0cd6-a33e-11e7-8261-54650c0f9c19', '2017-09-27 11:42:38', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '49bfbe55-a33e-11e7-8261-54650c0f9c19', '0000', 'delete', 'users', 'be797ed7-a33c-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('63d528a0-a33e-11e7-8261-54650c0f9c19', '2017-09-27 11:43:21', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '63c6da9a-a33e-11e7-8261-54650c0f9c19', '0000', 'add', 'users', '63b77293-a33e-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('689bc79d-a33e-11e7-8261-54650c0f9c19', '2017-09-27 11:43:29', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '6886cf22-a33e-11e7-8261-54650c0f9c19', '0000', 'delete', 'users', '63b77293-a33e-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('2e31d546-a33f-11e7-8261-54650c0f9c19', '2017-09-27 11:49:01', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '2e2d8089-a33f-11e7-8261-54650c0f9c19', '0000', 'add', 'users', '2e2344ab-a33f-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('4168b5a7-a33f-11e7-8261-54650c0f9c19', '2017-09-27 11:49:33', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '415668cf-a33f-11e7-8261-54650c0f9c19', '0000', 'delete', 'users', '2e2344ab-a33f-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('7d35e7a9-a33f-11e7-8261-54650c0f9c19', '2017-09-27 11:51:13', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '7d31e39a-a33f-11e7-8261-54650c0f9c19', '0000', 'add', 'users', '7d286558-a33f-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('80f9c90c-a33f-11e7-8261-54650c0f9c19', '2017-09-27 11:51:20', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '80e6ee67-a33f-11e7-8261-54650c0f9c19', '0000', 'delete', 'users', '7d286558-a33f-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('a5c11066-a33f-11e7-8261-54650c0f9c19', '2017-09-27 11:52:21', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'a5bc5cf2-a33f-11e7-8261-54650c0f9c19', '0000', 'add', 'users', 'a5af0a9a-a33f-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('b5a2726d-a33f-11e7-8261-54650c0f9c19', '2017-09-27 11:52:48', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'b5900fbe-a33f-11e7-8261-54650c0f9c19', '0000', 'delete', 'users', 'a5af0a9a-a33f-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('0644a755-a346-11e7-8261-54650c0f9c19', '2017-09-27 12:38:00', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '063f4ea1-a346-11e7-8261-54650c0f9c19', '0000', 'add', 'users', '062c76f2-a346-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('0b1f06aa-a346-11e7-8261-54650c0f9c19', '2017-09-27 12:38:08', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '0b094ce2-a346-11e7-8261-54650c0f9c19', '0000', 'delete', 'users', '062c76f2-a346-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('763cb0bf-a346-11e7-8261-54650c0f9c19', '2017-09-27 12:41:08', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '76399527-a346-11e7-8261-54650c0f9c19', '0000', 'add', 'users', '76295848-a346-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('7abf5ddb-a346-11e7-8261-54650c0f9c19', '2017-09-27 12:41:16', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '7aaa3bed-a346-11e7-8261-54650c0f9c19', '0000', 'delete', 'users', '76295848-a346-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('7acd4771-a346-11e7-8261-54650c0f9c19', '2017-09-27 12:41:16', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '7ac9724b-a346-11e7-8261-54650c0f9c19', '0000', 'delete', 'users', '98501005-a32f-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('d005453c-a346-11e7-8261-54650c0f9c19', '2017-09-27 12:43:39', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'cff6489e-a346-11e7-8261-54650c0f9c19', '0000', 'add', 'users', 'cfe5ecc4-a346-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('d78eb40b-a346-11e7-8261-54650c0f9c19', '2017-09-27 12:43:51', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'd78a92b0-a346-11e7-8261-54650c0f9c19', '0000', 'add', 'users', 'd77ac0e4-a346-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('dfd17f72-a346-11e7-8261-54650c0f9c19', '2017-09-27 12:44:05', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'dfc87b94-a346-11e7-8261-54650c0f9c19', '0000', 'add', 'users', 'dfb29a86-a346-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('e566bfd4-a346-11e7-8261-54650c0f9c19', '2017-09-27 12:44:15', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'e54fd40f-a346-11e7-8261-54650c0f9c19', '0000', 'delete', 'users', 'dfb29a86-a346-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('2942c3a2-a347-11e7-8261-54650c0f9c19', '2017-09-27 12:46:08', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '292bafd4-a347-11e7-8261-54650c0f9c19', '0000', 'delete', 'users', 'd77ac0e4-a346-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('296dd400-a347-11e7-8261-54650c0f9c19', '2017-09-27 12:46:09', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '2968e204-a347-11e7-8261-54650c0f9c19', '0000', 'delete', 'users', 'cfe5ecc4-a346-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('4c416ba5-a347-11e7-8261-54650c0f9c19', '2017-09-27 12:47:07', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '4c29ef3e-a347-11e7-8261-54650c0f9c19', '0000', 'add', 'users', '4c0b4c39-a347-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('53daf2c2-a347-11e7-8261-54650c0f9c19', '2017-09-27 12:47:20', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '53cc8c55-a347-11e7-8261-54650c0f9c19', '0000', 'add', 'users', '53ba15bd-a347-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('5abbe4c3-a347-11e7-8261-54650c0f9c19', '2017-09-27 12:47:31', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '5a8cdf2c-a347-11e7-8261-54650c0f9c19', '0000', 'add', 'users', '5a797479-a347-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('5f51eaa1-a347-11e7-8261-54650c0f9c19', '2017-09-27 12:47:39', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '5f479837-a347-11e7-8261-54650c0f9c19', '0000', 'delete', 'users', '4c0b4c39-a347-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('63f31d65-a347-11e7-8261-54650c0f9c19', '2017-09-27 12:47:47', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '63dc27b0-a347-11e7-8261-54650c0f9c19', '0000', 'delete', 'users', '5a797479-a347-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('6408b164-a347-11e7-8261-54650c0f9c19', '2017-09-27 12:47:47', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '6404d416-a347-11e7-8261-54650c0f9c19', '0000', 'delete', 'users', '53ba15bd-a347-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('a53739c5-a347-11e7-8261-54650c0f9c19', '2017-09-27 12:49:36', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'a5196b2b-a347-11e7-8261-54650c0f9c19', '0000', 'add', 'users', 'a50c8027-a347-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('db241fe6-a349-11e7-8261-54650c0f9c19', '2017-09-27 13:05:26', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'db16cea6-a349-11e7-8261-54650c0f9c19', '0000', 'add', 'users', 'db096d4a-a349-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('a6bb2e58-a34c-11e7-8261-54650c0f9c19', '2017-09-27 13:25:27', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'a6a91085-a34c-11e7-8261-54650c0f9c19', '0000', 'status', 'users', '');
INSERT INTO `audit_trails` VALUES ('e3834720-a34c-11e7-8261-54650c0f9c19', '2017-09-27 13:27:09', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'e3790aed-a34c-11e7-8261-54650c0f9c19', '0000', 'status', 'users', 'db096d4a-a349-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('221f0940-a34d-11e7-8261-54650c0f9c19', '2017-09-27 13:28:54', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '2215c899-a34d-11e7-8261-54650c0f9c19', '0000', 'status', 'users', 'db096d4a-a349-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('3af294b7-a34d-11e7-8261-54650c0f9c19', '2017-09-27 13:29:35', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '3ae8569c-a34d-11e7-8261-54650c0f9c19', '0000', 'status', 'users', 'db096d4a-a349-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('560cc6db-a34d-11e7-8261-54650c0f9c19', '2017-09-27 13:30:21', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '55f1f2ee-a34d-11e7-8261-54650c0f9c19', '0000', 'status', 'users', 'db096d4a-a349-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('5e7d44e1-a34d-11e7-8261-54650c0f9c19', '2017-09-27 13:30:35', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '5e75a8a7-a34d-11e7-8261-54650c0f9c19', '0000', 'status', 'users', 'db096d4a-a349-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('68d9eabc-a34d-11e7-8261-54650c0f9c19', '2017-09-27 13:30:52', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '68cd1c64-a34d-11e7-8261-54650c0f9c19', '0000', 'status', 'users', 'db096d4a-a349-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('7e226cdd-a34d-11e7-8261-54650c0f9c19', '2017-09-27 13:31:28', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '7e144e9b-a34d-11e7-8261-54650c0f9c19', '0000', 'status', 'users', 'db096d4a-a349-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('860de775-a34d-11e7-8261-54650c0f9c19', '2017-09-27 13:31:41', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '85fb9df6-a34d-11e7-8261-54650c0f9c19', '0000', 'status', 'users', 'db096d4a-a349-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('e5288a68-a34d-11e7-8261-54650c0f9c19', '2017-09-27 13:34:21', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'e51a7313-a34d-11e7-8261-54650c0f9c19', '0000', 'status', 'users', 'db096d4a-a349-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('f6c766b6-a34d-11e7-8261-54650c0f9c19', '2017-09-27 13:34:50', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'f6ba6352-a34d-11e7-8261-54650c0f9c19', '0000', 'status', 'users', 'db096d4a-a349-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('51e63182-a34e-11e7-8261-54650c0f9c19', '2017-09-27 13:37:23', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '51e10b84-a34e-11e7-8261-54650c0f9c19', '0000', 'status', 'users', 'db096d4a-a349-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('67831b7c-a34e-11e7-8261-54650c0f9c19', '2017-09-27 13:38:00', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '67765387-a34e-11e7-8261-54650c0f9c19', '0000', 'status', 'users', 'db096d4a-a349-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('99e6b9e0-a34e-11e7-8261-54650c0f9c19', '2017-09-27 13:39:24', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '99d83cd2-a34e-11e7-8261-54650c0f9c19', '0000', 'status', 'users', 'db096d4a-a349-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('a3a7f46c-a34e-11e7-8261-54650c0f9c19', '2017-09-27 13:39:41', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'a398962c-a34e-11e7-8261-54650c0f9c19', '0000', 'status', 'users', 'db096d4a-a349-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('bc2912ed-a34e-11e7-8261-54650c0f9c19', '2017-09-27 13:40:22', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'bc187c38-a34e-11e7-8261-54650c0f9c19', '0000', 'status', 'users', 'db096d4a-a349-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('361c0702-a34f-11e7-8261-54650c0f9c19', '2017-09-27 13:43:46', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '3612ff89-a34f-11e7-8261-54650c0f9c19', '0000', 'status', 'users', 'db096d4a-a349-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('62e3cf74-a34f-11e7-8261-54650c0f9c19', '2017-09-27 13:45:01', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '62dad701-a34f-11e7-8261-54650c0f9c19', '0000', 'status', 'users', 'db096d4a-a349-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('1b3d00ed-a350-11e7-8261-54650c0f9c19', '2017-09-27 13:50:11', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '1b340033-a350-11e7-8261-54650c0f9c19', '0000', 'status', 'users', 'db096d4a-a349-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('2384ce6f-a350-11e7-8261-54650c0f9c19', '2017-09-27 13:50:25', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '237b6b94-a350-11e7-8261-54650c0f9c19', '0000', 'status', 'users', 'db096d4a-a349-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('91b28a72-a351-11e7-8261-54650c0f9c19', '2017-09-27 14:00:39', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '91899dcb-a351-11e7-8261-54650c0f9c19', '0000', 'add', 'users', '916bf986-a351-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('9929b27e-a351-11e7-8261-54650c0f9c19', '2017-09-27 14:00:51', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '99248297-a351-11e7-8261-54650c0f9c19', '0000', 'add', 'users', '99140a7a-a351-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('a14293f5-a351-11e7-8261-54650c0f9c19', '2017-09-27 14:01:05', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'a119ae7e-a351-11e7-8261-54650c0f9c19', '0000', 'add', 'users', 'a10a91e2-a351-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('aad11775-a351-11e7-8261-54650c0f9c19', '2017-09-27 14:01:21', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'aac64294-a351-11e7-8261-54650c0f9c19', '0000', 'add', 'users', 'aab7b891-a351-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('c094eb2b-a35b-11e7-8261-54650c0f9c19', '2017-09-27 15:13:33', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'c0727e8b-a35b-11e7-8261-54650c0f9c19', '0000', 'add', 'users', 'c0611aca-a35b-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('ceab464b-a35b-11e7-8261-54650c0f9c19', '2017-09-27 15:13:56', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'cea61a22-a35b-11e7-8261-54650c0f9c19', '0000', 'add', 'users', 'ce948be2-a35b-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('691a4043-a3b7-11e7-bd60-54650c0f9c19', '2017-09-28 02:09:39', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '690bfb0b-a3b7-11e7-bd60-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('73cd6033-a3b7-11e7-bd60-54650c0f9c19', '2017-09-28 02:09:57', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '73b88523-a3b7-11e7-bd60-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('85b82f3a-a3b7-11e7-bd60-54650c0f9c19', '2017-09-28 02:10:27', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '85ad4f46-a3b7-11e7-bd60-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('b043fba4-a3bb-11e7-bd60-54650c0f9c19', '2017-09-28 02:40:16', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'b024a4f4-a3bb-11e7-bd60-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('ac66b990-a48a-11e7-8af8-54650c0f9c19', '2017-09-29 03:21:56', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'ac531e28-a48a-11e7-8af8-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('afdaa120-a48a-11e7-8af8-54650c0f9c19', '2017-09-29 03:22:02', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'afcde3ad-a48a-11e7-8af8-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('e36a9d96-a48d-11e7-8af8-54650c0f9c19', '2017-09-29 03:44:57', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'e358a34e-a48d-11e7-8af8-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('04c690f4-a48e-11e7-8af8-54650c0f9c19', '2017-09-29 03:45:53', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '04b3d2d8-a48e-11e7-8af8-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('169feb35-a48e-11e7-8af8-54650c0f9c19', '2017-09-29 03:46:23', '1111', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '169abdea-a48e-11e7-8af8-54650c0f9c19', '0000', 'delete', 'users', 'aab7b891-a351-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('16d176f9-a48e-11e7-8af8-54650c0f9c19', '2017-09-29 03:46:23', '1111', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '16ba8e4d-a48e-11e7-8af8-54650c0f9c19', '0000', 'delete', 'users', 'db096d4a-a349-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('16e20680-a48e-11e7-8af8-54650c0f9c19', '2017-09-29 03:46:23', '1111', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '16dce342-a48e-11e7-8af8-54650c0f9c19', '0000', 'delete', 'users', 'a50c8027-a347-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('16f1acb2-a48e-11e7-8af8-54650c0f9c19', '2017-09-29 03:46:23', '1111', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '16ed6c35-a48e-11e7-8af8-54650c0f9c19', '0000', 'delete', 'users', '916bf986-a351-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('17030b0f-a48e-11e7-8af8-54650c0f9c19', '2017-09-29 03:46:23', '1111', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '16fdf0d5-a48e-11e7-8af8-54650c0f9c19', '0000', 'delete', 'users', 'a10a91e2-a351-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('1bc7ea91-a48e-11e7-8af8-54650c0f9c19', '2017-09-29 03:46:31', '1111', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '1bb9d28e-a48e-11e7-8af8-54650c0f9c19', '0000', 'delete', 'users', 'ce948be2-a35b-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('1be513c0-a48e-11e7-8af8-54650c0f9c19', '2017-09-29 03:46:31', '1111', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '1bdff95f-a48e-11e7-8af8-54650c0f9c19', '0000', 'delete', 'users', '99140a7a-a351-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('1bf59d6b-a48e-11e7-8af8-54650c0f9c19', '2017-09-29 03:46:32', '1111', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '1bf080d8-a48e-11e7-8af8-54650c0f9c19', '0000', 'delete', 'users', 'c0611aca-a35b-11e7-8261-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('2b898f05-a48e-11e7-8af8-54650c0f9c19', '2017-09-29 03:46:58', '1111', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2b7c79c2-a48e-11e7-8af8-54650c0f9c19', '0000', 'add', 'users', '2b6cd002-a48e-11e7-8af8-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('3687b10c-a48e-11e7-8af8-54650c0f9c19', '2017-09-29 03:47:16', '1111', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '367c27de-a48e-11e7-8af8-54650c0f9c19', '0000', 'edit', 'users', '2b6cd002-a48e-11e7-8af8-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('4d34ced5-a48e-11e7-8af8-54650c0f9c19', '2017-09-29 03:47:54', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '4d239842-a48e-11e7-8af8-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('b35bc599-a4b7-11e7-b063-54650c0f9c19', '2017-09-29 08:44:14', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'b3502917-a4b7-11e7-b063-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('e0b479b9-a4b7-11e7-b063-54650c0f9c19', '2017-09-29 08:45:30', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'e0aa40ea-a4b7-11e7-b063-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('efb99df2-a4b7-11e7-b063-54650c0f9c19', '2017-09-29 08:45:56', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'efaad3f0-a4b7-11e7-b063-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('83e4255c-a4e0-11e7-b063-54650c0f9c19', '2017-09-29 13:36:24', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '83d8bb49-a4e0-11e7-b063-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('89f90957-a4e0-11e7-b063-54650c0f9c19', '2017-09-29 13:36:35', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '89e92c06-a4e0-11e7-b063-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('9778fc6b-a4e0-11e7-b063-54650c0f9c19', '2017-09-29 13:36:57', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '9769fcf0-a4e0-11e7-b063-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('d717ea3b-a4e0-11e7-b063-54650c0f9c19', '2017-09-29 13:38:44', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'd71420bf-a4e0-11e7-b063-54650c0f9c19', '0000', 'add', 'users', 'd706d42d-a4e0-11e7-b063-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('e72348c2-a4e0-11e7-b063-54650c0f9c19', '2017-09-29 13:39:11', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'e71bacb7-a4e0-11e7-b063-54650c0f9c19', '0000', 'edit', 'users', '2b6cd002-a48e-11e7-8af8-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('40f8cd93-a4e1-11e7-b063-54650c0f9c19', '2017-09-29 13:41:42', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '40e08deb-a4e1-11e7-b063-54650c0f9c19', '0000', 'edit', 'users', 'd706d42d-a4e0-11e7-b063-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('dcd088ee-a4e4-11e7-b063-54650c0f9c19', '2017-09-29 14:07:32', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'dccb8d25-a4e4-11e7-b063-54650c0f9c19', '0000', 'edit', 'roles', '2');
INSERT INTO `audit_trails` VALUES ('fcd0507d-a4e4-11e7-b063-54650c0f9c19', '2017-09-29 14:08:25', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'fcc0fc35-a4e4-11e7-b063-54650c0f9c19', '0000', 'edit', 'roles', '2');
INSERT INTO `audit_trails` VALUES ('f29c5448-a4e5-11e7-b063-54650c0f9c19', '2017-09-29 14:15:18', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'f287e14e-a4e5-11e7-b063-54650c0f9c19', '0000', 'edit', 'roles', '1');
INSERT INTO `audit_trails` VALUES ('bc1a870e-a4e7-11e7-b063-54650c0f9c19', '2017-09-29 14:28:05', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'bc0dd64e-a4e7-11e7-b063-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('c99703f2-a4e7-11e7-b063-54650c0f9c19', '2017-09-29 14:28:28', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'c98cabc7-a4e7-11e7-b063-54650c0f9c19', '0000', 'edit', 'roles', '1');
INSERT INTO `audit_trails` VALUES ('d0de04ef-a4e7-11e7-b063-54650c0f9c19', '2017-09-29 14:28:40', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'd0d03ec7-a4e7-11e7-b063-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('9697e390-a570-11e7-9fc1-54650c0f9c19', '2017-09-30 06:47:43', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '96737e65-a570-11e7-9fc1-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('4030a6db-a5a3-11e7-bee2-54650c0f9c19', '2017-09-30 12:50:23', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '4020b38f-a5a3-11e7-bee2-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('783dfa09-a5a7-11e7-bee2-54650c0f9c19', '2017-09-30 13:20:35', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '783855e8-a5a7-11e7-bee2-54650c0f9c19', '0000', 'edit', 'menus', '4');
INSERT INTO `audit_trails` VALUES ('80412e51-a5a7-11e7-bee2-54650c0f9c19', '2017-09-30 13:20:48', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '8036919f-a5a7-11e7-bee2-54650c0f9c19', '0000', 'edit', 'menus', '4');
INSERT INTO `audit_trails` VALUES ('8e370cfc-a5a7-11e7-bee2-54650c0f9c19', '2017-09-30 13:21:12', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '8e29c0d0-a5a7-11e7-bee2-54650c0f9c19', '0000', 'edit', 'menus', '4');
INSERT INTO `audit_trails` VALUES ('adde5632-a5aa-11e7-bee2-54650c0f9c19', '2017-09-30 13:43:33', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'add5438f-a5aa-11e7-bee2-54650c0f9c19', '0000', 'edit', 'roles', '1');
INSERT INTO `audit_trails` VALUES ('b637f7dd-a5aa-11e7-bee2-54650c0f9c19', '2017-09-30 13:43:47', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'b6272545-a5aa-11e7-bee2-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('77f03df0-a5e4-11e7-bee2-54650c0f9c19', '2017-09-30 20:37:16', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '77dc7418-a5e4-11e7-bee2-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('e2d681bf-a5e8-11e7-bee2-54650c0f9c19', '2017-09-30 21:08:53', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'e2ceb24e-a5e8-11e7-bee2-54650c0f9c19', '0000', 'add', 'products', 'e2bffe8e-a5e8-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('36842e8c-a622-11e7-bee2-54650c0f9c19', '2017-10-01 03:59:18', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '3670ba4e-a622-11e7-bee2-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('3f4b29a2-a624-11e7-bee2-54650c0f9c19', '2017-10-01 04:13:51', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '3f422f74-a624-11e7-bee2-54650c0f9c19', '0000', 'edit', 'products', 'e2bffe8e-a5e8-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('bb05c250-a62f-11e7-bee2-54650c0f9c19', '2017-10-01 05:36:03', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'bac91623-a62f-11e7-bee2-54650c0f9c19', '0000', 'add', 'products', 'babc039e-a62f-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('1df0ba60-a630-11e7-bee2-54650c0f9c19', '2017-10-01 05:38:49', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '1ded860b-a630-11e7-bee2-54650c0f9c19', '0000', 'add', 'products', '1ddf36b7-a630-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('6543f404-a63a-11e7-bee2-54650c0f9c19', '2017-10-01 06:52:25', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '653af7c0-a63a-11e7-bee2-54650c0f9c19', '0000', 'add', 'products', '652c096d-a63a-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('bcf5455a-a63a-11e7-bee2-54650c0f9c19', '2017-10-01 06:54:52', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'bceb7dc7-a63a-11e7-bee2-54650c0f9c19', '0000', 'add', 'products', 'bcdf4d4e-a63a-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('08cd57fd-a63b-11e7-bee2-54650c0f9c19', '2017-10-01 06:56:59', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '08c58107-a63b-11e7-bee2-54650c0f9c19', '0000', 'add', 'products', '08ace22d-a63b-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('2083cddc-a63d-11e7-bee2-54650c0f9c19', '2017-10-01 07:11:58', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2071a78e-a63d-11e7-bee2-54650c0f9c19', '0000', 'add', 'products', '20650133-a63d-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('985a601c-a63d-11e7-bee2-54650c0f9c19', '2017-10-01 07:15:19', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '984d851f-a63d-11e7-bee2-54650c0f9c19', '0000', 'add', 'products', '983ebfc6-a63d-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('6ffa71dc-a63e-11e7-bee2-54650c0f9c19', '2017-10-01 07:21:21', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '6ff37377-a63e-11e7-bee2-54650c0f9c19', '0000', 'add', 'products', '6fd9a04e-a63e-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('957c795c-a63e-11e7-bee2-54650c0f9c19', '2017-10-01 07:22:24', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '95752d24-a63e-11e7-bee2-54650c0f9c19', '0000', 'add', 'products', '95696fcd-a63e-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('be60857c-a63e-11e7-bee2-54650c0f9c19', '2017-10-01 07:23:32', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'be58d14b-a63e-11e7-bee2-54650c0f9c19', '0000', 'add', 'products', 'be469fbc-a63e-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('e9935ce0-a63f-11e7-bee2-54650c0f9c19', '2017-10-01 07:31:54', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'e98a4141-a63f-11e7-bee2-54650c0f9c19', '0000', 'edit', 'products', '95696fcd-a63e-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('01ee93ac-a640-11e7-bee2-54650c0f9c19', '2017-10-01 07:32:35', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '01e18ed0-a640-11e7-bee2-54650c0f9c19', '0000', 'edit', 'products', '95696fcd-a63e-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('45dfbf37-a641-11e7-bee2-54650c0f9c19', '2017-10-01 07:41:38', '', '::1', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Mobile Safari/537.36', '45d38d9c-a641-11e7-bee2-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('4036585a-a68b-11e7-a31c-54650c0f9c19', '2017-10-01 16:31:06', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '40268471-a68b-11e7-a31c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('407e8056-a68b-11e7-a31c-54650c0f9c19', '2017-10-01 16:31:07', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '406c5f72-a68b-11e7-a31c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('d78b1790-a68c-11e7-a31c-54650c0f9c19', '2017-10-01 16:42:30', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'd7821605-a68c-11e7-a31c-54650c0f9c19', '0000', 'edit', 'roles', '1');
INSERT INTO `audit_trails` VALUES ('e342145d-a68c-11e7-a31c-54650c0f9c19', '2017-10-01 16:42:49', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'e334a634-a68c-11e7-a31c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('25c7a1b0-a68f-11e7-a31c-54650c0f9c19', '2017-10-01 16:59:00', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '25b837cb-a68f-11e7-a31c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('427548e1-a69d-11e7-99dc-54650c0f9c19', '2017-10-01 18:40:01', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '426b8233-a69d-11e7-99dc-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('48d06bf3-a712-11e7-89f0-54650c0f9c19', '2017-10-02 08:37:43', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '48c47fc1-a712-11e7-89f0-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('ad4fe7ad-a73b-11e7-89f0-54650c0f9c19', '2017-10-02 13:34:00', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'ad430419-a73b-11e7-89f0-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('700cda5c-a74e-11e7-89f0-54650c0f9c19', '2017-10-02 15:48:18', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '70018c53-a74e-11e7-89f0-54650c0f9c19', '0000', 'add', 'anggotas', '70016c3d-a74e-11e7-89f0-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('f5fc3cec-a74e-11e7-89f0-54650c0f9c19', '2017-10-02 15:52:02', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'f5f70c88-a74e-11e7-89f0-54650c0f9c19', '0000', 'add', 'anggotas', 'f59eb893-a74e-11e7-89f0-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('751993f2-a750-11e7-89f0-54650c0f9c19', '2017-10-02 16:02:45', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '74ffacb5-a750-11e7-89f0-54650c0f9c19', '0000', 'add', 'anggotas', '74ed75ff-a750-11e7-89f0-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('b664fcb3-a750-11e7-89f0-54650c0f9c19', '2017-10-02 16:04:35', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'b65fceb6-a750-11e7-89f0-54650c0f9c19', '0000', 'add', 'anggotas', 'b6539c1f-a750-11e7-89f0-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('846cb370-a75d-11e7-89f0-54650c0f9c19', '2017-10-02 17:36:14', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '845d42d7-a75d-11e7-89f0-54650c0f9c19', '0000', 'edit', 'anggotas', 'b6539c1f-a750-11e7-89f0-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('a1f606c9-a762-11e7-89f0-54650c0f9c19', '2017-10-02 18:12:51', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'a1e940ac-a762-11e7-89f0-54650c0f9c19', '0000', 'edit', 'anggotas', 'b6539c1f-a750-11e7-89f0-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('09d84cb3-a7dd-11e7-bbff-54650c0f9c19', '2017-10-03 08:49:05', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '09cbf6df-a7dd-11e7-bbff-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('b87bf0ca-a7dd-11e7-bbff-54650c0f9c19', '2017-10-03 08:53:58', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'b85bddac-a7dd-11e7-bbff-54650c0f9c19', '0000', 'edit', 'roles', '1');
INSERT INTO `audit_trails` VALUES ('5a25febe-a7de-11e7-bbff-54650c0f9c19', '2017-10-03 08:58:29', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '5a1351a9-a7de-11e7-bbff-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('a293e799-a7e9-11e7-bbff-54650c0f9c19', '2017-10-03 10:19:15', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'a28eba0c-a7e9-11e7-bbff-54650c0f9c19', '0000', 'add', 'anggotas', 'a280c6d1-a7e9-11e7-bbff-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('ea97eb68-a7e9-11e7-bbff-54650c0f9c19', '2017-10-03 10:21:16', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'ea93c5ef-a7e9-11e7-bbff-54650c0f9c19', '0000', 'add', 'anggotas', 'ea861c0f-a7e9-11e7-bbff-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('3a28d1c2-a7fa-11e7-bbff-54650c0f9c19', '2017-10-03 12:18:02', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '3a23ada6-a7fa-11e7-bbff-54650c0f9c19', '0000', 'view', 'reqanggotas', '3a1acebe-a7fa-11e7-bbff-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('fb8877e8-a7fc-11e7-bbff-54650c0f9c19', '2017-10-03 12:37:45', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'fb835150-a7fc-11e7-bbff-54650c0f9c19', '0000', 'view', 'reqanggotas', 'fb7220b6-a7fc-11e7-bbff-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('fb99ea65-a7fc-11e7-bbff-54650c0f9c19', '2017-10-03 12:37:45', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'fb8ec9e4-a7fc-11e7-bbff-54650c0f9c19', '0000', 'view', 'reqanggotas', 'ea861c0f-a7e9-11e7-bbff-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('edf4cc13-a7fd-11e7-bbff-54650c0f9c19', '2017-10-03 12:44:32', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'edebd1d6-a7fd-11e7-bbff-54650c0f9c19', '0000', 'view', 'reqanggotas', 'ede4108c-a7fd-11e7-bbff-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('ee10b676-a7fd-11e7-bbff-54650c0f9c19', '2017-10-03 12:44:32', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'ee0a5aa6-a7fd-11e7-bbff-54650c0f9c19', '0000', 'view', 'reqanggotas', 'ea861c0f-a7e9-11e7-bbff-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('3bfc7efd-a80b-11e7-bbff-54650c0f9c19', '2017-10-03 14:19:47', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '3bf378e3-a80b-11e7-bbff-54650c0f9c19', '0000', 'reject', 'reqanggotas', 'ea861c0f-a7e9-11e7-bbff-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('886209b1-a80b-11e7-bbff-54650c0f9c19', '2017-10-03 14:21:55', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '88591321-a80b-11e7-bbff-54650c0f9c19', '0000', 'reject', 'reqanggotas', 'ea861c0f-a7e9-11e7-bbff-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('dd3e1bde-a8a2-11e7-8d92-54650c0f9c19', '2017-10-04 08:25:10', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'dd2584cd-a8a2-11e7-8d92-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('ee5ad01f-a8a2-11e7-8d92-54650c0f9c19', '2017-10-04 08:25:39', 'adm1', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'ee514e33-a8a2-11e7-8d92-54650c0f9c19', '0000', 'edit', 'roles', '1');
INSERT INTO `audit_trails` VALUES ('104e4f0e-a8a3-11e7-8d92-54650c0f9c19', '2017-10-04 08:26:36', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '104441fe-a8a3-11e7-8d92-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('827bf611-a8e6-11e7-8d92-54650c0f9c19', '2017-10-04 16:29:25', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '82698391-a8e6-11e7-8d92-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('cf8e3070-a8e6-11e7-8d92-54650c0f9c19', '2017-10-04 16:31:34', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'cf7d0348-a8e6-11e7-8d92-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('610fbe85-a8e7-11e7-8d92-54650c0f9c19', '2017-10-04 16:35:38', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '6101f219-a8e7-11e7-8d92-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('6afdefba-a8e9-11e7-8d92-54650c0f9c19', '2017-10-04 16:50:14', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '6aefd6ba-a8e9-11e7-8d92-54650c0f9c19', '0000', 'add', 'reqsaldos', '6adcf5f9-a8e9-11e7-8d92-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('94c4cf97-a92d-11e7-aa40-54650c0f9c19', '2017-10-05 00:58:09', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '94b597c2-a92d-11e7-aa40-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('6a2cdf41-a92e-11e7-aa40-54650c0f9c19', '2017-10-05 01:04:07', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '6a27babb-a92e-11e7-aa40-54650c0f9c19', '0000', 'add', 'reqsaldos', '6a1a3397-a92e-11e7-aa40-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('745b599a-a934-11e7-aa40-54650c0f9c19', '2017-10-05 01:47:21', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '744e7bf8-a934-11e7-aa40-54650c0f9c19', '0000', 'reject', 'reqsaldos', '6a1a3397-a92e-11e7-aa40-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('9c28f4c6-a934-11e7-aa40-54650c0f9c19', '2017-10-05 01:48:28', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '9c1eb16d-a934-11e7-aa40-54650c0f9c19', '0000', 'reject', 'reqsaldos', '6a1a3397-a92e-11e7-aa40-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('1007a51f-a93a-11e7-aa40-54650c0f9c19', '2017-10-05 02:27:30', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '0ffc6cd6-a93a-11e7-aa40-54650c0f9c19', '0000', 'view', 'reqsaldos', '6a1a3397-a92e-11e7-aa40-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('3e023954-a93a-11e7-aa40-54650c0f9c19', '2017-10-05 02:28:47', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '3dee01e0-a93a-11e7-aa40-54650c0f9c19', '0000', 'reject', 'reqsaldos', '');
INSERT INTO `audit_trails` VALUES ('5a180c7a-a93a-11e7-aa40-54650c0f9c19', '2017-10-05 02:29:34', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '5a080c57-a93a-11e7-aa40-54650c0f9c19', '0000', 'reject', 'reqsaldos', '');
INSERT INTO `audit_trails` VALUES ('c19f3ae5-a93a-11e7-aa40-54650c0f9c19', '2017-10-05 02:32:28', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'c199151f-a93a-11e7-aa40-54650c0f9c19', '0000', 'reject', 'reqsaldos', '6adcf5f9-a8e9-11e7-8d92-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('ddbf85db-a93a-11e7-aa40-54650c0f9c19', '2017-10-05 02:33:15', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'ddaec609-a93a-11e7-aa40-54650c0f9c19', '0000', 'reject', 'reqsaldos', '6adcf5f9-a8e9-11e7-8d92-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('1aee28a4-a93b-11e7-aa40-54650c0f9c19', '2017-10-05 02:34:57', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '1ae48a75-a93b-11e7-aa40-54650c0f9c19', '0000', 'reject', 'reqanggotas', 'ea861c0f-a7e9-11e7-bbff-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('556154be-a93b-11e7-aa40-54650c0f9c19', '2017-10-05 02:36:35', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '55585c78-a93b-11e7-aa40-54650c0f9c19', '0000', 'reject', 'reqsaldos', '6adcf5f9-a8e9-11e7-8d92-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('6e55dffb-a99d-11e7-8dd0-54650c0f9c19', '2017-10-05 14:18:49', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '6e452292-a99d-11e7-8dd0-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('7939a482-a99d-11e7-8dd0-54650c0f9c19', '2017-10-05 14:19:07', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '792df6a4-a99d-11e7-8dd0-54650c0f9c19', '0000', 'edit', 'roles', '1');
INSERT INTO `audit_trails` VALUES ('81e72bac-a99d-11e7-8dd0-54650c0f9c19', '2017-10-05 14:19:21', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '81d76555-a99d-11e7-8dd0-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('fa80abd2-a9f8-11e7-8a12-54650c0f9c19', '2017-10-06 01:14:07', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'fa73597c-a9f8-11e7-8a12-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('04bbd9c9-aa34-11e7-bddd-54650c0f9c19', '2017-10-06 08:16:45', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '04abb357-aa34-11e7-bddd-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('dd038c89-aa38-11e7-bddd-54650c0f9c19', '2017-10-06 08:51:26', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'dcf8d13c-aa38-11e7-bddd-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('b98788a9-aa3b-11e7-bddd-54650c0f9c19', '2017-10-06 09:11:55', '', '', '', 'b97ab781-aa3b-11e7-bddd-54650c0f9c19', '', '', '', '');
INSERT INTO `audit_trails` VALUES ('639252b4-aa3c-11e7-bddd-54650c0f9c19', '2017-10-06 09:16:40', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '638689bd-aa3c-11e7-bddd-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('6fc2106d-aa3c-11e7-bddd-54650c0f9c19', '2017-10-06 09:17:00', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '6fb90f5a-aa3c-11e7-bddd-54650c0f9c19', '0000', 'view', 'trxpesanans', 'DB1EC34F-7E36-4CAE-A2EB-E329D12EE9BC');
INSERT INTO `audit_trails` VALUES ('5bf27f06-aa3e-11e7-bddd-54650c0f9c19', '2017-10-06 09:30:46', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '5be46b8b-aa3e-11e7-bddd-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('8123c60b-aa3e-11e7-bddd-54650c0f9c19', '2017-10-06 09:31:48', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '81111b05-aa3e-11e7-bddd-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('8bfeb3b3-aa3e-11e7-bddd-54650c0f9c19', '2017-10-06 09:32:07', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '8bc0189a-aa3e-11e7-bddd-54650c0f9c19', '0000', 'view', 'trxpesanans', '37ACEBDF-F21E-4246-9B91-1056E66E2D4E');
INSERT INTO `audit_trails` VALUES ('3370de1e-aa3f-11e7-bddd-54650c0f9c19', '2017-10-06 09:36:48', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '3357ea4c-aa3f-11e7-bddd-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('3d15640f-aa3f-11e7-bddd-54650c0f9c19', '2017-10-06 09:37:04', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '3ce6bc1a-aa3f-11e7-bddd-54650c0f9c19', '0000', 'view', 'trxpesanans', 'F8F5405D-225B-40BE-A56C-5660A1C86015');
INSERT INTO `audit_trails` VALUES ('67571bf3-aa41-11e7-bddd-54650c0f9c19', '2017-10-06 09:52:34', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '674d1ff4-aa41-11e7-bddd-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('6b4b66d1-aa41-11e7-bddd-54650c0f9c19', '2017-10-06 09:52:40', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '6b4074ba-aa41-11e7-bddd-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('79d8027d-aa41-11e7-bddd-54650c0f9c19', '2017-10-06 09:53:05', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '79c12f44-aa41-11e7-bddd-54650c0f9c19', '0000', 'view', 'trxpesanans', '7F039353-7DC2-459E-95D9-B65193B70CAB');
INSERT INTO `audit_trails` VALUES ('f0f7a237-aa42-11e7-bddd-54650c0f9c19', '2017-10-06 10:03:34', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'f0f12f72-aa42-11e7-bddd-54650c0f9c19', '0000', 'view', 'trxpesanans', '1ddf36b7-a630-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('1213aa8e-aa43-11e7-bddd-54650c0f9c19', '2017-10-06 10:04:30', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '120d39ce-aa43-11e7-bddd-54650c0f9c19', '0000', 'view', 'trxpesanans', '08ace22d-a63b-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('34ff3ca4-aa43-11e7-bddd-54650c0f9c19', '2017-10-06 10:05:28', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '34f5e542-aa43-11e7-bddd-54650c0f9c19', '0000', 'view', 'trxpesanans', '1ddf36b7-a630-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('57f59707-aa43-11e7-bddd-54650c0f9c19', '2017-10-06 10:06:27', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '57f1384a-aa43-11e7-bddd-54650c0f9c19', '0000', 'view', 'trxpesanans', '08ace22d-a63b-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('7c53205b-aa43-11e7-bddd-54650c0f9c19', '2017-10-06 10:07:28', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '7c4778a3-aa43-11e7-bddd-54650c0f9c19', '0000', 'view', 'trxpesanans', '08ace22d-a63b-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('eb82113d-aa43-11e7-bddd-54650c0f9c19', '2017-10-06 10:10:35', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'eb7eca11-aa43-11e7-bddd-54650c0f9c19', '0000', 'view', 'trxpesanans', '652c096d-a63a-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('41976f1a-aa44-11e7-bddd-54650c0f9c19', '2017-10-06 10:12:59', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '4192389a-aa44-11e7-bddd-54650c0f9c19', '0000', 'view', 'trxpesanans', '08ace22d-a63b-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('51b71bbf-aa44-11e7-bddd-54650c0f9c19', '2017-10-06 10:13:26', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '51ad18f7-aa44-11e7-bddd-54650c0f9c19', '0000', 'view', 'trxpesanans', '08ace22d-a63b-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('68ae5d05-aa44-11e7-bddd-54650c0f9c19', '2017-10-06 10:14:04', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '68975e87-aa44-11e7-bddd-54650c0f9c19', '0000', 'view', 'trxpesanans', '1ddf36b7-a630-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('8e20a0c1-aa44-11e7-bddd-54650c0f9c19', '2017-10-06 10:15:07', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '8e135025-aa44-11e7-bddd-54650c0f9c19', '0000', 'view', 'trxpesanans', '08ace22d-a63b-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('94f01d27-aa44-11e7-bddd-54650c0f9c19', '2017-10-06 10:15:19', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '94e1a43a-aa44-11e7-bddd-54650c0f9c19', '0000', 'view', 'trxpesanans', '1ddf36b7-a630-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('405f7aaa-aa71-11e7-bddd-54650c0f9c19', '2017-10-06 15:35:05', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '40599a42-aa71-11e7-bddd-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('82737d3a-aa72-11e7-bddd-54650c0f9c19', '2017-10-06 15:44:05', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '8268a19e-aa72-11e7-bddd-54650c0f9c19', '0000', 'view', 'trxpesanans', '08ace22d-a63b-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('975435a3-aa72-11e7-bddd-54650c0f9c19', '2017-10-06 15:44:40', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '974c8d6a-aa72-11e7-bddd-54650c0f9c19', '0000', 'view', 'trxpesanans', '08ace22d-a63b-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('bf113253-aa72-11e7-bddd-54650c0f9c19', '2017-10-06 15:45:47', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'bf04dfb4-aa72-11e7-bddd-54650c0f9c19', '0000', 'view', 'trxpesanans', '08ace22d-a63b-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('c7e776e7-aa72-11e7-bddd-54650c0f9c19', '2017-10-06 15:46:02', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'c7ddd063-aa72-11e7-bddd-54650c0f9c19', '0000', 'view', 'trxpesanans', '2b6cd002-a48e-11e7-8af8-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('436c236b-aa94-11e7-bddd-54650c0f9c19', '2017-10-06 19:45:44', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '4362966f-aa94-11e7-bddd-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('5848c961-aaf9-11e7-bddd-54650c0f9c19', '2017-10-07 07:49:23', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '583cb3a9-aaf9-11e7-bddd-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('6c9316e8-aaf9-11e7-bddd-54650c0f9c19', '2017-10-07 07:49:57', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '6c86c055-aaf9-11e7-bddd-54650c0f9c19', '0000', 'view_trxpesanan_detail', 'trxpesanans', '6c71a013-aaf9-11e7-bddd-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('2e9153fa-aafa-11e7-bddd-54650c0f9c19', '2017-10-07 07:55:22', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2e8d092c-aafa-11e7-bddd-54650c0f9c19', '0000', 'view_trxpesanan_detail', 'trxpesanans', '7F039353-7DC2-459E-95D9-B65193B70CAB');
INSERT INTO `audit_trails` VALUES ('d8d9fd45-aafa-11e7-bddd-54650c0f9c19', '2017-10-07 08:00:08', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'd8d5b472-aafa-11e7-bddd-54650c0f9c19', '0000', 'edit', 'roles', '1');
INSERT INTO `audit_trails` VALUES ('2ff3eb7f-aafb-11e7-bddd-54650c0f9c19', '2017-10-07 08:02:34', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2fea760e-aafb-11e7-bddd-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('7c8361e9-ab32-11e7-bddd-54650c0f9c19', '2017-10-07 14:38:27', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '7c791e6d-ab32-11e7-bddd-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('5561316e-ab35-11e7-bddd-54650c0f9c19', '2017-10-07 14:58:50', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '555928a2-ab35-11e7-bddd-54650c0f9c19', '0000', 'view', 'trxpembayaranverify', '7F039353-7DC2-459E-95D9-B65193B70CAB');
INSERT INTO `audit_trails` VALUES ('28e3013d-ab37-11e7-bddd-54650c0f9c19', '2017-10-07 15:11:55', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '28d6bafb-ab37-11e7-bddd-54650c0f9c19', '0000', 'view', 'trxpembayaranverify', '7F039353-7DC2-459E-95D9-B65193B70CAB');
INSERT INTO `audit_trails` VALUES ('e4e109a7-ab37-11e7-bddd-54650c0f9c19', '2017-10-07 15:17:10', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'e4d8571d-ab37-11e7-bddd-54650c0f9c19', '0000', 'reject', 'trxpembayaranverify', '7F039353-7DC2-459E-95D9-B65193B70CAB');
INSERT INTO `audit_trails` VALUES ('231d45ac-ab38-11e7-bddd-54650c0f9c19', '2017-10-07 15:18:54', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '23143105-ab38-11e7-bddd-54650c0f9c19', '0000', 'reject', 'trxpembayaranverify', '7F039353-7DC2-459E-95D9-B65193B70CAB');
INSERT INTO `audit_trails` VALUES ('45754f67-ab39-11e7-bddd-54650c0f9c19', '2017-10-07 15:27:02', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '4571241b-ab39-11e7-bddd-54650c0f9c19', '0000', 'edit', 'roles', '1');
INSERT INTO `audit_trails` VALUES ('73c169e0-abb4-11e7-ac4a-54650c0f9c19', '2017-10-08 06:08:38', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '73b45965-abb4-11e7-ac4a-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('4346d3d6-abbc-11e7-ac4a-54650c0f9c19', '2017-10-08 07:04:32', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '433991c1-abbc-11e7-ac4a-54650c0f9c19', '0000', 'add', 'trxpengirimans', '7F039353-7DC2-459E-95D9-B65193B70CAB');
INSERT INTO `audit_trails` VALUES ('d438341d-abbc-11e7-ac4a-54650c0f9c19', '2017-10-08 07:08:36', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'd4351730-abbc-11e7-ac4a-54650c0f9c19', '0000', 'add', 'trxpengirimans', '7F039353-7DC2-459E-95D9-B65193B70CAB');
INSERT INTO `audit_trails` VALUES ('4ca10c05-abc4-11e7-ac4a-54650c0f9c19', '2017-10-08 08:02:04', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '4c976f33-abc4-11e7-ac4a-54650c0f9c19', '0000', 'edit', 'trxpengirimans', '7F039353-7DC2-459E-95D9-B65193B70CAB');
INSERT INTO `audit_trails` VALUES ('27f2b9b5-abc5-11e7-ac4a-54650c0f9c19', '2017-10-08 08:08:12', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '27ed770d-abc5-11e7-ac4a-54650c0f9c19', '0000', 'edit', 'roles', '1');

-- ----------------------------
-- Table structure for bank
-- ----------------------------
DROP TABLE IF EXISTS `bank`;
CREATE TABLE `bank` (
  `bank_id` int(11) NOT NULL,
  `bank_nama` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`bank_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bank
-- ----------------------------
INSERT INTO `bank` VALUES ('1', 'BCA');

-- ----------------------------
-- Table structure for configurationgroup
-- ----------------------------
DROP TABLE IF EXISTS `configurationgroup`;
CREATE TABLE `configurationgroup` (
  `configurationgroup_id` varchar(36) NOT NULL,
  `configurationgroup_name` varchar(100) NOT NULL,
  `configurationgroup_status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`configurationgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of configurationgroup
-- ----------------------------
INSERT INTO `configurationgroup` VALUES ('11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6E', 'parameter system', '1');
INSERT INTO `configurationgroup` VALUES ('11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'Jenis Identitas', '1');
INSERT INTO `configurationgroup` VALUES ('11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6G', 'Status Anggota', '1');
INSERT INTO `configurationgroup` VALUES ('11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6H', 'Status trxsaldo', '1');
INSERT INTO `configurationgroup` VALUES ('11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6I', 'Metode Pembayaran', '1');
INSERT INTO `configurationgroup` VALUES ('11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6J', 'Pembayaran status', '1');

-- ----------------------------
-- Table structure for configurations
-- ----------------------------
DROP TABLE IF EXISTS `configurations`;
CREATE TABLE `configurations` (
  `configuration_id` varchar(36) NOT NULL,
  `configuration_key` varchar(80) DEFAULT NULL,
  `configuration_value` longtext,
  `configuration_status` tinyint(4) DEFAULT NULL,
  `configurationgroup_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`configuration_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of configurations
-- ----------------------------
INSERT INTO `configurations` VALUES ('12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '1', 'KTP', null, '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F');
INSERT INTO `configurations` VALUES ('13EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '2', 'SIM', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F');
INSERT INTO `configurations` VALUES ('14EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'statusanggota.waiting', 'waiting', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6G');
INSERT INTO `configurations` VALUES ('15EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'statusanggota.active', 'active', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6G');
INSERT INTO `configurations` VALUES ('16EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'statusanggota.inactive', 'inactive', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6G');
INSERT INTO `configurations` VALUES ('17EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'statusanggota.rejected', 'rejected', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6G');
INSERT INTO `configurations` VALUES ('18EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '0', 'waiting', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6H');
INSERT INTO `configurations` VALUES ('19EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '1', 'approve', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6H');
INSERT INTO `configurations` VALUES ('20EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '2', 'reject', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6H');
INSERT INTO `configurations` VALUES ('21EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '1', 'Transfer', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6I');
INSERT INTO `configurations` VALUES ('22EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '2', 'Saldo', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6I');
INSERT INTO `configurations` VALUES ('23EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '1', 'waiting', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6J');
INSERT INTO `configurations` VALUES ('24EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '2', 'Approve', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6J');
INSERT INTO `configurations` VALUES ('25EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '3', 'Reject', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6J');
INSERT INTO `configurations` VALUES ('CF371C9618AB459299790F9A4A120F49', 'copyright', '2016. All rights reserved. andrimuhammad', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6E');
INSERT INTO `configurations` VALUES ('EB43051CAFAD49BDBACDC922F47D904C', 'created_by', 'templatelab', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6E');

-- ----------------------------
-- Table structure for dictionaries
-- ----------------------------
DROP TABLE IF EXISTS `dictionaries`;
CREATE TABLE `dictionaries` (
  `dictionary_id` int(11) NOT NULL,
  `dictionary_lang_id` char(4) NOT NULL,
  `dictionary_key` varchar(255) NOT NULL,
  `dictionary_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`dictionary_id`),
  KEY `dictionary_lang_id` (`dictionary_lang_id`),
  CONSTRAINT `dictionaries_ibfk_1` FOREIGN KEY (`dictionary_lang_id`) REFERENCES `languages` (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dictionaries
-- ----------------------------
INSERT INTO `dictionaries` VALUES ('0', 'id', 'base.page_title.products', 'Produk');
INSERT INTO `dictionaries` VALUES ('1', 'en', 'base.page_title.settings', 'Setting');
INSERT INTO `dictionaries` VALUES ('2', 'id', 'base.page_title.settings', 'Pengaturan');
INSERT INTO `dictionaries` VALUES ('3', 'en', 'base.page_title.menus', 'Menus');
INSERT INTO `dictionaries` VALUES ('4', 'id', 'base.page_title.menus', 'Menu');
INSERT INTO `dictionaries` VALUES ('5', 'en', 'base.page_title.users', 'Users');
INSERT INTO `dictionaries` VALUES ('6', 'id', 'base.page_title.users', 'User');
INSERT INTO `dictionaries` VALUES ('7', 'en', 'base.page_title.roles', 'Roles');
INSERT INTO `dictionaries` VALUES ('8', 'id', 'base.page_title.roles', 'Hak Akses');
INSERT INTO `dictionaries` VALUES ('9', 'en', 'base.page_title.languages', 'Languages');
INSERT INTO `dictionaries` VALUES ('10', 'id', 'base.page_title.languages', 'Bahasa');
INSERT INTO `dictionaries` VALUES ('11', 'en', 'base.page_title.dictionaries', 'Dictionaries');
INSERT INTO `dictionaries` VALUES ('12', 'id', 'base.page_title.dictionaries', 'Kamus Kata');
INSERT INTO `dictionaries` VALUES ('13', 'en', 'base.page_title.system_parameters', 'System Parameters');
INSERT INTO `dictionaries` VALUES ('14', 'id', 'base.page_title.system_parameters', 'Sistem Parameter');
INSERT INTO `dictionaries` VALUES ('15', 'en', 'base.page_title.group_parameters', 'Group Parameters');
INSERT INTO `dictionaries` VALUES ('16', 'id', 'base.page_title.group_parameters', 'Grup Parameter');
INSERT INTO `dictionaries` VALUES ('17', 'en', 'base.page_title.parameters', 'Parameters');
INSERT INTO `dictionaries` VALUES ('18', 'id', 'base.page_title.parameters', 'Parameter');
INSERT INTO `dictionaries` VALUES ('19', 'en', 'base.page_title.uids', 'UID');
INSERT INTO `dictionaries` VALUES ('20', 'id', 'base.page_title.uids', 'UID');
INSERT INTO `dictionaries` VALUES ('21', 'en', 'base.page_title.offices', 'Offices');
INSERT INTO `dictionaries` VALUES ('22', 'id', 'base.page_title.offices', 'Kantor');
INSERT INTO `dictionaries` VALUES ('23', 'en', 'base.page_title.parent_offices', 'Parent Offices');
INSERT INTO `dictionaries` VALUES ('24', 'id', 'base.page_title.parent_offices', 'Kantor Induk');
INSERT INTO `dictionaries` VALUES ('25', 'en', 'base.page_title.branch_offices', 'Branch Offices');
INSERT INTO `dictionaries` VALUES ('26', 'id', 'base.page_title.branch_offices', 'Kantor Cabang');
INSERT INTO `dictionaries` VALUES ('27', 'en', 'base.page_title.mapping', 'Mapping');
INSERT INTO `dictionaries` VALUES ('28', 'id', 'base.page_title.mapping', 'Mapping');
INSERT INTO `dictionaries` VALUES ('29', 'en', 'base.page_title.applications', 'Applications');
INSERT INTO `dictionaries` VALUES ('30', 'id', 'base.page_title.applications', 'Aplikasi');
INSERT INTO `dictionaries` VALUES ('31', 'en', 'base.page_title.functions', 'Functions');
INSERT INTO `dictionaries` VALUES ('32', 'id', 'base.page_title.functions', 'Fungsi');
INSERT INTO `dictionaries` VALUES ('33', 'en', 'base.page_title.mapping_access', 'UID Access');
INSERT INTO `dictionaries` VALUES ('34', 'id', 'base.page_title.mapping_access', 'Akses UID');
INSERT INTO `dictionaries` VALUES ('35', 'en', 'base.page_title.logs', 'Logs');
INSERT INTO `dictionaries` VALUES ('36', 'id', 'base.page_title.logs', 'Log');
INSERT INTO `dictionaries` VALUES ('39', 'en', 'base.page_title.servers', 'Servers');
INSERT INTO `dictionaries` VALUES ('40', 'id', 'base.page_title.servers', 'Server');
INSERT INTO `dictionaries` VALUES ('41', 'en', 'base.page_title.simulators', 'Simulator');
INSERT INTO `dictionaries` VALUES ('42', 'id', 'base.page_title.simulators', 'Simulator');
INSERT INTO `dictionaries` VALUES ('43', 'en', 'base.page_title.pc_address', 'PC IP Address');
INSERT INTO `dictionaries` VALUES ('44', 'id', 'base.page_title.pc_address', 'IP Address PC');
INSERT INTO `dictionaries` VALUES ('45', 'en', 'base.page_title.login_simulation', 'Login Simulation');
INSERT INTO `dictionaries` VALUES ('46', 'id', 'base.page_title.login_simulation', 'Simulasi Login');
INSERT INTO `dictionaries` VALUES ('47', 'en', 'menus.menu_name', 'Menu Name');
INSERT INTO `dictionaries` VALUES ('48', 'id', 'menus.menu_name', 'Nama Menu');
INSERT INTO `dictionaries` VALUES ('49', 'en', 'menus.menu_url', 'URL');
INSERT INTO `dictionaries` VALUES ('50', 'id', 'menus.menu_url', 'URL');
INSERT INTO `dictionaries` VALUES ('51', 'en', 'menus.menu_icon', 'Icon');
INSERT INTO `dictionaries` VALUES ('52', 'id', 'menus.menu_icon', 'Icon');
INSERT INTO `dictionaries` VALUES ('53', 'en', 'menus.menu_parent', 'Parent Menu');
INSERT INTO `dictionaries` VALUES ('54', 'id', 'menus.menu_parent', 'Menu Parent');
INSERT INTO `dictionaries` VALUES ('55', 'en', 'menus.menu_order', 'Order');
INSERT INTO `dictionaries` VALUES ('56', 'id', 'menus.menu_order', 'Urutan');
INSERT INTO `dictionaries` VALUES ('57', 'en', 'menus.menu_active', 'Status');
INSERT INTO `dictionaries` VALUES ('58', 'id', 'menus.menu_active', 'Menu');
INSERT INTO `dictionaries` VALUES ('59', 'en', 'menus.menu_create_module', 'Create Module File');
INSERT INTO `dictionaries` VALUES ('60', 'id', 'menus.menu_create_module', 'Buat File Module');
INSERT INTO `dictionaries` VALUES ('61', 'en', 'roles.role_name', 'Role Name');
INSERT INTO `dictionaries` VALUES ('62', 'id', 'roles.role_name', 'Nama Hak Akses');
INSERT INTO `dictionaries` VALUES ('63', 'en', 'roles.role_menu', 'Menus');
INSERT INTO `dictionaries` VALUES ('64', 'id', 'roles.role_menu', 'Menu');
INSERT INTO `dictionaries` VALUES ('65', 'en', 'roles.role_level', 'Level');
INSERT INTO `dictionaries` VALUES ('66', 'id', 'roles.role_level', 'Level');
INSERT INTO `dictionaries` VALUES ('67', 'en', 'roles.role_active', 'Status');
INSERT INTO `dictionaries` VALUES ('68', 'id', 'roles.role_active', 'Status');
INSERT INTO `dictionaries` VALUES ('69', 'en', 'roles.menu_id', 'Menu');
INSERT INTO `dictionaries` VALUES ('70', 'id', 'roles.menu_id', 'Menu');
INSERT INTO `dictionaries` VALUES ('71', 'en', 'users.identity', 'Identity');
INSERT INTO `dictionaries` VALUES ('72', 'id', 'users.identity', 'Identitas');
INSERT INTO `dictionaries` VALUES ('73', 'en', 'users.role_name', 'Role');
INSERT INTO `dictionaries` VALUES ('74', 'id', 'users.role_name', 'Hak Akses');
INSERT INTO `dictionaries` VALUES ('75', 'en', 'languages.language_id', 'Language');
INSERT INTO `dictionaries` VALUES ('76', 'id', 'languages.language_id', 'Bahasa');
INSERT INTO `dictionaries` VALUES ('77', 'en', 'languages.language_name', 'Language Name');
INSERT INTO `dictionaries` VALUES ('78', 'id', 'languages.language_name', 'Nama Bahasa');
INSERT INTO `dictionaries` VALUES ('79', 'en', 'languages.language_default', 'Default Language');
INSERT INTO `dictionaries` VALUES ('80', 'id', 'languages.language_default', 'Bahasa Utama');
INSERT INTO `dictionaries` VALUES ('81', 'en', 'languages.language_active', 'Status');
INSERT INTO `dictionaries` VALUES ('82', 'id', 'languages.language_active', 'Status');
INSERT INTO `dictionaries` VALUES ('83', 'en', 'dictionaries.dictionary_key', 'Dictionary Key');
INSERT INTO `dictionaries` VALUES ('84', 'id', 'dictionaries.dictionary_key', 'Kunci Kamus Kata');
INSERT INTO `dictionaries` VALUES ('85', 'en', 'dictionaries.dictionary_value', 'Dictionary Value');
INSERT INTO `dictionaries` VALUES ('86', 'id', 'dictionaries.dictionary_value', 'Isi Kamus Kata');
INSERT INTO `dictionaries` VALUES ('87', 'en', 'dictionaries.kamus', 'Dictionaries');
INSERT INTO `dictionaries` VALUES ('88', 'id', 'dictionaries.kamus', 'Kamus');
INSERT INTO `dictionaries` VALUES ('89', 'en', 'group_parameters.group_parameter_name', 'Group Parameter Name');
INSERT INTO `dictionaries` VALUES ('90', 'id', 'group_parameters.group_parameter_name', 'Nama Parameter Grup');
INSERT INTO `dictionaries` VALUES ('91', 'en', 'group_parameters.group_parameter_status', 'Status');
INSERT INTO `dictionaries` VALUES ('92', 'id', 'group_parameters.group_parameter_status', 'Status');
INSERT INTO `dictionaries` VALUES ('93', 'en', 'parameters.parameter_group_id', 'Group Parameter');
INSERT INTO `dictionaries` VALUES ('94', 'id', 'parameters.parameter_group_id', 'Parameter Grup');
INSERT INTO `dictionaries` VALUES ('95', 'en', 'parameters.parameter_name', 'Parameter');
INSERT INTO `dictionaries` VALUES ('96', 'id', 'parameters.parameter_name', 'Parameter');
INSERT INTO `dictionaries` VALUES ('97', 'en', 'parameters.parameter_status', 'Status');
INSERT INTO `dictionaries` VALUES ('98', 'id', 'parameters.parameter_status', 'Status');
INSERT INTO `dictionaries` VALUES ('99', 'en', 'uids.uid', 'UID');
INSERT INTO `dictionaries` VALUES ('100', 'id', 'uids.uid', 'UID');
INSERT INTO `dictionaries` VALUES ('101', 'en', 'uids.uid_name', 'Name');
INSERT INTO `dictionaries` VALUES ('102', 'id', 'uids.uid_name', 'Nama');
INSERT INTO `dictionaries` VALUES ('109', 'en', 'uids.uid_status', 'Status');
INSERT INTO `dictionaries` VALUES ('110', 'id', 'uids.uid_status', 'Status');
INSERT INTO `dictionaries` VALUES ('115', 'en', 'uids.uid_position', 'Position');
INSERT INTO `dictionaries` VALUES ('116', 'id', 'uids.uid_position', 'Posisi');
INSERT INTO `dictionaries` VALUES ('117', 'en', 'uids.uid_is_active', 'Active');
INSERT INTO `dictionaries` VALUES ('118', 'id', 'uids.uid_is_active', 'Aktif');
INSERT INTO `dictionaries` VALUES ('119', 'en', 'uids.uid_pass_is_expired', 'Password is Expired');
INSERT INTO `dictionaries` VALUES ('120', 'id', 'uids.uid_pass_is_expired', 'Password Kadaluarsa');
INSERT INTO `dictionaries` VALUES ('121', 'en', 'uids.uid_is_locked', 'Locked');
INSERT INTO `dictionaries` VALUES ('122', 'id', 'uids.uid_is_locked', 'Terkunci');
INSERT INTO `dictionaries` VALUES ('123', 'en', 'parent_offices.office_parent', 'Parent Office');
INSERT INTO `dictionaries` VALUES ('124', 'id', 'parent_offices.office_parent', 'Kantor Induk');
INSERT INTO `dictionaries` VALUES ('125', 'en', 'parent_offices.office_region', 'Office Region');
INSERT INTO `dictionaries` VALUES ('126', 'id', 'parent_offices.office_region', 'Kantor Wilayah');
INSERT INTO `dictionaries` VALUES ('127', 'en', 'parent_offices.office_parent_name', 'Parent Office Name');
INSERT INTO `dictionaries` VALUES ('128', 'id', 'parent_offices.office_parent_name', 'Nama Kantor Induk');
INSERT INTO `dictionaries` VALUES ('129', 'en', 'parent_offices.office_parent_code', 'Parent Office Code');
INSERT INTO `dictionaries` VALUES ('130', 'id', 'parent_offices.office_parent_code', 'Kode Kantor Induk');
INSERT INTO `dictionaries` VALUES ('131', 'en', 'branch_offices.office_branch', 'Branch Office');
INSERT INTO `dictionaries` VALUES ('132', 'id', 'branch_offices.office_branch', 'Kantor Cabang');
INSERT INTO `dictionaries` VALUES ('133', 'en', 'branch_offices.office_branch_parent', 'Parent Office');
INSERT INTO `dictionaries` VALUES ('134', 'id', 'branch_offices.office_branch_parent', 'Kantor Induk');
INSERT INTO `dictionaries` VALUES ('135', 'en', 'branch_offices.office_region', 'Office Region');
INSERT INTO `dictionaries` VALUES ('136', 'id', 'branch_offices.office_region', 'Kantor Wilayah');
INSERT INTO `dictionaries` VALUES ('137', 'en', 'branch_offices.office_parent_code', 'Parent Office Code');
INSERT INTO `dictionaries` VALUES ('138', 'id', 'branch_offices.office_parent_code', 'Kode Kantor Induk');
INSERT INTO `dictionaries` VALUES ('139', 'en', 'branch_offices.office_branch_name', 'Office Branch Name');
INSERT INTO `dictionaries` VALUES ('140', 'id', 'branch_offices.office_branch_name', 'Nama Kantor Cabang');
INSERT INTO `dictionaries` VALUES ('141', 'en', 'branch_offices.office_branch_code', 'Office Branch Code');
INSERT INTO `dictionaries` VALUES ('142', 'id', 'branch_offices.office_branch_code', 'Kode Kantor Cabang');
INSERT INTO `dictionaries` VALUES ('143', 'en', 'applications.application_name', 'Application Name');
INSERT INTO `dictionaries` VALUES ('144', 'id', 'applications.application_name', 'Nama Aplikasi');
INSERT INTO `dictionaries` VALUES ('145', 'en', 'applications.application_code', 'Application Code');
INSERT INTO `dictionaries` VALUES ('146', 'id', 'applications.application_code', 'Kode Aplikasi');
INSERT INTO `dictionaries` VALUES ('147', 'en', 'applications.application_status', 'Status');
INSERT INTO `dictionaries` VALUES ('148', 'id', 'applications.application_status', 'Status');
INSERT INTO `dictionaries` VALUES ('149', 'en', 'functions.function_name', 'Function Name');
INSERT INTO `dictionaries` VALUES ('150', 'id', 'functions.function_name', 'Nama Fungsi');
INSERT INTO `dictionaries` VALUES ('151', 'en', 'functions.function_level', 'Function Level');
INSERT INTO `dictionaries` VALUES ('152', 'id', 'functions.function_level', 'Level Fungsi');
INSERT INTO `dictionaries` VALUES ('153', 'en', 'functions.function_status', 'Status');
INSERT INTO `dictionaries` VALUES ('154', 'id', 'functions.function_status', 'Status');
INSERT INTO `dictionaries` VALUES ('155', 'en', 'functions.application_name', 'Application Name');
INSERT INTO `dictionaries` VALUES ('156', 'id', 'functions.application_name', 'Nama Aplikasi');
INSERT INTO `dictionaries` VALUES ('157', 'en', 'functions.function_application_id', 'Application ID');
INSERT INTO `dictionaries` VALUES ('158', 'id', 'functions.function_application_id', 'ID Aplikasi');
INSERT INTO `dictionaries` VALUES ('159', 'en', 'functions.function_application_detail', 'Function Detail');
INSERT INTO `dictionaries` VALUES ('160', 'id', 'functions.function_application_detail', 'Detil Fungsi');
INSERT INTO `dictionaries` VALUES ('161', 'en', 'mapping_access.access_uid', 'UID');
INSERT INTO `dictionaries` VALUES ('162', 'id', 'mapping_access.access_uid', 'UID');
INSERT INTO `dictionaries` VALUES ('163', 'en', 'mapping_access.access_office', 'Office');
INSERT INTO `dictionaries` VALUES ('164', 'id', 'mapping_access.access_office', 'Kantor');
INSERT INTO `dictionaries` VALUES ('165', 'en', 'mapping_access.access_application', 'Application');
INSERT INTO `dictionaries` VALUES ('166', 'id', 'mapping_access.access_application', 'Aplikasi');
INSERT INTO `dictionaries` VALUES ('167', 'en', 'mapping_access.access_function', 'Function');
INSERT INTO `dictionaries` VALUES ('168', 'id', 'mapping_access.access_function', 'Fungsi');
INSERT INTO `dictionaries` VALUES ('169', 'en', 'simulators.simulator_name', 'Simulator Name');
INSERT INTO `dictionaries` VALUES ('170', 'id', 'simulators.simulator_name', 'Nama Simulator');
INSERT INTO `dictionaries` VALUES ('171', 'en', 'simulators.simulator_ip', 'IP Address');
INSERT INTO `dictionaries` VALUES ('172', 'id', 'simulators.simulator_ip', 'IP Address');
INSERT INTO `dictionaries` VALUES ('173', 'en', 'simulators.simulator_port', 'Port');
INSERT INTO `dictionaries` VALUES ('174', 'id', 'simulators.simulator_port', 'Port');
INSERT INTO `dictionaries` VALUES ('175', 'en', 'simulators.simulator_ssh', 'SSH');
INSERT INTO `dictionaries` VALUES ('176', 'id', 'simulators.simulator_ssh', 'SSH');
INSERT INTO `dictionaries` VALUES ('177', 'en', 'simulators.simulator_running', 'Status');
INSERT INTO `dictionaries` VALUES ('178', 'id', 'simulators.simulator_running', 'Status');
INSERT INTO `dictionaries` VALUES ('179', 'en', 'simulators.simulator_ssh_username', 'SSH Username');
INSERT INTO `dictionaries` VALUES ('180', 'id', 'simulators.simulator_ssh_username', 'SSH Username');
INSERT INTO `dictionaries` VALUES ('181', 'en', 'simulators.simulator_ssh_password', 'SSH Password');
INSERT INTO `dictionaries` VALUES ('182', 'id', 'simulators.simulator_ssh_password', 'SSH Password');
INSERT INTO `dictionaries` VALUES ('183', 'en', 'simulators.simulator_ssh_port', 'SSH Port');
INSERT INTO `dictionaries` VALUES ('184', 'id', 'simulators.simulator_ssh_port', 'SSH Port');
INSERT INTO `dictionaries` VALUES ('185', 'en', 'pc_address.pc_address_ip', 'IP Address');
INSERT INTO `dictionaries` VALUES ('186', 'id', 'pc_address.pc_address_ip', 'IP Address');
INSERT INTO `dictionaries` VALUES ('187', 'en', 'pc_address.pc_address_name', 'PC Name');
INSERT INTO `dictionaries` VALUES ('188', 'id', 'pc_address.pc_address_name', 'Nama PC');
INSERT INTO `dictionaries` VALUES ('189', 'en', 'login_simulation.simulator_service', 'Service');
INSERT INTO `dictionaries` VALUES ('190', 'id', 'login_simulation.simulator_service', 'Service');
INSERT INTO `dictionaries` VALUES ('191', 'en', 'login_simulation.simulator_application', 'Application');
INSERT INTO `dictionaries` VALUES ('192', 'id', 'login_simulation.simulator_application', 'Aplikasi');
INSERT INTO `dictionaries` VALUES ('193', 'en', 'login_simulation.simulator_uid', 'UID');
INSERT INTO `dictionaries` VALUES ('194', 'id', 'login_simulation.simulator_uid', 'UID');
INSERT INTO `dictionaries` VALUES ('195', 'en', 'login_simulation.simulator_pass', 'Password');
INSERT INTO `dictionaries` VALUES ('196', 'id', 'login_simulation.simulator_pass', 'Password');
INSERT INTO `dictionaries` VALUES ('197', 'en', 'logs.log_request_time', 'Request Time');
INSERT INTO `dictionaries` VALUES ('198', 'id', 'logs.log_request_time', 'Waktu Request');
INSERT INTO `dictionaries` VALUES ('199', 'en', 'logs.log_ip_address', 'IP Address');
INSERT INTO `dictionaries` VALUES ('200', 'id', 'logs.log_ip_address', 'IP Address');
INSERT INTO `dictionaries` VALUES ('201', 'en', 'logs.log_uid', 'UID');
INSERT INTO `dictionaries` VALUES ('202', 'id', 'logs.log_uid', 'UID');
INSERT INTO `dictionaries` VALUES ('203', 'en', 'logs.log_application_code', 'Application');
INSERT INTO `dictionaries` VALUES ('204', 'id', 'logs.log_application_code', 'Aplikasi');
INSERT INTO `dictionaries` VALUES ('205', 'en', 'logs.log_request_message', 'Request Message');
INSERT INTO `dictionaries` VALUES ('206', 'id', 'logs.log_request_message', 'Request Message');
INSERT INTO `dictionaries` VALUES ('207', 'en', 'logs.log_response_message', 'Response Message');
INSERT INTO `dictionaries` VALUES ('208', 'id', 'logs.log_response_message', 'Response Message');
INSERT INTO `dictionaries` VALUES ('209', 'en', 'login_simulation.simulator_ip', 'IP Address');
INSERT INTO `dictionaries` VALUES ('210', 'id', 'login_simulation.simulator_ip', 'IP Address');
INSERT INTO `dictionaries` VALUES ('211', 'en', 'login_simulation.simulator_port', 'Port');
INSERT INTO `dictionaries` VALUES ('212', 'id', 'login_simulation.simulator_port', 'Port');
INSERT INTO `dictionaries` VALUES ('213', 'id', 'base.page_title.module_creators', 'Modul Creator');
INSERT INTO `dictionaries` VALUES ('216', 'id', 'base.page_title.module_creator_fields', 'Field Modul');
INSERT INTO `dictionaries` VALUES ('217', 'id', 'base.page_title.module_creator_setting', 'Pengaturan Modul');
INSERT INTO `dictionaries` VALUES ('218', 'id', 'base.page_title.module_creator_parent', 'Module Creator');
INSERT INTO `dictionaries` VALUES ('219', 'id', 'module_creators.module_name', 'Nama Modul');
INSERT INTO `dictionaries` VALUES ('220', 'id', 'module_creators.module_modelname', 'Nama Model');
INSERT INTO `dictionaries` VALUES ('221', 'id', 'module_creators.module_primarytablename', 'Tabel Utama');
INSERT INTO `dictionaries` VALUES ('222', 'id', 'module_creators.module_pkey', 'Primary Key');
INSERT INTO `dictionaries` VALUES ('223', 'id', 'module_creators.module_noticelabel', 'Notice Label');
INSERT INTO `dictionaries` VALUES ('224', 'id', 'module_creators.field_name', 'Nama Field');
INSERT INTO `dictionaries` VALUES ('225', 'id', 'module_creators.field_rules', 'Rules');
INSERT INTO `dictionaries` VALUES ('226', 'id', 'module_creator_fields.field_module_id', 'ID Modul');
INSERT INTO `dictionaries` VALUES ('227', 'id', 'module_creator_fields.field_name', 'Nama Field');
INSERT INTO `dictionaries` VALUES ('228', 'id', 'module_creator_fields.field_rules', 'Rules');
INSERT INTO `dictionaries` VALUES ('229', 'id', 'module_creator_fields.module_name', 'Nama Modul');
INSERT INTO `dictionaries` VALUES ('230', 'id', 'base.page_title.module_creator_master_rules', 'Master Rule');
INSERT INTO `dictionaries` VALUES ('231', 'id', 'base.page_title.module_creators', 'Module Creator');
INSERT INTO `dictionaries` VALUES ('232', 'id', 'module_creators.module_creators_title', 'Module Creator');
INSERT INTO `dictionaries` VALUES ('233', 'id', 'module_creators.module_created', 'Created');
INSERT INTO `dictionaries` VALUES ('234', 'id', 'module_creators.module_createdby', 'Created By');
INSERT INTO `dictionaries` VALUES ('235', 'id', 'module_creator_fields.module_creator_fields_title', 'Module Creator Fields');
INSERT INTO `dictionaries` VALUES ('236', 'id', 'module_creator_fields.module_id', 'ID Modul');
INSERT INTO `dictionaries` VALUES ('237', 'id', 'module_creator_fields.field_is_search', 'Field Pencarian');
INSERT INTO `dictionaries` VALUES ('238', 'id', 'module_creator_fields.field_is_on_grid', 'Ditampilkan di grid');
INSERT INTO `dictionaries` VALUES ('239', 'id', 'module_creator_fields.field_is_on_form', 'Field isian dlm form');
INSERT INTO `dictionaries` VALUES ('240', 'id', 'module_projects.module_projects_title', 'Projek');
INSERT INTO `dictionaries` VALUES ('241', 'id', 'base.page_title.module_projects', 'Daftar Projek');
INSERT INTO `dictionaries` VALUES ('242', 'id', 'module_projects.project_createdby', 'Dibuat oleh');
INSERT INTO `dictionaries` VALUES ('243', 'id', 'module_projects.project_created', 'Tgl. Pembuatan');
INSERT INTO `dictionaries` VALUES ('244', 'id', 'module_projects.project_name', 'Nama Projek');
INSERT INTO `dictionaries` VALUES ('245', 'id', 'module_projects.project_description', 'Deskripsi Projek');
INSERT INTO `dictionaries` VALUES ('246', 'id', 'module_creators.web_server', 'Web Server');
INSERT INTO `dictionaries` VALUES ('247', 'id', 'module_creators.app_server', 'Application Server');
INSERT INTO `dictionaries` VALUES ('248', 'id', 'module_creators.db_server', 'Database Server');
INSERT INTO `dictionaries` VALUES ('249', 'id', 'module_creators.project_list', 'Daftar Project');
INSERT INTO `dictionaries` VALUES ('250', 'id', 'module_creators.about_this_project', 'Tentang Project');
INSERT INTO `dictionaries` VALUES ('251', 'id', 'module_creators.project_description', 'Deskripsi Project');
INSERT INTO `dictionaries` VALUES ('252', 'id', 'base.page_title.menu_header', 'Menu Anda');
INSERT INTO `dictionaries` VALUES ('253', 'id', 'base.page_title.close_module_projects', 'Tutup Project');
INSERT INTO `dictionaries` VALUES ('254', 'id', 'module_projects.project_level', 'Level');
INSERT INTO `dictionaries` VALUES ('255', 'id', 'module_projects.project_status', 'Status');
INSERT INTO `dictionaries` VALUES ('256', 'id', 'module_projects.project_url', 'URL');
INSERT INTO `dictionaries` VALUES ('257', 'id', 'module_projects.project_user', 'User');
INSERT INTO `dictionaries` VALUES ('258', 'id', 'module_projects.project_appip', 'IP App Server');
INSERT INTO `dictionaries` VALUES ('259', 'id', 'module_projects.project_apptype', 'Type App Server');
INSERT INTO `dictionaries` VALUES ('260', 'id', 'module_projects.project_appname', 'Nama App Server');
INSERT INTO `dictionaries` VALUES ('261', 'id', 'module_projects.project_webip', 'IP Web Server');
INSERT INTO `dictionaries` VALUES ('262', 'id', 'module_projects.project_webtype', 'Type Web Server');
INSERT INTO `dictionaries` VALUES ('263', 'id', 'module_projects.project_webname', 'Nama Web Server');
INSERT INTO `dictionaries` VALUES ('264', 'id', 'module_projects.project_dbip', 'IP Database');
INSERT INTO `dictionaries` VALUES ('265', 'id', 'module_projects.project_dbtype', 'Type Database');
INSERT INTO `dictionaries` VALUES ('266', 'id', 'module_projects.project_dbname', 'Nama Database');
INSERT INTO `dictionaries` VALUES ('267', 'id', 'base.page_title.hoa_catalogues', 'HOA Catalogue');
INSERT INTO `dictionaries` VALUES ('268', 'id', 'base.page_title.meeting_rooms', 'Rang Rapat');
INSERT INTO `dictionaries` VALUES ('269', 'id', 'base.page_title.module_creator_properties', 'Properties');
INSERT INTO `dictionaries` VALUES ('270', 'id', 'module_creators.module_defaultsortfield', 'Urut berdasarkan');
INSERT INTO `dictionaries` VALUES ('271', 'id', 'module_creators.module_defaultsortmethod', 'Type Urutan');
INSERT INTO `dictionaries` VALUES ('272', 'id', 'module_creator_fields.field_datatype', 'Type Data');
INSERT INTO `dictionaries` VALUES ('273', 'id', 'module_creator_fields.field_length', 'Panjang karakter');
INSERT INTO `dictionaries` VALUES ('274', 'id', 'base.page_title.data_guru', 'Data Pegawai');
INSERT INTO `dictionaries` VALUES ('275', 'id', 'base.page_title.jabatan', 'Jabatan');
INSERT INTO `dictionaries` VALUES ('276', 'id', 'base.page_title.daftarhadir', 'Daftar Hadir');
INSERT INTO `dictionaries` VALUES ('277', 'id', 'base.page_title.mutasi', 'Mutasi');
INSERT INTO `dictionaries` VALUES ('278', 'id', 'base.page_title.payroll', 'Penggajian');
INSERT INTO `dictionaries` VALUES ('279', 'id', 'base.page_title.promosi', 'Promosi');
INSERT INTO `dictionaries` VALUES ('280', 'id', 'base.page_title.parameter_system', 'Parameter System');
INSERT INTO `dictionaries` VALUES ('281', 'id', 'base.page_title.master_data', 'Data Master');
INSERT INTO `dictionaries` VALUES ('282', 'id', 'base.page_title.g_komp_pks', 'Grup Komponen Penilaian');
INSERT INTO `dictionaries` VALUES ('283', 'id', 'base.page_title.komponen_penilaians', 'Komponen Penilaian');
INSERT INTO `dictionaries` VALUES ('284', 'id', 'base.page_title.penilaian', 'Penilaian Kinerja');
INSERT INTO `dictionaries` VALUES ('285', 'id', 'base.page_title.penilaiankinerjas', 'Penilaian Kinerja');
INSERT INTO `dictionaries` VALUES ('286', 'id', 'base.page_title.pk_details', 'Detail Penilaian Kerja');
INSERT INTO `dictionaries` VALUES ('287', 'id', 'base.page_title.anggota', 'Anggota');
INSERT INTO `dictionaries` VALUES ('288', 'id', 'base.page_title.keanggotaans', 'Keanggotaan');
INSERT INTO `dictionaries` VALUES ('289', 'id', 'base.page_title.anggotas', 'Anggota');
INSERT INTO `dictionaries` VALUES ('290', 'id', 'base.page_title.reqanggotas', 'Request Anggota');
INSERT INTO `dictionaries` VALUES ('291', 'id', 'base.page_title.saldos', 'Saldo');
INSERT INTO `dictionaries` VALUES ('292', 'id', 'base.page_title.reqsaldos', 'Request Saldo');
INSERT INTO `dictionaries` VALUES ('293', 'id', 'base.page_title.trxpesanans', 'Pemesanan');
INSERT INTO `dictionaries` VALUES ('294', 'id', 'base.page_title.trxpembayaranverify', 'Verif. Pembayaran');
INSERT INTO `dictionaries` VALUES ('295', 'id', 'base.page_title.trxpesananverify', 'Verif. Pesanan');
INSERT INTO `dictionaries` VALUES ('296', 'id', 'base.page_title.trxpengirimans', 'Pengiriman');

-- ----------------------------
-- Table structure for ekspedisi
-- ----------------------------
DROP TABLE IF EXISTS `ekspedisi`;
CREATE TABLE `ekspedisi` (
  `ekspedisi_id` int(11) NOT NULL,
  `ekspedisi_nama` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ekspedisi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ekspedisi
-- ----------------------------
INSERT INTO `ekspedisi` VALUES ('1', 'JNE');

-- ----------------------------
-- Table structure for histories
-- ----------------------------
DROP TABLE IF EXISTS `histories`;
CREATE TABLE `histories` (
  `history_id` varchar(36) NOT NULL,
  `history_detail` text NOT NULL,
  `history_active` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of histories
-- ----------------------------
INSERT INTO `histories` VALUES ('2d8d4047-a197-11e7-82fb-54650c0f9c19', '  ', '1');
INSERT INTO `histories` VALUES ('43a065bb-a198-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('76ac0620-a198-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('ad5208ae-a198-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('1f72262e-a19b-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('36a38f7a-a19b-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('7e199ad7-a19b-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('994bfd2e-a19b-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('1ba56384-a19d-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('29d9a420-a19d-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('46ef2bcb-a19e-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('4eb32108-a19e-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('7cd8bbf1-a19e-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('98b4ba37-a19e-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('de7ec82b-a19f-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('e6dbb52d-a19f-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('17b80710-a1a1-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('d0233878-a1a3-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('de3bdbc7-a1a3-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('3df6c7b9-a1a6-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('4ff591ae-a1a6-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('547924bd-a1a6-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('59419377-a1a6-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('cb8a5c63-a1a6-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('e7a23b02-a1a6-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('f1863bb2-a1aa-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('40d890d5-a1ad-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('e698747c-a1b0-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('ec4233ca-a1b0-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('63deb379-a1ba-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('e24472b7-a1bc-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('eb3e5ec1-a1bc-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('ee9e1acd-a1bc-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('f78e590b-a1bc-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('178f037f-a1bd-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('2a314af6-a1bd-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('da46e229-a1be-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('e0d78e53-a1be-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('ef5d55c3-a1be-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('06d2d58f-a1bf-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('1b9d7daa-a1bf-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('b89f0747-a1bf-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('c402ddb7-a1bf-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('d839f258-a1bf-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('e7571eef-a1bf-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('f6b7bfc1-a1bf-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('096ad7c0-a1c0-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('158f27de-a1c0-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('237bca06-a1c0-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('059e5f73-a1c1-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('5713f117-a1c1-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('627cbcc8-a1c1-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('71df9980-a1c1-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('7c72f98d-a1c1-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('aa04afe4-a1c1-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('c0f4760f-a1c1-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('ca115bfb-a1c1-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('ea2cd741-a1c1-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('2182bd50-a1c2-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('02a563a1-a1c3-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('19a6abc5-a1d2-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('724f1537-a22a-11e7-82fb-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('03211b11-a25d-11e7-9707-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('a4ca707d-a260-11e7-9707-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('8702d272-a261-11e7-9707-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('0801923f-a262-11e7-9707-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('ec35c0e9-a262-11e7-9707-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('2c7b9a85-a28f-11e7-9707-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('8756ad13-a28f-11e7-9707-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('d532b932-a28f-11e7-9707-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('46b40901-a290-11e7-9707-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('65f841a5-a299-11e7-9707-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('3eda9d4e-a2cc-11e7-8fb8-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('93316e63-a2cf-11e7-8fb8-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('1cfd324c-a324-11e7-8261-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('12032a5c-a335-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"G784\",\"user_name\":\"iqbal\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('48b8518d-a335-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"I518\",\"user_name\":\"indra\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('95fd8c4e-a335-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"7777\",\"user_name\":\"ariel\",\"role_id\":\"1\",\"user_id\":\"123\"}}]', '1');
INSERT INTO `histories` VALUES ('f0502eb8-a335-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"I518\",\"user_name\":\"indra permana\",\"role_id\":\"1\",\"user_id\":\"48aef523-a335-11e7-8261-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('4f36e47b-a336-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_id\":\"11f43519-a335-11e7-8261-54650c0f9c19\",\"user_identity\":\"G784\",\"user_role_id\":\"1\",\"user_name\":\"iqbal\",\"user_registerdate\":\"2017-09-27 10:36:39\",\"user_salt\":\"39336\",\"user_password\":\"5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5\",\"user_status\":\"1\"}},{\"after\":{\"user_identity\":\"G784\",\"user_name\":\"Muhammad iqbal\",\"role_id\":\"1\",\"user_id\":\"11f43519-a335-11e7-8261-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('ef8e1354-a337-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_identity\":\"1234\",\"user_name\":\"Tantan\",\"role_id\":null,\"user_id\":null}},{\"after\":{\"user_identity\":\"1234\",\"user_name\":\"Tantan suryana\",\"role_id\":\"1\",\"user_id\":\"98501005-a32f-11e7-8261-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('3efb1542-a338-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_identity\":\"1234\",\"user_name\":\"Tantan suryana\",\"role_id\":\"1\",\"user_id\":\"98501005-a32f-11e7-8261-54650c0f9c19\"}},{\"after\":{\"user_identity\":\"1234\",\"user_name\":\"Tantan suryana S.T.\",\"role_id\":\"1\",\"user_id\":\"98501005-a32f-11e7-8261-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('4df937b5-a339-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_id\":\"123\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('4d7f4bfc-a33b-11e7-8261-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"48aef523-a335-11e7-8261-54650c0f9c19\",\"11f43519-a335-11e7-8261-54650c0f9c19\"],\"user_id\":\"48aef523-a335-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('6b83207e-a33b-11e7-8261-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"48aef523-a335-11e7-8261-54650c0f9c19\",\"11f43519-a335-11e7-8261-54650c0f9c19\"],\"user_id\":\"11f43519-a335-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('9c32e3a2-a33c-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"1212\",\"user_name\":\"3333\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('adaf41bd-a33c-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"5656\",\"user_name\":\"6666\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('b3d6f1e0-a33c-11e7-8261-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"9c244b70-a33c-11e7-8261-54650c0f9c19\",\"ad9a9abe-a33c-11e7-8261-54650c0f9c19\"],\"user_id\":\"9c244b70-a33c-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('be8b8325-a33c-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"7676\",\"user_name\":\"fg\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('c9743a94-a33c-11e7-8261-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"ad9a9abe-a33c-11e7-8261-54650c0f9c19\",\"be797ed7-a33c-11e7-8261-54650c0f9c19\"],\"user_id\":\"ad9a9abe-a33c-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('1677fff3-a33d-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"ssss\",\"user_name\":\"dfsdf\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('23affaa2-a33d-11e7-8261-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"1669765e-a33d-11e7-8261-54650c0f9c19\",\"be797ed7-a33c-11e7-8261-54650c0f9c19\"],\"user_id\":\"1669765e-a33d-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('49bfbe55-a33e-11e7-8261-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"be797ed7-a33c-11e7-8261-54650c0f9c19\",\"98501005-a32f-11e7-8261-54650c0f9c19\"],\"user_id\":\"be797ed7-a33c-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('63c6da9a-a33e-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"asdf\",\"user_name\":\"aaa\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('6886cf22-a33e-11e7-8261-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"63b77293-a33e-11e7-8261-54650c0f9c19\",\"98501005-a32f-11e7-8261-54650c0f9c19\"],\"user_id\":\"63b77293-a33e-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('2e2d8089-a33f-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"ssss\",\"user_name\":\"sdf\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('415668cf-a33f-11e7-8261-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"2e2344ab-a33f-11e7-8261-54650c0f9c19\",\"98501005-a32f-11e7-8261-54650c0f9c19\"],\"user_id\":\"2e2344ab-a33f-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('7d31e39a-a33f-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"1212\",\"user_name\":\"2222\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('80e6ee67-a33f-11e7-8261-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"7d286558-a33f-11e7-8261-54650c0f9c19\",\"98501005-a32f-11e7-8261-54650c0f9c19\"],\"user_id\":\"7d286558-a33f-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('a5bc5cf2-a33f-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"3434\",\"user_name\":\"sdfs\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('b5900fbe-a33f-11e7-8261-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"a5af0a9a-a33f-11e7-8261-54650c0f9c19\",\"98501005-a32f-11e7-8261-54650c0f9c19\"],\"user_id\":\"a5af0a9a-a33f-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('063f4ea1-a346-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"sdfs\",\"user_name\":\"sdf\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('0b094ce2-a346-11e7-8261-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"062c76f2-a346-11e7-8261-54650c0f9c19\",\"98501005-a32f-11e7-8261-54650c0f9c19\"],\"user_id\":\"062c76f2-a346-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('76399527-a346-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"sdfa\",\"user_name\":\"sss\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('7aaa3bed-a346-11e7-8261-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"76295848-a346-11e7-8261-54650c0f9c19\",\"98501005-a32f-11e7-8261-54650c0f9c19\"],\"user_id\":\"76295848-a346-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('7ac9724b-a346-11e7-8261-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"76295848-a346-11e7-8261-54650c0f9c19\",\"98501005-a32f-11e7-8261-54650c0f9c19\"],\"user_id\":\"98501005-a32f-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('cff6489e-a346-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"1234\",\"user_name\":\"saf\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('d78a92b0-a346-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"dddd\",\"user_name\":\"sadf\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('dfc87b94-a346-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"eree\",\"user_name\":\"asdf\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('e54fd40f-a346-11e7-8261-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"dfb29a86-a346-11e7-8261-54650c0f9c19\",\"d77ac0e4-a346-11e7-8261-54650c0f9c19\"],\"user_id\":\"dfb29a86-a346-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('292bafd4-a347-11e7-8261-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"d77ac0e4-a346-11e7-8261-54650c0f9c19\",\"cfe5ecc4-a346-11e7-8261-54650c0f9c19\"],\"user_id\":\"d77ac0e4-a346-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('2968e204-a347-11e7-8261-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"d77ac0e4-a346-11e7-8261-54650c0f9c19\",\"cfe5ecc4-a346-11e7-8261-54650c0f9c19\"],\"user_id\":\"cfe5ecc4-a346-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('4c29ef3e-a347-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"1234\",\"user_name\":\"aasdf\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('53cc8c55-a347-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"5645\",\"user_name\":\"sadf\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('5a8cdf2c-a347-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"5678\",\"user_name\":\"asdf\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('5f479837-a347-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_id\":\"4c0b4c39-a347-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('63dc27b0-a347-11e7-8261-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"5a797479-a347-11e7-8261-54650c0f9c19\",\"53ba15bd-a347-11e7-8261-54650c0f9c19\"],\"user_id\":\"5a797479-a347-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('6404d416-a347-11e7-8261-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"5a797479-a347-11e7-8261-54650c0f9c19\",\"53ba15bd-a347-11e7-8261-54650c0f9c19\"],\"user_id\":\"53ba15bd-a347-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('a5196b2b-a347-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"tes1\",\"user_name\":\"sdfa\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('db16cea6-a349-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"aass\",\"user_name\":\"sdf\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('a6a91085-a34c-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"1\"}},{\"after\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"0\"}}]', '1');
INSERT INTO `histories` VALUES ('e3790aed-a34c-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"1\"}},{\"after\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"0\"}}]', '1');
INSERT INTO `histories` VALUES ('2215c899-a34d-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"1\"}},{\"after\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"0\"}}]', '1');
INSERT INTO `histories` VALUES ('3ae8569c-a34d-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('55f1f2ee-a34d-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"1\"}},{\"after\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"0\"}}]', '1');
INSERT INTO `histories` VALUES ('5e75a8a7-a34d-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('68cd1c64-a34d-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"1\"}},{\"after\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"0\"}}]', '1');
INSERT INTO `histories` VALUES ('7e144e9b-a34d-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('85fb9df6-a34d-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"1\"}},{\"after\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"0\"}}]', '1');
INSERT INTO `histories` VALUES ('e51a7313-a34d-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('f6ba6352-a34d-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"1\"}},{\"after\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"0\"}}]', '1');
INSERT INTO `histories` VALUES ('51e10b84-a34e-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('67765387-a34e-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"1\"}},{\"after\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"0\"}}]', '1');
INSERT INTO `histories` VALUES ('99d83cd2-a34e-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('a398962c-a34e-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"1\"}},{\"after\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"0\"}}]', '1');
INSERT INTO `histories` VALUES ('bc187c38-a34e-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('3612ff89-a34f-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"1\"}},{\"after\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"0\"}}]', '1');
INSERT INTO `histories` VALUES ('62dad701-a34f-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('1b340033-a350-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"1\"}},{\"after\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"0\"}}]', '1');
INSERT INTO `histories` VALUES ('237b6b94-a350-11e7-8261-54650c0f9c19', '[{\"before\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('91899dcb-a351-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"1111\",\"user_name\":\"sdfa\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('99248297-a351-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"werw\",\"user_name\":\"werw\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('a119ae7e-a351-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"4534\",\"user_name\":\"sewer\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('aac64294-a351-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"sdfss\",\"user_name\":\"asdf\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('c0727e8b-a35b-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"tttt\",\"user_name\":\"yyyy\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('cea61a22-a35b-11e7-8261-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"ssss\",\"user_name\":\"sss\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('690bfb0b-a3b7-11e7-bd60-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('73b88523-a3b7-11e7-bd60-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('85ad4f46-a3b7-11e7-bd60-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"1111\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('b024a4f4-a3bb-11e7-bd60-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"1111\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('ac531e28-a48a-11e7-8af8-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('afcde3ad-a48a-11e7-8af8-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"1111\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('e358a34e-a48d-11e7-8af8-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"1111\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('04b3d2d8-a48e-11e7-8af8-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"1111\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('169abdea-a48e-11e7-8af8-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"aab7b891-a351-11e7-8261-54650c0f9c19\",\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"a50c8027-a347-11e7-8261-54650c0f9c19\",\"916bf986-a351-11e7-8261-54650c0f9c19\",\"a10a91e2-a351-11e7-8261-54650c0f9c19\"],\"user_id\":\"aab7b891-a351-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('16ba8e4d-a48e-11e7-8af8-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"aab7b891-a351-11e7-8261-54650c0f9c19\",\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"a50c8027-a347-11e7-8261-54650c0f9c19\",\"916bf986-a351-11e7-8261-54650c0f9c19\",\"a10a91e2-a351-11e7-8261-54650c0f9c19\"],\"user_id\":\"db096d4a-a349-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('16dce342-a48e-11e7-8af8-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"aab7b891-a351-11e7-8261-54650c0f9c19\",\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"a50c8027-a347-11e7-8261-54650c0f9c19\",\"916bf986-a351-11e7-8261-54650c0f9c19\",\"a10a91e2-a351-11e7-8261-54650c0f9c19\"],\"user_id\":\"a50c8027-a347-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('16ed6c35-a48e-11e7-8af8-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"aab7b891-a351-11e7-8261-54650c0f9c19\",\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"a50c8027-a347-11e7-8261-54650c0f9c19\",\"916bf986-a351-11e7-8261-54650c0f9c19\",\"a10a91e2-a351-11e7-8261-54650c0f9c19\"],\"user_id\":\"916bf986-a351-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('16fdf0d5-a48e-11e7-8af8-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"aab7b891-a351-11e7-8261-54650c0f9c19\",\"db096d4a-a349-11e7-8261-54650c0f9c19\",\"a50c8027-a347-11e7-8261-54650c0f9c19\",\"916bf986-a351-11e7-8261-54650c0f9c19\",\"a10a91e2-a351-11e7-8261-54650c0f9c19\"],\"user_id\":\"a10a91e2-a351-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('1bb9d28e-a48e-11e7-8af8-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"ce948be2-a35b-11e7-8261-54650c0f9c19\",\"99140a7a-a351-11e7-8261-54650c0f9c19\",\"c0611aca-a35b-11e7-8261-54650c0f9c19\"],\"user_id\":\"ce948be2-a35b-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('1bdff95f-a48e-11e7-8af8-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"ce948be2-a35b-11e7-8261-54650c0f9c19\",\"99140a7a-a351-11e7-8261-54650c0f9c19\",\"c0611aca-a35b-11e7-8261-54650c0f9c19\"],\"user_id\":\"99140a7a-a351-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('1bf080d8-a48e-11e7-8af8-54650c0f9c19', '[{\"before\":{\"per_page\":\"5\",\"action_to\":[\"ce948be2-a35b-11e7-8261-54650c0f9c19\",\"99140a7a-a351-11e7-8261-54650c0f9c19\",\"c0611aca-a35b-11e7-8261-54650c0f9c19\"],\"user_id\":\"c0611aca-a35b-11e7-8261-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('2b7c79c2-a48e-11e7-8af8-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"adm01\",\"user_name\":\"administrator\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('367c27de-a48e-11e7-8af8-54650c0f9c19', '[{\"before\":{\"user_identity\":\"adm0\",\"user_name\":\"administrator\",\"role_id\":\"1\",\"user_id\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\"}},{\"after\":{\"user_identity\":\"adm1\",\"user_name\":\"administrator\",\"role_id\":\"1\",\"user_id\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('4d239842-a48e-11e7-8af8-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('b3502917-a4b7-11e7-b063-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('e0aa40ea-a4b7-11e7-b063-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('efaad3f0-a4b7-11e7-b063-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('83d8bb49-a4e0-11e7-b063-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('89e92c06-a4e0-11e7-b063-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('9769fcf0-a4e0-11e7-b063-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('d71420bf-a4e0-11e7-b063-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"adm02\",\"user_name\":\"admin 2\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('e71bacb7-a4e0-11e7-b063-54650c0f9c19', '[{\"before\":{\"user_identity\":\"adm1\",\"user_name\":\"administrator\",\"role_id\":\"1\",\"user_id\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\"}},{\"after\":{\"user_identity\":\"adm1\",\"user_name\":\"administrator\",\"role_id\":\"2\",\"user_id\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('40e08deb-a4e1-11e7-b063-54650c0f9c19', '[{\"before\":{\"user_identity\":\"adm0\",\"user_name\":\"admin 2\",\"role_id\":\"1\",\"user_id\":\"d706d42d-a4e0-11e7-b063-54650c0f9c19\"}},{\"after\":{\"user_identity\":\"adm2\",\"user_name\":\"admin 2\",\"role_id\":\"2\",\"user_id\":\"d706d42d-a4e0-11e7-b063-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('09b46a49-a4e3-11e7-b063-54650c0f9c19', '[{\"before\":{\"role_id\":\"2\",\"menu_id\":[]}},{\"after\":{\"menu_id\":[\"1\",\"2\"],\"role_id\":\"2\"}}]', '1');
INSERT INTO `histories` VALUES ('51e6262f-a4e3-11e7-b063-54650c0f9c19', '[{\"before\":{\"role_id\":\"2\",\"menu_id\":[\"1\",\"2\"]}},{\"after\":{\"menu_id\":[\"2\",\"3\"],\"role_id\":\"2\"}}]', '1');
INSERT INTO `histories` VALUES ('7e7b0aa6-a4e3-11e7-b063-54650c0f9c19', '[{\"before\":{\"role_id\":\"2\",\"menu_id\":[\"2\",\"3\"]}},{\"after\":{\"menu_id\":[\"1\",\"2\"],\"role_id\":\"2\"}}]', '1');
INSERT INTO `histories` VALUES ('987e88d6-a4e3-11e7-b063-54650c0f9c19', '[{\"before\":{\"role_id\":\"2\",\"menu_id\":[\"1\",\"2\"]}},{\"after\":{\"menu_id\":[\"2\",\"3\"],\"role_id\":\"2\"}}]', '1');
INSERT INTO `histories` VALUES ('b82042b9-a4e3-11e7-b063-54650c0f9c19', '[{\"before\":{\"role_id\":\"2\",\"menu_id\":[\"2\",\"3\"]}},{\"after\":{\"menu_id\":[\"1\",\"2\"],\"role_id\":\"2\"}}]', '1');
INSERT INTO `histories` VALUES ('f51ac8ac-a4e3-11e7-b063-54650c0f9c19', '[{\"before\":{\"role_id\":\"2\",\"menu_id\":[\"1\",\"2\"]}},{\"after\":{\"menu_id\":[\"2\",\"3\"],\"role_id\":\"2\"}}]', '1');
INSERT INTO `histories` VALUES ('74aa6cbb-a4e4-11e7-b063-54650c0f9c19', '[{\"before\":{\"role_id\":\"2\",\"menu_id\":[\"1\",\"2\"]}},{\"after\":{\"menu_id\":[\"2\",\"3\"],\"role_id\":\"2\"}}]', '1');
INSERT INTO `histories` VALUES ('8ac8c712-a4e4-11e7-b063-54650c0f9c19', '[{\"before\":{\"role_id\":\"2\",\"menu_id\":[\"1\",\"2\"]}},{\"after\":{\"menu_id\":[\"2\",\"3\"],\"role_id\":\"2\"}}]', '1');
INSERT INTO `histories` VALUES ('9ea11b7b-a4e4-11e7-b063-54650c0f9c19', '[{\"before\":{\"role_id\":\"2\",\"menu_id\":[\"1\",\"2\"]}},{\"after\":{\"menu_id\":[\"2\",\"3\"],\"role_id\":\"2\"}}]', '1');
INSERT INTO `histories` VALUES ('a87e5f4c-a4e4-11e7-b063-54650c0f9c19', '[{\"before\":{\"role_id\":\"2\",\"menu_id\":[\"1\",\"2\"]}},{\"after\":{\"menu_id\":[\"2\",\"3\"],\"role_id\":\"2\"}}]', '1');
INSERT INTO `histories` VALUES ('b19e58f0-a4e4-11e7-b063-54650c0f9c19', '[{\"before\":{\"role_id\":\"2\",\"menu_id\":[\"1\",\"2\"]}},{\"after\":{\"menu_id\":[\"2\",\"3\"],\"role_id\":\"2\"}}]', '1');
INSERT INTO `histories` VALUES ('ca6b989f-a4e4-11e7-b063-54650c0f9c19', '[{\"before\":{\"role_id\":\"2\",\"menu_id\":[\"1\",\"2\"]}},{\"after\":{\"menu_id\":[\"2\",\"3\"],\"role_id\":\"2\"}}]', '1');
INSERT INTO `histories` VALUES ('dccb8d25-a4e4-11e7-b063-54650c0f9c19', '[{\"before\":{\"role_id\":\"2\",\"menu_id\":[\"1\",\"2\"]}},{\"after\":{\"menu_id\":[\"2\",\"3\"],\"role_id\":\"2\"}}]', '1');
INSERT INTO `histories` VALUES ('fcc0fc35-a4e4-11e7-b063-54650c0f9c19', '[{\"before\":{\"role_id\":\"2\",\"menu_id\":[\"2\",\"3\"]}},{\"after\":{\"menu_id\":[\"1\"],\"role_id\":\"2\"}}]', '1');
INSERT INTO `histories` VALUES ('f287e14e-a4e5-11e7-b063-54650c0f9c19', '[{\"before\":{\"role_id\":\"1\",\"menu_id\":[]}},{\"after\":{\"menu_id\":[\"1\",\"2\",\"3\"],\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('bc0dd64e-a4e7-11e7-b063-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('c98cabc7-a4e7-11e7-b063-54650c0f9c19', '[{\"before\":{\"role_id\":\"1\",\"menu_id\":[\"1\",\"2\",\"3\"]}},{\"after\":{\"menu_id\":[\"4\"],\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('d0d03ec7-a4e7-11e7-b063-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('96737e65-a570-11e7-9fc1-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('4020b38f-a5a3-11e7-bee2-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('3ab329e2-a5a6-11e7-bee2-54650c0f9c19', '[{\"before\":{\"menu_id\":\"4\",\"menu_icon\":\"glyphicon glyphicon-asterisk\",\"menu_active\":\"1\"}},{\"after\":{\"menu_icon\":\"glyphicon glyphicon-asterisk\",\"menu_active\":\"1\",\"menu_id\":\"4\"}}]', '1');
INSERT INTO `histories` VALUES ('9ba1281c-a5a6-11e7-bee2-54650c0f9c19', ' [{ \"before\" :{ \"menu_id\" : \"4\",\r\n \"menu_icon\" : \"glyphicon glyphicon-bed\",\r\n \"menu_active\" : \"0\" }},{ \"after\" :{ \"menu_icon\" : \"glyphicon glyphicon-bed\",\r\n \"menu_active\" : \"0\",\r\n \"menu_id\" : \"4\" }}] ', '1');
INSERT INTO `histories` VALUES ('c40a72b4-a5a6-11e7-bee2-54650c0f9c19', '[{\"before\":{\"menu_id\":\"4\",\"menu_icon\":\"glyphicon glyphicon-arrow-right\",\"menu_active\":\"1\"}},{\"after\":{\"menu_icon\":\"glyphicon glyphicon-arrow-right\",\"menu_active\":\"1\",\"menu_id\":\"4\"}}]', '1');
INSERT INTO `histories` VALUES ('047bee76-a5a7-11e7-bee2-54650c0f9c19', '[{\"before\":{\"menu_id\":\"4\",\"menu_icon\":\"glyphicon glyphicon-barcode\",\"menu_active\":\"1\"}},{\"after\":{\"menu_icon\":\"glyphicon glyphicon-barcode\",\"menu_active\":\"1\",\"menu_id\":\"4\"}}]', '1');
INSERT INTO `histories` VALUES ('378601d2-a5a7-11e7-bee2-54650c0f9c19', '[{\"before\":{\"menu_id\":\"4\",\"menu_icon\":\"glyphicon glyphicon-apple\",\"menu_active\":\"1\"}},{\"after\":{\"menu_icon\":\"glyphicon glyphicon-apple\",\"menu_active\":\"1\",\"menu_id\":\"4\"}}]', '1');
INSERT INTO `histories` VALUES ('783855e8-a5a7-11e7-bee2-54650c0f9c19', '[{\"before\":{\"menu_id\":\"4\",\"menu_icon\":\"glyphicon glyphicon-copyright-mark\",\"menu_active\":\"1\"}},{\"after\":{\"menu_icon\":\"glyphicon glyphicon-copyright-mark\",\"menu_active\":\"1\",\"menu_id\":\"4\"}}]', '1');
INSERT INTO `histories` VALUES ('8036919f-a5a7-11e7-bee2-54650c0f9c19', '[{\"before\":{\"menu_id\":\"4\",\"menu_icon\":\"glyphicon glyphicon-copyright-mark\",\"menu_active\":\"0\"}},{\"after\":{\"menu_icon\":\"glyphicon glyphicon-copyright-mark\",\"menu_active\":\"0\",\"menu_id\":\"4\"}}]', '1');
INSERT INTO `histories` VALUES ('8e29c0d0-a5a7-11e7-bee2-54650c0f9c19', '[{\"before\":{\"menu_id\":\"4\",\"menu_icon\":\"glyphicon glyphicon-equalizer\",\"menu_active\":\"1\"}},{\"after\":{\"menu_icon\":\"glyphicon glyphicon-equalizer\",\"menu_active\":\"1\",\"menu_id\":\"4\"}}]', '1');
INSERT INTO `histories` VALUES ('add5438f-a5aa-11e7-bee2-54650c0f9c19', '[{\"before\":{\"role_id\":\"1\",\"menu_id\":[\"1\",\"2\",\"3\",\"4\"]}},{\"after\":{\"menu_id\":[\"5\"],\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('b6272545-a5aa-11e7-bee2-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('77dc7418-a5e4-11e7-bee2-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('e2ceb24e-a5e8-11e7-bee2-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_identity\":\"P002\",\"produk_nama\":\"telor\",\"produk_harga\":\"5000\",\"produk_stok\":\"5\",\"produk_satuan\":\"kilo\",\"produk_photo\":\"asdf\",\"produk_description\":\"telor ayam\",\"produk_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('3670ba4e-a622-11e7-bee2-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('3f422f74-a624-11e7-bee2-54650c0f9c19', '[{\"before\":{\"produk_id\":\"e2bffe8e-a5e8-11e7-bee2-54650c0f9c19\",\"produk_identity\":\"P002\",\"produk_nama\":\"telor\",\"produk_harga\":\"5000.00\",\"produk_stok\":\"5\",\"produk_satuan\":\"kilo\",\"produk_photo\":\"asdf\",\"produk_description\":\"telor ayam\",\"produk_status\":\"1\"}},{\"after\":{\"produk_identity\":\"P002\",\"produk_nama\":\"telor edit\",\"produk_harga\":\"5000.00\",\"produk_stok\":\"5\",\"produk_satuan\":\"kilo\",\"produk_photo\":\"asdf\",\"produk_description\":\"telor ayam\",\"produk_status\":\"1\",\"produk_id\":\"e2bffe8e-a5e8-11e7-bee2-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('bac91623-a62f-11e7-bee2-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_identity\":\"sda\",\"produk_nama\":\"sdf\",\"produk_harga\":\"98\",\"produk_stok\":\"8\",\"produk_satuan\":\"jh\",\"produk_description\":\"980\",\"produk_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('1ded860b-a630-11e7-bee2-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_identity\":\"890\",\"produk_nama\":\"809\",\"produk_harga\":\"809\",\"produk_stok\":\"98\",\"produk_satuan\":\"iu\",\"produk_description\":\"098\",\"produk_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('653af7c0-a63a-11e7-bee2-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_identity\":\"sa\",\"produk_nama\":\"sd\",\"produk_harga\":\"90\",\"produk_stok\":\"0\",\"produk_satuan\":\"kkj\",\"produk_description\":\"df\",\"produk_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('bceb7dc7-a63a-11e7-bee2-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_identity\":\"j\",\"produk_nama\":\"kl\",\"produk_harga\":\"9\",\"produk_stok\":\"0\",\"produk_satuan\":\"kl\",\"produk_description\":\"klj\",\"produk_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('08c58107-a63b-11e7-bee2-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_identity\":\"jjkl\",\"produk_nama\":\"klj\",\"produk_harga\":\"9\",\"produk_stok\":\"9\",\"produk_satuan\":\"jnk\",\"produk_description\":\"jjk\",\"produk_status\":\"1\",\"produk_photo\":\"15414_IMG_7206_(2).JPG\"}}]', '1');
INSERT INTO `histories` VALUES ('2071a78e-a63d-11e7-bee2-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_identity\":\"pppp\",\"produk_nama\":\"pppp\",\"produk_harga\":\"8888\",\"produk_stok\":\"8\",\"produk_satuan\":\"jkj\",\"produk_description\":\"jk\",\"produk_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('984d851f-a63d-11e7-bee2-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_identity\":\"jkl\",\"produk_nama\":\"kjk\",\"produk_harga\":\"0\",\"produk_stok\":\"00\",\"produk_satuan\":\"k\",\"produk_description\":\"kj\",\"produk_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('6ff37377-a63e-11e7-bee2-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_identity\":\"ret\",\"produk_nama\":\"ert\",\"produk_harga\":\"3\",\"produk_stok\":\"34\",\"produk_satuan\":\"f\",\"produk_description\":\"sdf\",\"produk_status\":\"1\",\"produk_photo\":\"3967_KPR.jpg\"}}]', '1');
INSERT INTO `histories` VALUES ('95752d24-a63e-11e7-bee2-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_identity\":\"g001\",\"produk_nama\":\"gula\",\"produk_harga\":\"8000\",\"produk_stok\":\"9\",\"produk_satuan\":\"pcs\",\"produk_description\":\"sadfsd\",\"produk_status\":\"1\",\"produk_photo\":\"88858_1.jpg\"}}]', '1');
INSERT INTO `histories` VALUES ('be58d14b-a63e-11e7-bee2-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_identity\":\"asdf\",\"produk_nama\":\"ddd\",\"produk_harga\":\"45\",\"produk_stok\":\"34\",\"produk_satuan\":\"sdf\",\"produk_description\":\"sdfasdf\",\"produk_status\":\"1\",\"produk_photo\":\"40386_ShowImage.jpg\"}}]', '1');
INSERT INTO `histories` VALUES ('e98a4141-a63f-11e7-bee2-54650c0f9c19', '[{\"before\":{\"produk_id\":\"95696fcd-a63e-11e7-bee2-54650c0f9c19\",\"produk_identity\":\"g001\",\"produk_nama\":\"gula\",\"produk_harga\":\"8000.00\",\"produk_stok\":\"9\",\"produk_satuan\":\"pcs\",\"produk_photo\":\"88858_1.jpg\",\"produk_description\":\"sadfsd\",\"produk_status\":\"1\"}},{\"after\":{\"produk_identity\":\"g001\",\"produk_nama\":\"gula edit\",\"produk_harga\":\"8000.00\",\"produk_stok\":\"9\",\"produk_satuan\":\"pcs\",\"produk_photo\":\"88858_1.jpg\",\"produk_description\":\"sadfsd\",\"produk_status\":\"1\",\"produk_id\":\"95696fcd-a63e-11e7-bee2-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('01e18ed0-a640-11e7-bee2-54650c0f9c19', '[{\"before\":{\"produk_id\":\"95696fcd-a63e-11e7-bee2-54650c0f9c19\",\"produk_identity\":\"g001\",\"produk_nama\":\"gula edit\",\"produk_harga\":\"8000.00\",\"produk_stok\":\"9\",\"produk_satuan\":\"pcs\",\"produk_photo\":\"88858_1.jpg\",\"produk_description\":\"sadfsd\",\"produk_status\":\"1\"}},{\"after\":{\"produk_identity\":\"g001\",\"produk_nama\":\"gula edit\",\"produk_harga\":\"8000.00\",\"produk_stok\":\"9\",\"produk_satuan\":\"pcs\",\"produk_description\":\"sadfsd\",\"produk_status\":\"1\",\"produk_id\":\"95696fcd-a63e-11e7-bee2-54650c0f9c19\",\"produk_photo\":\"46392_237600_kn_(2).jpg\"}}]', '1');
INSERT INTO `histories` VALUES ('45d38d9c-a641-11e7-bee2-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('40268471-a68b-11e7-a31c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('406c5f72-a68b-11e7-a31c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('d7821605-a68c-11e7-a31c-54650c0f9c19', '[{\"before\":{\"role_id\":\"1\",\"menu_id\":[\"1\",\"2\",\"3\",\"4\",\"5\"]}},{\"after\":{\"menu_id\":[\"6\",\"7\"],\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('e334a634-a68c-11e7-a31c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('25b837cb-a68f-11e7-a31c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('426b8233-a69d-11e7-99dc-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('48c47fc1-a712-11e7-89f0-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('ad430419-a73b-11e7-89f0-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('70018c53-a74e-11e7-89f0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"sdfa\",\"anggota_nama\":\"jkj\",\"anggota_tmplahir\":\"kljkl\",\"anggota_tgllahir\":\"10/13/2017\",\"anggota_alamat\":\"kjklj\",\"anggota_jnskelamin\":\"L\",\"anggota_notlp\":\"09889\",\"anggota_email\":\"98098\",\"anggota_jnsidentitas\":\"12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"anggota_noidentitas\":\"8789\",\"anggota_persetujuan\":\"anggota_persetujuan\",\"anggota_scanidentitas\":\"79605_Chrysanthemum.jpg\",\"anggota_photo\":\"69317_Desert.jpg\"}}]', '1');
INSERT INTO `histories` VALUES ('f5f70c88-a74e-11e7-89f0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"p001\",\"anggota_nama\":\"tantan\",\"anggota_tmplahir\":\"bandung\",\"anggota_tgllahir\":\"10/10/2017\",\"anggota_alamat\":\"rumahku\",\"anggota_jnskelamin\":\"L\",\"anggota_notlp\":\"0987654321\",\"anggota_email\":\"tan@mail.com\",\"anggota_jnsidentitas\":\"12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"anggota_noidentitas\":\"1234567890\",\"anggota_persetujuan\":\"1\",\"anggota_scanidentitas\":\"27670_Chrysanthemum.jpg\",\"anggota_photo\":\"74047_Desert.jpg\"}}]', '1');
INSERT INTO `histories` VALUES ('74ffacb5-a750-11e7-89f0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"P002\",\"anggota_nama\":\"Suryana\",\"anggota_tmplahir\":\"bandung\",\"anggota_tgllahir\":\"2017-10-02\",\"anggota_alamat\":\"rumahku 2\",\"anggota_jnskelamin\":\"L\",\"anggota_notlp\":\"0987654321\",\"anggota_email\":\"tan@mail.com\",\"anggota_jnsidentitas\":\"12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"anggota_noidentitas\":\"1234567890\",\"anggota_persetujuan\":\"1\",\"anggota_scanidentitas\":\"5355_Chrysanthemum.jpg\",\"anggota_photo\":\"66772_Desert.jpg\"}}]', '1');
INSERT INTO `histories` VALUES ('b65fceb6-a750-11e7-89f0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"asdf\",\"anggota_nama\":\"rrr\",\"anggota_tmplahir\":\"rrrr\",\"anggota_tgllahir\":\"2017-10-24\",\"anggota_alamat\":\"rrrr\",\"anggota_jnskelamin\":\"L\",\"anggota_notlp\":\"333\",\"anggota_email\":\"33@f3\",\"anggota_jnsidentitas\":\"12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"anggota_noidentitas\":\"3333\",\"anggota_persetujuan\":\"1\",\"anggota_scanidentitas\":\"28372_Chrysanthemum.jpg\",\"anggota_photo\":\"66180_Desert.jpg\"}}]', '1');
INSERT INTO `histories` VALUES ('845d42d7-a75d-11e7-89f0-54650c0f9c19', '[{\"before\":{\"user_identity\":\"asdf\",\"anggota_nama\":\"rrr\",\"anggota_tmplahir\":\"rrrr\",\"anggota_tgllahir\":\"2017-10-24\",\"anggota_alamat\":\"rrrr\",\"anggota_jnskelamin\":\"L\",\"anggota_notlp\":\"333\",\"anggota_email\":\"33@f3\",\"anggota_jnsidentitas\":\"12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"anggota_noidentitas\":\"3333\",\"anggota_scanidentitas\":\"28372_Chrysanthemum.jpg\",\"anggota_photo\":\"66180_Desert.jpg\",\"anggota_persetujuan\":\"1\"}},{\"after\":{\"user_identity\":\"asdf\",\"anggota_nama\":\"adara\",\"anggota_tmplahir\":\"bandung\",\"anggota_tgllahir\":\"2017-10-01\",\"anggota_alamat\":\"green\",\"anggota_jnskelamin\":\"P\",\"anggota_notlp\":\"0987\",\"anggota_email\":\"33@f389\",\"anggota_jnsidentitas\":\"13EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"anggota_noidentitas\":\"1234\",\"anggota_persetujuan\":\"1\",\"user_id\":\"b6539c1f-a750-11e7-89f0-54650c0f9c19\",\"anggota_scanidentitas\":\"76193_Tulips.jpg\",\"anggota_photo\":\"69299_Hydrangeas.jpg\"}}]', '1');
INSERT INTO `histories` VALUES ('a1e940ac-a762-11e7-89f0-54650c0f9c19', '[{\"before\":{\"user_identity\":\"asdf\",\"anggota_nama\":\"adara\",\"anggota_tmplahir\":\"bandung\",\"anggota_tgllahir\":\"2017-10-01\",\"anggota_alamat\":\"green\",\"anggota_jnskelamin\":\"P\",\"anggota_notlp\":\"0987\",\"anggota_email\":\"33@f389\",\"anggota_jnsidentitas\":\"13EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"anggota_noidentitas\":\"1234\",\"anggota_scanidentitas\":\"76193_Tulips.jpg\",\"anggota_photo\":\"69299_Hydrangeas.jpg\",\"anggota_persetujuan\":\"1\"}},{\"after\":{\"user_identity\":\"asdf\",\"anggota_nama\":\"adara\",\"anggota_tmplahir\":\"bandung\",\"anggota_tgllahir\":\"2017-10-01\",\"anggota_alamat\":\"green\",\"anggota_jnskelamin\":\"P\",\"anggota_notlp\":\"0987\",\"anggota_email\":\"33@f389.com\",\"anggota_jnsidentitas\":\"13EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"anggota_noidentitas\":\"1234\",\"anggota_scanidentitas\":\"69299_Hydrangeas.jpg\",\"anggota_photo\":\"\",\"anggota_persetujuan\":\"1\",\"user_id\":\"b6539c1f-a750-11e7-89f0-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('09cbf6df-a7dd-11e7-bbff-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('b85bddac-a7dd-11e7-bbff-54650c0f9c19', '[{\"before\":{\"role_id\":\"1\",\"menu_id\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\"]}},{\"after\":{\"menu_id\":[\"8\"],\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('5a1351a9-a7de-11e7-bbff-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('a28eba0c-a7e9-11e7-bbff-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"tes1\",\"anggota_nama\":\"testing\",\"anggota_tmplahir\":\"bandung\",\"anggota_tgllahir\":\"2017-10-11\",\"anggota_alamat\":\"alll\",\"anggota_jnskelamin\":\"L\",\"anggota_notlp\":\"0987\",\"anggota_email\":\"98\",\"anggota_jnsidentitas\":\"12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"anggota_noidentitas\":\"098\",\"anggota_persetujuan\":\"1\",\"anggota_scanidentitas\":\"80740_Chrysanthemum.jpg\",\"anggota_photo\":\"61810_Desert.jpg\"}}]', '1');
INSERT INTO `histories` VALUES ('ea93c5ef-a7e9-11e7-bbff-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"tes2\",\"anggota_nama\":\"testing 2\",\"anggota_tmplahir\":\"sdf\",\"anggota_tgllahir\":\"2017-10-11\",\"anggota_alamat\":\"sadf\",\"anggota_jnskelamin\":\"L\",\"anggota_notlp\":\"098\",\"anggota_email\":\"lkjl\",\"anggota_jnsidentitas\":\"12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"anggota_noidentitas\":\"0987\",\"anggota_persetujuan\":\"1\",\"anggota_scanidentitas\":\"3121_Jellyfish.jpg\",\"anggota_photo\":\"17028_Hydrangeas.jpg\"}}]', '1');
INSERT INTO `histories` VALUES ('3a23ada6-a7fa-11e7-bbff-54650c0f9c19', '[{\"before\":{\"user_id\":\"ea861c0f-a7e9-11e7-bbff-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"ea861c0f-a7e9-11e7-bbff-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('fb835150-a7fc-11e7-bbff-54650c0f9c19', '[{\"before\":{\"user_id\":\"ea861c0f-a7e9-11e7-bbff-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"ea861c0f-a7e9-11e7-bbff-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('fb8ec9e4-a7fc-11e7-bbff-54650c0f9c19', '[{\"before\":{\"user_id\":\"ea861c0f-a7e9-11e7-bbff-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"ea861c0f-a7e9-11e7-bbff-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('edebd1d6-a7fd-11e7-bbff-54650c0f9c19', '[{\"before\":{\"user_id\":\"ea861c0f-a7e9-11e7-bbff-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"ea861c0f-a7e9-11e7-bbff-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('ee0a5aa6-a7fd-11e7-bbff-54650c0f9c19', '[{\"before\":{\"user_id\":\"ea861c0f-a7e9-11e7-bbff-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"ea861c0f-a7e9-11e7-bbff-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('3bf378e3-a80b-11e7-bbff-54650c0f9c19', '[{\"before\":{\"user_id\":\"ea861c0f-a7e9-11e7-bbff-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"anggota_note\":\"tes\",\"user_id\":\"ea861c0f-a7e9-11e7-bbff-54650c0f9c19\",\"user_status\":\"2\"}}]', '1');
INSERT INTO `histories` VALUES ('88591321-a80b-11e7-bbff-54650c0f9c19', '[{\"before\":{\"user_id\":\"ea861c0f-a7e9-11e7-bbff-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"anggota_note\":\"tidak layak\",\"user_id\":\"ea861c0f-a7e9-11e7-bbff-54650c0f9c19\",\"user_status\":\"2\"}}]', '1');
INSERT INTO `histories` VALUES ('dd2584cd-a8a2-11e7-8d92-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('ee514e33-a8a2-11e7-8d92-54650c0f9c19', '[{\"before\":{\"role_id\":\"1\",\"menu_id\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\"]}},{\"after\":{\"menu_id\":[\"9\",\"10\",\"11\"],\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('104441fe-a8a3-11e7-8d92-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('82698391-a8e6-11e7-8d92-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('cf7d0348-a8e6-11e7-8d92-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"adm1\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('6101f219-a8e7-11e7-8d92-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('6aefd6ba-a8e9-11e7-8d92-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxsaldo_noref\":\"1234567890\",\"trxsaldo_kdbank\":\"1\",\"trxsaldo_nominal\":\"50000\",\"trxsaldo_user_id\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('94b597c2-a92d-11e7-aa40-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('6a27babb-a92e-11e7-aa40-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxsaldo_noref\":\"987654321\",\"trxsaldo_kdbank\":\"1\",\"trxsaldo_nominal\":\"100000\",\"trxsaldo_user_id\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('1373c9a2-a933-11e7-aa40-54650c0f9c19', '[{\"before\":{\"trxsaldo_id\":\"6a1a3397-a92e-11e7-aa40-54650c0f9c19\",\"trxsaldo_status\":\"0\"}},{\"after\":{\"trxsaldo_id\":\"6a1a3397-a92e-11e7-aa40-54650c0f9c19\",\"trxsaldo_status\":\"2\",\"trxsaldo_verifiedby\":\"adm1\"}}]', '1');
INSERT INTO `histories` VALUES ('3c02b568-a933-11e7-aa40-54650c0f9c19', '[{\"before\":{\"trxsaldo_id\":\"6a1a3397-a92e-11e7-aa40-54650c0f9c19\",\"trxsaldo_status\":\"2\"}},{\"after\":{\"trxsaldo_id\":\"6a1a3397-a92e-11e7-aa40-54650c0f9c19\",\"trxsaldo_status\":\"2\",\"trxsaldo_verifiedby\":\"adm1\"}}]', '1');
INSERT INTO `histories` VALUES ('7fb9a7a7-a933-11e7-aa40-54650c0f9c19', '[{\"before\":{\"trxsaldo_id\":\"6a1a3397-a92e-11e7-aa40-54650c0f9c19\",\"trxsaldo_status\":\"2\"}},{\"after\":{\"trxsaldo_id\":\"6a1a3397-a92e-11e7-aa40-54650c0f9c19\",\"trxsaldo_status\":\"2\",\"trxsaldo_verifiedby\":\"adm1\"}}]', '1');
INSERT INTO `histories` VALUES ('fcbe6909-a933-11e7-aa40-54650c0f9c19', '[{\"before\":{\"trxsaldo_id\":\"6a1a3397-a92e-11e7-aa40-54650c0f9c19\",\"trxsaldo_status\":\"2\"}},{\"after\":{\"trxsaldo_id\":\"6a1a3397-a92e-11e7-aa40-54650c0f9c19\",\"trxsaldo_status\":\"2\",\"trxsaldo_verifiedby\":\"adm1\"}}]', '1');
INSERT INTO `histories` VALUES ('744e7bf8-a934-11e7-aa40-54650c0f9c19', '[{\"before\":{\"trxsaldo_id\":\"6a1a3397-a92e-11e7-aa40-54650c0f9c19\",\"trxsaldo_status\":\"2\"}},{\"after\":{\"trxsaldo_id\":\"6a1a3397-a92e-11e7-aa40-54650c0f9c19\",\"trxsaldo_status\":\"2\",\"trxsaldo_verifiedby\":\"adm1\"}}]', '1');
INSERT INTO `histories` VALUES ('9c1eb16d-a934-11e7-aa40-54650c0f9c19', '[{\"before\":{\"trxsaldo_id\":\"6a1a3397-a92e-11e7-aa40-54650c0f9c19\",\"trxsaldo_status\":\"2\"}},{\"after\":{\"trxsaldo_id\":\"6a1a3397-a92e-11e7-aa40-54650c0f9c19\",\"trxsaldo_status\":\"2\",\"trxsaldo_verifiedby\":\"adm1\"}}]', '1');
INSERT INTO `histories` VALUES ('0ffc6cd6-a93a-11e7-aa40-54650c0f9c19', '[{\"before\":{\"trxsaldo_id\":\"6a1a3397-a92e-11e7-aa40-54650c0f9c19\",\"trxsaldo_status\":\"2\"}},{\"after\":{\"trxsaldo_id\":\"6a1a3397-a92e-11e7-aa40-54650c0f9c19\",\"trxsaldo_status\":\"1\",\"trxsaldo_verifiedby\":\"adm1\"}}]', '1');
INSERT INTO `histories` VALUES ('3dee01e0-a93a-11e7-aa40-54650c0f9c19', '[{\"before\":{\"trxsaldo_id\":null,\"trxsaldo_status\":null}},{\"after\":{\"trxsaldo_note\":\"ditolak\",\"trxsaldo_id\":null,\"trxsaldo_status\":\"2\",\"trxsaldo_verifiedby\":\"adm1\"}}]', '1');
INSERT INTO `histories` VALUES ('5a080c57-a93a-11e7-aa40-54650c0f9c19', '[{\"before\":{\"trxsaldo_id\":null,\"trxsaldo_status\":null}},{\"after\":{\"trxsaldo_note\":\"tes\",\"trxsaldo_id\":null,\"trxsaldo_status\":\"2\",\"trxsaldo_verifiedby\":\"adm1\"}}]', '1');
INSERT INTO `histories` VALUES ('c199151f-a93a-11e7-aa40-54650c0f9c19', '[{\"before\":{\"trxsaldo_id\":\"6adcf5f9-a8e9-11e7-8d92-54650c0f9c19\",\"trxsaldo_status\":\"0\"}},{\"after\":{\"trxsaldo_note\":\"asdf\",\"trxsaldo_id\":\"6adcf5f9-a8e9-11e7-8d92-54650c0f9c19\",\"trxsaldo_status\":\"2\",\"trxsaldo_verifiedby\":\"adm1\"}}]', '1');
INSERT INTO `histories` VALUES ('ddaec609-a93a-11e7-aa40-54650c0f9c19', '[{\"before\":{\"trxsaldo_id\":\"6adcf5f9-a8e9-11e7-8d92-54650c0f9c19\",\"trxsaldo_status\":\"2\"}},{\"after\":{\"trxsaldo_note\":\"o\",\"trxsaldo_id\":\"6adcf5f9-a8e9-11e7-8d92-54650c0f9c19\",\"trxsaldo_status\":\"2\",\"trxsaldo_verifiedby\":\"adm1\"}}]', '1');
INSERT INTO `histories` VALUES ('1ae48a75-a93b-11e7-aa40-54650c0f9c19', '[{\"before\":{\"user_id\":\"ea861c0f-a7e9-11e7-bbff-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"anggota_note\":\"tidak layak\",\"user_id\":\"ea861c0f-a7e9-11e7-bbff-54650c0f9c19\",\"user_status\":\"2\"}}]', '1');
INSERT INTO `histories` VALUES ('55585c78-a93b-11e7-aa40-54650c0f9c19', '[{\"before\":{\"trxsaldo_id\":\"6adcf5f9-a8e9-11e7-8d92-54650c0f9c19\",\"trxsaldo_status\":\"2\"}},{\"after\":{\"trxsaldo_note\":\"qwe\",\"trxsaldo_id\":\"6adcf5f9-a8e9-11e7-8d92-54650c0f9c19\",\"trxsaldo_status\":\"2\",\"trxsaldo_verifiedby\":\"adm1\"}}]', '1');
INSERT INTO `histories` VALUES ('6e452292-a99d-11e7-8dd0-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('792df6a4-a99d-11e7-8dd0-54650c0f9c19', '[{\"before\":{\"role_id\":\"1\",\"menu_id\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\"]}},{\"after\":{\"menu_id\":[\"12\"],\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('81d76555-a99d-11e7-8dd0-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('fa73597c-a9f8-11e7-8a12-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('04abb357-aa34-11e7-bddd-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('dcf8d13c-aa38-11e7-bddd-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('b97ab781-aa3b-11e7-bddd-54650c0f9c19', '', '1');
INSERT INTO `histories` VALUES ('638689bd-aa3c-11e7-bddd-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('6fb90f5a-aa3c-11e7-bddd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"DB1EC34F-7E36-4CAE-A2EB-E329D12EE9BC\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"25054\"}}]', '1');
INSERT INTO `histories` VALUES ('5be46b8b-aa3e-11e7-bddd-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('81111b05-aa3e-11e7-bddd-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('8bc0189a-aa3e-11e7-bddd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"37ACEBDF-F21E-4246-9B91-1056E66E2D4E\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"19530\"}}]', '1');
INSERT INTO `histories` VALUES ('3357ea4c-aa3f-11e7-bddd-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('3ce6bc1a-aa3f-11e7-bddd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"F8F5405D-225B-40BE-A56C-5660A1C86015\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"26518\"}}]', '1');
INSERT INTO `histories` VALUES ('674d1ff4-aa41-11e7-bddd-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('6b4074ba-aa41-11e7-bddd-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('79c12f44-aa41-11e7-bddd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"97830\"}}]', '1');
INSERT INTO `histories` VALUES ('f0f12f72-aa42-11e7-bddd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"1ddf36b7-a630-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"1\",\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\"}}]', '1');
INSERT INTO `histories` VALUES ('120d39ce-aa43-11e7-bddd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"08ace22d-a63b-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"1\",\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\"}}]', '1');
INSERT INTO `histories` VALUES ('34f5e542-aa43-11e7-bddd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"1ddf36b7-a630-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"1\",\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\"}}]', '1');
INSERT INTO `histories` VALUES ('57f1384a-aa43-11e7-bddd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"08ace22d-a63b-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"1\",\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\"}}]', '1');
INSERT INTO `histories` VALUES ('7c4778a3-aa43-11e7-bddd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"08ace22d-a63b-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"1\",\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\"}}]', '1');
INSERT INTO `histories` VALUES ('eb7eca11-aa43-11e7-bddd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"652c096d-a63a-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"1\",\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\"}}]', '1');
INSERT INTO `histories` VALUES ('4192389a-aa44-11e7-bddd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"08ace22d-a63b-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"1\",\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\"}}]', '1');
INSERT INTO `histories` VALUES ('51ad18f7-aa44-11e7-bddd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"08ace22d-a63b-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"1\",\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\"}}]', '1');
INSERT INTO `histories` VALUES ('68975e87-aa44-11e7-bddd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"1ddf36b7-a630-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"1\",\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\"}}]', '1');
INSERT INTO `histories` VALUES ('8e135025-aa44-11e7-bddd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"08ace22d-a63b-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"1\",\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\"}}]', '1');
INSERT INTO `histories` VALUES ('94e1a43a-aa44-11e7-bddd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"1ddf36b7-a630-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"2\",\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\"}}]', '1');
INSERT INTO `histories` VALUES ('40599a42-aa71-11e7-bddd-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('8268a19e-aa72-11e7-bddd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"08ace22d-a63b-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"2\",\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\"}}]', '1');
INSERT INTO `histories` VALUES ('974c8d6a-aa72-11e7-bddd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"08ace22d-a63b-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"3\",\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\"}}]', '1');
INSERT INTO `histories` VALUES ('bf04dfb4-aa72-11e7-bddd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"08ace22d-a63b-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"2\",\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\"}}]', '1');
INSERT INTO `histories` VALUES ('c7ddd063-aa72-11e7-bddd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"produk_qty\":\"2\",\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\"}}]', '1');
INSERT INTO `histories` VALUES ('4362966f-aa94-11e7-bddd-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('583cb3a9-aaf9-11e7-bddd-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('6c86c055-aaf9-11e7-bddd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpembayaran_metode\":\"21EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"trxpembayaran_nominal\":\"234\",\"trxpembayaran_kdbank\":\"1\",\"trxpembayaran_noref\":\"2345\",\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\"}}]', '1');
INSERT INTO `histories` VALUES ('2e8d092c-aafa-11e7-bddd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpembayaran_metode\":\"21EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"trxpembayaran_nominal\":\"100000\",\"trxpembayaran_kdbank\":\"1\",\"trxpembayaran_noref\":\"1234567890\",\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\"}}]', '1');
INSERT INTO `histories` VALUES ('d8d5b472-aafa-11e7-bddd-54650c0f9c19', '[{\"before\":{\"role_id\":\"1\",\"menu_id\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"12\"]}},{\"after\":{\"menu_id\":[\"13\",\"14\"],\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('2fea760e-aafb-11e7-bddd-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('7c791e6d-ab32-11e7-bddd-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('555928a2-ab35-11e7-bddd-54650c0f9c19', '[{\"before\":{\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\",\"trxpembayaran_status\":\"0\"}},{\"after\":{\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\",\"trxpembayaran_verifiedby\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('28d6bafb-ab37-11e7-bddd-54650c0f9c19', '[{\"before\":{\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\",\"trxpembayaran_status\":\"1\"}},{\"after\":{\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\",\"trxpembayaran_verifiedby\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('e4d8571d-ab37-11e7-bddd-54650c0f9c19', '[{\"before\":{\"trxpesanan_id\":null,\"trxpembayaran_status\":null}},{\"after\":{\"trxpembayaran_note\":\"tes\",\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\",\"trxpembayaran_verifiedby\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('23143105-ab38-11e7-bddd-54650c0f9c19', '[{\"before\":{\"trxpesanan_id\":null,\"trxpembayaran_status\":null}},{\"after\":{\"trxpembayaran_note\":\"123\",\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\",\"trxpembayaran_verifiedby\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('4571241b-ab39-11e7-bddd-54650c0f9c19', '[{\"before\":{\"role_id\":\"1\",\"menu_id\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"12\",\"13\",\"14\"]}},{\"after\":{\"menu_id\":[\"15\",\"16\"],\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('73b45965-abb4-11e7-ac4a-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('433991c1-abbc-11e7-ac4a-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\",\"trxpengiriman_ekspedisi\":\"1\",\"trxpengiriman_tglkirim\":\"2017-10-30\"}}]', '1');
INSERT INTO `histories` VALUES ('d4351730-abbc-11e7-ac4a-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\",\"trxpengiriman_ekspedisi\":\"1\",\"trxpengiriman_tglkirim\":\"2017-10-02\"}}]', '1');
INSERT INTO `histories` VALUES ('4c976f33-abc4-11e7-ac4a-54650c0f9c19', '[{\"before\":{\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\",\"trxpengiriman_noresi\":null}},{\"after\":{\"trxpesanan_id\":\"7F039353-7DC2-459E-95D9-B65193B70CAB\",\"trxpengiriman_ekspedisi\":\"1\",\"trxpengiriman_tglkirim\":\"2017-10-02\",\"trxpengiriman_noresi\":\"987654321\"}}]', '1');
INSERT INTO `histories` VALUES ('27ed770d-abc5-11e7-ac4a-54650c0f9c19', '[{\"before\":{\"role_id\":\"1\",\"menu_id\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"12\",\"13\",\"14\",\"15\",\"16\"]}},{\"after\":{\"menu_id\":[\"17\"],\"role_id\":\"1\"}}]', '1');

-- ----------------------------
-- Table structure for icon
-- ----------------------------
DROP TABLE IF EXISTS `icon`;
CREATE TABLE `icon` (
  `icon_name` varchar(100) NOT NULL,
  `icon_group` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`icon_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of icon
-- ----------------------------
INSERT INTO `icon` VALUES ('glyphicon glyphicon-adjust', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-alert', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-align-center', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-align-justify', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-align-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-align-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-apple', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-arrow-down', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-arrow-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-arrow-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-arrow-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-asterisk', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-baby-formula', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-backward', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-ban-circle', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-barcode', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-bed', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-bell', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-bishop', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-bitcoin', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-blackboard', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-bold', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-book', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-bookmark', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-briefcase', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-btc', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-bullhorn', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-calendar', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-camera', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-cd', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-certificate', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-check', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-chevron-down', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-chevron-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-chevron-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-chevron-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-circle-arrow-down', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-circle-arrow-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-circle-arrow-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-circle-arrow-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-cloud', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-cloud-download', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-cloud-upload', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-cog', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-collapse-down', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-collapse-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-comment', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-compressed', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-console', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-copy', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-copyright-mark', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-credit-card', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-cutlery', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-dashboard', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-download', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-download-alt', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-duplicate', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-earphone', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-edit', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-education', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-eject', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-envelope', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-equalizer', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-erase', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-eur', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-euro', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-exclamation-sign', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-expand', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-export', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-eye-close', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-eye-open', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-facetime-video', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-fast-backward', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-fast-forward', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-file', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-film', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-filter', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-fire', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-flag', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-flash', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-floppy-disk', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-floppy-open', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-floppy-remove', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-floppy-save', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-floppy-saved', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-folder-close', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-folder-open', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-font', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-forward', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-fullscreen', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-gbp', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-gift', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-glass', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-globe', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-grain', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-hand-down', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-hand-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-hand-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-hand-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-hd-video', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-hdd', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-header', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-headphones', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-heart', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-heart-empty', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-home', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-hourglass', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-ice-lolly', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-ice-lolly-tasted', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-import', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-inbox', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-indent-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-indent-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-info-sign', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-italic', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-jpy', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-king', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-knight', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-lamp', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-leaf', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-level-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-link', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-list', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-list-alt', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-lock', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-log-in', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-log-out', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-magnet', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-map-marker', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-menu-down', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-menu-hamburger', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-menu-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-menu-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-menu-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-minus', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-minus-sign', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-modal-window', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-move', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-music', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-new-window', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-object-align-bottom', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-object-align-horizontal', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-object-align-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-object-align-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-object-align-top', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-object-align-vertical', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-off', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-oil', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-ok', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-ok-circle', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-ok-sign', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-open', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-open-file', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-option-horizontal', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-option-vertical', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-paperclip', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-paste', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-pause', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-pawn', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-pencil', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-phone', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-phone-alt', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-picture', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-piggy-bank', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-plane', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-play', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-play-circle', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-plus', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-plus-sign', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-print', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-pushpin', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-qrcode', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-queen', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-question-sign', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-random', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-record', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-refresh', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-registration-mark', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-remove', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-remove-circle', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-remove-sign', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-repeat', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-resize-full', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-resize-horizontal', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-resize-small', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-resize-vertical', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-retweet', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-road', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-rub', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-ruble', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-save', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-save-file', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-saved', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-scale', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-scissors', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-screenshot', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sd-video', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-search', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-send', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-share', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-share-alt', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-shopping-cart', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-signal', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sort', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sort-by-alphabet', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sort-by-alphabet-alt', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sort-by-attributes', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sort-by-attributes-alt', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sort-by-order', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sort-by-order-alt', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sound-5-1', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sound-6-1', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sound-7-1', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sound-dolby', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sound-stereo', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-star', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-star-empty', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-stats', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-step-backward', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-step-forward', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-stop', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-subscript', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-subtitles', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sunglasses', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-superscript', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-tag', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-tags', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-tasks', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-tent', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-text-background', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-text-color', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-text-height', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-text-size', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-text-width', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-th', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-th-large', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-th-list', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-thumbs-down', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-thumbs-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-time', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-tint', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-tower', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-transfer', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-trash', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-tree-conifer', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-tree-deciduous', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-triangle-bottom', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-triangle-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-triangle-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-triangle-top', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-unchecked', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-upload', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-usd', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-user', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-volume-down', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-volume-off', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-volume-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-warning-sign', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-wrench', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-xbt', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-yen', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-zoom-in', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-zoom-out', 'glyphicon');

-- ----------------------------
-- Table structure for languages
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `language_id` char(4) NOT NULL,
  `language_name` varchar(50) NOT NULL,
  `language_active` tinyint(4) NOT NULL,
  `language_default` tinyint(4) NOT NULL,
  PRIMARY KEY (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of languages
-- ----------------------------
INSERT INTO `languages` VALUES ('id', 'Indonesia', '1', '1');

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(50) NOT NULL,
  `menu_url` varchar(50) NOT NULL,
  `menu_icon` varchar(50) NOT NULL,
  `menu_parent` int(11) NOT NULL,
  `menu_order` smallint(6) NOT NULL,
  `menu_active` tinyint(4) NOT NULL,
  `menu_create_module` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES ('1', 'settings', '#', 'glyphicon glyphicon-cog', '0', '1', '1', '0');
INSERT INTO `menus` VALUES ('2', 'users', 'users', 'glyphicon glyphicon-user', '1', '1', '1', '0');
INSERT INTO `menus` VALUES ('3', 'roles', 'roles', 'glyphicon glyphicon-cog', '1', '2', '1', '0');
INSERT INTO `menus` VALUES ('4', 'menus', 'menus', 'glyphicon glyphicon-equalizer', '1', '3', '1', '0');
INSERT INTO `menus` VALUES ('5', 'products', 'products', 'glyphicon glyphicon-equalizer', '0', '2', '1', '0');
INSERT INTO `menus` VALUES ('6', 'keanggotaans', 'keanggotaans', 'glyphicon glyphicon-cog', '0', '3', '1', '0');
INSERT INTO `menus` VALUES ('7', 'anggotas', 'anggotas', 'glyphicon glyphicon-cog', '6', '1', '1', '0');
INSERT INTO `menus` VALUES ('8', 'reqanggotas', 'reqanggotas', 'glyphicon glyphicon-cog', '6', '2', '1', '0');
INSERT INTO `menus` VALUES ('9', 'saldos', 'saldos', 'glyphicon glyphicon-cog', '0', '4', '1', '0');
INSERT INTO `menus` VALUES ('10', 'saldos', 'saldos', 'glyphicon glyphicon-cog', '9', '1', '1', '0');
INSERT INTO `menus` VALUES ('11', 'reqsaldos', 'reqsaldos', 'glyphicon glyphicon-cog', '9', '2', '1', '0');
INSERT INTO `menus` VALUES ('12', 'trxpesanans', 'trxpesanans', 'glyphicon glyphicon-cog', '0', '5', '1', '0');
INSERT INTO `menus` VALUES ('13', 'trxpesanans', 'trxpesanans', 'glyphicon glyphicon-cog', '12', '1', '1', '0');
INSERT INTO `menus` VALUES ('14', 'trxpembayaranverify', 'trxpembayaranverify', 'glyphicon glyphicon-cog', '12', '2', '1', '0');
INSERT INTO `menus` VALUES ('15', 'trxpesananverify', 'trxpesananverify', 'glyphicon glyphicon-cog', '12', '3', '1', '0');
INSERT INTO `menus` VALUES ('16', 'trxpengirimans', 'trxpengirimans', 'glyphicon glyphicon-cog', '12', '4', '1', '0');
INSERT INTO `menus` VALUES ('17', 'trackings', 'trackings', 'glyphicon glyphicon-cog', '0', '6', '1', '0');

-- ----------------------------
-- Table structure for produk
-- ----------------------------
DROP TABLE IF EXISTS `produk`;
CREATE TABLE `produk` (
  `produk_id` varchar(36) NOT NULL,
  `produk_identity` varchar(5) NOT NULL,
  `produk_nama` varchar(100) NOT NULL,
  `produk_harga` decimal(10,2) NOT NULL,
  `produk_stok` int(11) NOT NULL,
  `produk_satuan` varchar(25) NOT NULL,
  `produk_status` bit(1) NOT NULL,
  `produk_photo` text,
  `produk_description` text,
  PRIMARY KEY (`produk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of produk
-- ----------------------------
INSERT INTO `produk` VALUES ('08ace22d-a63b-11e7-bee2-54650c0f9c19', 'jjkl', 'klj', '9.00', '9', 'jnk', '', '15414_IMG_7206_(2).JPG', 'jjk');
INSERT INTO `produk` VALUES ('1ddf36b7-a630-11e7-bee2-54650c0f9c19', '890', '809', '809.00', '98', 'iu', '', '', '098');
INSERT INTO `produk` VALUES ('20650133-a63d-11e7-bee2-54650c0f9c19', 'pppp', 'pppp', '8888.00', '8', 'jkj', '', '', 'jk');
INSERT INTO `produk` VALUES ('2b6cd002-a48e-11e7-8af8-54650c0f9c19', 'P001', 'beras', '100000.00', '5', 'karung', '', null, 'testing');
INSERT INTO `produk` VALUES ('652c096d-a63a-11e7-bee2-54650c0f9c19', 'sa', 'sd', '90.00', '0', 'kkj', '', '', 'df');
INSERT INTO `produk` VALUES ('6fd9a04e-a63e-11e7-bee2-54650c0f9c19', 'ret', 'ert', '3.00', '34', 'f', '', '3967_KPR.jpg', 'sdf');
INSERT INTO `produk` VALUES ('95696fcd-a63e-11e7-bee2-54650c0f9c19', 'g001', 'gula edit', '8000.00', '9', 'pcs', '', '46392_237600_kn_(2).jpg', 'sadfsd');
INSERT INTO `produk` VALUES ('983ebfc6-a63d-11e7-bee2-54650c0f9c19', 'jkl', 'kjk', '0.00', '0', 'k', '', '', 'kj');
INSERT INTO `produk` VALUES ('babc039e-a62f-11e7-bee2-54650c0f9c19', 'sda', 'sdf', '98.00', '8', 'jh', '', '', '980');
INSERT INTO `produk` VALUES ('bcdf4d4e-a63a-11e7-bee2-54650c0f9c19', 'j', 'kl', '9.00', '0', 'kl', '', '', 'klj');
INSERT INTO `produk` VALUES ('be469fbc-a63e-11e7-bee2-54650c0f9c19', 'asdf', 'ddd', '45.00', '34', 'sdf', '', '40386_ShowImage.jpg', 'sdfasdf');
INSERT INTO `produk` VALUES ('e2bffe8e-a5e8-11e7-bee2-54650c0f9c19', 'P002', 'telor edit', '5000.00', '5', 'kilo', '', 'asdf', 'telor ayam');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(50) NOT NULL,
  `role_level` int(11) NOT NULL,
  `role_active` tinyint(4) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'SUPER ADMIN', '99', '1');
INSERT INTO `roles` VALUES ('2', 'MEMBER', '1', '1');
INSERT INTO `roles` VALUES ('3', 'VISITOR', '2', '1');

-- ----------------------------
-- Table structure for role_menus
-- ----------------------------
DROP TABLE IF EXISTS `role_menus`;
CREATE TABLE `role_menus` (
  `role_menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`role_menu_id`),
  KEY `role_id` (`role_id`),
  KEY `role_menus_ibfk_1` (`menu_id`),
  CONSTRAINT `role_menus_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`menu_id`),
  CONSTRAINT `role_menus_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of role_menus
-- ----------------------------
INSERT INTO `role_menus` VALUES ('115', '1', '1');
INSERT INTO `role_menus` VALUES ('116', '1', '2');
INSERT INTO `role_menus` VALUES ('117', '1', '3');
INSERT INTO `role_menus` VALUES ('118', '1', '4');
INSERT INTO `role_menus` VALUES ('119', '1', '5');
INSERT INTO `role_menus` VALUES ('120', '1', '6');
INSERT INTO `role_menus` VALUES ('121', '1', '7');
INSERT INTO `role_menus` VALUES ('122', '1', '8');
INSERT INTO `role_menus` VALUES ('123', '1', '9');
INSERT INTO `role_menus` VALUES ('124', '1', '10');
INSERT INTO `role_menus` VALUES ('125', '1', '11');
INSERT INTO `role_menus` VALUES ('126', '1', '12');
INSERT INTO `role_menus` VALUES ('127', '1', '13');
INSERT INTO `role_menus` VALUES ('128', '1', '14');
INSERT INTO `role_menus` VALUES ('129', '1', '15');
INSERT INTO `role_menus` VALUES ('130', '1', '16');
INSERT INTO `role_menus` VALUES ('131', '1', '17');

-- ----------------------------
-- Table structure for saldo
-- ----------------------------
DROP TABLE IF EXISTS `saldo`;
CREATE TABLE `saldo` (
  `user_id` varchar(36) NOT NULL,
  `saldo_saldo` decimal(18,2) DEFAULT NULL,
  `saldo_lastupdate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of saldo
-- ----------------------------
INSERT INTO `saldo` VALUES ('ede4108c-a7fd-11e7-bbff-54650c0f9c19', '50000000.00', '2017-10-04 09:26:15');

-- ----------------------------
-- Table structure for temp_anggota
-- ----------------------------
DROP TABLE IF EXISTS `temp_anggota`;
CREATE TABLE `temp_anggota` (
  `user_id` varchar(36) NOT NULL,
  `user_identity` varchar(4) DEFAULT NULL,
  `anggota_nama` varchar(100) DEFAULT NULL,
  `anggota_tmplahir` varchar(100) DEFAULT NULL,
  `anggota_tgllahir` date DEFAULT NULL,
  `anggota_alamat` text,
  `anggota_jnskelamin` varchar(1) DEFAULT NULL,
  `anggota_tglregistrasi` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `anggota_notlp` varchar(50) DEFAULT NULL,
  `anggota_email` varchar(50) DEFAULT NULL,
  `anggota_noidentitas` varchar(50) DEFAULT NULL,
  `anggota_jnsidentitas` varchar(36) DEFAULT NULL,
  `anggota_scanidentitas` text,
  `anggota_approvedby` varchar(36) DEFAULT NULL,
  `anggota_tglapprove` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `anggota_persetujuan` bit(1) DEFAULT NULL,
  `anggota_photo` text,
  `user_registerdate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `user_status` smallint(1) DEFAULT NULL,
  `anggota_note` text,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of temp_anggota
-- ----------------------------
INSERT INTO `temp_anggota` VALUES ('ea861c0f-a7e9-11e7-bbff-54650c0f9c19', 'tes2', 'testing 2', 'sdf', '2017-10-11', 'sadf', 'L', '2017-10-03 14:21:55', '098', 'lkjl', '0987', '12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '3121_Jellyfish.jpg', null, '2017-10-03 14:21:55', '', '17028_Hydrangeas.jpg', '2017-10-03 14:21:55', '2', 'tidak layak');

-- ----------------------------
-- Table structure for trxpengiriman
-- ----------------------------
DROP TABLE IF EXISTS `trxpengiriman`;
CREATE TABLE `trxpengiriman` (
  `trxpesanan_id` varchar(36) NOT NULL,
  `trxpengiriman_tglkirim` date DEFAULT NULL,
  `trxpengiriman_ekspedisi` int(11) DEFAULT NULL,
  `trxpengiriman_noresi` varchar(100) DEFAULT NULL,
  `trxpengiriman_status` smallint(1) DEFAULT NULL,
  `trxpengiriman_verifiedby` varchar(36) DEFAULT NULL,
  `trxpengiriman_verifieddate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `trxpengiriman_note` text,
  PRIMARY KEY (`trxpesanan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of trxpengiriman
-- ----------------------------
INSERT INTO `trxpengiriman` VALUES ('7F039353-7DC2-459E-95D9-B65193B70CAB', '2017-10-02', '1', '987654321', '0', null, '2017-10-08 08:02:04', null);

-- ----------------------------
-- Table structure for trxpesanan
-- ----------------------------
DROP TABLE IF EXISTS `trxpesanan`;
CREATE TABLE `trxpesanan` (
  `trxpesanan_id` varchar(36) NOT NULL,
  `trxpesanan_tgl` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `trxpesanan_userid` varchar(36) DEFAULT NULL,
  `trxpesanan_status` tinyint(1) DEFAULT NULL,
  `trxpesanan_invoiceid` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`trxpesanan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of trxpesanan
-- ----------------------------
INSERT INTO `trxpesanan` VALUES ('7F039353-7DC2-459E-95D9-B65193B70CAB', '2017-10-07 07:55:22', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '1', '97830');

-- ----------------------------
-- Table structure for trxpesanan_detail
-- ----------------------------
DROP TABLE IF EXISTS `trxpesanan_detail`;
CREATE TABLE `trxpesanan_detail` (
  `trxpesanan_id` varchar(36) NOT NULL,
  `trxpesanan_produkid` varchar(36) NOT NULL,
  `trxpesanan_qty` int(11) DEFAULT NULL,
  PRIMARY KEY (`trxpesanan_id`,`trxpesanan_produkid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of trxpesanan_detail
-- ----------------------------
INSERT INTO `trxpesanan_detail` VALUES ('7F039353-7DC2-459E-95D9-B65193B70CAB', '08ace22d-a63b-11e7-bee2-54650c0f9c19', '7');
INSERT INTO `trxpesanan_detail` VALUES ('7F039353-7DC2-459E-95D9-B65193B70CAB', '1ddf36b7-a630-11e7-bee2-54650c0f9c19', '2');
INSERT INTO `trxpesanan_detail` VALUES ('7F039353-7DC2-459E-95D9-B65193B70CAB', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '2');

-- ----------------------------
-- Table structure for trxpesanan_pembayaran
-- ----------------------------
DROP TABLE IF EXISTS `trxpesanan_pembayaran`;
CREATE TABLE `trxpesanan_pembayaran` (
  `trxpesanan_id` varchar(36) NOT NULL,
  `trxpembayaran_metode` varchar(36) DEFAULT NULL,
  `trxpembayaran_status` smallint(1) DEFAULT NULL,
  `trxpembayaran_tgl` date DEFAULT NULL,
  `trxpembayaran_nominal` decimal(18,2) DEFAULT NULL,
  `trxpembayaran_kdbank` varchar(3) DEFAULT NULL,
  `trxpembayaran_noref` varchar(100) DEFAULT NULL,
  `trxpembayaran_verifiedby` varchar(36) DEFAULT NULL,
  `trxpembayaran_verifieddate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `trxpembayaran_note` text,
  PRIMARY KEY (`trxpesanan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of trxpesanan_pembayaran
-- ----------------------------
INSERT INTO `trxpesanan_pembayaran` VALUES ('7F039353-7DC2-459E-95D9-B65193B70CAB', '21EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '2', '2017-10-07', '100000.00', '1', '1234567890', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '2017-10-07 15:18:54', '123');

-- ----------------------------
-- Table structure for trxsaldo
-- ----------------------------
DROP TABLE IF EXISTS `trxsaldo`;
CREATE TABLE `trxsaldo` (
  `trxsaldo_id` varchar(36) NOT NULL,
  `trxsaldo_user_id` varchar(36) DEFAULT NULL,
  `trxsaldo_tgl` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `trxsaldo_nominal` decimal(18,2) DEFAULT NULL,
  `trxsaldo_kdbank` varchar(3) DEFAULT NULL,
  `trxsaldo_noref` varchar(100) DEFAULT NULL,
  `trxsaldo_status` tinyint(1) DEFAULT NULL,
  `trxsaldo_verifiedby` varchar(36) DEFAULT NULL,
  `trxsaldo_verifieddate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `trxsaldo_note` text,
  PRIMARY KEY (`trxsaldo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of trxsaldo
-- ----------------------------
INSERT INTO `trxsaldo` VALUES ('11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6H', 'ede4108c-a7fd-11e7-bbff-54650c0f9c19', '2017-10-04 11:16:46', '100000.00', '1', '1234567890', '0', null, null, null);
INSERT INTO `trxsaldo` VALUES ('6a1a3397-a92e-11e7-aa40-54650c0f9c19', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '2017-10-05 02:27:29', '100000.00', '1', '987654321', '1', 'adm1', '2017-10-05 02:27:29', null);
INSERT INTO `trxsaldo` VALUES ('6adcf5f9-a8e9-11e7-8d92-54650c0f9c19', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '2017-10-05 02:36:35', '50000.00', '1', '1234567890', '2', 'adm1', '2017-10-05 02:36:35', null);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` varchar(36) NOT NULL,
  `user_identity` varchar(4) DEFAULT NULL,
  `user_role_id` int(11) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_registerdate` timestamp NULL DEFAULT NULL,
  `user_salt` varchar(5) DEFAULT NULL,
  `user_password` text,
  `user_status` bit(1) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `role_id` (`user_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('2b6cd002-a48e-11e7-8af8-54650c0f9c19', 'adm1', '1', 'administrator', '2017-09-29 03:46:57', '79760', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', '');
INSERT INTO `users` VALUES ('ede4108c-a7fd-11e7-bbff-54650c0f9c19', 'tes2', '2', 'testing 2', '2017-10-03 12:44:32', '29174', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', '');

-- ----------------------------
-- View structure for view_saldo_summary
-- ----------------------------
DROP VIEW IF EXISTS `view_saldo_summary`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `view_saldo_summary` AS SELECT
		a.user_id,
		c.user_identity,
		b.anggota_nama,
		a.saldo_lastupdate,
		a.saldo_saldo,
		(SELECT SUM(trxsaldo_nominal) FROM trxsaldo) AS total_trxsaldo,
		(SELECT SUM(trxsaldo_nominal) FROM trxsaldo) AS total_trxpemesanan
	FROM
		saldo a
	LEFT JOIN anggota b ON a.user_id = b.user_id
	LEFT JOIN users c ON a.user_id = c.user_id ;

-- ----------------------------
-- View structure for view_trxpembayaran
-- ----------------------------
DROP VIEW IF EXISTS `view_trxpembayaran`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `view_trxpembayaran` AS SELECT
	a.*,c.anggota_nama,d.configuration_value AS metodepembayaran_label,e.bank_nama
FROM
	trxpesanan_pembayaran a
INNER JOIN trxpesanan b ON a.trxpesanan_id = b.trxpesanan_id
INNER JOIN anggota c ON b.trxpesanan_userid = c.user_id
LEFT JOIN configurations d ON a.trxpembayaran_metode = d.configuration_id
LEFT JOIN bank e ON a.trxpembayaran_kdbank = e.bank_id ;

-- ----------------------------
-- View structure for view_trxsaldo
-- ----------------------------
DROP VIEW IF EXISTS `view_trxsaldo`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `view_trxsaldo` AS SELECT
	a.*,c.user_identity,b.anggota_nama,d.bank_nama,e.configuration_value AS trxsaldo_status_label
FROM
	trxsaldo a
LEFT JOIN anggota b ON a.trxsaldo_user_id = b.user_id
LEFT JOIN users c ON a.trxsaldo_user_id = c.user_id
LEFT JOIN bank d ON a.trxsaldo_kdbank = d.bank_id
LEFT JOIN configurations e ON a.trxsaldo_status = e.configuration_key
WHERE
	e.configurationgroup_id = '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6H' ;

-- ----------------------------
-- Procedure structure for log_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `log_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `log_insert`(IN id_status varchar(20), IN user_id varchar(4),IN ip_address varchar(16),IN browser varchar(255),IN p_status varchar(4),IN method_name varchar(25),IN module_name varchar(32),IN history_detail TEXT)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @is_success = @success;
			SET @history_id = @last_history_id;

				IF @is_success = 1 THEN
					CALL sp_audit_trail_insert (user_id,ip_address,browser,@history_id,p_status,method_name,module_name,id_status,@success);
					SET @is_success = @success;
					
					IF @is_success = 1 THEN
						COMMIT;
					END IF;
				END IF;
		SELECT @is_success AS jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_anggota_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_anggota_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_anggota_insert`(IN p_user_identity varchar(4),  IN p_anggota_nama varchar(100),  IN p_anggota_tmplahir varchar(100),  IN p_anggota_tgllahir date,  IN p_anggota_alamat text,  IN p_anggota_jnskelamin varchar(1),  IN p_anggota_notlp varchar(50),  IN p_anggota_email varchar(50),  IN p_anggota_jnsidentitas varchar(36),  IN p_anggota_noidentitas varchar(50),  IN p_anggota_persetujuan BIT,  IN p_anggota_scanidentitas text,  IN p_anggota_photo text,IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text,IN user_salt varchar(5),IN user_password text,IN user_status BIT)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @user_id = (SELECT UUID());
			SET @currentdate = (SELECT NOW());

			INSERT INTO anggota(user_id,anggota_nama,anggota_tmplahir,anggota_tgllahir,anggota_alamat,anggota_jnskelamin,anggota_tglregistrasi,anggota_notlp,anggota_email,anggota_noidentitas,anggota_jnsidentitas,anggota_scanidentitas,anggota_persetujuan,anggota_photo,anggota_approvedby,anggota_tglapprove)
				VALUES(@user_id,p_anggota_nama,p_anggota_tmplahir,p_anggota_tgllahir,p_anggota_alamat,p_anggota_jnskelamin,@currentdate,p_anggota_notlp,p_anggota_email,p_anggota_noidentitas,p_anggota_jnsidentitas,p_anggota_scanidentitas,p_anggota_persetujuan,p_anggota_photo,userid,@currentdate);

			INSERT INTO users(user_id,user_identity, user_role_id,user_name,user_registerdate,user_salt,user_password,user_status)
				VALUES(@user_id,p_user_identity,'2',p_anggota_nama,@currentdate,user_salt,user_password,user_status);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,@user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_anggota_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_anggota_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_anggota_update`(IN p_user_id varchar(36),IN p_user_identity varchar(4),  IN p_anggota_nama varchar(100),  IN p_anggota_tmplahir varchar(100),  IN p_anggota_tgllahir date,  IN p_anggota_alamat text,  IN p_anggota_jnskelamin varchar(1),  IN p_anggota_notlp varchar(50),  IN p_anggota_email varchar(50),  IN p_anggota_jnsidentitas varchar(36),  IN p_anggota_noidentitas varchar(50),  IN p_anggota_persetujuan BIT,  IN p_anggota_scanidentitas text,  IN p_anggota_photo text,IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			UPDATE anggota
			SET
				anggota_nama = p_anggota_nama,
				anggota_tmplahir = p_anggota_tmplahir,
				anggota_tgllahir = p_anggota_tgllahir,
				anggota_alamat = p_anggota_alamat,
				anggota_jnskelamin = p_anggota_jnskelamin,
				anggota_notlp = p_anggota_notlp,
				anggota_email = p_anggota_email,
				anggota_jnsidentitas = p_anggota_jnsidentitas,
				anggota_noidentitas = p_anggota_noidentitas,
				anggota_persetujuan = p_anggota_persetujuan,
				anggota_scanidentitas = p_anggota_scanidentitas,
				anggota_photo = p_anggota_photo
			WHERE
				user_id = p_user_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_audit_trail_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_audit_trail_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_audit_trail_insert`(IN user_id varchar(36), IN ip_address varchar(16),
 IN browser varchar(255), IN history_id VARCHAR(36), IN p_status varchar(4), IN action_id varchar(25), IN module_id varchar(32),IN record_id varchar(36), OUT is_success BIT)
BEGIN 
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @audit_id = (SELECT UUID());

			INSERT INTO audit_trails
				VALUES(@audit_id, CURRENT_TIMESTAMP, user_id, ip_address, browser, history_id, p_status, action_id, module_id, record_id);

			SET is_success = 1;
		COMMIT;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_get_saldo_summary
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_get_saldo_summary`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_saldo_summary`(IN p_user_id varchar(36))
BEGIN
	#Routine body goes here...
	SELECT
		a.user_id,
		c.user_identity,
		b.anggota_nama,
		a.saldo_lastupdate,
		a.saldo_saldo,
		(SELECT SUM(trxsaldo_nominal) FROM trxsaldo) AS total_trxsaldo,
		(SELECT SUM(trxsaldo_nominal) FROM trxsaldo) AS total_trxpemesanan
	FROM
		saldo a
	LEFT JOIN anggota b ON a.user_id = b.user_id
	LEFT JOIN users c ON a.user_id = c.user_id
	WHERE
		a.user_id = p_user_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_histories_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_histories_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_histories_insert`(IN history_detail text, IN history_active INT, OUT is_success BIT,OUT history_id varchar(36))
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @history_id = (SELECT UUID());

			INSERT INTO histories(history_id,history_detail, history_active)
				VALUES(@history_id,history_detail, history_active);

			SET is_success = 1;
			SET history_id = @history_id;
		COMMIT;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_icon_list
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_icon_list`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_icon_list`()
BEGIN
	#Routine body goes here...
	SELECT icon_name FROM icon ORDER BY icon_name ASC;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_menu_list
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_menu_list`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_menu_list`()
BEGIN
	#Routine body goes here...
	SELECT a.*,(CASE WHEN menu_active = '1' THEN 'active' ELSE 'inactive' END) AS menu_active_label,
	(CASE WHEN menu_parent = 0 THEN
		'PARENT'
	ELSE
		(SELECT menu_name FROM menus WHERE menu_id = a.menu_parent)
	END) AS menus
	FROM menus a;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_menu_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_menu_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_menu_update`(IN p_menu_id INT,IN p_menu_icon varchar(50),IN p_menu_active tinyint,IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

	START TRANSACTION;
		UPDATE menus
		SET	
			menu_icon = p_menu_icon,
			menu_active = p_menu_active
		WHERE menu_id = p_menu_id;

		CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
		SET @histories_success = @success;
		SET @history_id = @last_history_id;

		IF @histories_success = 1 THEN
			CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_menu_id,@success);
			SET @is_success = @success;
		END IF;

		SET @is_success = 1;
	COMMIT;
	SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_produk_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_produk_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_produk_insert`(IN p_produk_identity varchar(5),IN p_produk_nama varchar(100),IN p_produk_harga decimal(10,2),IN p_produk_stok INT,IN p_produk_satuan varchar(25),IN p_produk_photo text,IN p_produk_description text,IN p_produk_status BIT,IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @produk_id = (SELECT UUID());

			INSERT INTO produk(produk_id,produk_identity,produk_nama,produk_harga,produk_stok,produk_satuan,produk_status,produk_photo,produk_description)
				VALUES(@produk_id,p_produk_identity,p_produk_nama,p_produk_harga,p_produk_stok,p_produk_satuan,p_produk_status,p_produk_photo,p_produk_description);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,@produk_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_produk_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_produk_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_produk_update`(IN p_produk_id varchar(36),IN p_produk_identity varchar(5),IN p_produk_nama varchar(100),IN p_produk_harga decimal(10,2),IN p_produk_stok INT,IN p_produk_satuan varchar(25),IN p_produk_photo text,IN p_produk_description text,IN p_produk_status BIT,IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			UPDATE produk
			SET
				produk_identity = p_produk_identity,
				produk_nama = p_produk_nama,
				produk_harga = p_produk_harga,
				produk_stok = p_produk_stok,
				produk_satuan = p_produk_satuan,
				produk_photo = p_produk_photo,
				produk_description = p_produk_description,
				produk_status = p_produk_status
			WHERE
				produk_id = p_produk_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_produk_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_reqsaldo_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_reqsaldo_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reqsaldo_insert`(IN p_trxsaldo_noref varchar(100),  IN p_trxsaldo_kdbank varchar(3),  IN p_trxsaldo_nominal decimal(18,2),IN p_trxsaldo_user_id varchar(36),IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @trxsaldo_id = (SELECT UUID());
			SET @currentdate = (SELECT NOW());

			INSERT INTO trxsaldo(trxsaldo_id,trxsaldo_user_id,trxsaldo_tgl,trxsaldo_nominal,trxsaldo_kdbank,trxsaldo_noref,trxsaldo_status)
				VALUES(@trxsaldo_id,p_trxsaldo_user_id,@currentdate,p_trxsaldo_nominal,p_trxsaldo_kdbank,p_trxsaldo_noref,0);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,@trxsaldo_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_role_list
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_role_list`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_role_list`()
BEGIN
	#Routine body goes here...
	SELECT 
		c.role_id,
		c.role_name,
		role_level,
		role_active,
		CASE WHEN role_active = '1' THEN 'active' ELSE 'inactive' END AS role_active_label,
		GROUP_CONCAT(b.menu_name SEPARATOR ', ') AS roles
	FROM role_menus a
	LEFT JOIN menus b ON a.menu_id = b.menu_id
	RIGHT JOIN roles c ON a.role_id = c.role_id
	GROUP BY role_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_role_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_role_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_role_update`(IN p_role_id INT,IN p_menu_id varchar(255),IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

	START TRANSACTION;
		DELETE FROM role_menus WHERE role_id = p_role_id;

		iterator:
		LOOP
			IF LENGTH(TRIM(p_menu_id)) = 0 OR p_menu_id IS NULL THEN
				LEAVE iterator;
			END IF;

			SET @next = SUBSTRING_INDEX(p_menu_id,',',1);
			SET @nextlen = LENGTH(@next);
			SET @value = TRIM(@next);
			INSERT INTO role_menus (role_id,menu_id) VALUES (p_role_id,@next);
			SET p_menu_id = INSERT(p_menu_id,1,@nextlen + 1,'');

		END LOOP;

		CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
		SET @histories_success = @success;
		SET @history_id = @last_history_id;

		IF @histories_success = 1 THEN
			CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_role_id,@success);
			SET @is_success = @success;
		END IF;

		SET @is_success = 1;
	COMMIT;
	SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_tempanggota_approval
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_tempanggota_approval`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_tempanggota_approval`(IN p_user_id varchar(36),IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text,IN p_user_salt varchar(5), IN p_user_password text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SELECT 
				user_identity,anggota_nama,anggota_tmplahir,anggota_tgllahir,anggota_alamat,anggota_jnskelamin,anggota_notlp,
				anggota_email,anggota_jnsidentitas,anggota_noidentitas,anggota_persetujuan,anggota_scanidentitas,anggota_photo
			INTO
				@user_identity,@anggota_nama,@anggota_tmplahir,@anggota_tgllahir,@anggota_alamat,@anggota_jnskelamin,@anggota_notlp,
				@anggota_email,@anggota_jnsidentitas,@anggota_noidentitas,@anggota_persetujuan,@anggota_scanidentitas,@anggota_photo
			FROM 
				temp_anggota
			WHERE
				user_id = p_user_id;

			CALL sp_anggota_insert(@user_identity,@anggota_nama,@anggota_tmplahir,@anggota_tgllahir,@anggota_alamat,@anggota_jnskelamin,
				@anggota_notlp,@anggota_email,@anggota_jnsidentitas,@anggota_noidentitas,@anggota_persetujuan,@anggota_scanidentitas,
				@anggota_photo,userid,ip_address,browser,p_status,method_name,module_name,history_detail ,p_user_salt,p_user_password,1);
			
			UPDATE temp_anggota SET user_status = 1 WHERE user_id = p_user_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_tempanggota_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_tempanggota_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_tempanggota_insert`(IN p_user_identity varchar(4),  IN p_anggota_nama varchar(100),  IN p_anggota_tmplahir varchar(100),  IN p_anggota_tgllahir date,  IN p_anggota_alamat text,  IN p_anggota_jnskelamin varchar(1),  IN p_anggota_notlp varchar(50),  IN p_anggota_email varchar(50),  IN p_anggota_jnsidentitas varchar(36),  IN p_anggota_noidentitas varchar(50),  IN p_anggota_persetujuan BIT,  IN p_anggota_scanidentitas text,  IN p_anggota_photo text,IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text,IN user_salt varchar(5),IN user_password text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @user_id = (SELECT UUID());
			SET @currentdate = (SELECT NOW());

			INSERT INTO temp_anggota(user_id,user_identity,anggota_nama,anggota_tmplahir,anggota_tgllahir,anggota_alamat,anggota_jnskelamin,anggota_tglregistrasi,anggota_notlp,anggota_email,anggota_noidentitas,anggota_jnsidentitas,anggota_scanidentitas,anggota_persetujuan,anggota_photo,user_registerdate,user_status)
				VALUES(@user_id,p_user_identity,p_anggota_nama,p_anggota_tmplahir,p_anggota_tgllahir,p_anggota_alamat,p_anggota_jnskelamin,@currentdate,p_anggota_notlp,p_anggota_email,p_anggota_noidentitas,p_anggota_jnsidentitas,p_anggota_scanidentitas,p_anggota_persetujuan,p_anggota_photo,@currentdate,'0');

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,@user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_tempanggota_reject
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_tempanggota_reject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_tempanggota_reject`(IN p_user_id varchar(36),IN p_anggota_note text,IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
					
			UPDATE temp_anggota SET user_status = 2,anggota_note = p_anggota_note WHERE user_id = p_user_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trxpengiriman_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trxpengiriman_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trxpengiriman_insert`(IN p_trxpesanan_id varchar(36),IN p_trxpengiriman_ekspedisi INT, IN p_trxpengiriman_tglkirim date,  IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;

			INSERT INTO trxpengiriman(trxpesanan_id,trxpengiriman_tglkirim,trxpengiriman_ekspedisi,trxpengiriman_status)
				VALUES(p_trxpesanan_id,p_trxpengiriman_tglkirim,p_trxpengiriman_ekspedisi,0);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_trxpesanan_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trxpengiriman_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trxpengiriman_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trxpengiriman_update`(IN p_trxpesanan_id varchar(36),IN p_trxpengiriman_noresi varchar(100),  IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;

			UPDATE
				trxpengiriman
			SET
				trxpengiriman_noresi = p_trxpengiriman_noresi
			WHERE
				trxpesanan_id = p_trxpesanan_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_trxpesanan_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trxpesanandetail_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trxpesanandetail_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trxpesanandetail_insert`(IN p_trxpesanan_id varchar(36), IN p_produk_id varchar(36),  IN p_produk_qty INT,IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;

			INSERT INTO trxpesanan_detail(trxpesanan_id,trxpesanan_produkid,trxpesanan_qty)
				VALUES(p_trxpesanan_id,p_produk_id, p_produk_qty);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_produk_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trxpesanandetail_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trxpesanandetail_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trxpesanandetail_update`(IN p_trxpesanan_id varchar(36), IN p_produk_id varchar(36),  IN p_produk_qty INT,IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @totalQty = (SELECT
												trxpesanan_qty
											FROM
												trxpesanan_detail
											WHERE
												trxpesanan_id = p_trxpesanan_id
												AND trxpesanan_produkid = p_produk_id) + p_produk_qty;

			UPDATE trxpesanan_detail
			SET
				trxpesanan_qty = @totalQty
			WHERE
				trxpesanan_id = p_trxpesanan_id
				AND trxpesanan_produkid = p_produk_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_produk_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trxpesananpembayaran_approve
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trxpesananpembayaran_approve`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trxpesananpembayaran_approve`(IN p_trxpesanan_id varchar(36),IN p_trxpembayaran_verifiedby varchar(36),IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @currentdate = (SELECT NOW());
			
			UPDATE 
				trxpesanan_pembayaran 
			SET 
				trxpembayaran_status = 1,
				trxpembayaran_verifiedby = p_trxpembayaran_verifiedby,
				trxpembayaran_verifieddate = @currentdate
			WHERE 
				trxpesanan_id = p_trxpesanan_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_trxpesanan_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trxpesananpembayaran_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trxpesananpembayaran_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trxpesananpembayaran_insert`(IN p_trxpembayaran_metode varchar(36),IN p_trxpembayaran_nominal decimal(18,2),IN p_trxpembayaran_kdbank varchar(3),IN p_trxpembayaran_noref varchar(100),IN p_trxpesanan_id varchar(36),IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			/*SET @trxpesananpembayaran_id = (SELECT UUID());*/
			SET @currentdate = (SELECT NOW());

			INSERT INTO trxpesanan_pembayaran(trxpesanan_id,trxpembayaran_metode,trxpembayaran_status,trxpembayaran_tgl,trxpembayaran_nominal,trxpembayaran_kdbank,trxpembayaran_noref)
				VALUES(p_trxpesanan_id,p_trxpembayaran_metode,0,@currentdate,p_trxpembayaran_nominal,p_trxpembayaran_kdbank,p_trxpembayaran_noref);

			UPDATE 
				trxpesanan
			SET
				trxpesanan_status = 1
			WHERE
				trxpesanan_id = p_trxpesanan_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_trxpesanan_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trxpesananpembayaran_reject
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trxpesananpembayaran_reject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trxpesananpembayaran_reject`(IN p_trxpesanan_id varchar(36),IN p_trxpembayaran_verifiedby varchar(36),IN p_trxpembayaran_note text,IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @currentdate = (SELECT NOW());
			
			UPDATE 
				trxpesanan_pembayaran 
			SET 
				trxpembayaran_status = 2,
				trxpembayaran_verifiedby = p_trxpembayaran_verifiedby,
				trxpembayaran_note = p_trxpembayaran_note,
				trxpembayaran_verifieddate = @currentdate
			WHERE 
				trxpesanan_id = p_trxpesanan_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_trxpesanan_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trxpesanan_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trxpesanan_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trxpesanan_insert`(IN p_trxpesanan_id varchar(36), IN p_trxpesanan_userid varchar(36),  IN p_trxpesanan_invoiceid varchar(10),IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @currentdate = (SELECT NOW());

			INSERT INTO trxpesanan(trxpesanan_id,trxpesanan_tgl,trxpesanan_userid,trxpesanan_status,trxpesanan_invoiceid)
				VALUES(p_trxpesanan_id,@currentdate,p_trxpesanan_userid,0,p_trxpesanan_invoiceid);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_trxpesanan_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trxsaldo_response
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trxsaldo_response`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trxsaldo_response`(IN p_trxsaldo_id varchar(36),IN p_trxsaldo_status tinyint,IN p_trxsaldo_verifiedby varchar(36),IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;	
			SET @currentdate = (SELECT NOW());

			UPDATE trxsaldo 
			SET 
				trxsaldo_status = p_trxsaldo_status,
				trxsaldo_verifiedby = p_trxsaldo_verifiedby,
				trxsaldo_verifieddate = @currentdate
			WHERE 
				trxsaldo_id = p_trxsaldo_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_trxsaldo_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_users_delete
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_users_delete`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_users_delete`(IN p_user_id varchar(36),IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			DELETE FROM users
			WHERE
				user_id = p_user_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_users_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_users_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_users_insert`(IN user_identity varchar(4),IN user_name varchar(100),IN role_id INT,IN user_salt varchar(5),IN user_password text,IN user_status BIT,IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @user_id = (SELECT UUID());
			SET @currentdate = (SELECT NOW());

			INSERT INTO users(user_id,user_identity, user_role_id,user_name,user_registerdate,user_salt,user_password,user_status)
				VALUES(@user_id,user_identity,role_id,user_name,@currentdate,user_salt,user_password,user_status);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,@user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_users_status
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_users_status`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_users_status`(IN p_user_id varchar(36),IN p_user_status BIT,IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			UPDATE users
			SET
				user_status = p_user_status
			WHERE
				user_id = p_user_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_users_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_users_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_users_update`(IN p_user_id varchar(36),IN user_identity varchar(4),IN user_name varchar(100),IN role_id INT,IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			UPDATE users
			SET
				user_identity = user_identity,
				user_role_id = role_id,
				user_name = user_name
			WHERE
				user_id = p_user_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;
