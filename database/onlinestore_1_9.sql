/*
Navicat MySQL Data Transfer

Source Server         : localhost_mySQL
Source Server Version : 50626
Source Host           : localhost:3306
Source Database       : onlinestore_1

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2017-10-13 16:48:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for anggota
-- ----------------------------
DROP TABLE IF EXISTS `anggota`;
CREATE TABLE `anggota` (
  `user_id` varchar(36) NOT NULL,
  `anggota_nama` varchar(100) DEFAULT NULL,
  `anggota_tmplahir` varchar(100) DEFAULT NULL,
  `anggota_tgllahir` date DEFAULT NULL,
  `anggota_alamat` text,
  `anggota_jnskelamin` varchar(1) DEFAULT NULL,
  `anggota_tglregistrasi` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `anggota_notlp` varchar(50) DEFAULT NULL,
  `anggota_email` varchar(50) DEFAULT NULL,
  `anggota_noidentitas` varchar(50) DEFAULT NULL,
  `anggota_jnsidentitas` varchar(36) DEFAULT NULL,
  `anggota_scanidentitas` text,
  `anggota_approvedby` varchar(36) DEFAULT NULL,
  `anggota_tglapprove` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `anggota_persetujuan` bit(1) DEFAULT NULL,
  `anggota_photo` text,
  `anggota_areaid` int(2) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of anggota
-- ----------------------------
INSERT INTO `anggota` VALUES ('1169f43c-ad88-11e7-bf38-54650c0f9c19', 'poiu', 'jjh', '2017-10-10', 'kjklj', 'L', '2017-10-10 13:55:57', '0987', 't@m.m', '09876', '12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '5560_Chrysanthemum.jpg', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '2017-10-10 13:55:57', '', '71871_Desert.jpg', null);
INSERT INTO `anggota` VALUES ('2b6cd002-a48e-11e7-8af8-54650c0f9c19', 'administrator 01', 'bandung', '2017-10-01', 'green city view', 'L', '2017-10-12 11:27:41', '089898798', 'saya@mail.com', '1234567890', 'KTP', 'tes', 'adm1', '2017-10-12 11:27:41', '', 'tes1', '2');
INSERT INTO `anggota` VALUES ('610eaee3-ad68-11e7-96d5-54650c0f9c19', 'T Suryana', 'Bandung', '2017-10-16', 'The Green', 'L', '2017-10-10 11:43:49', '098765432', 'tan@mail.com', '1234567890', '12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '55892_Koala.jpg', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '2017-10-10 11:43:49', '', '317_Lighthouse.jpg', '2');
INSERT INTO `anggota` VALUES ('c0bedf89-ad87-11e7-bf38-54650c0f9c19', 'ppp', 'pppp', '2017-10-04', 'asf', 'P', '2017-10-10 13:53:42', '987', '6@m.m', '789', '12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '2691_Desert.jpg', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '2017-10-10 13:53:42', '', '77938_Hydrangeas.jpg', '2');
INSERT INTO `anggota` VALUES ('c7ae2667-ad82-11e7-bf38-54650c0f9c19', 'saya', 'asdf', '0000-00-00', 'asdf', 'L', '2017-10-10 13:48:06', '1234', '3@m.m', 'sdf', '12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '2017-10-10 13:48:06', '', '', '1');
INSERT INTO `anggota` VALUES ('d2afa5b9-ad67-11e7-96d5-54650c0f9c19', 'Tantan Suryana', 'Bandung', '2017-10-06', 'Green City View', 'L', '2017-10-10 10:05:09', '086543222', 'tan@mail.com', '123456789', '12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '28280_Hydrangeas.jpg', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '2017-10-10 10:05:09', '', '28076_Jellyfish.jpg', '2');
INSERT INTO `anggota` VALUES ('ede4108c-a7fd-11e7-bbff-54650c0f9c19', 'testing 2', 'sdf', '2017-10-11', 'sadf', 'L', '2017-10-03 12:44:32', '098', 'lkjl', '0987', '12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '3121_Jellyfish.jpg', 'adm1', '2017-10-03 12:44:32', '', '17028_Hydrangeas.jpg', null);

-- ----------------------------
-- Table structure for area
-- ----------------------------
DROP TABLE IF EXISTS `area`;
CREATE TABLE `area` (
  `area_id` int(11) NOT NULL,
  `area_nama` varchar(255) DEFAULT NULL,
  `area_biayakirim` decimal(18,2) DEFAULT NULL,
  PRIMARY KEY (`area_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of area
-- ----------------------------
INSERT INTO `area` VALUES ('1', 'Bandung I', '10000.00');
INSERT INTO `area` VALUES ('2', 'Bandung II', '20000.00');

-- ----------------------------
-- Table structure for audit_trails
-- ----------------------------
DROP TABLE IF EXISTS `audit_trails`;
CREATE TABLE `audit_trails` (
  `audit_trail_id` varchar(36) DEFAULT NULL,
  `audit_trail_action_date` datetime DEFAULT NULL,
  `audit_trail_user_id` varchar(36) DEFAULT NULL,
  `audit_trail_ip_address` varchar(16) DEFAULT NULL,
  `audit_trail_browser_ua` text,
  `audit_trail_history_id` varchar(36) DEFAULT NULL,
  `audit_trail_status` varchar(4) DEFAULT NULL,
  `audit_trail_action_id` varchar(25) DEFAULT NULL,
  `audit_trail_module_id` varchar(32) DEFAULT NULL,
  `audit_trail_record_id` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of audit_trails
-- ----------------------------
INSERT INTO `audit_trails` VALUES ('f9de388b-ac5e-11e7-902b-54650c0f9c19', '2017-10-09 02:29:17', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'f9d67e2f-ac5e-11e7-902b-54650c0f9c19', '0000', 'add', 'users', 'f9c996cc-ac5e-11e7-902b-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('39905749-ac5f-11e7-902b-54650c0f9c19', '2017-10-09 02:31:04', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '39783bf2-ac5f-11e7-902b-54650c0f9c19', '0000', 'add', 'users', '396440f8-ac5f-11e7-902b-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('389fde9f-ac60-11e7-902b-54650c0f9c19', '2017-10-09 02:38:12', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '38930c3c-ac60-11e7-902b-54650c0f9c19', '0000', 'edit', 'users', 'f9c996cc-ac5e-11e7-902b-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('7e52e2ab-ac65-11e7-902b-54650c0f9c19', '2017-10-09 03:15:57', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '7e461723-ac65-11e7-902b-54650c0f9c19', '0000', 'edit', 'users', 'f9c996cc-ac5e-11e7-902b-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('f9d0c3e7-ac66-11e7-902b-54650c0f9c19', '2017-10-09 03:26:33', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'f9cccd59-ac66-11e7-902b-54650c0f9c19', '0000', 'delete', 'users', 'c0514c8f-abf3-11e7-ac4a-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('1d08a64d-ac67-11e7-902b-54650c0f9c19', '2017-10-09 03:27:32', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '1cfb3030-ac67-11e7-902b-54650c0f9c19', '0000', 'status', 'users', 'cd329b31-abf3-11e7-ac4a-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('7e859e50-ac67-11e7-902b-54650c0f9c19', '2017-10-09 03:30:16', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '7e71188c-ac67-11e7-902b-54650c0f9c19', '0000', 'delete', 'users', 'ede4108c-a7fd-11e7-bbff-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('7e9c7110-ac67-11e7-902b-54650c0f9c19', '2017-10-09 03:30:16', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '7e90f88f-ac67-11e7-902b-54650c0f9c19', '0000', 'delete', 'users', 'aabedccf-abf3-11e7-ac4a-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('7ee11b13-ac67-11e7-902b-54650c0f9c19', '2017-10-09 03:30:17', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '7ed6eaf9-ac67-11e7-902b-54650c0f9c19', '0000', 'delete', 'users', 'ca2029b8-abf1-11e7-ac4a-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('9f4a8c5f-ac67-11e7-902b-54650c0f9c19', '2017-10-09 03:31:11', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '9f3f0b01-ac67-11e7-902b-54650c0f9c19', '0000', 'status', 'users', 'b489c5a5-abf3-11e7-ac4a-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('8a0b2b8b-ac93-11e7-814e-54650c0f9c19', '2017-10-09 08:45:33', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '89f91195-ac93-11e7-814e-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('5a9b28e0-ac9c-11e7-814e-54650c0f9c19', '2017-10-09 09:48:39', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '5a8e5aca-ac9c-11e7-814e-54650c0f9c19', '0000', 'add', 'users', '5a7c3a86-ac9c-11e7-814e-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('6c64dffa-ac9c-11e7-814e-54650c0f9c19', '2017-10-09 09:49:09', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '6c5a9162-ac9c-11e7-814e-54650c0f9c19', '0000', 'edit', 'users', '5a7c3a86-ac9c-11e7-814e-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('81c64cef-ac9e-11e7-814e-54650c0f9c19', '2017-10-09 10:04:04', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '81b9740d-ac9e-11e7-814e-54650c0f9c19', '0000', 'resetpassword', 'users', '5a7c3a86-ac9c-11e7-814e-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('9f1a08b5-ac9e-11e7-814e-54650c0f9c19', '2017-10-09 10:04:53', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '9f14dab8-ac9e-11e7-814e-54650c0f9c19', '0000', 'resetpassword', 'users', '5a7c3a86-ac9c-11e7-814e-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('d7fcbeb4-ac9e-11e7-814e-54650c0f9c19', '2017-10-09 10:06:29', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'd7eead01-ac9e-11e7-814e-54650c0f9c19', '0000', 'resetpassword', 'users', '5a7c3a86-ac9c-11e7-814e-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('2ce0fa6f-ac9f-11e7-814e-54650c0f9c19', '2017-10-09 10:08:51', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2cd5ce10-ac9f-11e7-814e-54650c0f9c19', '0000', 'resetpassword', 'users', '5a7c3a86-ac9c-11e7-814e-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('27a43c64-aca0-11e7-814e-54650c0f9c19', '2017-10-09 10:15:52', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '278c0b0c-aca0-11e7-814e-54650c0f9c19', '0000', 'add', 'users', '277d788c-aca0-11e7-814e-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('9f7568ea-aca0-11e7-814e-54650c0f9c19', '2017-10-09 10:19:13', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '9f6c637a-aca0-11e7-814e-54650c0f9c19', '0000', 'resetpassword', 'users', '277d788c-aca0-11e7-814e-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('c72142de-aca5-11e7-814e-54650c0f9c19', '2017-10-09 10:56:07', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'c71848ae-aca5-11e7-814e-54650c0f9c19', '0000', 'edit', 'roles', '2');
INSERT INTO `audit_trails` VALUES ('c96b4919-acb8-11e7-814e-54650c0f9c19', '2017-10-09 13:12:11', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'c950d4e7-acb8-11e7-814e-54650c0f9c19', '0000', 'edit', 'menus', '2');
INSERT INTO `audit_trails` VALUES ('e143c2ce-acb8-11e7-814e-54650c0f9c19', '2017-10-09 13:12:51', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'e1219f43-acb8-11e7-814e-54650c0f9c19', '0000', 'edit', 'menus', '2');
INSERT INTO `audit_trails` VALUES ('ec4c42c1-acb8-11e7-814e-54650c0f9c19', '2017-10-09 13:13:10', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'ec3e30c4-acb8-11e7-814e-54650c0f9c19', '0000', 'edit', 'menus', '2');
INSERT INTO `audit_trails` VALUES ('b13be44f-acbf-11e7-814e-54650c0f9c19', '2017-10-09 14:01:37', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'b136ba6a-acbf-11e7-814e-54650c0f9c19', '0000', 'add', 'products', 'b131680a-acbf-11e7-814e-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('cc1138f5-acdb-11e7-814e-54650c0f9c19', '2017-10-09 17:22:49', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'cc06cd20-acdb-11e7-814e-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('c182f5a0-acdd-11e7-814e-54650c0f9c19', '2017-10-09 17:36:50', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'c17f1475-acdd-11e7-814e-54650c0f9c19', '0000', 'edit', 'products', '1ddf36b7-a630-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('c1dff688-acde-11e7-814e-54650c0f9c19', '2017-10-09 17:44:00', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'c1dacb6b-acde-11e7-814e-54650c0f9c19', '0000', 'edit', 'products', 'be469fbc-a63e-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('d0320d60-acde-11e7-814e-54650c0f9c19', '2017-10-09 17:44:24', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'd0291676-acde-11e7-814e-54650c0f9c19', '0000', 'edit', 'products', 'be469fbc-a63e-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('de97429c-acde-11e7-814e-54650c0f9c19', '2017-10-09 17:44:48', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'de8a7901-acde-11e7-814e-54650c0f9c19', '0000', 'edit', 'products', 'be469fbc-a63e-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('b192a647-acdf-11e7-814e-54650c0f9c19', '2017-10-09 17:50:42', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'b185d5fa-acdf-11e7-814e-54650c0f9c19', '0000', 'edit', 'products', 'be469fbc-a63e-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('04b0e329-ace0-11e7-814e-54650c0f9c19', '2017-10-09 17:53:01', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '04abb281-ace0-11e7-814e-54650c0f9c19', '0000', 'edit', 'products', 'be469fbc-a63e-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('2253645a-ace0-11e7-814e-54650c0f9c19', '2017-10-09 17:53:51', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '22492a0e-ace0-11e7-814e-54650c0f9c19', '0000', 'edit', 'products', 'be469fbc-a63e-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('2def3075-ace0-11e7-814e-54650c0f9c19', '2017-10-09 17:54:11', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2de64569-ace0-11e7-814e-54650c0f9c19', '0000', 'edit', 'products', 'be469fbc-a63e-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('4bfc9d25-ace0-11e7-814e-54650c0f9c19', '2017-10-09 17:55:01', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '4bf40284-ace0-11e7-814e-54650c0f9c19', '0000', 'edit', 'products', '20650133-a63d-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('fab6fd4e-ace0-11e7-814e-54650c0f9c19', '2017-10-09 17:59:54', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'fa9f54bb-ace0-11e7-814e-54650c0f9c19', '0000', 'add', 'products', 'fa998123-ace0-11e7-814e-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('303847fd-ace1-11e7-814e-54650c0f9c19', '2017-10-09 18:01:24', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '30332265-ace1-11e7-814e-54650c0f9c19', '0000', 'edit', 'products', 'fa998123-ace0-11e7-814e-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('5fd76d69-ace1-11e7-814e-54650c0f9c19', '2017-10-09 18:02:44', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '5fce7884-ace1-11e7-814e-54650c0f9c19', '0000', 'edit', 'products', 'fa998123-ace0-11e7-814e-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('f93cb814-ad35-11e7-96d5-54650c0f9c19', '2017-10-10 04:08:18', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'f92f0990-ad35-11e7-96d5-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('9bae9c0a-ad39-11e7-96d5-54650c0f9c19', '2017-10-10 04:34:19', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '9ba6d4ac-ad39-11e7-96d5-54650c0f9c19', '0000', 'add', 'reqanggotas', '9b9633c1-ad39-11e7-96d5-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('2110879d-ad3a-11e7-96d5-54650c0f9c19', '2017-10-10 04:38:03', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '20f801f0-ad3a-11e7-96d5-54650c0f9c19', '0000', 'add', 'reqanggotas', '20e8af85-ad3a-11e7-96d5-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('fd81f359-ad3a-11e7-96d5-54650c0f9c19', '2017-10-10 04:44:13', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'fd757761-ad3a-11e7-96d5-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('38bf0c8a-ad55-11e7-96d5-54650c0f9c19', '2017-10-10 07:52:00', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '38b29443-ad55-11e7-96d5-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('2fa6e56b-ad60-11e7-96d5-54650c0f9c19', '2017-10-10 09:10:29', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2f98cad9-ad60-11e7-96d5-54650c0f9c19', '0000', 'add', 'reqanggotas', '2f8afe30-ad60-11e7-96d5-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('335398ee-ad64-11e7-96d5-54650c0f9c19', '2017-10-10 09:39:13', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '334e6b24-ad64-11e7-96d5-54650c0f9c19', '0000', 'reject', 'reqanggotas', '2f8afe30-ad60-11e7-96d5-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('6300b213-ad64-11e7-96d5-54650c0f9c19', '2017-10-10 09:40:33', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '62e9d4b4-ad64-11e7-96d5-54650c0f9c19', '0000', 'add', 'reqanggotas', '62d77e15-ad64-11e7-96d5-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('1db18a49-ad66-11e7-96d5-54650c0f9c19', '2017-10-10 09:52:56', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '1dacdf98-ad66-11e7-96d5-54650c0f9c19', '0000', 'view', 'reqanggotas', '1d9c69b6-ad66-11e7-96d5-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('1dc34aa2-ad66-11e7-96d5-54650c0f9c19', '2017-10-10 09:52:56', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '1dbc1a1c-ad66-11e7-96d5-54650c0f9c19', '0000', 'view', 'reqanggotas', '62d77e15-ad64-11e7-96d5-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('d2bf07ad-ad67-11e7-96d5-54650c0f9c19', '2017-10-10 10:05:09', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'd2b9de5a-ad67-11e7-96d5-54650c0f9c19', '0000', 'view', 'reqanggotas', 'd2afa5b9-ad67-11e7-96d5-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('d2e432e9-ad67-11e7-96d5-54650c0f9c19', '2017-10-10 10:05:09', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'd2d311d9-ad67-11e7-96d5-54650c0f9c19', '0000', 'view', 'reqanggotas', '62d77e15-ad64-11e7-96d5-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('51832bcf-ad68-11e7-96d5-54650c0f9c19', '2017-10-10 10:08:42', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '5176639c-ad68-11e7-96d5-54650c0f9c19', '0000', 'add', 'reqanggotas', '5164e61a-ad68-11e7-96d5-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('6132c88f-ad68-11e7-96d5-54650c0f9c19', '2017-10-10 10:09:08', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '61211cf9-ad68-11e7-96d5-54650c0f9c19', '0000', 'view', 'reqanggotas', '610eaee3-ad68-11e7-96d5-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('614ead1e-ad68-11e7-96d5-54650c0f9c19', '2017-10-10 10:09:08', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '61456dc3-ad68-11e7-96d5-54650c0f9c19', '0000', 'view', 'reqanggotas', '5164e61a-ad68-11e7-96d5-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('affa9ac1-ad6e-11e7-bf38-54650c0f9c19', '2017-10-10 10:54:16', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'afe8d51c-ad6e-11e7-bf38-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('56783cf6-ad73-11e7-bf38-54650c0f9c19', '2017-10-10 11:27:34', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '566df86e-ad73-11e7-bf38-54650c0f9c19', '0000', 'edit', 'anggotas', '610eaee3-ad68-11e7-96d5-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('c0e1250a-ad73-11e7-bf38-54650c0f9c19', '2017-10-10 11:30:32', '2b6c', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'c0d45a70-ad73-11e7-bf38-54650c0f9c19', '0000', 'edit', 'anggotas', '610eaee3-ad68-11e7-96d5-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('9c0d91f4-ad75-11e7-bf38-54650c0f9c19', '2017-10-10 11:43:49', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '9c087e26-ad75-11e7-bf38-54650c0f9c19', '0000', 'edit', 'anggotas', '610eaee3-ad68-11e7-96d5-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('c1b32497-ad82-11e7-bf38-54650c0f9c19', '2017-10-10 13:17:56', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'c1a79070-ad82-11e7-bf38-54650c0f9c19', '0000', 'add', 'reqanggotas', 'c18f5f3f-ad82-11e7-bf38-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('c7bfd307-ad82-11e7-bf38-54650c0f9c19', '2017-10-10 13:18:06', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'c7baa57e-ad82-11e7-bf38-54650c0f9c19', '0000', 'view', 'reqanggotas', 'c7ae2667-ad82-11e7-bf38-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('c7e0296b-ad82-11e7-bf38-54650c0f9c19', '2017-10-10 13:18:07', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'c7d08537-ad82-11e7-bf38-54650c0f9c19', '0000', 'view', 'reqanggotas', 'c18f5f3f-ad82-11e7-bf38-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('14a510e9-ad84-11e7-bf38-54650c0f9c19', '2017-10-10 13:27:25', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '149bdcfe-ad84-11e7-bf38-54650c0f9c19', '0000', 'resetpassword', 'anggotas', 'c7ae2667-ad82-11e7-bf38-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('cbcb6f12-ad85-11e7-bf38-54650c0f9c19', '2017-10-10 13:39:42', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'cbc26c50-ad85-11e7-bf38-54650c0f9c19', '0000', 'resetpassword', 'anggotas', 'c7ae2667-ad82-11e7-bf38-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('3e96d3d8-ad86-11e7-bf38-54650c0f9c19', '2017-10-10 13:42:54', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '3e8dde63-ad86-11e7-bf38-54650c0f9c19', '0000', 'resetpassword', 'anggotas', 'c7ae2667-ad82-11e7-bf38-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('4f412ed8-ad86-11e7-bf38-54650c0f9c19', '2017-10-10 13:43:22', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '4f3835e0-ad86-11e7-bf38-54650c0f9c19', '0000', 'resetpassword', 'anggotas', 'c7ae2667-ad82-11e7-bf38-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('f8ae0727-ad86-11e7-bf38-54650c0f9c19', '2017-10-10 13:48:06', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'f8a51226-ad86-11e7-bf38-54650c0f9c19', '0000', 'edit', 'anggotas', 'c7ae2667-ad82-11e7-bf38-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('07beeb1e-ad87-11e7-bf38-54650c0f9c19', '2017-10-10 13:48:32', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '07b5f14e-ad87-11e7-bf38-54650c0f9c19', '0000', 'resetpassword', 'anggotas', 'c7ae2667-ad82-11e7-bf38-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('416572bf-ad87-11e7-bf38-54650c0f9c19', '2017-10-10 13:50:08', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '41593548-ad87-11e7-bf38-54650c0f9c19', '0000', 'resetpassword', 'anggotas', '610eaee3-ad68-11e7-96d5-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('bbbdc52c-ad87-11e7-bf38-54650c0f9c19', '2017-10-10 13:53:34', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'bb9ba8e7-ad87-11e7-bf38-54650c0f9c19', '0000', 'add', 'reqanggotas', 'bb7a6b78-ad87-11e7-bf38-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('c0e605cf-ad87-11e7-bf38-54650c0f9c19', '2017-10-10 13:53:42', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'c0ca30c6-ad87-11e7-bf38-54650c0f9c19', '0000', 'view', 'reqanggotas', 'c0bedf89-ad87-11e7-bf38-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('c0fc561a-ad87-11e7-bf38-54650c0f9c19', '2017-10-10 13:53:42', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'c0f64ffe-ad87-11e7-bf38-54650c0f9c19', '0000', 'view', 'reqanggotas', 'bb7a6b78-ad87-11e7-bf38-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('d54ef635-ad87-11e7-bf38-54650c0f9c19', '2017-10-10 13:54:17', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'd549c1e0-ad87-11e7-bf38-54650c0f9c19', '0000', 'resetpassword', 'anggotas', 'c0bedf89-ad87-11e7-bf38-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('e9327dfc-ad87-11e7-bf38-54650c0f9c19', '2017-10-10 13:54:50', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'e92d5ef8-ad87-11e7-bf38-54650c0f9c19', '0000', 'resetpassword', 'anggotas', 'c0bedf89-ad87-11e7-bf38-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('fbb0c7f3-ad87-11e7-bf38-54650c0f9c19', '2017-10-10 13:55:21', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'fbabd7cd-ad87-11e7-bf38-54650c0f9c19', '0000', 'resetpassword', 'anggotas', 'c0bedf89-ad87-11e7-bf38-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('11853ddc-ad88-11e7-bf38-54650c0f9c19', '2017-10-10 13:55:58', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '1175b645-ad88-11e7-bf38-54650c0f9c19', '0000', 'view', 'reqanggotas', '1169f43c-ad88-11e7-bf38-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('119e7b58-ad88-11e7-bf38-54650c0f9c19', '2017-10-10 13:55:58', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '119824bb-ad88-11e7-bf38-54650c0f9c19', '0000', 'view', 'reqanggotas', '9b9633c1-ad39-11e7-96d5-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('09702b8a-ad94-11e7-bf38-54650c0f9c19', '2017-10-10 15:21:38', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '096b52ab-ad94-11e7-bf38-54650c0f9c19', '0000', 'add', 'reqsaldos', '09614e34-ad94-11e7-bf38-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('d137b33e-ad98-11e7-bf38-54650c0f9c19', '2017-10-10 15:55:51', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'd1299f04-ad98-11e7-bf38-54650c0f9c19', '0000', 'add', 'reqsaldos', 'd1203090-ad98-11e7-bf38-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('77970271-ad9a-11e7-bf38-54650c0f9c19', '2017-10-10 16:07:40', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '777588bc-ad9a-11e7-bf38-54650c0f9c19', '0000', 'add', 'reqsaldos', '776682fe-ad9a-11e7-bf38-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('b4019d02-ad9c-11e7-bf38-54650c0f9c19', '2017-10-10 16:23:40', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'b3f8a958-ad9c-11e7-bf38-54650c0f9c19', '0000', 'add', 'reqsaldos', 'b3ee47b3-ad9c-11e7-bf38-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('04f802fe-ad9d-11e7-bf38-54650c0f9c19', '2017-10-10 16:25:56', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '04e8a01f-ad9d-11e7-bf38-54650c0f9c19', '0000', 'add', 'reqsaldos', '04df1435-ad9d-11e7-bf38-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('df8febd9-ada0-11e7-bf38-54650c0f9c19', '2017-10-10 16:53:31', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'df85d198-ada0-11e7-bf38-54650c0f9c19', '0000', 'reject', 'reqsaldos', '04df1435-ad9d-11e7-bf38-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('a6cd1b9d-ada1-11e7-bf38-54650c0f9c19', '2017-10-10 16:59:05', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'a6c08c7c-ada1-11e7-bf38-54650c0f9c19', '0000', 'reject', 'reqsaldos', 'b3ee47b3-ad9c-11e7-bf38-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('033e3be0-ada2-11e7-bf38-54650c0f9c19', '2017-10-10 17:01:41', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '0330e94a-ada2-11e7-bf38-54650c0f9c19', '0000', 'view', 'reqsaldos', '04df1435-ad9d-11e7-bf38-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('5d0ef88c-ae05-11e7-b4b4-54650c0f9c19', '2017-10-11 04:52:51', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '5cfb2347-ae05-11e7-b4b4-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('9e5f786d-ae09-11e7-b4b4-54650c0f9c19', '2017-10-11 05:23:19', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '9e55c3d8-ae09-11e7-b4b4-54650c0f9c19', '0000', 'add', 'reqsaldos', '9e43774b-ae09-11e7-b4b4-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('eb33db3f-ae09-11e7-b4b4-54650c0f9c19', '2017-10-11 05:25:28', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'eb266104-ae09-11e7-b4b4-54650c0f9c19', '0000', 'view', 'reqsaldos', '9e43774b-ae09-11e7-b4b4-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('3b47f4ea-ae0a-11e7-b4b4-54650c0f9c19', '2017-10-11 05:27:42', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '3b2fac5f-ae0a-11e7-b4b4-54650c0f9c19', '0000', 'add', 'reqsaldos', '3b1f40f0-ae0a-11e7-b4b4-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('40d122c7-ae0a-11e7-b4b4-54650c0f9c19', '2017-10-11 05:27:51', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '40c6dc39-ae0a-11e7-b4b4-54650c0f9c19', '0000', 'view', 'reqsaldos', '3b1f40f0-ae0a-11e7-b4b4-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('5feb7d45-ae0a-11e7-b4b4-54650c0f9c19', '2017-10-11 05:28:44', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '5fdf4f95-ae0a-11e7-b4b4-54650c0f9c19', '0000', 'add', 'reqsaldos', '5fce8e29-ae0a-11e7-b4b4-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('6c78fa0f-ae0a-11e7-b4b4-54650c0f9c19', '2017-10-11 05:29:05', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '6c61ccc2-ae0a-11e7-b4b4-54650c0f9c19', '0000', 'add', 'reqsaldos', '6c5418a5-ae0a-11e7-b4b4-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('7d79c759-ae0a-11e7-b4b4-54650c0f9c19', '2017-10-11 05:29:33', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '7d74c9ac-ae0a-11e7-b4b4-54650c0f9c19', '0000', 'view', 'reqsaldos', '5fce8e29-ae0a-11e7-b4b4-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('a01c4960-ae0a-11e7-b4b4-54650c0f9c19', '2017-10-11 05:30:31', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'a00f7c0d-ae0a-11e7-b4b4-54650c0f9c19', '0000', 'reject', 'reqsaldos', '6c5418a5-ae0a-11e7-b4b4-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('6e62b2d6-ae20-11e7-912c-54650c0f9c19', '2017-10-11 08:06:37', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '6e2cef78-ae20-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('adc5f400-ae34-11e7-912c-54650c0f9c19', '2017-10-11 10:31:33', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'adbcf870-ae34-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', 'FB34FB34-4A4D-43AD-84B3-08E9A383D118');
INSERT INTO `audit_trails` VALUES ('0522d39a-ae35-11e7-912c-54650c0f9c19', '2017-10-11 10:33:59', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '05198992-ae35-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('177b2b21-ae35-11e7-912c-54650c0f9c19', '2017-10-11 10:34:30', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '176ac599-ae35-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '1E880E38-B108-4721-B30A-8B24B5E243A0');
INSERT INTO `audit_trails` VALUES ('b8491a1a-ae35-11e7-912c-54650c0f9c19', '2017-10-11 10:39:00', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'b84001fe-ae35-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('e8e5623c-ae35-11e7-912c-54650c0f9c19', '2017-10-11 10:40:21', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'e8cc1ca0-ae35-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '87423DB2-6209-4D9D-AE2B-248FFFD894E5');
INSERT INTO `audit_trails` VALUES ('0f5fddf4-ae36-11e7-912c-54650c0f9c19', '2017-10-11 10:41:26', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '0f509601-ae36-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('18b6cad8-ae36-11e7-912c-54650c0f9c19', '2017-10-11 10:41:42', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '18a904a5-ae36-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '6193EC28-F9FA-48BA-8E42-EDB31B2A9E36');
INSERT INTO `audit_trails` VALUES ('38e4ef51-ae36-11e7-912c-54650c0f9c19', '2017-10-11 10:42:36', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '38d5df31-ae36-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('429b31e1-ae36-11e7-912c-54650c0f9c19', '2017-10-11 10:42:52', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '427f278b-ae36-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', 'AE9328B6-8A27-452B-BA88-26986D0F8E3E');
INSERT INTO `audit_trails` VALUES ('7f3567de-ae36-11e7-912c-54650c0f9c19', '2017-10-11 10:44:34', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '7f1ebbcc-ae36-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('98d3a9ad-ae36-11e7-912c-54650c0f9c19', '2017-10-11 10:45:17', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '98b78346-ae36-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', 'F3535B9F-228C-4309-9330-E07685D26EAF');
INSERT INTO `audit_trails` VALUES ('c38a6990-ae36-11e7-912c-54650c0f9c19', '2017-10-11 10:46:28', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'c37559d5-ae36-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', 'A25435AD-7DF2-440E-A1F8-193FC73B323F');
INSERT INTO `audit_trails` VALUES ('db9e7752-ae36-11e7-912c-54650c0f9c19', '2017-10-11 10:47:09', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'db95bd49-ae36-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('e572bc2a-ae36-11e7-912c-54650c0f9c19', '2017-10-11 10:47:25', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'e54eadc5-ae36-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '02C95793-FF7B-46C0-A389-67F1E33193EE');
INSERT INTO `audit_trails` VALUES ('04af8acc-ae37-11e7-912c-54650c0f9c19', '2017-10-11 10:48:17', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '04a894c0-ae37-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('0ef4924e-ae37-11e7-912c-54650c0f9c19', '2017-10-11 10:48:35', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '0ee4eb73-ae37-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', 'E45023AC-383A-42AE-9BCF-62C66D9E1EA6');
INSERT INTO `audit_trails` VALUES ('20d09566-ae37-11e7-912c-54650c0f9c19', '2017-10-11 10:49:05', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '20bd5640-ae37-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('2a6dd2f9-ae37-11e7-912c-54650c0f9c19', '2017-10-11 10:49:21', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2a30cf2e-ae37-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '2186E326-83A1-46A8-A79A-BD2FD1520352');
INSERT INTO `audit_trails` VALUES ('4a53acd0-ae37-11e7-912c-54650c0f9c19', '2017-10-11 10:50:14', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '4a4783d3-ae37-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('65f3d64b-ae37-11e7-912c-54650c0f9c19', '2017-10-11 10:51:01', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '65e1f32f-ae37-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', 'C8338ABF-9845-4FC4-B90A-D1E22006EC3D');
INSERT INTO `audit_trails` VALUES ('cc8c1538-ae37-11e7-912c-54650c0f9c19', '2017-10-11 10:53:53', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'cc7bb72f-ae37-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('d74e9c51-ae37-11e7-912c-54650c0f9c19', '2017-10-11 10:54:11', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'd737bad4-ae37-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '3D57CD6C-238C-42D7-B26E-647A8C1015AE');
INSERT INTO `audit_trails` VALUES ('8d9e8c43-ae38-11e7-912c-54650c0f9c19', '2017-10-11 10:59:17', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '8d903658-ae38-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('98420ddb-ae38-11e7-912c-54650c0f9c19', '2017-10-11 10:59:35', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '983173a0-ae38-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '0E59B156-3B3D-4942-AFDD-ABCCEC096DD4');
INSERT INTO `audit_trails` VALUES ('db995428-ae38-11e7-912c-54650c0f9c19', '2017-10-11 11:01:28', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'db8ab7f0-ae38-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('e70526d9-ae38-11e7-912c-54650c0f9c19', '2017-10-11 11:01:47', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'e6e90656-ae38-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', 'EF88C9E3-251B-4BCE-A31F-4815C1E8542D');
INSERT INTO `audit_trails` VALUES ('05fa0fe1-ae39-11e7-912c-54650c0f9c19', '2017-10-11 11:02:39', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '05eb293e-ae39-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('11855781-ae39-11e7-912c-54650c0f9c19', '2017-10-11 11:02:58', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '116fea4b-ae39-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '3F1DD241-7BFB-432B-A34F-AB4C58BB2A9A');
INSERT INTO `audit_trails` VALUES ('48f5448f-ae39-11e7-912c-54650c0f9c19', '2017-10-11 11:04:31', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '48eac9cd-ae39-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('5563be35-ae39-11e7-912c-54650c0f9c19', '2017-10-11 11:04:52', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '5551c38e-ae39-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', 'E101D015-F9DD-463E-8B2A-9FCA818562A9');
INSERT INTO `audit_trails` VALUES ('6ff45cf1-ae39-11e7-912c-54650c0f9c19', '2017-10-11 11:05:36', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '6fe1fe5a-ae39-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('7ca6c667-ae39-11e7-912c-54650c0f9c19', '2017-10-11 11:05:58', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '7c97c9a2-ae39-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', 'CB1B9499-1F7A-4E51-8E57-47658CC90412');
INSERT INTO `audit_trails` VALUES ('97b974b0-ae39-11e7-912c-54650c0f9c19', '2017-10-11 11:06:43', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '97aa7148-ae39-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('a47fc315-ae39-11e7-912c-54650c0f9c19', '2017-10-11 11:07:05', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'a4569b36-ae39-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', 'E2C4C938-9B77-4D1D-BA42-CB38C96636CD');
INSERT INTO `audit_trails` VALUES ('3315b8f7-ae3a-11e7-912c-54650c0f9c19', '2017-10-11 11:11:04', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '33043741-ae3a-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('426e647f-ae3a-11e7-912c-54650c0f9c19', '2017-10-11 11:11:30', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '42544cc1-ae3a-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', 'C57AFFF9-E219-4A36-B5BC-78EB45172DE4');
INSERT INTO `audit_trails` VALUES ('5f68648b-ae3a-11e7-912c-54650c0f9c19', '2017-10-11 11:12:18', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '5f555010-ae3a-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('7e7eebb3-ae3a-11e7-912c-54650c0f9c19', '2017-10-11 11:13:10', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '7e654d2f-ae3a-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '71930024-687D-4E6B-9CDD-7FBE2C2C9D3C');
INSERT INTO `audit_trails` VALUES ('17d973ec-ae3b-11e7-912c-54650c0f9c19', '2017-10-11 11:17:28', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '17cbe0f6-ae3b-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('24008465-ae3b-11e7-912c-54650c0f9c19', '2017-10-11 11:17:48', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '23ecfdbb-ae3b-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', 'A5E138D7-5EFC-4534-A71A-45690C56509A');
INSERT INTO `audit_trails` VALUES ('35eddad7-ae3b-11e7-912c-54650c0f9c19', '2017-10-11 11:18:18', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '35d88072-ae3b-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('43573e88-ae3b-11e7-912c-54650c0f9c19', '2017-10-11 11:18:41', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '43404a5e-ae3b-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', 'CACB1E84-B898-4E2C-B2B9-0D8577870BC5');
INSERT INTO `audit_trails` VALUES ('7c58fa6f-ae3b-11e7-912c-54650c0f9c19', '2017-10-11 11:20:16', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '7c47f856-ae3b-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('8bb847d4-ae3b-11e7-912c-54650c0f9c19', '2017-10-11 11:20:42', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '8ba21f98-ae3b-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', 'BCD6E2F6-9E9C-4613-862A-4FF4C7A36F5B');
INSERT INTO `audit_trails` VALUES ('ab66c3b8-ae3b-11e7-912c-54650c0f9c19', '2017-10-11 11:21:35', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'ab61fd23-ae3b-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('b5c5efbf-ae3b-11e7-912c-54650c0f9c19', '2017-10-11 11:21:52', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'b5b2a682-ae3b-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '53DDE0B2-EAD3-41D8-95E0-D926DF7255A1');
INSERT INTO `audit_trails` VALUES ('4da5d508-ae3c-11e7-912c-54650c0f9c19', '2017-10-11 11:26:07', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '4d9d9003-ae3c-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('587bb229-ae3c-11e7-912c-54650c0f9c19', '2017-10-11 11:26:25', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '58627676-ae3c-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '6D3560F4-EE66-4904-8D53-F3E32C69EE1B');
INSERT INTO `audit_trails` VALUES ('c25414c7-ae3e-11e7-912c-54650c0f9c19', '2017-10-11 11:43:42', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'c246bc56-ae3e-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('cb7d7064-ae3e-11e7-912c-54650c0f9c19', '2017-10-11 11:43:58', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'cb6f5822-ae3e-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '09B8F0EB-648A-4ED3-AF45-F398ACA33B22');
INSERT INTO `audit_trails` VALUES ('f08d2cd8-ae3e-11e7-912c-54650c0f9c19', '2017-10-11 11:45:00', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'f07d93a2-ae3e-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('02056a83-ae3f-11e7-912c-54650c0f9c19', '2017-10-11 11:45:29', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '01f66b56-ae3f-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '4B3924F1-A224-4D69-A7FD-5F2BEB82E880');
INSERT INTO `audit_trails` VALUES ('25a10a73-ae3f-11e7-912c-54650c0f9c19', '2017-10-11 11:46:29', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2593809d-ae3f-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('32c36c51-ae3f-11e7-912c-54650c0f9c19', '2017-10-11 11:46:51', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '32bed9be-ae3f-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '76B0AD30-EA45-44A0-9567-ED70B9D5D70D');
INSERT INTO `audit_trails` VALUES ('4ec6d523-ae3f-11e7-912c-54650c0f9c19', '2017-10-11 11:47:38', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '4eb50ff6-ae3f-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('59c25c75-ae3f-11e7-912c-54650c0f9c19', '2017-10-11 11:47:56', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '59b45766-ae3f-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '6ED2C7D1-5944-453F-88D9-B30BF136A06A');
INSERT INTO `audit_trails` VALUES ('59e9b9b4-ae3f-11e7-912c-54650c0f9c19', '2017-10-11 11:47:56', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '59dcfb3a-ae3f-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '08ace22d-a63b-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('7b3b0e44-ae4a-11e7-912c-54650c0f9c19', '2017-10-11 13:07:37', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '7b3637fe-ae4a-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '1ddf36b7-a630-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('9f01ed26-ae4a-11e7-912c-54650c0f9c19', '2017-10-11 13:08:37', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '9ef5f471-ae4a-11e7-912c-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('bba0650d-ae4b-11e7-912c-54650c0f9c19', '2017-10-11 13:16:35', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'bb7a6ab0-ae4b-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', 'F8EE837D-D333-47B3-A826-8D9143276F3B');
INSERT INTO `audit_trails` VALUES ('bbba4622-ae4b-11e7-912c-54650c0f9c19', '2017-10-11 13:16:35', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'bbaed417-ae4b-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '08ace22d-a63b-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('ca483df7-ae4b-11e7-912c-54650c0f9c19', '2017-10-11 13:16:59', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'ca3b2adb-ae4b-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '08ace22d-a63b-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('da1a66ea-ae4b-11e7-912c-54650c0f9c19', '2017-10-11 13:17:26', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'da0eddec-ae4b-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '08ace22d-a63b-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('2df1dff3-ae55-11e7-912c-54650c0f9c19', '2017-10-11 14:24:12', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2dea1be3-ae55-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '1ddf36b7-a630-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('5f70a1fc-ae64-11e7-912c-54650c0f9c19', '2017-10-11 16:12:58', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '5f5582af-ae64-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '08ace22d-a63b-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('6faf15e2-ae64-11e7-912c-54650c0f9c19', '2017-10-11 16:13:25', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '6fa4cc18-ae64-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '1ddf36b7-a630-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('0fe9d8b2-ae65-11e7-912c-54650c0f9c19', '2017-10-11 16:17:54', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '0fd9c0e4-ae65-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '08ace22d-a63b-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('e1a5611a-ae67-11e7-912c-54650c0f9c19', '2017-10-11 16:38:04', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'e196bc3e-ae67-11e7-912c-54650c0f9c19', '0000', 'delete_item', 'trxpesanans', '08ace22d-a63b-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('f269ce93-ae67-11e7-912c-54650c0f9c19', '2017-10-11 16:38:33', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'f26571c4-ae67-11e7-912c-54650c0f9c19', '0000', 'view', 'trxpesanans', '08ace22d-a63b-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('f9b0df69-ae67-11e7-912c-54650c0f9c19', '2017-10-11 16:38:45', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'f99bf6aa-ae67-11e7-912c-54650c0f9c19', '0000', 'delete_item', 'trxpesanans', '08ace22d-a63b-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('32850c63-ae6a-11e7-912c-54650c0f9c19', '2017-10-11 16:54:39', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '327f21e2-ae6a-11e7-912c-54650c0f9c19', '0000', 'view_trxpesanan_detail', 'trxpesanans', 'F8EE837D-D333-47B3-A826-8D9143276F3B');
INSERT INTO `audit_trails` VALUES ('317e2f12-ae6b-11e7-912c-54650c0f9c19', '2017-10-11 17:01:47', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '3178fe2d-ae6b-11e7-912c-54650c0f9c19', '0000', 'view_trxpesanan_detail', 'trxpesanans', 'F8EE837D-D333-47B3-A826-8D9143276F3B');
INSERT INTO `audit_trails` VALUES ('f7ab5ab1-aebb-11e7-80e0-54650c0f9c19', '2017-10-12 02:39:59', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'f7936462-aebb-11e7-80e0-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('4b613b25-aebc-11e7-80e0-54650c0f9c19', '2017-10-12 02:42:19', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '4b516ab7-aebc-11e7-80e0-54650c0f9c19', '0000', 'view', 'trxpesanans', 'B266BDEA-016B-4642-9628-6F7C86625064');
INSERT INTO `audit_trails` VALUES ('4b71ad50-aebc-11e7-80e0-54650c0f9c19', '2017-10-12 02:42:20', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '4b6dff24-aebc-11e7-80e0-54650c0f9c19', '0000', 'view', 'trxpesanans', '1ddf36b7-a630-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('9c5ca2cc-aebc-11e7-80e0-54650c0f9c19', '2017-10-12 02:44:35', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '9c52919d-aebc-11e7-80e0-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('c0491c3f-aebc-11e7-80e0-54650c0f9c19', '2017-10-12 02:45:36', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'c026abba-aebc-11e7-80e0-54650c0f9c19', '0000', 'view', 'trxpesanans', '4DC07000-6EF4-4F08-A93E-92956C62F06C');
INSERT INTO `audit_trails` VALUES ('d43be44a-aebc-11e7-80e0-54650c0f9c19', '2017-10-12 02:46:09', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'd425fc18-aebc-11e7-80e0-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('e5197059-aebc-11e7-80e0-54650c0f9c19', '2017-10-12 02:46:37', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'e4ffa746-aebc-11e7-80e0-54650c0f9c19', '0000', 'view', 'trxpesanans', 'D9FAD796-5F33-4C38-BF65-CC5DD8283453');
INSERT INTO `audit_trails` VALUES ('1abd4205-aebd-11e7-80e0-54650c0f9c19', '2017-10-12 02:48:07', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '1aa3a7ac-aebd-11e7-80e0-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('26c5716a-aebd-11e7-80e0-54650c0f9c19', '2017-10-12 02:48:28', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '26beb7cd-aebd-11e7-80e0-54650c0f9c19', '0000', 'view', 'trxpesanans', '60A642FD-1E91-41DD-96AE-3E5A836D60C4');
INSERT INTO `audit_trails` VALUES ('26e3e548-aebd-11e7-80e0-54650c0f9c19', '2017-10-12 02:48:28', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '26d7cb5f-aebd-11e7-80e0-54650c0f9c19', '0000', 'view', 'trxpesanans', '1ddf36b7-a630-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('5ec70c69-aebd-11e7-80e0-54650c0f9c19', '2017-10-12 02:50:02', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '5ebf39cc-aebd-11e7-80e0-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('6cf976f9-aebd-11e7-80e0-54650c0f9c19', '2017-10-12 02:50:25', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '6cdeb874-aebd-11e7-80e0-54650c0f9c19', '0000', 'view', 'trxpesanans', '3456F874-821C-4680-B310-37D61586C7D1');
INSERT INTO `audit_trails` VALUES ('0f85140c-aebe-11e7-80e0-54650c0f9c19', '2017-10-12 02:54:58', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '0f71088b-aebe-11e7-80e0-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('1a80ae1a-aebe-11e7-80e0-54650c0f9c19', '2017-10-12 02:55:16', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '1a75f9a9-aebe-11e7-80e0-54650c0f9c19', '0000', 'view', 'trxpesanans', '0FF79D9C-6F47-49F8-A0D7-4A2F8D87ADF5');
INSERT INTO `audit_trails` VALUES ('1aa6a19b-aebe-11e7-80e0-54650c0f9c19', '2017-10-12 02:55:17', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '1a9715e6-aebe-11e7-80e0-54650c0f9c19', '0000', 'view', 'trxpesanans', '1ddf36b7-a630-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('c74e5aa9-aebf-11e7-80e0-54650c0f9c19', '2017-10-12 03:07:16', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'c7417b8b-aebf-11e7-80e0-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('d308ae9b-aebf-11e7-80e0-54650c0f9c19', '2017-10-12 03:07:36', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'd303857c-aebf-11e7-80e0-54650c0f9c19', '0000', 'view', 'trxpesanans', '1DC1C831-6622-4947-9EB5-FDCD26FABD05');
INSERT INTO `audit_trails` VALUES ('01886b00-aec0-11e7-80e0-54650c0f9c19', '2017-10-12 03:08:54', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '017a2683-aec0-11e7-80e0-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('0e71652f-aec0-11e7-80e0-54650c0f9c19', '2017-10-12 03:09:15', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '0e680e11-aec0-11e7-80e0-54650c0f9c19', '0000', 'view', 'trxpesanans', '20D611A0-D236-4CF6-9F38-5C40E690969B');
INSERT INTO `audit_trails` VALUES ('486e75ea-aec0-11e7-80e0-54650c0f9c19', '2017-10-12 03:10:53', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '48612935-aec0-11e7-80e0-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('57729a92-aec0-11e7-80e0-54650c0f9c19', '2017-10-12 03:11:18', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '5769a17e-aec0-11e7-80e0-54650c0f9c19', '0000', 'view', 'trxpesanans', '519AF5E9-E99F-4826-B617-844AA8684B32');
INSERT INTO `audit_trails` VALUES ('57882da6-aec0-11e7-80e0-54650c0f9c19', '2017-10-12 03:11:18', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '5783108e-aec0-11e7-80e0-54650c0f9c19', '0000', 'view', 'trxpesanans', '1ddf36b7-a630-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('6d9ded35-aec0-11e7-80e0-54650c0f9c19', '2017-10-12 03:11:55', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '6d8e9132-aec0-11e7-80e0-54650c0f9c19', '0000', 'view', 'trxpesanans', '2b6cd002-a48e-11e7-8af8-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('feb94e02-aec3-11e7-80e0-54650c0f9c19', '2017-10-12 03:37:27', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'fea9f3a4-aec3-11e7-80e0-54650c0f9c19', '0000', 'delete_transaction', 'trxpesanans', '519AF5E9-E99F-4826-B617-844AA8684B32');
INSERT INTO `audit_trails` VALUES ('65d8587a-aec4-11e7-80e0-54650c0f9c19', '2017-10-12 03:40:20', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '65c68912-aec4-11e7-80e0-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('7020b0b7-aec4-11e7-80e0-54650c0f9c19', '2017-10-12 03:40:37', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '7005c141-aec4-11e7-80e0-54650c0f9c19', '0000', 'view', 'trxpesanans', '613C93A7-265F-49D6-AAFE-8182562725A1');
INSERT INTO `audit_trails` VALUES ('703f214e-aec4-11e7-80e0-54650c0f9c19', '2017-10-12 03:40:37', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '702fdb4d-aec4-11e7-80e0-54650c0f9c19', '0000', 'view', 'trxpesanans', '1ddf36b7-a630-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('7a73187a-aec4-11e7-80e0-54650c0f9c19', '2017-10-12 03:40:55', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '7a6df24d-aec4-11e7-80e0-54650c0f9c19', '0000', 'delete_transaction', 'trxpesanans', '613C93A7-265F-49D6-AAFE-8182562725A1');
INSERT INTO `audit_trails` VALUES ('9855431a-aec4-11e7-80e0-54650c0f9c19', '2017-10-12 03:41:45', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '984c74cf-aec4-11e7-80e0-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('a436ff9a-aec4-11e7-80e0-54650c0f9c19', '2017-10-12 03:42:05', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'a4200aa7-aec4-11e7-80e0-54650c0f9c19', '0000', 'view', 'trxpesanans', 'F360D70C-E54F-4BBA-A7D6-7AABA906BA37');
INSERT INTO `audit_trails` VALUES ('a4594c2d-aec4-11e7-80e0-54650c0f9c19', '2017-10-12 03:42:05', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'a4463566-aec4-11e7-80e0-54650c0f9c19', '0000', 'view', 'trxpesanans', '1ddf36b7-a630-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('b4e15b4e-aec4-11e7-80e0-54650c0f9c19', '2017-10-12 03:42:33', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'b4da43c5-aec4-11e7-80e0-54650c0f9c19', '0000', 'delete_transaction', 'trxpesanans', 'F360D70C-E54F-4BBA-A7D6-7AABA906BA37');
INSERT INTO `audit_trails` VALUES ('cd0bd7c4-aec4-11e7-80e0-54650c0f9c19', '2017-10-12 03:43:13', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'ccf5ccb5-aec4-11e7-80e0-54650c0f9c19', '0000', 'view', 'trxpesanans', 'D895C1E6-E879-4DA9-9B7D-F7D2C2446C6E');
INSERT INTO `audit_trails` VALUES ('cd290802-aec4-11e7-80e0-54650c0f9c19', '2017-10-12 03:43:13', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'cd19c1cc-aec4-11e7-80e0-54650c0f9c19', '0000', 'view', 'trxpesanans', '08ace22d-a63b-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('d13fa1ae-aec4-11e7-80e0-54650c0f9c19', '2017-10-12 03:43:20', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'd13b6ada-aec4-11e7-80e0-54650c0f9c19', '0000', 'delete_transaction', 'trxpesanans', 'D895C1E6-E879-4DA9-9B7D-F7D2C2446C6E');
INSERT INTO `audit_trails` VALUES ('031edb29-aec5-11e7-80e0-54650c0f9c19', '2017-10-12 03:44:44', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '03003f85-aec5-11e7-80e0-54650c0f9c19', '0000', 'view', 'trxpesanans', '31463CC4-855F-422C-BBD2-CBEC958F0BBF');
INSERT INTO `audit_trails` VALUES ('033d4aeb-aec5-11e7-80e0-54650c0f9c19', '2017-10-12 03:44:44', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '032e03cc-aec5-11e7-80e0-54650c0f9c19', '0000', 'view', 'trxpesanans', '1ddf36b7-a630-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('0e8e273f-aec5-11e7-80e0-54650c0f9c19', '2017-10-12 03:45:03', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '0e87b4ce-aec5-11e7-80e0-54650c0f9c19', '0000', 'view', 'trxpesanans', '2b6cd002-a48e-11e7-8af8-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('3821d026-aeff-11e7-a09e-54650c0f9c19', '2017-10-12 10:41:23', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '380db96b-aeff-11e7-a09e-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('2a691b54-af03-11e7-a09e-54650c0f9c19', '2017-10-12 11:09:38', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '2a5c7853-af03-11e7-a09e-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('3d2f7db5-af03-11e7-a09e-54650c0f9c19', '2017-10-12 11:10:10', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '3d1439de-af03-11e7-a09e-54650c0f9c19', '0000', 'view', 'trxpesanans', 'C902CC8F-86DD-4177-A2D8-B1399F494963');
INSERT INTO `audit_trails` VALUES ('3d479458-af03-11e7-a09e-54650c0f9c19', '2017-10-12 11:10:10', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '3d3fed77-af03-11e7-a09e-54650c0f9c19', '0000', 'view', 'trxpesanans', '1ddf36b7-a630-11e7-bee2-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('4668e5fb-af03-11e7-a09e-54650c0f9c19', '2017-10-12 11:10:25', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '464b8e7c-af03-11e7-a09e-54650c0f9c19', '0000', 'view', 'trxpesanans', '2b6cd002-a48e-11e7-8af8-54650c0f9c19');
INSERT INTO `audit_trails` VALUES ('0b9af856-af1c-11e7-a09e-54650c0f9c19', '2017-10-12 14:07:44', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '0b91fd90-af1c-11e7-a09e-54650c0f9c19', '0000', 'view_trxpesanan_detail', 'trxpesanans', 'C902CC8F-86DD-4177-A2D8-B1399F494963');
INSERT INTO `audit_trails` VALUES ('3a9d2aba-af35-11e7-a09e-54650c0f9c19', '2017-10-12 17:08:01', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '3a909802-af35-11e7-a09e-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('9678e4e6-afb5-11e7-a09e-54650c0f9c19', '2017-10-13 08:26:58', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '966d6ffe-afb5-11e7-a09e-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('feab9a54-afba-11e7-a09e-54650c0f9c19', '2017-10-13 09:05:40', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'fea29e97-afba-11e7-a09e-54650c0f9c19', '0000', 'view', 'trxpembayaranverify', 'C902CC8F-86DD-4177-A2D8-B1399F494963');
INSERT INTO `audit_trails` VALUES ('51e65649-afbc-11e7-a09e-54650c0f9c19', '2017-10-13 09:15:09', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '51dd5aec-afbc-11e7-a09e-54650c0f9c19', '0000', 'view', 'trxpembayaranverify', 'C902CC8F-86DD-4177-A2D8-B1399F494963');
INSERT INTO `audit_trails` VALUES ('c1a112ff-afbc-11e7-a09e-54650c0f9c19', '2017-10-13 09:18:17', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'c197e6cd-afbc-11e7-a09e-54650c0f9c19', '0000', 'reject', 'trxpembayaranverify', 'C902CC8F-86DD-4177-A2D8-B1399F494963');
INSERT INTO `audit_trails` VALUES ('578a7d91-afce-11e7-a09e-54650c0f9c19', '2017-10-13 11:24:10', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '57844b3f-afce-11e7-a09e-54650c0f9c19', '0000', 'view', 'trxpembayaranverify', 'C902CC8F-86DD-4177-A2D8-B1399F494963');
INSERT INTO `audit_trails` VALUES ('9c560129-afe2-11e7-a09e-54650c0f9c19', '2017-10-13 13:49:15', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '9c3dd0db-afe2-11e7-a09e-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('f1c3cbb5-aff2-11e7-a09e-54650c0f9c19', '2017-10-13 15:46:11', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'f1b703cf-aff2-11e7-a09e-54650c0f9c19', '0000', 'view', 'trxpesananverify', 'C902CC8F-86DD-4177-A2D8-B1399F494963');
INSERT INTO `audit_trails` VALUES ('f1e618c5-aff2-11e7-a09e-54650c0f9c19', '2017-10-13 15:46:11', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'f1daa401-aff2-11e7-a09e-54650c0f9c19', '0000', 'view', 'trxpesananverify', 'C902CC8F-86DD-4177-A2D8-B1399F494963');
INSERT INTO `audit_trails` VALUES ('7f9d957a-aff3-11e7-a09e-54650c0f9c19', '2017-10-13 15:50:09', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '7f97278c-aff3-11e7-a09e-54650c0f9c19', '0000', 'view', 'trxpesananverify', 'C902CC8F-86DD-4177-A2D8-B1399F494963');
INSERT INTO `audit_trails` VALUES ('7fbe7b0c-aff3-11e7-a09e-54650c0f9c19', '2017-10-13 15:50:09', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '7fb088d3-aff3-11e7-a09e-54650c0f9c19', '0000', 'view', 'trxpesananverify', 'C902CC8F-86DD-4177-A2D8-B1399F494963');
INSERT INTO `audit_trails` VALUES ('ba700fab-aff3-11e7-a09e-54650c0f9c19', '2017-10-13 15:51:47', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'ba647723-aff3-11e7-a09e-54650c0f9c19', '0000', 'view', 'trxpesananverify', 'C902CC8F-86DD-4177-A2D8-B1399F494963');
INSERT INTO `audit_trails` VALUES ('baa7e4e2-aff3-11e7-a09e-54650c0f9c19', '2017-10-13 15:51:48', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 'ba989ba5-aff3-11e7-a09e-54650c0f9c19', '0000', 'view', 'trxpesananverify', 'C902CC8F-86DD-4177-A2D8-B1399F494963');
INSERT INTO `audit_trails` VALUES ('453b11c2-aff5-11e7-a09e-54650c0f9c19', '2017-10-13 16:02:50', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', '45310823-aff5-11e7-a09e-54650c0f9c19', '0000', 'login', 'main', 'login');

-- ----------------------------
-- Table structure for bank
-- ----------------------------
DROP TABLE IF EXISTS `bank`;
CREATE TABLE `bank` (
  `bank_id` int(11) NOT NULL,
  `bank_nama` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`bank_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bank
-- ----------------------------
INSERT INTO `bank` VALUES ('1', 'BCA');

-- ----------------------------
-- Table structure for configurationgroup
-- ----------------------------
DROP TABLE IF EXISTS `configurationgroup`;
CREATE TABLE `configurationgroup` (
  `configurationgroup_id` varchar(36) NOT NULL,
  `configurationgroup_name` varchar(100) NOT NULL,
  `configurationgroup_status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`configurationgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of configurationgroup
-- ----------------------------
INSERT INTO `configurationgroup` VALUES ('11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6E', 'parameter system', '1');
INSERT INTO `configurationgroup` VALUES ('11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'Jenis Identitas', '1');
INSERT INTO `configurationgroup` VALUES ('11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6G', 'Status Anggota', '1');
INSERT INTO `configurationgroup` VALUES ('11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6H', 'Status trxsaldo', '1');
INSERT INTO `configurationgroup` VALUES ('11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6I', 'Metode Pembayaran', '1');
INSERT INTO `configurationgroup` VALUES ('11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6J', 'Pembayaran status', '1');
INSERT INTO `configurationgroup` VALUES ('11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6K', 'Pesanan Status', '1');

-- ----------------------------
-- Table structure for configurations
-- ----------------------------
DROP TABLE IF EXISTS `configurations`;
CREATE TABLE `configurations` (
  `configuration_id` varchar(36) NOT NULL,
  `configuration_key` varchar(80) DEFAULT NULL,
  `configuration_value` longtext,
  `configuration_status` tinyint(4) DEFAULT NULL,
  `configurationgroup_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`configuration_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of configurations
-- ----------------------------
INSERT INTO `configurations` VALUES ('12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '1', 'KTP', null, '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F');
INSERT INTO `configurations` VALUES ('13EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '2', 'SIM', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F');
INSERT INTO `configurations` VALUES ('14EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'statusanggota.waiting', 'waiting', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6G');
INSERT INTO `configurations` VALUES ('15EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'statusanggota.active', 'active', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6G');
INSERT INTO `configurations` VALUES ('16EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'statusanggota.inactive', 'inactive', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6G');
INSERT INTO `configurations` VALUES ('17EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'statusanggota.rejected', 'rejected', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6G');
INSERT INTO `configurations` VALUES ('18EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '0', 'waiting', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6H');
INSERT INTO `configurations` VALUES ('19EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '1', 'approve', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6H');
INSERT INTO `configurations` VALUES ('20EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '2', 'reject', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6H');
INSERT INTO `configurations` VALUES ('21EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '1', 'Transfer', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6I');
INSERT INTO `configurations` VALUES ('22EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '2', 'Tunai', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6I');
INSERT INTO `configurations` VALUES ('23EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '0', 'waiting', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6J');
INSERT INTO `configurations` VALUES ('24EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '1', 'Approve', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6J');
INSERT INTO `configurations` VALUES ('25EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '2', 'Reject', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6J');
INSERT INTO `configurations` VALUES ('26EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'MIN_POINT_ORDER', '10', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6E');
INSERT INTO `configurations` VALUES ('27EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '0', 'waiting', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6K');
INSERT INTO `configurations` VALUES ('28EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '1', 'approve', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6K');
INSERT INTO `configurations` VALUES ('29EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '2', 'reject', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6K');
INSERT INTO `configurations` VALUES ('30EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '3', 'packing', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6K');
INSERT INTO `configurations` VALUES ('31EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '4', 'ready_to_send', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6K');
INSERT INTO `configurations` VALUES ('CF371C9618AB459299790F9A4A120F49', 'copyright', '2016. All rights reserved. andrimuhammad', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6E');
INSERT INTO `configurations` VALUES ('EB43051CAFAD49BDBACDC922F47D904C', 'created_by', 'templatelab', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6E');

-- ----------------------------
-- Table structure for dictionaries
-- ----------------------------
DROP TABLE IF EXISTS `dictionaries`;
CREATE TABLE `dictionaries` (
  `dictionary_id` int(11) NOT NULL,
  `dictionary_lang_id` char(4) NOT NULL,
  `dictionary_key` varchar(255) NOT NULL,
  `dictionary_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`dictionary_id`),
  KEY `dictionary_lang_id` (`dictionary_lang_id`),
  CONSTRAINT `dictionaries_ibfk_1` FOREIGN KEY (`dictionary_lang_id`) REFERENCES `languages` (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dictionaries
-- ----------------------------
INSERT INTO `dictionaries` VALUES ('0', 'id', 'base.page_title.products', 'Produk');
INSERT INTO `dictionaries` VALUES ('1', 'en', 'base.page_title.settings', 'Setting');
INSERT INTO `dictionaries` VALUES ('2', 'id', 'base.page_title.settings', 'Pengaturan');
INSERT INTO `dictionaries` VALUES ('3', 'en', 'base.page_title.menus', 'Menus');
INSERT INTO `dictionaries` VALUES ('4', 'id', 'base.page_title.menus', 'Menu');
INSERT INTO `dictionaries` VALUES ('5', 'en', 'base.page_title.users', 'Users');
INSERT INTO `dictionaries` VALUES ('6', 'id', 'base.page_title.users', 'User');
INSERT INTO `dictionaries` VALUES ('7', 'en', 'base.page_title.roles', 'Roles');
INSERT INTO `dictionaries` VALUES ('8', 'id', 'base.page_title.roles', 'Hak Akses');
INSERT INTO `dictionaries` VALUES ('9', 'en', 'base.page_title.languages', 'Languages');
INSERT INTO `dictionaries` VALUES ('10', 'id', 'base.page_title.languages', 'Bahasa');
INSERT INTO `dictionaries` VALUES ('11', 'en', 'base.page_title.dictionaries', 'Dictionaries');
INSERT INTO `dictionaries` VALUES ('12', 'id', 'base.page_title.dictionaries', 'Kamus Kata');
INSERT INTO `dictionaries` VALUES ('13', 'en', 'base.page_title.system_parameters', 'System Parameters');
INSERT INTO `dictionaries` VALUES ('14', 'id', 'base.page_title.system_parameters', 'Sistem Parameter');
INSERT INTO `dictionaries` VALUES ('15', 'en', 'base.page_title.group_parameters', 'Group Parameters');
INSERT INTO `dictionaries` VALUES ('16', 'id', 'base.page_title.group_parameters', 'Grup Parameter');
INSERT INTO `dictionaries` VALUES ('17', 'en', 'base.page_title.parameters', 'Parameters');
INSERT INTO `dictionaries` VALUES ('18', 'id', 'base.page_title.parameters', 'Parameter');
INSERT INTO `dictionaries` VALUES ('19', 'en', 'base.page_title.uids', 'UID');
INSERT INTO `dictionaries` VALUES ('20', 'id', 'base.page_title.uids', 'UID');
INSERT INTO `dictionaries` VALUES ('21', 'en', 'base.page_title.offices', 'Offices');
INSERT INTO `dictionaries` VALUES ('22', 'id', 'base.page_title.offices', 'Kantor');
INSERT INTO `dictionaries` VALUES ('23', 'en', 'base.page_title.parent_offices', 'Parent Offices');
INSERT INTO `dictionaries` VALUES ('24', 'id', 'base.page_title.parent_offices', 'Kantor Induk');
INSERT INTO `dictionaries` VALUES ('25', 'en', 'base.page_title.branch_offices', 'Branch Offices');
INSERT INTO `dictionaries` VALUES ('26', 'id', 'base.page_title.branch_offices', 'Kantor Cabang');
INSERT INTO `dictionaries` VALUES ('27', 'en', 'base.page_title.mapping', 'Mapping');
INSERT INTO `dictionaries` VALUES ('28', 'id', 'base.page_title.mapping', 'Mapping');
INSERT INTO `dictionaries` VALUES ('29', 'en', 'base.page_title.applications', 'Applications');
INSERT INTO `dictionaries` VALUES ('30', 'id', 'base.page_title.applications', 'Aplikasi');
INSERT INTO `dictionaries` VALUES ('31', 'en', 'base.page_title.functions', 'Functions');
INSERT INTO `dictionaries` VALUES ('32', 'id', 'base.page_title.functions', 'Fungsi');
INSERT INTO `dictionaries` VALUES ('33', 'en', 'base.page_title.mapping_access', 'UID Access');
INSERT INTO `dictionaries` VALUES ('34', 'id', 'base.page_title.mapping_access', 'Akses UID');
INSERT INTO `dictionaries` VALUES ('35', 'en', 'base.page_title.logs', 'Logs');
INSERT INTO `dictionaries` VALUES ('36', 'id', 'base.page_title.logs', 'Log');
INSERT INTO `dictionaries` VALUES ('39', 'en', 'base.page_title.servers', 'Servers');
INSERT INTO `dictionaries` VALUES ('40', 'id', 'base.page_title.servers', 'Server');
INSERT INTO `dictionaries` VALUES ('41', 'en', 'base.page_title.simulators', 'Simulator');
INSERT INTO `dictionaries` VALUES ('42', 'id', 'base.page_title.simulators', 'Simulator');
INSERT INTO `dictionaries` VALUES ('43', 'en', 'base.page_title.pc_address', 'PC IP Address');
INSERT INTO `dictionaries` VALUES ('44', 'id', 'base.page_title.pc_address', 'IP Address PC');
INSERT INTO `dictionaries` VALUES ('45', 'en', 'base.page_title.login_simulation', 'Login Simulation');
INSERT INTO `dictionaries` VALUES ('46', 'id', 'base.page_title.login_simulation', 'Simulasi Login');
INSERT INTO `dictionaries` VALUES ('47', 'en', 'menus.menu_name', 'Menu Name');
INSERT INTO `dictionaries` VALUES ('48', 'id', 'menus.menu_name', 'Nama Menu');
INSERT INTO `dictionaries` VALUES ('49', 'en', 'menus.menu_url', 'URL');
INSERT INTO `dictionaries` VALUES ('50', 'id', 'menus.menu_url', 'URL');
INSERT INTO `dictionaries` VALUES ('51', 'en', 'menus.menu_icon', 'Icon');
INSERT INTO `dictionaries` VALUES ('52', 'id', 'menus.menu_icon', 'Icon');
INSERT INTO `dictionaries` VALUES ('53', 'en', 'menus.menu_parent', 'Parent Menu');
INSERT INTO `dictionaries` VALUES ('54', 'id', 'menus.menu_parent', 'Menu Parent');
INSERT INTO `dictionaries` VALUES ('55', 'en', 'menus.menu_order', 'Order');
INSERT INTO `dictionaries` VALUES ('56', 'id', 'menus.menu_order', 'Urutan');
INSERT INTO `dictionaries` VALUES ('57', 'en', 'menus.menu_active', 'Status');
INSERT INTO `dictionaries` VALUES ('58', 'id', 'menus.menu_active', 'Menu');
INSERT INTO `dictionaries` VALUES ('59', 'en', 'menus.menu_create_module', 'Create Module File');
INSERT INTO `dictionaries` VALUES ('60', 'id', 'menus.menu_create_module', 'Buat File Module');
INSERT INTO `dictionaries` VALUES ('61', 'en', 'roles.role_name', 'Role Name');
INSERT INTO `dictionaries` VALUES ('62', 'id', 'roles.role_name', 'Nama Hak Akses');
INSERT INTO `dictionaries` VALUES ('63', 'en', 'roles.role_menu', 'Menus');
INSERT INTO `dictionaries` VALUES ('64', 'id', 'roles.role_menu', 'Menu');
INSERT INTO `dictionaries` VALUES ('65', 'en', 'roles.role_level', 'Level');
INSERT INTO `dictionaries` VALUES ('66', 'id', 'roles.role_level', 'Level');
INSERT INTO `dictionaries` VALUES ('67', 'en', 'roles.role_active', 'Status');
INSERT INTO `dictionaries` VALUES ('68', 'id', 'roles.role_active', 'Status');
INSERT INTO `dictionaries` VALUES ('69', 'en', 'roles.menu_id', 'Menu');
INSERT INTO `dictionaries` VALUES ('70', 'id', 'roles.menu_id', 'Menu');
INSERT INTO `dictionaries` VALUES ('71', 'en', 'users.identity', 'Identity');
INSERT INTO `dictionaries` VALUES ('72', 'id', 'users.identity', 'Identitas');
INSERT INTO `dictionaries` VALUES ('73', 'en', 'users.role_name', 'Role');
INSERT INTO `dictionaries` VALUES ('74', 'id', 'users.role_name', 'Hak Akses');
INSERT INTO `dictionaries` VALUES ('75', 'en', 'languages.language_id', 'Language');
INSERT INTO `dictionaries` VALUES ('76', 'id', 'languages.language_id', 'Bahasa');
INSERT INTO `dictionaries` VALUES ('77', 'en', 'languages.language_name', 'Language Name');
INSERT INTO `dictionaries` VALUES ('78', 'id', 'languages.language_name', 'Nama Bahasa');
INSERT INTO `dictionaries` VALUES ('79', 'en', 'languages.language_default', 'Default Language');
INSERT INTO `dictionaries` VALUES ('80', 'id', 'languages.language_default', 'Bahasa Utama');
INSERT INTO `dictionaries` VALUES ('81', 'en', 'languages.language_active', 'Status');
INSERT INTO `dictionaries` VALUES ('82', 'id', 'languages.language_active', 'Status');
INSERT INTO `dictionaries` VALUES ('83', 'en', 'dictionaries.dictionary_key', 'Dictionary Key');
INSERT INTO `dictionaries` VALUES ('84', 'id', 'dictionaries.dictionary_key', 'Kunci Kamus Kata');
INSERT INTO `dictionaries` VALUES ('85', 'en', 'dictionaries.dictionary_value', 'Dictionary Value');
INSERT INTO `dictionaries` VALUES ('86', 'id', 'dictionaries.dictionary_value', 'Isi Kamus Kata');
INSERT INTO `dictionaries` VALUES ('87', 'en', 'dictionaries.kamus', 'Dictionaries');
INSERT INTO `dictionaries` VALUES ('88', 'id', 'dictionaries.kamus', 'Kamus');
INSERT INTO `dictionaries` VALUES ('89', 'en', 'group_parameters.group_parameter_name', 'Group Parameter Name');
INSERT INTO `dictionaries` VALUES ('90', 'id', 'group_parameters.group_parameter_name', 'Nama Parameter Grup');
INSERT INTO `dictionaries` VALUES ('91', 'en', 'group_parameters.group_parameter_status', 'Status');
INSERT INTO `dictionaries` VALUES ('92', 'id', 'group_parameters.group_parameter_status', 'Status');
INSERT INTO `dictionaries` VALUES ('93', 'en', 'parameters.parameter_group_id', 'Group Parameter');
INSERT INTO `dictionaries` VALUES ('94', 'id', 'parameters.parameter_group_id', 'Parameter Grup');
INSERT INTO `dictionaries` VALUES ('95', 'en', 'parameters.parameter_name', 'Parameter');
INSERT INTO `dictionaries` VALUES ('96', 'id', 'parameters.parameter_name', 'Parameter');
INSERT INTO `dictionaries` VALUES ('97', 'en', 'parameters.parameter_status', 'Status');
INSERT INTO `dictionaries` VALUES ('98', 'id', 'parameters.parameter_status', 'Status');
INSERT INTO `dictionaries` VALUES ('99', 'en', 'uids.uid', 'UID');
INSERT INTO `dictionaries` VALUES ('100', 'id', 'uids.uid', 'UID');
INSERT INTO `dictionaries` VALUES ('101', 'en', 'uids.uid_name', 'Name');
INSERT INTO `dictionaries` VALUES ('102', 'id', 'uids.uid_name', 'Nama');
INSERT INTO `dictionaries` VALUES ('109', 'en', 'uids.uid_status', 'Status');
INSERT INTO `dictionaries` VALUES ('110', 'id', 'uids.uid_status', 'Status');
INSERT INTO `dictionaries` VALUES ('115', 'en', 'uids.uid_position', 'Position');
INSERT INTO `dictionaries` VALUES ('116', 'id', 'uids.uid_position', 'Posisi');
INSERT INTO `dictionaries` VALUES ('117', 'en', 'uids.uid_is_active', 'Active');
INSERT INTO `dictionaries` VALUES ('118', 'id', 'uids.uid_is_active', 'Aktif');
INSERT INTO `dictionaries` VALUES ('119', 'en', 'uids.uid_pass_is_expired', 'Password is Expired');
INSERT INTO `dictionaries` VALUES ('120', 'id', 'uids.uid_pass_is_expired', 'Password Kadaluarsa');
INSERT INTO `dictionaries` VALUES ('121', 'en', 'uids.uid_is_locked', 'Locked');
INSERT INTO `dictionaries` VALUES ('122', 'id', 'uids.uid_is_locked', 'Terkunci');
INSERT INTO `dictionaries` VALUES ('123', 'en', 'parent_offices.office_parent', 'Parent Office');
INSERT INTO `dictionaries` VALUES ('124', 'id', 'parent_offices.office_parent', 'Kantor Induk');
INSERT INTO `dictionaries` VALUES ('125', 'en', 'parent_offices.office_region', 'Office Region');
INSERT INTO `dictionaries` VALUES ('126', 'id', 'parent_offices.office_region', 'Kantor Wilayah');
INSERT INTO `dictionaries` VALUES ('127', 'en', 'parent_offices.office_parent_name', 'Parent Office Name');
INSERT INTO `dictionaries` VALUES ('128', 'id', 'parent_offices.office_parent_name', 'Nama Kantor Induk');
INSERT INTO `dictionaries` VALUES ('129', 'en', 'parent_offices.office_parent_code', 'Parent Office Code');
INSERT INTO `dictionaries` VALUES ('130', 'id', 'parent_offices.office_parent_code', 'Kode Kantor Induk');
INSERT INTO `dictionaries` VALUES ('131', 'en', 'branch_offices.office_branch', 'Branch Office');
INSERT INTO `dictionaries` VALUES ('132', 'id', 'branch_offices.office_branch', 'Kantor Cabang');
INSERT INTO `dictionaries` VALUES ('133', 'en', 'branch_offices.office_branch_parent', 'Parent Office');
INSERT INTO `dictionaries` VALUES ('134', 'id', 'branch_offices.office_branch_parent', 'Kantor Induk');
INSERT INTO `dictionaries` VALUES ('135', 'en', 'branch_offices.office_region', 'Office Region');
INSERT INTO `dictionaries` VALUES ('136', 'id', 'branch_offices.office_region', 'Kantor Wilayah');
INSERT INTO `dictionaries` VALUES ('137', 'en', 'branch_offices.office_parent_code', 'Parent Office Code');
INSERT INTO `dictionaries` VALUES ('138', 'id', 'branch_offices.office_parent_code', 'Kode Kantor Induk');
INSERT INTO `dictionaries` VALUES ('139', 'en', 'branch_offices.office_branch_name', 'Office Branch Name');
INSERT INTO `dictionaries` VALUES ('140', 'id', 'branch_offices.office_branch_name', 'Nama Kantor Cabang');
INSERT INTO `dictionaries` VALUES ('141', 'en', 'branch_offices.office_branch_code', 'Office Branch Code');
INSERT INTO `dictionaries` VALUES ('142', 'id', 'branch_offices.office_branch_code', 'Kode Kantor Cabang');
INSERT INTO `dictionaries` VALUES ('143', 'en', 'applications.application_name', 'Application Name');
INSERT INTO `dictionaries` VALUES ('144', 'id', 'applications.application_name', 'Nama Aplikasi');
INSERT INTO `dictionaries` VALUES ('145', 'en', 'applications.application_code', 'Application Code');
INSERT INTO `dictionaries` VALUES ('146', 'id', 'applications.application_code', 'Kode Aplikasi');
INSERT INTO `dictionaries` VALUES ('147', 'en', 'applications.application_status', 'Status');
INSERT INTO `dictionaries` VALUES ('148', 'id', 'applications.application_status', 'Status');
INSERT INTO `dictionaries` VALUES ('149', 'en', 'functions.function_name', 'Function Name');
INSERT INTO `dictionaries` VALUES ('150', 'id', 'functions.function_name', 'Nama Fungsi');
INSERT INTO `dictionaries` VALUES ('151', 'en', 'functions.function_level', 'Function Level');
INSERT INTO `dictionaries` VALUES ('152', 'id', 'functions.function_level', 'Level Fungsi');
INSERT INTO `dictionaries` VALUES ('153', 'en', 'functions.function_status', 'Status');
INSERT INTO `dictionaries` VALUES ('154', 'id', 'functions.function_status', 'Status');
INSERT INTO `dictionaries` VALUES ('155', 'en', 'functions.application_name', 'Application Name');
INSERT INTO `dictionaries` VALUES ('156', 'id', 'functions.application_name', 'Nama Aplikasi');
INSERT INTO `dictionaries` VALUES ('157', 'en', 'functions.function_application_id', 'Application ID');
INSERT INTO `dictionaries` VALUES ('158', 'id', 'functions.function_application_id', 'ID Aplikasi');
INSERT INTO `dictionaries` VALUES ('159', 'en', 'functions.function_application_detail', 'Function Detail');
INSERT INTO `dictionaries` VALUES ('160', 'id', 'functions.function_application_detail', 'Detil Fungsi');
INSERT INTO `dictionaries` VALUES ('161', 'en', 'mapping_access.access_uid', 'UID');
INSERT INTO `dictionaries` VALUES ('162', 'id', 'mapping_access.access_uid', 'UID');
INSERT INTO `dictionaries` VALUES ('163', 'en', 'mapping_access.access_office', 'Office');
INSERT INTO `dictionaries` VALUES ('164', 'id', 'mapping_access.access_office', 'Kantor');
INSERT INTO `dictionaries` VALUES ('165', 'en', 'mapping_access.access_application', 'Application');
INSERT INTO `dictionaries` VALUES ('166', 'id', 'mapping_access.access_application', 'Aplikasi');
INSERT INTO `dictionaries` VALUES ('167', 'en', 'mapping_access.access_function', 'Function');
INSERT INTO `dictionaries` VALUES ('168', 'id', 'mapping_access.access_function', 'Fungsi');
INSERT INTO `dictionaries` VALUES ('169', 'en', 'simulators.simulator_name', 'Simulator Name');
INSERT INTO `dictionaries` VALUES ('170', 'id', 'simulators.simulator_name', 'Nama Simulator');
INSERT INTO `dictionaries` VALUES ('171', 'en', 'simulators.simulator_ip', 'IP Address');
INSERT INTO `dictionaries` VALUES ('172', 'id', 'simulators.simulator_ip', 'IP Address');
INSERT INTO `dictionaries` VALUES ('173', 'en', 'simulators.simulator_port', 'Port');
INSERT INTO `dictionaries` VALUES ('174', 'id', 'simulators.simulator_port', 'Port');
INSERT INTO `dictionaries` VALUES ('175', 'en', 'simulators.simulator_ssh', 'SSH');
INSERT INTO `dictionaries` VALUES ('176', 'id', 'simulators.simulator_ssh', 'SSH');
INSERT INTO `dictionaries` VALUES ('177', 'en', 'simulators.simulator_running', 'Status');
INSERT INTO `dictionaries` VALUES ('178', 'id', 'simulators.simulator_running', 'Status');
INSERT INTO `dictionaries` VALUES ('179', 'en', 'simulators.simulator_ssh_username', 'SSH Username');
INSERT INTO `dictionaries` VALUES ('180', 'id', 'simulators.simulator_ssh_username', 'SSH Username');
INSERT INTO `dictionaries` VALUES ('181', 'en', 'simulators.simulator_ssh_password', 'SSH Password');
INSERT INTO `dictionaries` VALUES ('182', 'id', 'simulators.simulator_ssh_password', 'SSH Password');
INSERT INTO `dictionaries` VALUES ('183', 'en', 'simulators.simulator_ssh_port', 'SSH Port');
INSERT INTO `dictionaries` VALUES ('184', 'id', 'simulators.simulator_ssh_port', 'SSH Port');
INSERT INTO `dictionaries` VALUES ('185', 'en', 'pc_address.pc_address_ip', 'IP Address');
INSERT INTO `dictionaries` VALUES ('186', 'id', 'pc_address.pc_address_ip', 'IP Address');
INSERT INTO `dictionaries` VALUES ('187', 'en', 'pc_address.pc_address_name', 'PC Name');
INSERT INTO `dictionaries` VALUES ('188', 'id', 'pc_address.pc_address_name', 'Nama PC');
INSERT INTO `dictionaries` VALUES ('189', 'en', 'login_simulation.simulator_service', 'Service');
INSERT INTO `dictionaries` VALUES ('190', 'id', 'login_simulation.simulator_service', 'Service');
INSERT INTO `dictionaries` VALUES ('191', 'en', 'login_simulation.simulator_application', 'Application');
INSERT INTO `dictionaries` VALUES ('192', 'id', 'login_simulation.simulator_application', 'Aplikasi');
INSERT INTO `dictionaries` VALUES ('193', 'en', 'login_simulation.simulator_uid', 'UID');
INSERT INTO `dictionaries` VALUES ('194', 'id', 'login_simulation.simulator_uid', 'UID');
INSERT INTO `dictionaries` VALUES ('195', 'en', 'login_simulation.simulator_pass', 'Password');
INSERT INTO `dictionaries` VALUES ('196', 'id', 'login_simulation.simulator_pass', 'Password');
INSERT INTO `dictionaries` VALUES ('197', 'en', 'logs.log_request_time', 'Request Time');
INSERT INTO `dictionaries` VALUES ('198', 'id', 'logs.log_request_time', 'Waktu Request');
INSERT INTO `dictionaries` VALUES ('199', 'en', 'logs.log_ip_address', 'IP Address');
INSERT INTO `dictionaries` VALUES ('200', 'id', 'logs.log_ip_address', 'IP Address');
INSERT INTO `dictionaries` VALUES ('201', 'en', 'logs.log_uid', 'UID');
INSERT INTO `dictionaries` VALUES ('202', 'id', 'logs.log_uid', 'UID');
INSERT INTO `dictionaries` VALUES ('203', 'en', 'logs.log_application_code', 'Application');
INSERT INTO `dictionaries` VALUES ('204', 'id', 'logs.log_application_code', 'Aplikasi');
INSERT INTO `dictionaries` VALUES ('205', 'en', 'logs.log_request_message', 'Request Message');
INSERT INTO `dictionaries` VALUES ('206', 'id', 'logs.log_request_message', 'Request Message');
INSERT INTO `dictionaries` VALUES ('207', 'en', 'logs.log_response_message', 'Response Message');
INSERT INTO `dictionaries` VALUES ('208', 'id', 'logs.log_response_message', 'Response Message');
INSERT INTO `dictionaries` VALUES ('209', 'en', 'login_simulation.simulator_ip', 'IP Address');
INSERT INTO `dictionaries` VALUES ('210', 'id', 'login_simulation.simulator_ip', 'IP Address');
INSERT INTO `dictionaries` VALUES ('211', 'en', 'login_simulation.simulator_port', 'Port');
INSERT INTO `dictionaries` VALUES ('212', 'id', 'login_simulation.simulator_port', 'Port');
INSERT INTO `dictionaries` VALUES ('213', 'id', 'base.page_title.module_creators', 'Modul Creator');
INSERT INTO `dictionaries` VALUES ('216', 'id', 'base.page_title.module_creator_fields', 'Field Modul');
INSERT INTO `dictionaries` VALUES ('217', 'id', 'base.page_title.module_creator_setting', 'Pengaturan Modul');
INSERT INTO `dictionaries` VALUES ('218', 'id', 'base.page_title.module_creator_parent', 'Module Creator');
INSERT INTO `dictionaries` VALUES ('219', 'id', 'module_creators.module_name', 'Nama Modul');
INSERT INTO `dictionaries` VALUES ('220', 'id', 'module_creators.module_modelname', 'Nama Model');
INSERT INTO `dictionaries` VALUES ('221', 'id', 'module_creators.module_primarytablename', 'Tabel Utama');
INSERT INTO `dictionaries` VALUES ('222', 'id', 'module_creators.module_pkey', 'Primary Key');
INSERT INTO `dictionaries` VALUES ('223', 'id', 'module_creators.module_noticelabel', 'Notice Label');
INSERT INTO `dictionaries` VALUES ('224', 'id', 'module_creators.field_name', 'Nama Field');
INSERT INTO `dictionaries` VALUES ('225', 'id', 'module_creators.field_rules', 'Rules');
INSERT INTO `dictionaries` VALUES ('226', 'id', 'module_creator_fields.field_module_id', 'ID Modul');
INSERT INTO `dictionaries` VALUES ('227', 'id', 'module_creator_fields.field_name', 'Nama Field');
INSERT INTO `dictionaries` VALUES ('228', 'id', 'module_creator_fields.field_rules', 'Rules');
INSERT INTO `dictionaries` VALUES ('229', 'id', 'module_creator_fields.module_name', 'Nama Modul');
INSERT INTO `dictionaries` VALUES ('230', 'id', 'base.page_title.module_creator_master_rules', 'Master Rule');
INSERT INTO `dictionaries` VALUES ('231', 'id', 'base.page_title.module_creators', 'Module Creator');
INSERT INTO `dictionaries` VALUES ('232', 'id', 'module_creators.module_creators_title', 'Module Creator');
INSERT INTO `dictionaries` VALUES ('233', 'id', 'module_creators.module_created', 'Created');
INSERT INTO `dictionaries` VALUES ('234', 'id', 'module_creators.module_createdby', 'Created By');
INSERT INTO `dictionaries` VALUES ('235', 'id', 'module_creator_fields.module_creator_fields_title', 'Module Creator Fields');
INSERT INTO `dictionaries` VALUES ('236', 'id', 'module_creator_fields.module_id', 'ID Modul');
INSERT INTO `dictionaries` VALUES ('237', 'id', 'module_creator_fields.field_is_search', 'Field Pencarian');
INSERT INTO `dictionaries` VALUES ('238', 'id', 'module_creator_fields.field_is_on_grid', 'Ditampilkan di grid');
INSERT INTO `dictionaries` VALUES ('239', 'id', 'module_creator_fields.field_is_on_form', 'Field isian dlm form');
INSERT INTO `dictionaries` VALUES ('240', 'id', 'module_projects.module_projects_title', 'Projek');
INSERT INTO `dictionaries` VALUES ('241', 'id', 'base.page_title.module_projects', 'Daftar Projek');
INSERT INTO `dictionaries` VALUES ('242', 'id', 'module_projects.project_createdby', 'Dibuat oleh');
INSERT INTO `dictionaries` VALUES ('243', 'id', 'module_projects.project_created', 'Tgl. Pembuatan');
INSERT INTO `dictionaries` VALUES ('244', 'id', 'module_projects.project_name', 'Nama Projek');
INSERT INTO `dictionaries` VALUES ('245', 'id', 'module_projects.project_description', 'Deskripsi Projek');
INSERT INTO `dictionaries` VALUES ('246', 'id', 'module_creators.web_server', 'Web Server');
INSERT INTO `dictionaries` VALUES ('247', 'id', 'module_creators.app_server', 'Application Server');
INSERT INTO `dictionaries` VALUES ('248', 'id', 'module_creators.db_server', 'Database Server');
INSERT INTO `dictionaries` VALUES ('249', 'id', 'module_creators.project_list', 'Daftar Project');
INSERT INTO `dictionaries` VALUES ('250', 'id', 'module_creators.about_this_project', 'Tentang Project');
INSERT INTO `dictionaries` VALUES ('251', 'id', 'module_creators.project_description', 'Deskripsi Project');
INSERT INTO `dictionaries` VALUES ('252', 'id', 'base.page_title.menu_header', 'Menu Anda');
INSERT INTO `dictionaries` VALUES ('253', 'id', 'base.page_title.close_module_projects', 'Tutup Project');
INSERT INTO `dictionaries` VALUES ('254', 'id', 'module_projects.project_level', 'Level');
INSERT INTO `dictionaries` VALUES ('255', 'id', 'module_projects.project_status', 'Status');
INSERT INTO `dictionaries` VALUES ('256', 'id', 'module_projects.project_url', 'URL');
INSERT INTO `dictionaries` VALUES ('257', 'id', 'module_projects.project_user', 'User');
INSERT INTO `dictionaries` VALUES ('258', 'id', 'module_projects.project_appip', 'IP App Server');
INSERT INTO `dictionaries` VALUES ('259', 'id', 'module_projects.project_apptype', 'Type App Server');
INSERT INTO `dictionaries` VALUES ('260', 'id', 'module_projects.project_appname', 'Nama App Server');
INSERT INTO `dictionaries` VALUES ('261', 'id', 'module_projects.project_webip', 'IP Web Server');
INSERT INTO `dictionaries` VALUES ('262', 'id', 'module_projects.project_webtype', 'Type Web Server');
INSERT INTO `dictionaries` VALUES ('263', 'id', 'module_projects.project_webname', 'Nama Web Server');
INSERT INTO `dictionaries` VALUES ('264', 'id', 'module_projects.project_dbip', 'IP Database');
INSERT INTO `dictionaries` VALUES ('265', 'id', 'module_projects.project_dbtype', 'Type Database');
INSERT INTO `dictionaries` VALUES ('266', 'id', 'module_projects.project_dbname', 'Nama Database');
INSERT INTO `dictionaries` VALUES ('267', 'id', 'base.page_title.hoa_catalogues', 'HOA Catalogue');
INSERT INTO `dictionaries` VALUES ('268', 'id', 'base.page_title.meeting_rooms', 'Rang Rapat');
INSERT INTO `dictionaries` VALUES ('269', 'id', 'base.page_title.module_creator_properties', 'Properties');
INSERT INTO `dictionaries` VALUES ('270', 'id', 'module_creators.module_defaultsortfield', 'Urut berdasarkan');
INSERT INTO `dictionaries` VALUES ('271', 'id', 'module_creators.module_defaultsortmethod', 'Type Urutan');
INSERT INTO `dictionaries` VALUES ('272', 'id', 'module_creator_fields.field_datatype', 'Type Data');
INSERT INTO `dictionaries` VALUES ('273', 'id', 'module_creator_fields.field_length', 'Panjang karakter');
INSERT INTO `dictionaries` VALUES ('274', 'id', 'base.page_title.data_guru', 'Data Pegawai');
INSERT INTO `dictionaries` VALUES ('275', 'id', 'base.page_title.jabatan', 'Jabatan');
INSERT INTO `dictionaries` VALUES ('276', 'id', 'base.page_title.daftarhadir', 'Daftar Hadir');
INSERT INTO `dictionaries` VALUES ('277', 'id', 'base.page_title.mutasi', 'Mutasi');
INSERT INTO `dictionaries` VALUES ('278', 'id', 'base.page_title.payroll', 'Penggajian');
INSERT INTO `dictionaries` VALUES ('279', 'id', 'base.page_title.promosi', 'Promosi');
INSERT INTO `dictionaries` VALUES ('280', 'id', 'base.page_title.parameter_system', 'Parameter System');
INSERT INTO `dictionaries` VALUES ('281', 'id', 'base.page_title.master_data', 'Data Master');
INSERT INTO `dictionaries` VALUES ('282', 'id', 'base.page_title.g_komp_pks', 'Grup Komponen Penilaian');
INSERT INTO `dictionaries` VALUES ('283', 'id', 'base.page_title.komponen_penilaians', 'Komponen Penilaian');
INSERT INTO `dictionaries` VALUES ('284', 'id', 'base.page_title.penilaian', 'Penilaian Kinerja');
INSERT INTO `dictionaries` VALUES ('285', 'id', 'base.page_title.penilaiankinerjas', 'Penilaian Kinerja');
INSERT INTO `dictionaries` VALUES ('286', 'id', 'base.page_title.pk_details', 'Detail Penilaian Kerja');
INSERT INTO `dictionaries` VALUES ('287', 'id', 'base.page_title.anggota', 'Anggota');
INSERT INTO `dictionaries` VALUES ('288', 'id', 'base.page_title.keanggotaans', 'Keanggotaan');
INSERT INTO `dictionaries` VALUES ('289', 'id', 'base.page_title.anggotas', 'Anggota');
INSERT INTO `dictionaries` VALUES ('290', 'id', 'base.page_title.reqanggotas', 'Request Anggota');
INSERT INTO `dictionaries` VALUES ('291', 'id', 'base.page_title.saldos', 'Saldo');
INSERT INTO `dictionaries` VALUES ('292', 'id', 'base.page_title.reqsaldos', 'Request Saldo');
INSERT INTO `dictionaries` VALUES ('293', 'id', 'base.page_title.trxpesanans', 'Pemesanan');
INSERT INTO `dictionaries` VALUES ('294', 'id', 'base.page_title.trxpembayaranverify', 'Verif. Pembayaran');
INSERT INTO `dictionaries` VALUES ('295', 'id', 'base.page_title.trxpesananverify', 'Verif. Pesanan');
INSERT INTO `dictionaries` VALUES ('296', 'id', 'base.page_title.trxpengirimans', 'Pengiriman');
INSERT INTO `dictionaries` VALUES ('297', 'id', 'users.user_identity', 'User ID');
INSERT INTO `dictionaries` VALUES ('298', 'id', 'users.user_name', 'Nama User');
INSERT INTO `dictionaries` VALUES ('299', 'id', 'users.user_registerdate', 'Tgl Daftar');
INSERT INTO `dictionaries` VALUES ('300', 'id', 'users.user_status', 'Status');
INSERT INTO `dictionaries` VALUES ('301', 'id', 'users.user_password', 'Password');
INSERT INTO `dictionaries` VALUES ('302', 'id', 'users.user_password_confirm', 'Confirm Password');
INSERT INTO `dictionaries` VALUES ('303', 'id', 'roles.role_active_label', 'Status');
INSERT INTO `dictionaries` VALUES ('304', 'id', 'menus.menu_active_label', 'Status');
INSERT INTO `dictionaries` VALUES ('305', 'id', 'products.produk_identity', 'ID Produk');
INSERT INTO `dictionaries` VALUES ('306', 'id', 'products.produk_nama', 'Nama Produk');
INSERT INTO `dictionaries` VALUES ('307', 'id', 'products.produk_harga', 'Harga');
INSERT INTO `dictionaries` VALUES ('308', 'id', 'products.produk_stok', 'Stok');
INSERT INTO `dictionaries` VALUES ('309', 'id', 'products.produk_status_label', 'Status');
INSERT INTO `dictionaries` VALUES ('310', 'id', 'products.produk_satuan', 'Satuan');
INSERT INTO `dictionaries` VALUES ('311', 'id', 'products.produk_photo', 'Photo');
INSERT INTO `dictionaries` VALUES ('312', 'id', 'products.produk_description', 'Deskripsi');
INSERT INTO `dictionaries` VALUES ('313', 'id', 'products.produk_status', 'Status');
INSERT INTO `dictionaries` VALUES ('314', 'id', 'anggotas.anggota_nama', 'Nama');
INSERT INTO `dictionaries` VALUES ('315', 'id', 'anggotas.user_identity', 'User ID');
INSERT INTO `dictionaries` VALUES ('316', 'id', 'anggotas.anggota_jnskelamin_label', 'Jns. Kelamin');
INSERT INTO `dictionaries` VALUES ('317', 'id', 'anggotas.anggota_email', 'Email');
INSERT INTO `dictionaries` VALUES ('318', 'id', 'anggotas.anggota_noidentitas', 'No. Identitas');
INSERT INTO `dictionaries` VALUES ('319', 'id', 'anggotas.user_status_label', 'Status');
INSERT INTO `dictionaries` VALUES ('320', 'id', 'anggotas.anggota_tmplahir', 'Tmp. Lahir');
INSERT INTO `dictionaries` VALUES ('321', 'id', 'anggotas.anggota_tgllahir', 'Tgl. Lahir');
INSERT INTO `dictionaries` VALUES ('322', 'id', 'anggotas.anggota_alamat', 'Alamat');
INSERT INTO `dictionaries` VALUES ('323', 'id', 'anggotas.anggota_jnskelamin', 'Jns. Kelamin');
INSERT INTO `dictionaries` VALUES ('324', 'id', 'anggotas.anggota_notlp', 'No. Tlp');
INSERT INTO `dictionaries` VALUES ('325', 'id', 'anggotas.anggota_jnsidentitas', 'Jns. Identitas');
INSERT INTO `dictionaries` VALUES ('326', 'id', 'anggotas.anggota_scanidentitas', 'Scan Identitas');
INSERT INTO `dictionaries` VALUES ('327', 'id', 'anggotas.anggota_photo', 'Photo');
INSERT INTO `dictionaries` VALUES ('328', 'id', 'reqanggotas.anggota_nama', 'Nama');
INSERT INTO `dictionaries` VALUES ('329', 'id', 'reqanggotas.user_identity', 'User ID');
INSERT INTO `dictionaries` VALUES ('330', 'id', 'reqanggotas.anggota_jnskelamin_label', 'Jns. Kelamin');
INSERT INTO `dictionaries` VALUES ('331', 'id', 'reqanggotas.anggota_email', 'Email');
INSERT INTO `dictionaries` VALUES ('332', 'id', 'reqanggotas.anggota_noidentitas', 'No.Identitas');
INSERT INTO `dictionaries` VALUES ('333', 'id', 'reqanggotas.user_status_label', 'Status');
INSERT INTO `dictionaries` VALUES ('334', 'id', 'reqanggotas.anggota_tmplahir', 'Tmp. Lahir');
INSERT INTO `dictionaries` VALUES ('335', 'id', 'reqanggotas.anggota_tgllahir', 'Tgl. Lahir');
INSERT INTO `dictionaries` VALUES ('336', 'id', 'reqanggotas.anggota_alamat', 'Alamat');
INSERT INTO `dictionaries` VALUES ('337', 'id', 'reqanggotas.anggota_jnskelamin', 'Jns.Kelamin');
INSERT INTO `dictionaries` VALUES ('338', 'id', 'reqanggotas.anggota_notlp', 'No. Tlp');
INSERT INTO `dictionaries` VALUES ('339', 'id', 'reqanggotas.anggota_jnsidentitas', 'Jns. Identitas');
INSERT INTO `dictionaries` VALUES ('340', 'id', 'reqanggotas.anggota_scanidentitas', 'Scan Identitas');
INSERT INTO `dictionaries` VALUES ('341', 'id', 'reqanggotas.anggota_photo', 'Photo');
INSERT INTO `dictionaries` VALUES ('342', 'id', 'reqanggotas.user_password', 'Password');
INSERT INTO `dictionaries` VALUES ('343', 'id', 'reqanggotas.user_password_confirm', 'Confirm Password');
INSERT INTO `dictionaries` VALUES ('344', 'id', 'reqanggotas.anggota_persetujuan', 'Persetujuan Keanggotaan');
INSERT INTO `dictionaries` VALUES ('345', 'id', 'reqanggotas.anggota_area', 'Area');
INSERT INTO `dictionaries` VALUES ('346', 'id', 'reqanggotas.anggota_note', 'Catatan');
INSERT INTO `dictionaries` VALUES ('347', 'id', 'reqanggotas.anggota_areaid', 'Area');
INSERT INTO `dictionaries` VALUES ('348', 'id', 'anggotas.old_password', 'Password lama');
INSERT INTO `dictionaries` VALUES ('349', 'id', 'anggotas.new_password', 'Password baru');
INSERT INTO `dictionaries` VALUES ('350', 'id', 'anggotas.confirm_new_password', 'Confirm password baru');
INSERT INTO `dictionaries` VALUES ('351', 'id', 'anggotas.anggota_areaid', 'Area');
INSERT INTO `dictionaries` VALUES ('352', 'id', 'reqsaldos.anggota_nama', 'Nama');
INSERT INTO `dictionaries` VALUES ('353', 'id', 'reqsaldos.user_identity', 'User ID');
INSERT INTO `dictionaries` VALUES ('354', 'id', 'reqsaldos.trxsaldo_noref', 'No. Refferensi');
INSERT INTO `dictionaries` VALUES ('355', 'id', 'reqsaldos.trxsaldo_tgl', 'Tanggal');
INSERT INTO `dictionaries` VALUES ('356', 'id', 'reqsaldos.bank_nama', 'Nama Bank');
INSERT INTO `dictionaries` VALUES ('357', 'id', 'reqsaldos.trxsaldo_nominal', 'Nominal');
INSERT INTO `dictionaries` VALUES ('358', 'id', 'reqsaldos.trxsaldo_status_label', 'Status');
INSERT INTO `dictionaries` VALUES ('359', 'id', 'reqsaldos.trxsaldo_kdbank', 'Bank');
INSERT INTO `dictionaries` VALUES ('360', 'id', 'reqsaldos.trxsaldo_methode', 'Metode');
INSERT INTO `dictionaries` VALUES ('361', 'id', 'reqsaldos.trxsaldo_user_id', 'Anggota');
INSERT INTO `dictionaries` VALUES ('362', 'id', 'reqsaldos.trxsaldo_methode_label', 'Metode');
INSERT INTO `dictionaries` VALUES ('363', 'id', 'reqsaldos.trxsaldo_note', 'Catatan');
INSERT INTO `dictionaries` VALUES ('364', 'id', 'saldos.anggota_nama', 'Nama');
INSERT INTO `dictionaries` VALUES ('365', 'id', 'saldos.user_identity', 'User ID');
INSERT INTO `dictionaries` VALUES ('366', 'id', 'saldos.saldo_lastupdate', 'Update terakhir');
INSERT INTO `dictionaries` VALUES ('367', 'id', 'saldos.saldo_saldo', 'Saldo awal');
INSERT INTO `dictionaries` VALUES ('368', 'id', 'saldos.total_trxsaldo', 'Tambah saldo berjalan');
INSERT INTO `dictionaries` VALUES ('369', 'id', 'saldos.total_trxpemesanan', 'Pemesanan berjalan');
INSERT INTO `dictionaries` VALUES ('370', 'id', 'trxpesanans.trxpemesanans_invoiceid', 'ID Pesanan');
INSERT INTO `dictionaries` VALUES ('371', 'id', 'trxpesanans.trxpemesanans_itemcount', 'Jml Produk');
INSERT INTO `dictionaries` VALUES ('372', 'id', 'trxpesanans.produk_identity', 'Produk ID');
INSERT INTO `dictionaries` VALUES ('373', 'id', 'trxpesanans.produk_nama', 'Nama Produk');
INSERT INTO `dictionaries` VALUES ('374', 'id', 'trxpesanans.produk_harga', 'Harga');
INSERT INTO `dictionaries` VALUES ('375', 'id', 'trxpesanans.produk_stok', 'Stok');
INSERT INTO `dictionaries` VALUES ('376', 'id', 'trxpesanans.produk_satuan', 'Satuan');
INSERT INTO `dictionaries` VALUES ('377', 'id', 'trxpesanans.produk_status', 'Status');
INSERT INTO `dictionaries` VALUES ('378', 'id', 'trxpesanans.produk_qty', 'Jml dipesan');
INSERT INTO `dictionaries` VALUES ('379', 'id', 'trxpesanans.produk_pointorder', 'Point Order');
INSERT INTO `dictionaries` VALUES ('380', 'id', 'trxpesanans.trxpemesanans_total', 'Subtotal');
INSERT INTO `dictionaries` VALUES ('381', 'id', 'trxpesanans.trxpemesanans_pointorder', 'PointOrder');
INSERT INTO `dictionaries` VALUES ('382', 'id', 'trxpesanans.trxpembayaran_metode', 'Metode Pembayaran');
INSERT INTO `dictionaries` VALUES ('383', 'id', 'trxpesanans.trxpembayaran_nominal', 'Nominal');
INSERT INTO `dictionaries` VALUES ('384', 'id', 'trxpesanans.trxpembayaran_kdbank', 'Bank');
INSERT INTO `dictionaries` VALUES ('385', 'id', 'trxpesanans.trxpembayaran_noref', 'No. Refferensi');
INSERT INTO `dictionaries` VALUES ('386', 'id', 'trxpesanans.trxpesanan_qty', 'Qty');
INSERT INTO `dictionaries` VALUES ('389', 'id', 'trxpembayaranverify.anggota_nama', 'Nama Anggota');
INSERT INTO `dictionaries` VALUES ('390', 'id', 'trxpembayaranverify.user_identity', 'User ID');
INSERT INTO `dictionaries` VALUES ('391', 'id', 'trxpembayaranverify.trxpembayaran_noref', 'No. Refferensi');
INSERT INTO `dictionaries` VALUES ('392', 'id', 'trxpembayaranverify.trxpembayaran_tgl', 'Tanggal');
INSERT INTO `dictionaries` VALUES ('393', 'id', 'trxpembayaranverify.trxpembayaran_nominal', 'Nominal');
INSERT INTO `dictionaries` VALUES ('394', 'id', 'trxpembayaranverify.trxpembayaran_status_label', 'Status');
INSERT INTO `dictionaries` VALUES ('395', 'id', 'trxpesanans.anggota_nama', 'Nama');
INSERT INTO `dictionaries` VALUES ('396', 'id', 'trxpesanans.anggota_alamat', 'Alamat');
INSERT INTO `dictionaries` VALUES ('397', 'id', 'trxpesanans.anggota_notlp', 'No.Tlp');
INSERT INTO `dictionaries` VALUES ('398', 'id', 'trxpesanans.anggota_email', 'Email');
INSERT INTO `dictionaries` VALUES ('399', 'id', 'trxpesanans.area_nama', 'Area');
INSERT INTO `dictionaries` VALUES ('400', 'id', 'trxpesanans.area_biayakirim', 'Biaya Kirim');
INSERT INTO `dictionaries` VALUES ('401', 'id', 'trxpesanans.trxpemesanans_totalbayar', 'Total Bayar');
INSERT INTO `dictionaries` VALUES ('402', 'id', 'trxpembayaranverify.trxpesanan_invoiceid', 'ID Pesanan');
INSERT INTO `dictionaries` VALUES ('403', 'id', 'trxpembayaranverify.trxpembayaran_methode', 'Metode');
INSERT INTO `dictionaries` VALUES ('404', 'id', 'trxpembayaranverify.bank_nama', 'Nama Bank');
INSERT INTO `dictionaries` VALUES ('405', 'id', 'trxpembayaranverify.anggota_alamat', 'Alamat');
INSERT INTO `dictionaries` VALUES ('406', 'id', 'trxpembayaranverify.anggota_notlp', 'No.Tlp');
INSERT INTO `dictionaries` VALUES ('407', 'id', 'trxpembayaranverify.anggota_email', 'Email');
INSERT INTO `dictionaries` VALUES ('408', 'id', 'trxpembayaranverify.trxpesanan_pointorder', 'Point Order');
INSERT INTO `dictionaries` VALUES ('409', 'id', 'trxpembayaranverify.trxpesanan_biayakirim', 'Biaya Kirim');
INSERT INTO `dictionaries` VALUES ('410', 'id', 'trxpembayaranverify.trxpembayaran_metode', 'Metode');
INSERT INTO `dictionaries` VALUES ('411', 'id', 'trxpembayaranverify.trxpembayaran_status', 'Status');
INSERT INTO `dictionaries` VALUES ('412', 'id', 'trxpembayaranverify.trxpembayaran_kdbank', 'Bank');
INSERT INTO `dictionaries` VALUES ('413', 'id', 'trxpembayaranverify.trxpesanan_subtotal', 'Subtotal');
INSERT INTO `dictionaries` VALUES ('414', 'id', 'trxpembayaranverify.trxpesanan_totalbayar', 'Total');
INSERT INTO `dictionaries` VALUES ('415', 'id', 'trxpembayaranverify.trxpembayaran_note', 'Catatan');
INSERT INTO `dictionaries` VALUES ('416', 'id', 'trxpesananverify.trxpesanan_invoiceid', 'ID Pesanan');
INSERT INTO `dictionaries` VALUES ('417', 'id', 'trxpesananverify.trxpesanan_tgl', 'Tanggal');
INSERT INTO `dictionaries` VALUES ('418', 'id', 'trxpesananverify.user_identity', 'User ID');
INSERT INTO `dictionaries` VALUES ('419', 'id', 'trxpesananverify.anggota_nama', 'Nama Anggota');
INSERT INTO `dictionaries` VALUES ('420', 'id', 'trxpesananverify.trxpesanan_status', 'Status');
INSERT INTO `dictionaries` VALUES ('421', 'id', 'trxpesananverify.trxpesanan_produkid', 'Produk');
INSERT INTO `dictionaries` VALUES ('422', 'id', 'trxpesananverify.trxpesanan_qty', 'Jumlah');
INSERT INTO `dictionaries` VALUES ('423', 'id', 'trxpesananverify.trxpesanan_qty_realisasi', 'Realisasi');
INSERT INTO `dictionaries` VALUES ('424', 'id', 'trxpengirimans.trxpesanan_invoiceid', 'ID Pesanan');
INSERT INTO `dictionaries` VALUES ('425', 'id', 'trxpengirimans.user_identity', 'User ID');
INSERT INTO `dictionaries` VALUES ('426', 'id', 'trxpengirimans.trxpesanan_tgl', 'Tanggal');
INSERT INTO `dictionaries` VALUES ('427', 'id', 'trxpengirimans.user_identity', 'Anggota ID');
INSERT INTO `dictionaries` VALUES ('428', 'id', 'trxpengirimans.anggota_nama', 'Nama Anggota');
INSERT INTO `dictionaries` VALUES ('429', 'id', 'trxpengirimans.trxpesanan_status', 'Status');

-- ----------------------------
-- Table structure for ekspedisi
-- ----------------------------
DROP TABLE IF EXISTS `ekspedisi`;
CREATE TABLE `ekspedisi` (
  `ekspedisi_id` int(11) NOT NULL,
  `ekspedisi_nama` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ekspedisi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ekspedisi
-- ----------------------------
INSERT INTO `ekspedisi` VALUES ('1', 'JNE');

-- ----------------------------
-- Table structure for histories
-- ----------------------------
DROP TABLE IF EXISTS `histories`;
CREATE TABLE `histories` (
  `history_id` varchar(36) NOT NULL,
  `history_detail` text NOT NULL,
  `history_active` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of histories
-- ----------------------------
INSERT INTO `histories` VALUES ('f9d67e2f-ac5e-11e7-902b-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"testinguseridpanjang\",\"user_name\":\"Tantan Suryana\",\"role_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('39783bf2-ac5f-11e7-902b-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"testinguserpanjang2\",\"user_name\":\"Suryana Tantan\",\"role_id\":\"2\"}}]', '1');
INSERT INTO `histories` VALUES ('38930c3c-ac60-11e7-902b-54650c0f9c19', '[{\"before\":{\"user_identity\":\"testinguseridpanjang\",\"user_name\":\"Tantan Suryana\",\"role_id\":\"1\",\"user_id\":\"f9c996cc-ac5e-11e7-902b-54650c0f9c19\"}},{\"after\":{\"user_identity\":\"testinguseridpanjang1\",\"user_name\":\"Tantan Suryana\",\"role_id\":\"1\",\"user_id\":\"f9c996cc-ac5e-11e7-902b-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('7e461723-ac65-11e7-902b-54650c0f9c19', '[{\"before\":{\"user_identity\":\"test\",\"user_name\":\"Tantan Suryana\",\"role_id\":\"1\",\"user_id\":\"f9c996cc-ac5e-11e7-902b-54650c0f9c19\"}},{\"after\":{\"user_identity\":\"testingdenganidpanjang4\",\"user_name\":\"Tantan Suryana\",\"role_id\":\"1\",\"user_id\":\"f9c996cc-ac5e-11e7-902b-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('f9cccd59-ac66-11e7-902b-54650c0f9c19', '[{\"before\":{\"user_id\":\"c0514c8f-abf3-11e7-ac4a-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('1cfb3030-ac67-11e7-902b-54650c0f9c19', '[{\"before\":{\"user_id\":\"cd329b31-abf3-11e7-ac4a-54650c0f9c19\",\"user_status\":\"1\"}},{\"after\":{\"user_id\":\"cd329b31-abf3-11e7-ac4a-54650c0f9c19\",\"user_status\":\"0\"}}]', '1');
INSERT INTO `histories` VALUES ('7e71188c-ac67-11e7-902b-54650c0f9c19', '[{\"before\":{\"per_page\":\"10\",\"action_to\":[\"ede4108c-a7fd-11e7-bbff-54650c0f9c19\",\"aabedccf-abf3-11e7-ac4a-54650c0f9c19\",\"ca2029b8-abf1-11e7-ac4a-54650c0f9c19\"],\"user_id\":\"ede4108c-a7fd-11e7-bbff-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('7e90f88f-ac67-11e7-902b-54650c0f9c19', '[{\"before\":{\"per_page\":\"10\",\"action_to\":[\"ede4108c-a7fd-11e7-bbff-54650c0f9c19\",\"aabedccf-abf3-11e7-ac4a-54650c0f9c19\",\"ca2029b8-abf1-11e7-ac4a-54650c0f9c19\"],\"user_id\":\"aabedccf-abf3-11e7-ac4a-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('7ed6eaf9-ac67-11e7-902b-54650c0f9c19', '[{\"before\":{\"per_page\":\"10\",\"action_to\":[\"ede4108c-a7fd-11e7-bbff-54650c0f9c19\",\"aabedccf-abf3-11e7-ac4a-54650c0f9c19\",\"ca2029b8-abf1-11e7-ac4a-54650c0f9c19\"],\"user_id\":\"ca2029b8-abf1-11e7-ac4a-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('9f3f0b01-ac67-11e7-902b-54650c0f9c19', '[{\"before\":{\"user_id\":\"b489c5a5-abf3-11e7-ac4a-54650c0f9c19\",\"user_status\":\"1\"}},{\"after\":{\"user_id\":\"b489c5a5-abf3-11e7-ac4a-54650c0f9c19\",\"user_status\":\"0\"}}]', '1');
INSERT INTO `histories` VALUES ('89f91195-ac93-11e7-814e-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('5a8e5aca-ac9c-11e7-814e-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"tanz117\",\"user_name\":\"tantan suryana\",\"role_id\":\"1\",\"user_password\":\"Tantan\",\"user_password_confirm\":\"Tantan\"}}]', '1');
INSERT INTO `histories` VALUES ('6c5a9162-ac9c-11e7-814e-54650c0f9c19', '[{\"before\":{\"user_identity\":\"tanz117\",\"user_name\":\"tantan suryana\",\"role_id\":\"1\",\"user_id\":\"5a7c3a86-ac9c-11e7-814e-54650c0f9c19\"}},{\"after\":{\"user_identity\":\"tanz117\",\"user_name\":\"tantan suryana ST\",\"role_id\":\"1\",\"user_id\":\"5a7c3a86-ac9c-11e7-814e-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('81b9740d-ac9e-11e7-814e-54650c0f9c19', '[{\"before\":{\"user_identity\":\"tanz117\",\"user_name\":\"tantan suryana ST\",\"role_id\":\"1\",\"user_id\":\"5a7c3a86-ac9c-11e7-814e-54650c0f9c19\"}},{\"after\":{\"user_password\":\"satu\",\"user_password_confirm\":\"satu\",\"user_id\":\"5a7c3a86-ac9c-11e7-814e-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('9f14dab8-ac9e-11e7-814e-54650c0f9c19', '[{\"before\":{\"user_identity\":\"tanz117\",\"user_name\":\"tantan suryana ST\",\"role_id\":\"1\",\"user_id\":\"5a7c3a86-ac9c-11e7-814e-54650c0f9c19\"}},{\"after\":{\"user_password\":\"12345\",\"user_password_confirm\":\"12345\",\"user_id\":\"5a7c3a86-ac9c-11e7-814e-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('d7eead01-ac9e-11e7-814e-54650c0f9c19', '[{\"before\":{\"user_identity\":\"tanz117\",\"user_name\":\"tantan suryana ST\",\"role_id\":\"1\",\"user_id\":\"5a7c3a86-ac9c-11e7-814e-54650c0f9c19\"}},{\"after\":{\"user_password\":\"12345\",\"user_password_confirm\":\"12345\",\"user_id\":\"5a7c3a86-ac9c-11e7-814e-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('2cd5ce10-ac9f-11e7-814e-54650c0f9c19', '[{\"before\":{\"user_identity\":\"tanz117\",\"user_name\":\"tantan suryana ST\",\"role_id\":\"1\",\"user_id\":\"5a7c3a86-ac9c-11e7-814e-54650c0f9c19\"}},{\"after\":{\"user_password\":\"5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5\",\"user_password_confirm\":\"12345\",\"user_id\":\"5a7c3a86-ac9c-11e7-814e-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('278c0b0c-aca0-11e7-814e-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"tantan2\",\"user_name\":\"Surya\",\"role_id\":\"2\",\"user_password\":\"dbff5341acad5e2a58db4efd5e72e2d9a0a843a28e02b1183c68162d0a3a3de6\",\"user_password_confirm\":\"9876\"}}]', '1');
INSERT INTO `histories` VALUES ('9f6c637a-aca0-11e7-814e-54650c0f9c19', '[{\"before\":{\"user_identity\":\"tantan2\",\"user_name\":\"Surya\",\"role_id\":\"2\",\"user_id\":\"277d788c-aca0-11e7-814e-54650c0f9c19\"}},{\"after\":{\"user_password\":\"b03ddf3ca2e714a6548e7495e2a03f5e824eaac9837cd7f159c67b90fb4b7342\",\"user_password_confirm\":\"P@ssw0rd\",\"user_id\":\"277d788c-aca0-11e7-814e-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('c71848ae-aca5-11e7-814e-54650c0f9c19', '[{\"before\":{\"role_id\":\"2\",\"menu_id\":[]}},{\"after\":{\"menu_id\":[\"15\",\"16\"],\"role_id\":\"2\"}}]', '1');
INSERT INTO `histories` VALUES ('c950d4e7-acb8-11e7-814e-54650c0f9c19', '[{\"before\":{\"menu_id\":\"2\",\"menu_icon\":\"glyphicon glyphicon-user\",\"menu_active\":\"0\"}},{\"after\":{\"menu_icon\":\"glyphicon glyphicon-user\",\"menu_active\":\"0\",\"menu_id\":\"2\"}}]', '1');
INSERT INTO `histories` VALUES ('e1219f43-acb8-11e7-814e-54650c0f9c19', '[{\"before\":{\"menu_id\":\"2\",\"menu_icon\":\"glyphicon glyphicon-usd\",\"menu_active\":\"0\"}},{\"after\":{\"menu_icon\":\"glyphicon glyphicon-usd\",\"menu_active\":\"0\",\"menu_id\":\"2\"}}]', '1');
INSERT INTO `histories` VALUES ('ec3e30c4-acb8-11e7-814e-54650c0f9c19', '[{\"before\":{\"menu_id\":\"2\",\"menu_icon\":\"glyphicon glyphicon-user\",\"menu_active\":\"1\"}},{\"after\":{\"menu_icon\":\"glyphicon glyphicon-user\",\"menu_active\":\"1\",\"menu_id\":\"2\"}}]', '1');
INSERT INTO `histories` VALUES ('b136ba6a-acbf-11e7-814e-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_identity\":\"P003\",\"produk_nama\":\"Polo Shirt\",\"produk_harga\":\"100000\",\"produk_stok\":\"10\",\"produk_satuan\":\"pcs\",\"produk_description\":\"ini adalah fitur sebagai percontohan untuk module produk\",\"produk_status\":\"1\",\"produk_photo\":\"3271_Koala.jpg\"}}]', '1');
INSERT INTO `histories` VALUES ('cc06cd20-acdb-11e7-814e-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('c17f1475-acdd-11e7-814e-54650c0f9c19', '[{\"before\":{\"produk_id\":\"1ddf36b7-a630-11e7-bee2-54650c0f9c19\",\"produk_identity\":\"890\",\"produk_nama\":\"809\",\"produk_harga\":\"809.00\",\"produk_stok\":\"98\",\"produk_satuan\":\"iu\",\"produk_photo\":\"\",\"produk_description\":\"098\",\"produk_status\":\"1\"}},{\"after\":{\"produk_identity\":\"P890\",\"produk_nama\":\"Silver Queen\",\"produk_harga\":\"18000\",\"produk_stok\":\"100\",\"produk_satuan\":\"pcs\",\"produk_description\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.\",\"produk_status\":\"1\",\"produk_id\":\"1ddf36b7-a630-11e7-bee2-54650c0f9c19\",\"produk_photo\":\"25973_Desert.jpg\"}}]', '1');
INSERT INTO `histories` VALUES ('c1dacb6b-acde-11e7-814e-54650c0f9c19', '[{\"before\":{\"produk_id\":\"be469fbc-a63e-11e7-bee2-54650c0f9c19\",\"produk_identity\":\"asdf\",\"produk_nama\":\"ddd\",\"produk_harga\":\"45.00\",\"produk_stok\":\"34\",\"produk_satuan\":\"sdf\",\"produk_photo\":\"40386_ShowImage.jpg\",\"produk_description\":\"sdfasdf\",\"produk_status\":\"1\"}},{\"after\":{\"produk_identity\":\"asdf\",\"produk_nama\":\"ddd\",\"produk_harga\":\"45.00\",\"produk_stok\":\"34\",\"produk_satuan\":\"sdf\",\"produk_description\":\"sdfasdf\",\"produk_status\":\"1\",\"produk_id\":\"be469fbc-a63e-11e7-bee2-54650c0f9c19\",\"produk_photo\":\"91018_Hydrangeas.jpg\"}}]', '1');
INSERT INTO `histories` VALUES ('d0291676-acde-11e7-814e-54650c0f9c19', '[{\"before\":{\"produk_id\":\"be469fbc-a63e-11e7-bee2-54650c0f9c19\",\"produk_identity\":\"asdf\",\"produk_nama\":\"ddd\",\"produk_harga\":\"45.00\",\"produk_stok\":\"34\",\"produk_satuan\":\"sdf\",\"produk_photo\":\"91018_Hydrangeas.jpg\",\"produk_description\":\"sdfasdf\",\"produk_status\":\"1\"}},{\"after\":{\"produk_identity\":\"asdf\",\"produk_nama\":\"ddd\",\"produk_harga\":\"45.00\",\"produk_stok\":\"34\",\"produk_satuan\":\"sdf\",\"produk_photo\":\"91018_Hydrangeas.jpg\",\"produk_description\":\"sdfasdf\",\"produk_status\":\"0\",\"produk_id\":\"be469fbc-a63e-11e7-bee2-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('de8a7901-acde-11e7-814e-54650c0f9c19', '[{\"before\":{\"produk_id\":\"be469fbc-a63e-11e7-bee2-54650c0f9c19\",\"produk_identity\":\"asdf\",\"produk_nama\":\"ddd\",\"produk_harga\":\"45.00\",\"produk_stok\":\"34\",\"produk_satuan\":\"sdf\",\"produk_photo\":\"91018_Hydrangeas.jpg\",\"produk_description\":\"sdfasdf\",\"produk_status\":\"1\"}},{\"after\":{\"produk_identity\":\"asdf\",\"produk_nama\":\"ddd\",\"produk_harga\":\"45.00\",\"produk_stok\":\"34\",\"produk_satuan\":\"sdf\",\"produk_photo\":\"91018_Hydrangeas.jpg\",\"produk_description\":\"sdfasdf\",\"produk_status\":\"0\",\"produk_id\":\"be469fbc-a63e-11e7-bee2-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('b185d5fa-acdf-11e7-814e-54650c0f9c19', '[{\"before\":{\"produk_id\":\"be469fbc-a63e-11e7-bee2-54650c0f9c19\",\"produk_identity\":\"asdf\",\"produk_nama\":\"ddd\",\"produk_harga\":\"45.00\",\"produk_stok\":\"34\",\"produk_satuan\":\"sdf\",\"produk_photo\":\"91018_Hydrangeas.jpg\",\"produk_description\":\"sdfasdf\",\"produk_status\":\"1\"}},{\"after\":{\"produk_identity\":\"asdf\",\"produk_nama\":\"ddd\",\"produk_harga\":\"45.00\",\"produk_stok\":\"34\",\"produk_satuan\":\"sdf\",\"produk_photo\":\"91018_Hydrangeas.jpg\",\"produk_description\":\"sdfasdf\",\"produk_status\":\"0\",\"produk_id\":\"be469fbc-a63e-11e7-bee2-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('04abb281-ace0-11e7-814e-54650c0f9c19', '[{\"before\":{\"produk_id\":\"be469fbc-a63e-11e7-bee2-54650c0f9c19\",\"produk_identity\":\"asdf\",\"produk_nama\":\"ddd\",\"produk_harga\":\"45.00\",\"produk_stok\":\"34\",\"produk_satuan\":\"sdf\",\"produk_photo\":\"91018_Hydrangeas.jpg\",\"produk_description\":\"sdfasdf\",\"produk_status\":\"1\"}},{\"after\":{\"produk_identity\":\"asdf\",\"produk_nama\":\"ddd\",\"produk_harga\":\"45.00\",\"produk_stok\":\"34\",\"produk_satuan\":\"sdf\",\"produk_photo\":\"91018_Hydrangeas.jpg\",\"produk_description\":\"sdfasdf\",\"produk_status\":\"0\",\"produk_id\":\"be469fbc-a63e-11e7-bee2-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('22492a0e-ace0-11e7-814e-54650c0f9c19', '[{\"before\":{\"produk_id\":\"be469fbc-a63e-11e7-bee2-54650c0f9c19\",\"produk_identity\":\"asdf\",\"produk_nama\":\"ddd\",\"produk_harga\":\"45.00\",\"produk_stok\":\"34\",\"produk_satuan\":\"sdf\",\"produk_photo\":\"91018_Hydrangeas.jpg\",\"produk_description\":\"sdfasdf\",\"produk_status\":\"1\"}},{\"after\":{\"produk_identity\":\"asdf\",\"produk_nama\":\"ddd\",\"produk_harga\":\"45.00\",\"produk_stok\":\"34\",\"produk_satuan\":\"sdf\",\"produk_photo\":\"91018_Hydrangeas.jpg\",\"produk_description\":\"sdfasdf\",\"produk_status\":\"0\",\"produk_id\":\"be469fbc-a63e-11e7-bee2-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('2de64569-ace0-11e7-814e-54650c0f9c19', '[{\"before\":{\"produk_id\":\"be469fbc-a63e-11e7-bee2-54650c0f9c19\",\"produk_identity\":\"asdf\",\"produk_nama\":\"ddd\",\"produk_harga\":\"45.00\",\"produk_stok\":\"34\",\"produk_satuan\":\"sdf\",\"produk_photo\":\"91018_Hydrangeas.jpg\",\"produk_description\":\"sdfasdf\",\"produk_status\":\"1\"}},{\"after\":{\"produk_identity\":\"asdf\",\"produk_nama\":\"ddd\",\"produk_harga\":\"45.00\",\"produk_stok\":\"34\",\"produk_satuan\":\"sdf\",\"produk_photo\":\"91018_Hydrangeas.jpg\",\"produk_description\":\"sdfasdf\",\"produk_status\":\"0\",\"produk_id\":\"be469fbc-a63e-11e7-bee2-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('4bf40284-ace0-11e7-814e-54650c0f9c19', '[{\"before\":{\"produk_id\":\"20650133-a63d-11e7-bee2-54650c0f9c19\",\"produk_identity\":\"pppp\",\"produk_nama\":\"pppp\",\"produk_harga\":\"8888.00\",\"produk_stok\":\"8\",\"produk_satuan\":\"jkj\",\"produk_photo\":\"\",\"produk_description\":\"jk\",\"produk_status\":\"1\"}},{\"after\":{\"produk_identity\":\"pppp\",\"produk_nama\":\"pppp\",\"produk_harga\":\"8888.00\",\"produk_stok\":\"8\",\"produk_satuan\":\"jkj\",\"produk_photo\":\"\",\"produk_description\":\"jk\",\"produk_status\":\"0\",\"produk_id\":\"20650133-a63d-11e7-bee2-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('fa9f54bb-ace0-11e7-814e-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_identity\":\"p00438\",\"produk_nama\":\"meja belajar\",\"produk_harga\":\"400000\",\"produk_stok\":\"4\",\"produk_satuan\":\"box\",\"produk_description\":\"Hey i am totally new in Codeigniter.In the Controllers folder I created a file named caller.php and I created a file home1.php in Views. In root directory i created a image folder name Images and also created a css folder named css.In images Folder there are 6 picture. In css folder style.css file exist.\",\"produk_status\":\"1\",\"produk_photo\":\"16632_Tulips.jpg\"}}]', '1');
INSERT INTO `histories` VALUES ('30332265-ace1-11e7-814e-54650c0f9c19', '[{\"before\":{\"produk_id\":\"fa998123-ace0-11e7-814e-54650c0f9c19\",\"produk_identity\":\"p0043\",\"produk_nama\":\"meja belajar\",\"produk_harga\":\"400000.00\",\"produk_stok\":\"4\",\"produk_satuan\":\"box\",\"produk_photo\":\"16632_Tulips.jpg\",\"produk_description\":\"Hey i am totally new in Codeigniter.In the Controllers folder I created a file named caller.php and I created a file home1.php in Views. In root directory i created a image folder name Images and also created a css folder named css.In images Folder there are 6 picture. In css folder style.css file exist.\",\"produk_status\":\"1\"}},{\"after\":{\"produk_identity\":\"p0043\",\"produk_nama\":\"meja belajar\",\"produk_harga\":\"500000.00\",\"produk_stok\":\"4\",\"produk_satuan\":\"box\",\"produk_photo\":\"16632_Tulips.jpg\",\"produk_description\":\"Hey i am totally new in Codeigniter.In the Controllers folder I created a file named caller.php and I created a file home1.php in Views. In root directory i created a image folder name Images and also created a css folder named css.In images Folder there are 6 picture. In css folder style.css file exist.\",\"produk_status\":\"1\",\"produk_id\":\"fa998123-ace0-11e7-814e-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('5fce7884-ace1-11e7-814e-54650c0f9c19', '[{\"before\":{\"produk_id\":\"fa998123-ace0-11e7-814e-54650c0f9c19\",\"produk_identity\":\"p0043\",\"produk_nama\":\"meja belajar\",\"produk_harga\":\"500000.00\",\"produk_stok\":\"4\",\"produk_satuan\":\"box\",\"produk_photo\":\"16632_Tulips.jpg\",\"produk_description\":\"Hey i am totally new in Codeigniter.In the Controllers folder I created a file named caller.php and I created a file home1.php in Views. In root directory i created a image folder name Images and also created a css folder named css.In images Folder there are 6 picture. In css folder style.css file exist.\",\"produk_status\":\"1\"}},{\"after\":{\"produk_identity\":\"p0043\",\"produk_nama\":\"meja belajar\",\"produk_harga\":\"600000.00\",\"produk_stok\":\"4\",\"produk_satuan\":\"box\",\"produk_photo\":\"16632_Tulips.jpg\",\"produk_description\":\"Hey i am totally new in Codeigniter.In the Controllers folder I created a file named caller.php and I created a file home1.php in Views. In root directory i created a image folder name Images and also created a css folder named css.In images Folder there are 6 picture. In css folder style.css file exist.\",\"produk_status\":\"1\",\"produk_id\":\"fa998123-ace0-11e7-814e-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('f92f0990-ad35-11e7-96d5-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('9ba6d4ac-ad39-11e7-96d5-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"A001\",\"anggota_nama\":\"poiu\",\"anggota_tmplahir\":\"jjh\",\"anggota_tgllahir\":\"2017-10-10\",\"anggota_alamat\":\"kjklj\",\"anggota_jnskelamin\":\"L\",\"anggota_notlp\":\"0987\",\"anggota_email\":\"t@m.m\",\"anggota_jnsidentitas\":\"12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"anggota_noidentitas\":\"09876\",\"user_password\":\"5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5\",\"user_password_confirm\":\"5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5\",\"anggota_persetujuan\":\"1\",\"anggota_scanidentitas\":\"5560_Chrysanthemum.jpg\",\"anggota_photo\":\"71871_Desert.jpg\"}}]', '1');
INSERT INTO `histories` VALUES ('20f801f0-ad3a-11e7-96d5-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"A002\",\"anggota_nama\":\"jkh\",\"anggota_tmplahir\":\"jjklj\",\"anggota_tgllahir\":\"2017-10-30\",\"anggota_alamat\":\"9898\",\"anggota_jnskelamin\":\"L\",\"anggota_notlp\":\"8789\",\"anggota_email\":\"w@m.m\",\"anggota_jnsidentitas\":\"12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"anggota_noidentitas\":\"0987\",\"user_password\":\"5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5\",\"user_password_confirm\":\"5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5\",\"anggota_persetujuan\":\"1\",\"anggota_scanidentitas\":\"10778_Chrysanthemum.jpg\",\"anggota_photo\":\"43814_Desert.jpg\"}}]', '1');
INSERT INTO `histories` VALUES ('fd757761-ad3a-11e7-96d5-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('38b29443-ad55-11e7-96d5-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('2f98cad9-ad60-11e7-96d5-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"A007\",\"anggota_nama\":\"Tantan\",\"anggota_tmplahir\":\"bandung\",\"anggota_tgllahir\":\"2017-10-13\",\"anggota_alamat\":\"jatihandap bandung\",\"anggota_area\":\"1\",\"anggota_jnskelamin\":\"L\",\"anggota_notlp\":\"0987654321\",\"anggota_email\":\"tan@mail.com\",\"anggota_jnsidentitas\":\"12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"anggota_noidentitas\":\"1234567890\",\"user_password\":\"b03ddf3ca2e714a6548e7495e2a03f5e824eaac9837cd7f159c67b90fb4b7342\",\"user_password_confirm\":\"b03ddf3ca2e714a6548e7495e2a03f5e824eaac9837cd7f159c67b90fb4b7342\",\"anggota_persetujuan\":\"1\",\"anggota_scanidentitas\":\"65197_Chrysanthemum.jpg\",\"anggota_photo\":\"12979_Desert.jpg\"}}]', '1');
INSERT INTO `histories` VALUES ('334e6b24-ad64-11e7-96d5-54650c0f9c19', '[{\"before\":{\"user_id\":\"2f8afe30-ad60-11e7-96d5-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"anggota_note\":\"tolong diulangi\",\"user_id\":\"2f8afe30-ad60-11e7-96d5-54650c0f9c19\",\"user_status\":\"2\"}}]', '1');
INSERT INTO `histories` VALUES ('62e9d4b4-ad64-11e7-96d5-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"A008\",\"anggota_nama\":\"Tantan Suryana\",\"anggota_tmplahir\":\"Bandung\",\"anggota_tgllahir\":\"2017-10-06\",\"anggota_alamat\":\"Green City View\",\"anggota_area\":\"2\",\"anggota_jnskelamin\":\"L\",\"anggota_notlp\":\"086543222\",\"anggota_email\":\"tan@mail.com\",\"anggota_jnsidentitas\":\"12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"anggota_noidentitas\":\"123456789\",\"user_password\":\"98e1201a4236fa48192f5e279113949658dbbe970661bd99281953a172aaed4f\",\"user_password_confirm\":\"5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5\",\"anggota_persetujuan\":\"1\",\"anggota_scanidentitas\":\"28280_Hydrangeas.jpg\",\"anggota_photo\":\"28076_Jellyfish.jpg\",\"user_salt\":\"17516\"}}]', '1');
INSERT INTO `histories` VALUES ('1dacdf98-ad66-11e7-96d5-54650c0f9c19', '[{\"before\":{\"user_id\":\"62d77e15-ad64-11e7-96d5-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"62d77e15-ad64-11e7-96d5-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('1dbc1a1c-ad66-11e7-96d5-54650c0f9c19', '[{\"before\":{\"user_id\":\"62d77e15-ad64-11e7-96d5-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"62d77e15-ad64-11e7-96d5-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('d2b9de5a-ad67-11e7-96d5-54650c0f9c19', '[{\"before\":{\"user_id\":\"62d77e15-ad64-11e7-96d5-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"62d77e15-ad64-11e7-96d5-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('d2d311d9-ad67-11e7-96d5-54650c0f9c19', '[{\"before\":{\"user_id\":\"62d77e15-ad64-11e7-96d5-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"62d77e15-ad64-11e7-96d5-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('5176639c-ad68-11e7-96d5-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"A009\",\"anggota_nama\":\"T Suryana\",\"anggota_tmplahir\":\"Bandung\",\"anggota_tgllahir\":\"2017-10-16\",\"anggota_alamat\":\"The Green\",\"anggota_area\":\"2\",\"anggota_jnskelamin\":\"L\",\"anggota_notlp\":\"098765432\",\"anggota_email\":\"tan@mail.com\",\"anggota_jnsidentitas\":\"12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"anggota_noidentitas\":\"1234567890\",\"user_password\":\"6f705103c782bb4de4d0bbcb51faa0bb98fccbbf5530d7ed97e30c28b02008b1\",\"user_password_confirm\":\"5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5\",\"anggota_persetujuan\":\"1\",\"anggota_scanidentitas\":\"9042_Koala.jpg\",\"anggota_photo\":\"81103_Penguins.jpg\",\"user_salt\":\"58637\"}}]', '1');
INSERT INTO `histories` VALUES ('61211cf9-ad68-11e7-96d5-54650c0f9c19', '[{\"before\":{\"user_id\":\"5164e61a-ad68-11e7-96d5-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"5164e61a-ad68-11e7-96d5-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('61456dc3-ad68-11e7-96d5-54650c0f9c19', '[{\"before\":{\"user_id\":\"5164e61a-ad68-11e7-96d5-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"5164e61a-ad68-11e7-96d5-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('afe8d51c-ad6e-11e7-bf38-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('566df86e-ad73-11e7-bf38-54650c0f9c19', '[{\"before\":{\"user_identity\":\"A009\",\"anggota_nama\":\"T Suryana\",\"anggota_tmplahir\":\"Bandung\",\"anggota_tgllahir\":\"2017-10-16\",\"anggota_alamat\":\"The Green\",\"anggota_jnskelamin\":\"L\",\"anggota_notlp\":\"098765432\",\"anggota_email\":\"tan@mail.com\",\"anggota_jnsidentitas\":\"12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"anggota_noidentitas\":\"1234567890\",\"anggota_scanidentitas\":\"9042_Koala.jpg\",\"anggota_photo\":\"81103_Penguins.jpg\",\"anggota_persetujuan\":\"1\"}},{\"after\":{\"user_identity\":\"A009\",\"anggota_nama\":\"Tans Suryana\",\"anggota_tmplahir\":\"Bandung\",\"anggota_tgllahir\":\"2017-10-16\",\"anggota_alamat\":\"The Green\",\"anggota_areaid\":\"2\",\"anggota_jnskelamin\":\"L\",\"anggota_notlp\":\"098765432\",\"anggota_email\":\"tan@mail.com\",\"anggota_jnsidentitas\":\"12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"anggota_noidentitas\":\"1234567890\",\"anggota_scanidentitas\":\"81103_Penguins.jpg\",\"anggota_photo\":\"\",\"anggota_persetujuan\":\"1\",\"user_id\":\"610eaee3-ad68-11e7-96d5-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('c0d45a70-ad73-11e7-bf38-54650c0f9c19', '[{\"before\":{\"user_identity\":\"A009\",\"anggota_nama\":\"Tans Suryana\",\"anggota_tmplahir\":\"Bandung\",\"anggota_tgllahir\":\"2017-10-16\",\"anggota_alamat\":\"The Green\",\"anggota_jnskelamin\":\"L\",\"anggota_notlp\":\"098765432\",\"anggota_email\":\"tan@mail.com\",\"anggota_jnsidentitas\":\"12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"anggota_noidentitas\":\"1234567890\",\"anggota_scanidentitas\":\"81103_Penguins.jpg\",\"anggota_photo\":\"\",\"anggota_persetujuan\":\"1\"}},{\"after\":{\"user_identity\":\"A009\",\"anggota_nama\":\"Tans Suryana\",\"anggota_tmplahir\":\"Bandung\",\"anggota_tgllahir\":\"2017-10-16\",\"anggota_alamat\":\"The Green\",\"anggota_areaid\":\"2\",\"anggota_jnskelamin\":\"L\",\"anggota_notlp\":\"098765432\",\"anggota_email\":\"tan@mail.com\",\"anggota_jnsidentitas\":\"12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"anggota_noidentitas\":\"1234567890\",\"anggota_persetujuan\":\"1\",\"user_id\":\"610eaee3-ad68-11e7-96d5-54650c0f9c19\",\"anggota_scanidentitas\":\"55892_Koala.jpg\",\"anggota_photo\":\"317_Lighthouse.jpg\"}}]', '1');
INSERT INTO `histories` VALUES ('9c087e26-ad75-11e7-bf38-54650c0f9c19', '[{\"before\":{\"user_identity\":\"A009\",\"anggota_nama\":\"Tans Suryana\",\"anggota_tmplahir\":\"Bandung\",\"anggota_tgllahir\":\"2017-10-16\",\"anggota_alamat\":\"The Green\",\"anggota_jnskelamin\":\"L\",\"anggota_notlp\":\"098765432\",\"anggota_email\":\"tan@mail.com\",\"anggota_jnsidentitas\":\"12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"anggota_noidentitas\":\"1234567890\",\"anggota_scanidentitas\":\"55892_Koala.jpg\",\"anggota_photo\":\"317_Lighthouse.jpg\",\"anggota_persetujuan\":\"1\"}},{\"after\":{\"user_identity\":\"A009\",\"anggota_nama\":\"T Suryana\",\"anggota_tmplahir\":\"Bandung\",\"anggota_tgllahir\":\"2017-10-16\",\"anggota_alamat\":\"The Green\",\"anggota_areaid\":\"2\",\"anggota_jnskelamin\":\"L\",\"anggota_notlp\":\"098765432\",\"anggota_email\":\"tan@mail.com\",\"anggota_jnsidentitas\":\"12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"anggota_noidentitas\":\"1234567890\",\"anggota_scanidentitas\":\"55892_Koala.jpg\",\"anggota_photo\":\"317_Lighthouse.jpg\",\"anggota_persetujuan\":\"1\",\"user_id\":\"610eaee3-ad68-11e7-96d5-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('c1a79070-ad82-11e7-bf38-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"A010\",\"anggota_nama\":\"tes\",\"anggota_tmplahir\":\"bandung\",\"anggota_tgllahir\":\"2017-10-12\",\"anggota_alamat\":\"tes\",\"anggota_area\":\"1\",\"anggota_jnskelamin\":\"P\",\"anggota_notlp\":\"09876543\",\"anggota_email\":\"t@m.m\",\"anggota_jnsidentitas\":\"13EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"anggota_noidentitas\":\"2345\",\"user_password\":\"317f24673140b837003a2a3fe3dda7ac308d602d613c0f98379aa08b8756a1ff\",\"user_password_confirm\":\"5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5\",\"anggota_persetujuan\":\"1\",\"anggota_scanidentitas\":\"74084_Penguins.jpg\",\"anggota_photo\":\"31631_Tulips.jpg\",\"user_salt\":\"55994\"}}]', '1');
INSERT INTO `histories` VALUES ('c7baa57e-ad82-11e7-bf38-54650c0f9c19', '[{\"before\":{\"user_id\":\"c18f5f3f-ad82-11e7-bf38-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"c18f5f3f-ad82-11e7-bf38-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('c7d08537-ad82-11e7-bf38-54650c0f9c19', '[{\"before\":{\"user_id\":\"c18f5f3f-ad82-11e7-bf38-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"c18f5f3f-ad82-11e7-bf38-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('149bdcfe-ad84-11e7-bf38-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"old_password\":\"12345\",\"user_id\":\"c7ae2667-ad82-11e7-bf38-54650c0f9c19\",\"new_password\":\"11111\",\"confirm_new_password\":\"11111\"}}]', '1');
INSERT INTO `histories` VALUES ('cbc26c50-ad85-11e7-bf38-54650c0f9c19', '[{\"before\":{\"uid\":\"c7ae2667-ad82-11e7-bf38-54650c0f9c19\"}},{\"after\":{\"user_id\":\"c7ae2667-ad82-11e7-bf38-54650c0f9c19\",\"user_salt\":\"21872\",\"user_password\":\"5e8fe08c787e14c05045060ae5d2929523422e40fe14b2ed4f96ca491e0708d9\"}}]', '1');
INSERT INTO `histories` VALUES ('3e8dde63-ad86-11e7-bf38-54650c0f9c19', '[{\"before\":{\"uid\":\"c7ae2667-ad82-11e7-bf38-54650c0f9c19\"}},{\"after\":{\"user_id\":\"c7ae2667-ad82-11e7-bf38-54650c0f9c19\",\"user_salt\":\"12720\",\"user_password\":\"0a2df8fc8bafb3e63af029569f519ed9b7121871a3ecfea00002988b30e3ce0e\"}}]', '1');
INSERT INTO `histories` VALUES ('4f3835e0-ad86-11e7-bf38-54650c0f9c19', '[{\"before\":{\"uid\":\"c7ae2667-ad82-11e7-bf38-54650c0f9c19\"}},{\"after\":{\"user_id\":\"c7ae2667-ad82-11e7-bf38-54650c0f9c19\",\"user_salt\":\"50706\",\"user_password\":\"706405a4f4ba8b149a5294a1738e91636dbfb554736d2162789fe08ac2fca618\"}}]', '1');
INSERT INTO `histories` VALUES ('f8a51226-ad86-11e7-bf38-54650c0f9c19', '[{\"before\":{\"user_identity\":\"A010\",\"anggota_nama\":\"\",\"anggota_tmplahir\":\"\",\"anggota_tgllahir\":\"0000-00-00\",\"anggota_alamat\":\"\",\"anggota_jnskelamin\":\"\",\"anggota_notlp\":\"\",\"anggota_email\":\"\",\"anggota_jnsidentitas\":\"\",\"anggota_noidentitas\":\"\",\"anggota_scanidentitas\":\"\",\"anggota_photo\":\"\",\"anggota_persetujuan\":\"0\"}},{\"after\":{\"user_identity\":\"A010\",\"anggota_nama\":\"saya\",\"anggota_tmplahir\":\"asdf\",\"anggota_tgllahir\":\"0000-00-00\",\"anggota_alamat\":\"asdf\",\"anggota_areaid\":\"1\",\"anggota_jnskelamin\":\"L\",\"anggota_notlp\":\"1234\",\"anggota_email\":\"3@m.m\",\"anggota_jnsidentitas\":\"12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"anggota_noidentitas\":\"sdf\",\"anggota_scanidentitas\":\"\",\"anggota_photo\":\"\",\"anggota_persetujuan\":\"1\",\"user_id\":\"c7ae2667-ad82-11e7-bf38-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('07b5f14e-ad87-11e7-bf38-54650c0f9c19', '[{\"before\":{\"uid\":\"c7ae2667-ad82-11e7-bf38-54650c0f9c19\"}},{\"after\":{\"user_id\":\"c7ae2667-ad82-11e7-bf38-54650c0f9c19\",\"user_salt\":\"08944\",\"user_password\":\"33b06187c914a054c2c507fc70b1bf03594eac0ff71107c9025d71c99f671a76\"}}]', '1');
INSERT INTO `histories` VALUES ('41593548-ad87-11e7-bf38-54650c0f9c19', '[{\"before\":{\"uid\":\"610eaee3-ad68-11e7-96d5-54650c0f9c19\"}},{\"after\":{\"user_id\":\"610eaee3-ad68-11e7-96d5-54650c0f9c19\",\"user_salt\":\"72026\",\"user_password\":\"484f82ae3aeb9d63c3aaa31bde972fde5459046a7d86585122523fd3158c6879\"}}]', '1');
INSERT INTO `histories` VALUES ('bb9ba8e7-ad87-11e7-bf38-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"user_identity\":\"A111\",\"anggota_nama\":\"ppp\",\"anggota_tmplahir\":\"pppp\",\"anggota_tgllahir\":\"2017-10-04\",\"anggota_alamat\":\"asf\",\"anggota_area\":\"2\",\"anggota_jnskelamin\":\"P\",\"anggota_notlp\":\"987\",\"anggota_email\":\"6@m.m\",\"anggota_jnsidentitas\":\"12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F\",\"anggota_noidentitas\":\"789\",\"user_password\":\"032688908ac08732935afd2d3d8f71425ac27d47bc7e5ba52c0e8d4d5e9fdfab\",\"user_password_confirm\":\"d17f25ecfbcc7857f7bebea469308be0b2580943e96d13a3ad98a13675c4bfc2\",\"anggota_persetujuan\":\"1\",\"anggota_scanidentitas\":\"2691_Desert.jpg\",\"anggota_photo\":\"77938_Hydrangeas.jpg\",\"user_salt\":\"98550\"}}]', '1');
INSERT INTO `histories` VALUES ('c0ca30c6-ad87-11e7-bf38-54650c0f9c19', '[{\"before\":{\"user_id\":\"bb7a6b78-ad87-11e7-bf38-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"bb7a6b78-ad87-11e7-bf38-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('c0f64ffe-ad87-11e7-bf38-54650c0f9c19', '[{\"before\":{\"user_id\":\"bb7a6b78-ad87-11e7-bf38-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"bb7a6b78-ad87-11e7-bf38-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('d549c1e0-ad87-11e7-bf38-54650c0f9c19', '[{\"before\":{\"uid\":\"c0bedf89-ad87-11e7-bf38-54650c0f9c19\"}},{\"after\":{\"user_id\":\"c0bedf89-ad87-11e7-bf38-54650c0f9c19\",\"user_salt\":\"70891\",\"user_password\":\"10a5ea98bf466bfecff93f3ad0d4f096d347552aee52fba96d8b32ab870edca3\"}}]', '1');
INSERT INTO `histories` VALUES ('e92d5ef8-ad87-11e7-bf38-54650c0f9c19', '[{\"before\":{\"uid\":\"c0bedf89-ad87-11e7-bf38-54650c0f9c19\"}},{\"after\":{\"user_id\":\"c0bedf89-ad87-11e7-bf38-54650c0f9c19\",\"user_salt\":\"74493\",\"user_password\":\"18f85eb1a209edb730d312a28d6f8dc0f1229e014234a540780355a82e910d48\"}}]', '1');
INSERT INTO `histories` VALUES ('fbabd7cd-ad87-11e7-bf38-54650c0f9c19', '[{\"before\":{\"uid\":\"c0bedf89-ad87-11e7-bf38-54650c0f9c19\"}},{\"after\":{\"user_id\":\"c0bedf89-ad87-11e7-bf38-54650c0f9c19\",\"user_salt\":\"95705\",\"user_password\":\"4575f7a0acd773850cc300fff7ab2ef8420138e4a0e98a51425afaf4125a0be7\"}}]', '1');
INSERT INTO `histories` VALUES ('1175b645-ad88-11e7-bf38-54650c0f9c19', '[{\"before\":{\"user_id\":\"9b9633c1-ad39-11e7-96d5-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"9b9633c1-ad39-11e7-96d5-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('119824bb-ad88-11e7-bf38-54650c0f9c19', '[{\"before\":{\"user_id\":\"9b9633c1-ad39-11e7-96d5-54650c0f9c19\",\"user_status\":\"0\"}},{\"after\":{\"user_id\":\"9b9633c1-ad39-11e7-96d5-54650c0f9c19\",\"user_status\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('096b52ab-ad94-11e7-bf38-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxsaldo_noref\":\"00000000000\",\"trxsaldo_kdbank\":\"1\",\"trxsaldo_nominal\":\"123000\",\"trxsaldo_user_id\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('d1299f04-ad98-11e7-bf38-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxsaldo_noref\":\"0000000000\",\"trxsaldo_methode\":\"1\",\"trxsaldo_kdbank\":\"1\",\"trxsaldo_nominal\":\"111000\",\"trxsaldo_user_id\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('777588bc-ad9a-11e7-bf38-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxsaldo_noref\":\"1234567890\",\"trxsaldo_methode\":\"1\",\"trxsaldo_kdbank\":\"1\",\"trxsaldo_nominal\":\"210000\",\"trxsaldo_user_id\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('b3f8a958-ad9c-11e7-bf38-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxsaldo_user_id\":\"610eaee3-ad68-11e7-96d5-54650c0f9c19\",\"trxsaldo_methode\":\"1\",\"trxsaldo_noref\":\"987654321\",\"trxsaldo_kdbank\":\"1\",\"trxsaldo_nominal\":\"900000\"}}]', '1');
INSERT INTO `histories` VALUES ('04e8a01f-ad9d-11e7-bf38-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxsaldo_user_id\":\"610eaee3-ad68-11e7-96d5-54650c0f9c19\",\"trxsaldo_methode\":\"2\",\"trxsaldo_noref\":\"10102017162546\",\"trxsaldo_nominal\":\"100000\"}}]', '1');
INSERT INTO `histories` VALUES ('df85d198-ada0-11e7-bf38-54650c0f9c19', '[{\"before\":{\"trxsaldo_id\":\"04df1435-ad9d-11e7-bf38-54650c0f9c19\",\"trxsaldo_status\":\"0\"}},{\"after\":{\"trxsaldo_note\":\"salah input\",\"trxsaldo_id\":\"04df1435-ad9d-11e7-bf38-54650c0f9c19\",\"trxsaldo_status\":\"2\",\"trxsaldo_verifiedby\":\"adm1\"}}]', '1');
INSERT INTO `histories` VALUES ('a6c08c7c-ada1-11e7-bf38-54650c0f9c19', '[{\"before\":{\"trxsaldo_id\":\"b3ee47b3-ad9c-11e7-bf38-54650c0f9c19\",\"trxsaldo_status\":\"0\"}},{\"after\":{\"trxsaldo_note\":\"asdf\",\"trxsaldo_id\":\"b3ee47b3-ad9c-11e7-bf38-54650c0f9c19\",\"trxsaldo_status\":\"2\",\"trxsaldo_verifiedby\":\"adm1\"}}]', '1');
INSERT INTO `histories` VALUES ('0330e94a-ada2-11e7-bf38-54650c0f9c19', '[{\"before\":{\"trxsaldo_id\":\"04df1435-ad9d-11e7-bf38-54650c0f9c19\",\"trxsaldo_status\":\"0\"}},{\"after\":{\"trxsaldo_id\":\"04df1435-ad9d-11e7-bf38-54650c0f9c19\",\"trxsaldo_status\":\"1\",\"trxsaldo_verifiedby\":\"adm1\"}}]', '1');
INSERT INTO `histories` VALUES ('5cfb2347-ae05-11e7-b4b4-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('9e55c3d8-ae09-11e7-b4b4-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxsaldo_user_id\":\"610eaee3-ad68-11e7-96d5-54650c0f9c19\",\"trxsaldo_methode\":\"1\",\"trxsaldo_noref\":\"123456789\",\"trxsaldo_kdbank\":\"1\",\"trxsaldo_nominal\":\"100000\"}}]', '1');
INSERT INTO `histories` VALUES ('eb266104-ae09-11e7-b4b4-54650c0f9c19', '[{\"before\":{\"trxsaldo_id\":\"9e43774b-ae09-11e7-b4b4-54650c0f9c19\",\"trxsaldo_status\":\"0\"}},{\"after\":{\"trxsaldo_id\":\"9e43774b-ae09-11e7-b4b4-54650c0f9c19\",\"trxsaldo_status\":\"1\",\"trxsaldo_verifiedby\":\"adm1\"}}]', '1');
INSERT INTO `histories` VALUES ('3b2fac5f-ae0a-11e7-b4b4-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxsaldo_user_id\":\"610eaee3-ad68-11e7-96d5-54650c0f9c19\",\"trxsaldo_methode\":\"2\",\"trxsaldo_noref\":\"1110201752734\",\"trxsaldo_nominal\":\"250000\"}}]', '1');
INSERT INTO `histories` VALUES ('40c6dc39-ae0a-11e7-b4b4-54650c0f9c19', '[{\"before\":{\"trxsaldo_id\":\"3b1f40f0-ae0a-11e7-b4b4-54650c0f9c19\",\"trxsaldo_status\":\"0\"}},{\"after\":{\"trxsaldo_id\":\"3b1f40f0-ae0a-11e7-b4b4-54650c0f9c19\",\"trxsaldo_status\":\"1\",\"trxsaldo_verifiedby\":\"adm1\"}}]', '1');
INSERT INTO `histories` VALUES ('5fdf4f95-ae0a-11e7-b4b4-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxsaldo_user_id\":\"d2afa5b9-ad67-11e7-96d5-54650c0f9c19\",\"trxsaldo_methode\":\"1\",\"trxsaldo_noref\":\"123123123\",\"trxsaldo_kdbank\":\"1\",\"trxsaldo_nominal\":\"231000\"}}]', '1');
INSERT INTO `histories` VALUES ('6c61ccc2-ae0a-11e7-b4b4-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxsaldo_user_id\":\"d2afa5b9-ad67-11e7-96d5-54650c0f9c19\",\"trxsaldo_methode\":\"2\",\"trxsaldo_noref\":\"1110201752857\",\"trxsaldo_nominal\":\"100000\"}}]', '1');
INSERT INTO `histories` VALUES ('7d74c9ac-ae0a-11e7-b4b4-54650c0f9c19', '[{\"before\":{\"trxsaldo_id\":\"5fce8e29-ae0a-11e7-b4b4-54650c0f9c19\",\"trxsaldo_status\":\"0\"}},{\"after\":{\"trxsaldo_id\":\"5fce8e29-ae0a-11e7-b4b4-54650c0f9c19\",\"trxsaldo_status\":\"1\",\"trxsaldo_verifiedby\":\"adm1\"}}]', '1');
INSERT INTO `histories` VALUES ('a00f7c0d-ae0a-11e7-b4b4-54650c0f9c19', '[{\"before\":{\"trxsaldo_id\":\"6c5418a5-ae0a-11e7-b4b4-54650c0f9c19\",\"trxsaldo_status\":\"0\"}},{\"after\":{\"trxsaldo_note\":\"dibatalkan\",\"trxsaldo_id\":\"6c5418a5-ae0a-11e7-b4b4-54650c0f9c19\",\"trxsaldo_status\":\"2\",\"trxsaldo_verifiedby\":\"adm1\"}}]', '1');
INSERT INTO `histories` VALUES ('6e2cef78-ae20-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('adbcf870-ae34-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"FB34FB34-4A4D-43AD-84B3-08E9A383D118\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"82638\"}}]', '1');
INSERT INTO `histories` VALUES ('05198992-ae35-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('176ac599-ae35-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"1E880E38-B108-4721-B30A-8B24B5E243A0\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"39572\"}}]', '1');
INSERT INTO `histories` VALUES ('b84001fe-ae35-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('e8cc1ca0-ae35-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"87423DB2-6209-4D9D-AE2B-248FFFD894E5\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"02619\"}}]', '1');
INSERT INTO `histories` VALUES ('0f509601-ae36-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('18a904a5-ae36-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"6193EC28-F9FA-48BA-8E42-EDB31B2A9E36\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"24895\"}}]', '1');
INSERT INTO `histories` VALUES ('38d5df31-ae36-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('427f278b-ae36-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"AE9328B6-8A27-452B-BA88-26986D0F8E3E\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"54519\"}}]', '1');
INSERT INTO `histories` VALUES ('7f1ebbcc-ae36-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('98b78346-ae36-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"F3535B9F-228C-4309-9330-E07685D26EAF\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"74093\"}}]', '1');
INSERT INTO `histories` VALUES ('c37559d5-ae36-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"A25435AD-7DF2-440E-A1F8-193FC73B323F\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"11916\"}}]', '1');
INSERT INTO `histories` VALUES ('db95bd49-ae36-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('e54eadc5-ae36-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"02C95793-FF7B-46C0-A389-67F1E33193EE\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"28471\"}}]', '1');
INSERT INTO `histories` VALUES ('04a894c0-ae37-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('0ee4eb73-ae37-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"E45023AC-383A-42AE-9BCF-62C66D9E1EA6\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"15187\"}}]', '1');
INSERT INTO `histories` VALUES ('20bd5640-ae37-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('2a30cf2e-ae37-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"2186E326-83A1-46A8-A79A-BD2FD1520352\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"27336\"}}]', '1');
INSERT INTO `histories` VALUES ('4a4783d3-ae37-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('65e1f32f-ae37-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"C8338ABF-9845-4FC4-B90A-D1E22006EC3D\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"05209\"}}]', '1');
INSERT INTO `histories` VALUES ('cc7bb72f-ae37-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('d737bad4-ae37-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"3D57CD6C-238C-42D7-B26E-647A8C1015AE\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"69061\"}}]', '1');
INSERT INTO `histories` VALUES ('8d903658-ae38-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('983173a0-ae38-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"0E59B156-3B3D-4942-AFDD-ABCCEC096DD4\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"72816\"}}]', '1');
INSERT INTO `histories` VALUES ('db8ab7f0-ae38-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('e6e90656-ae38-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"EF88C9E3-251B-4BCE-A31F-4815C1E8542D\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"12186\"}}]', '1');
INSERT INTO `histories` VALUES ('05eb293e-ae39-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('116fea4b-ae39-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"3F1DD241-7BFB-432B-A34F-AB4C58BB2A9A\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"14650\"}}]', '1');
INSERT INTO `histories` VALUES ('48eac9cd-ae39-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('5551c38e-ae39-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"E101D015-F9DD-463E-8B2A-9FCA818562A9\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"69642\"}}]', '1');
INSERT INTO `histories` VALUES ('6fe1fe5a-ae39-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('7c97c9a2-ae39-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"CB1B9499-1F7A-4E51-8E57-47658CC90412\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"94738\"}}]', '1');
INSERT INTO `histories` VALUES ('97aa7148-ae39-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('a4569b36-ae39-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"E2C4C938-9B77-4D1D-BA42-CB38C96636CD\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"73741\"}}]', '1');
INSERT INTO `histories` VALUES ('33043741-ae3a-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('42544cc1-ae3a-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"C57AFFF9-E219-4A36-B5BC-78EB45172DE4\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"08041\"}}]', '1');
INSERT INTO `histories` VALUES ('5f555010-ae3a-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('7e654d2f-ae3a-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"71930024-687D-4E6B-9CDD-7FBE2C2C9D3C\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"03288\"}}]', '1');
INSERT INTO `histories` VALUES ('17cbe0f6-ae3b-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('23ecfdbb-ae3b-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"A5E138D7-5EFC-4534-A71A-45690C56509A\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"10206\"}}]', '1');
INSERT INTO `histories` VALUES ('35d88072-ae3b-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('43404a5e-ae3b-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"CACB1E84-B898-4E2C-B2B9-0D8577870BC5\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"63124\"}}]', '1');
INSERT INTO `histories` VALUES ('7c47f856-ae3b-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('8ba21f98-ae3b-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"BCD6E2F6-9E9C-4613-862A-4FF4C7A36F5B\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"62019\"}}]', '1');
INSERT INTO `histories` VALUES ('ab61fd23-ae3b-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('b5b2a682-ae3b-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"53DDE0B2-EAD3-41D8-95E0-D926DF7255A1\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"32066\"}}]', '1');
INSERT INTO `histories` VALUES ('4d9d9003-ae3c-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('58627676-ae3c-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"6D3560F4-EE66-4904-8D53-F3E32C69EE1B\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"05471\"}}]', '1');
INSERT INTO `histories` VALUES ('c246bc56-ae3e-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('cb6f5822-ae3e-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"09B8F0EB-648A-4ED3-AF45-F398ACA33B22\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"80421\"}}]', '1');
INSERT INTO `histories` VALUES ('f07d93a2-ae3e-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('01f66b56-ae3f-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"4B3924F1-A224-4D69-A7FD-5F2BEB82E880\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"90424\"}}]', '1');
INSERT INTO `histories` VALUES ('2593809d-ae3f-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('32bed9be-ae3f-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"76B0AD30-EA45-44A0-9567-ED70B9D5D70D\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"89776\"}}]', '1');
INSERT INTO `histories` VALUES ('4eb50ff6-ae3f-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('59b45766-ae3f-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"6ED2C7D1-5944-453F-88D9-B30BF136A06A\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"33989\"}}]', '1');
INSERT INTO `histories` VALUES ('59dcfb3a-ae3f-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"08ace22d-a63b-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"1\",\"trxpesanan_id\":\"6ED2C7D1-5944-453F-88D9-B30BF136A06A\"}}]', '1');
INSERT INTO `histories` VALUES ('7b3637fe-ae4a-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"1ddf36b7-a630-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"1\",\"trxpesanan_id\":\"6ED2C7D1-5944-453F-88D9-B30BF136A06A\"}}]', '1');
INSERT INTO `histories` VALUES ('9ef5f471-ae4a-11e7-912c-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('bb7a6ab0-ae4b-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"F8EE837D-D333-47B3-A826-8D9143276F3B\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"171011081634\"}}]', '1');
INSERT INTO `histories` VALUES ('bbaed417-ae4b-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"08ace22d-a63b-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"1\",\"trxpesanan_id\":\"F8EE837D-D333-47B3-A826-8D9143276F3B\"}}]', '1');
INSERT INTO `histories` VALUES ('ca3b2adb-ae4b-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"08ace22d-a63b-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"2\",\"trxpesanan_id\":\"F8EE837D-D333-47B3-A826-8D9143276F3B\"}}]', '1');
INSERT INTO `histories` VALUES ('da0eddec-ae4b-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"08ace22d-a63b-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"1\",\"trxpesanan_id\":\"F8EE837D-D333-47B3-A826-8D9143276F3B\"}}]', '1');
INSERT INTO `histories` VALUES ('2dea1be3-ae55-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"1ddf36b7-a630-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"5\",\"trxpesanan_id\":\"F8EE837D-D333-47B3-A826-8D9143276F3B\"}}]', '1');
INSERT INTO `histories` VALUES ('5f5582af-ae64-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"08ace22d-a63b-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"2\",\"trxpesanan_id\":\"F8EE837D-D333-47B3-A826-8D9143276F3B\"}}]', '1');
INSERT INTO `histories` VALUES ('6fa4cc18-ae64-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"1ddf36b7-a630-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"3\",\"trxpesanan_id\":\"F8EE837D-D333-47B3-A826-8D9143276F3B\"}}]', '1');
INSERT INTO `histories` VALUES ('0fd9c0e4-ae65-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"08ace22d-a63b-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"3\",\"trxpesanan_id\":\"F8EE837D-D333-47B3-A826-8D9143276F3B\"}}]', '1');
INSERT INTO `histories` VALUES ('e196bc3e-ae67-11e7-912c-54650c0f9c19', '[{\"before\":{\"trxpesanan_id\":\"F8EE837D-D333-47B3-A826-8D9143276F3B\",\"trxpesanan_produkid\":\"08ace22d-a63b-11e7-bee2-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('f26571c4-ae67-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"08ace22d-a63b-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"4\",\"trxpesanan_id\":\"F8EE837D-D333-47B3-A826-8D9143276F3B\"}}]', '1');
INSERT INTO `histories` VALUES ('f99bf6aa-ae67-11e7-912c-54650c0f9c19', '[{\"before\":{\"trxpesanan_id\":\"F8EE837D-D333-47B3-A826-8D9143276F3B\",\"trxpesanan_produkid\":\"08ace22d-a63b-11e7-bee2-54650c0f9c19\"}},{\"after\":[]}]', '1');
INSERT INTO `histories` VALUES ('327f21e2-ae6a-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpembayaran_metode\":\"1\",\"trxpembayaran_nominal\":\"54000\",\"trxpembayaran_kdbank\":\"1\",\"trxpembayaran_noref\":\"123456789\",\"trxpesanan_id\":\"F8EE837D-D333-47B3-A826-8D9143276F3B\"}}]', '1');
INSERT INTO `histories` VALUES ('3178fe2d-ae6b-11e7-912c-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpembayaran_metode\":\"1\",\"trxpembayaran_nominal\":\"54000\",\"trxpembayaran_kdbank\":\"1\",\"trxpembayaran_noref\":\"1234567890\",\"trxpesanan_id\":\"F8EE837D-D333-47B3-A826-8D9143276F3B\"}}]', '1');
INSERT INTO `histories` VALUES ('f7936462-aebb-11e7-80e0-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('4b516ab7-aebc-11e7-80e0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"B266BDEA-016B-4642-9628-6F7C86625064\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"171011214219\"}}]', '1');
INSERT INTO `histories` VALUES ('4b6dff24-aebc-11e7-80e0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"1ddf36b7-a630-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"2\",\"trxpesanan_id\":\"B266BDEA-016B-4642-9628-6F7C86625064\"}}]', '1');
INSERT INTO `histories` VALUES ('9c52919d-aebc-11e7-80e0-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('c026abba-aebc-11e7-80e0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"4DC07000-6EF4-4F08-A93E-92956C62F06C\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"171011214535\"}}]', '1');
INSERT INTO `histories` VALUES ('d425fc18-aebc-11e7-80e0-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('e4ffa746-aebc-11e7-80e0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"D9FAD796-5F33-4C38-BF65-CC5DD8283453\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"171011214637\"}}]', '1');
INSERT INTO `histories` VALUES ('1aa3a7ac-aebd-11e7-80e0-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('26beb7cd-aebd-11e7-80e0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"60A642FD-1E91-41DD-96AE-3E5A836D60C4\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"171011214827\"}}]', '1');
INSERT INTO `histories` VALUES ('26d7cb5f-aebd-11e7-80e0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"1ddf36b7-a630-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"1\",\"trxpesanan_id\":\"60A642FD-1E91-41DD-96AE-3E5A836D60C4\"}}]', '1');
INSERT INTO `histories` VALUES ('5ebf39cc-aebd-11e7-80e0-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('6cdeb874-aebd-11e7-80e0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"3456F874-821C-4680-B310-37D61586C7D1\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"171011215025\"}}]', '1');
INSERT INTO `histories` VALUES ('0f71088b-aebe-11e7-80e0-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('1a75f9a9-aebe-11e7-80e0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"0FF79D9C-6F47-49F8-A0D7-4A2F8D87ADF5\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"171011215516\"}}]', '1');
INSERT INTO `histories` VALUES ('1a9715e6-aebe-11e7-80e0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"1ddf36b7-a630-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"1\",\"trxpesanan_id\":\"0FF79D9C-6F47-49F8-A0D7-4A2F8D87ADF5\"}}]', '1');
INSERT INTO `histories` VALUES ('c7417b8b-aebf-11e7-80e0-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('d303857c-aebf-11e7-80e0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"1DC1C831-6622-4947-9EB5-FDCD26FABD05\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"171011220735\"}}]', '1');
INSERT INTO `histories` VALUES ('017a2683-aec0-11e7-80e0-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('0e680e11-aec0-11e7-80e0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"20D611A0-D236-4CF6-9F38-5C40E690969B\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"171011220915\"}}]', '1');
INSERT INTO `histories` VALUES ('48612935-aec0-11e7-80e0-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('5769a17e-aec0-11e7-80e0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"519AF5E9-E99F-4826-B617-844AA8684B32\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"171011221118\"}}]', '1');
INSERT INTO `histories` VALUES ('5783108e-aec0-11e7-80e0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"1ddf36b7-a630-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"1\",\"trxpesanan_id\":\"519AF5E9-E99F-4826-B617-844AA8684B32\"}}]', '1');
INSERT INTO `histories` VALUES ('6d8e9132-aec0-11e7-80e0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"produk_qty\":\"2\",\"trxpesanan_id\":\"519AF5E9-E99F-4826-B617-844AA8684B32\"}}]', '1');
INSERT INTO `histories` VALUES ('fea9f3a4-aec3-11e7-80e0-54650c0f9c19', '[{\"before\":{\"trxpesanan_id\":\"519AF5E9-E99F-4826-B617-844AA8684B32\",\"trxpesanan_tgl\":\"2017-10-12 03:11:18\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_status\":\"0\",\"trxpesanan_invoiceid\":\"1710112211\"}},{\"after\":{\"trxpesanan_id\":\"519AF5E9-E99F-4826-B617-844AA8684B32\"}}]', '1');
INSERT INTO `histories` VALUES ('65c68912-aec4-11e7-80e0-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('7005c141-aec4-11e7-80e0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"613C93A7-265F-49D6-AAFE-8182562725A1\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"171011224037\"}}]', '1');
INSERT INTO `histories` VALUES ('702fdb4d-aec4-11e7-80e0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"1ddf36b7-a630-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"1\",\"trxpesanan_id\":\"613C93A7-265F-49D6-AAFE-8182562725A1\"}}]', '1');
INSERT INTO `histories` VALUES ('7a6df24d-aec4-11e7-80e0-54650c0f9c19', '[{\"before\":{\"trxpesanan_id\":\"613C93A7-265F-49D6-AAFE-8182562725A1\",\"trxpesanan_tgl\":\"2017-10-12 03:40:37\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_status\":\"0\",\"trxpesanan_invoiceid\":\"1710112240\"}},{\"after\":{\"trxpesanan_id\":\"613C93A7-265F-49D6-AAFE-8182562725A1\"}}]', '1');
INSERT INTO `histories` VALUES ('984c74cf-aec4-11e7-80e0-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('a4200aa7-aec4-11e7-80e0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"F360D70C-E54F-4BBA-A7D6-7AABA906BA37\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"171011224204\"}}]', '1');
INSERT INTO `histories` VALUES ('a4463566-aec4-11e7-80e0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"1ddf36b7-a630-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"1\",\"trxpesanan_id\":\"F360D70C-E54F-4BBA-A7D6-7AABA906BA37\"}}]', '1');
INSERT INTO `histories` VALUES ('b4da43c5-aec4-11e7-80e0-54650c0f9c19', '[{\"before\":{\"trxpesanan_id\":\"F360D70C-E54F-4BBA-A7D6-7AABA906BA37\",\"trxpesanan_tgl\":\"2017-10-12 03:42:04\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_status\":\"0\",\"trxpesanan_invoiceid\":\"1710112242\"}},{\"after\":{\"trxpesanan_id\":\"F360D70C-E54F-4BBA-A7D6-7AABA906BA37\"}}]', '1');
INSERT INTO `histories` VALUES ('ccf5ccb5-aec4-11e7-80e0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"D895C1E6-E879-4DA9-9B7D-F7D2C2446C6E\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"171011224313\"}}]', '1');
INSERT INTO `histories` VALUES ('cd19c1cc-aec4-11e7-80e0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"08ace22d-a63b-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"1\",\"trxpesanan_id\":\"D895C1E6-E879-4DA9-9B7D-F7D2C2446C6E\"}}]', '1');
INSERT INTO `histories` VALUES ('d13b6ada-aec4-11e7-80e0-54650c0f9c19', '[{\"before\":{\"trxpesanan_id\":\"D895C1E6-E879-4DA9-9B7D-F7D2C2446C6E\",\"trxpesanan_tgl\":\"2017-10-12 03:43:13\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_status\":\"0\",\"trxpesanan_invoiceid\":\"1710112243\"}},{\"after\":{\"trxpesanan_id\":\"D895C1E6-E879-4DA9-9B7D-F7D2C2446C6E\"}}]', '1');
INSERT INTO `histories` VALUES ('03003f85-aec5-11e7-80e0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"31463CC4-855F-422C-BBD2-CBEC958F0BBF\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"171011224443\"}}]', '1');
INSERT INTO `histories` VALUES ('032e03cc-aec5-11e7-80e0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"1ddf36b7-a630-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"2\",\"trxpesanan_id\":\"31463CC4-855F-422C-BBD2-CBEC958F0BBF\"}}]', '1');
INSERT INTO `histories` VALUES ('0e87b4ce-aec5-11e7-80e0-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"produk_qty\":\"3\",\"trxpesanan_id\":\"31463CC4-855F-422C-BBD2-CBEC958F0BBF\"}}]', '1');
INSERT INTO `histories` VALUES ('380db96b-aeff-11e7-a09e-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('2a5c7853-af03-11e7-a09e-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('3d1439de-af03-11e7-a09e-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpesanan_id\":\"C902CC8F-86DD-4177-A2D8-B1399F494963\",\"trxpesanan_userid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_invoiceid\":\"171012061010\"}}]', '1');
INSERT INTO `histories` VALUES ('3d3fed77-af03-11e7-a09e-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"1ddf36b7-a630-11e7-bee2-54650c0f9c19\",\"produk_qty\":\"1\",\"trxpesanan_id\":\"C902CC8F-86DD-4177-A2D8-B1399F494963\"}}]', '1');
INSERT INTO `histories` VALUES ('464b8e7c-af03-11e7-a09e-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"produk_id\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"produk_qty\":\"4\",\"trxpesanan_id\":\"C902CC8F-86DD-4177-A2D8-B1399F494963\"}}]', '1');
INSERT INTO `histories` VALUES ('0b91fd90-af1c-11e7-a09e-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"trxpembayaran_metode\":\"1\",\"trxpembayaran_nominal\":\"438000\",\"trxpembayaran_kdbank\":\"1\",\"trxpembayaran_noref\":\"1234567890\",\"trxpesanan_id\":\"C902CC8F-86DD-4177-A2D8-B1399F494963\",\"trxpesanan_pointorder\":\"5\",\"trxpesanan_biayakirim\":\"20000.00\"}}]', '1');
INSERT INTO `histories` VALUES ('3a909802-af35-11e7-a09e-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('966d6ffe-afb5-11e7-a09e-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('fea29e97-afba-11e7-a09e-54650c0f9c19', '[{\"before\":{\"trxpesanan_id\":\"C902CC8F-86DD-4177-A2D8-B1399F494963\",\"trxpembayaran_status\":\"0\"}},{\"after\":{\"trxpesanan_id\":\"C902CC8F-86DD-4177-A2D8-B1399F494963\",\"trxpembayaran_verifiedby\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('51dd5aec-afbc-11e7-a09e-54650c0f9c19', '[{\"before\":{\"trxpesanan_id\":\"C902CC8F-86DD-4177-A2D8-B1399F494963\",\"trxpembayaran_status\":\"0\"}},{\"after\":{\"trxpesanan_id\":\"C902CC8F-86DD-4177-A2D8-B1399F494963\",\"trxpembayaran_verifiedby\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('c197e6cd-afbc-11e7-a09e-54650c0f9c19', '[{\"before\":{\"trxpesanan_id\":null,\"trxpembayaran_status\":null}},{\"after\":{\"trxpembayaran_note\":\"tolong dilengkapi\",\"trxpesanan_id\":\"C902CC8F-86DD-4177-A2D8-B1399F494963\",\"trxpembayaran_verifiedby\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('57844b3f-afce-11e7-a09e-54650c0f9c19', '[{\"before\":{\"trxpesanan_id\":\"C902CC8F-86DD-4177-A2D8-B1399F494963\",\"trxpembayaran_status\":\"0\"}},{\"after\":{\"trxpesanan_id\":\"C902CC8F-86DD-4177-A2D8-B1399F494963\",\"trxpembayaran_verifiedby\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\"}}]', '1');
INSERT INTO `histories` VALUES ('9c3dd0db-afe2-11e7-a09e-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('f1b703cf-aff2-11e7-a09e-54650c0f9c19', '[{\"before\":{\"trxpesanan_id\":\"C902CC8F-86DD-4177-A2D8-B1399F494963\",\"trxpesanan_qty_realisasi\":null}},{\"after\":{\"trxpesanan_id\":\"C902CC8F-86DD-4177-A2D8-B1399F494963\",\"trxpesanan_produkid\":\"1ddf36b7-a630-11e7-bee2-54650c0f9c19\",\"trxpesanan_qty_realisasi\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('f1daa401-aff2-11e7-a09e-54650c0f9c19', '[{\"before\":{\"trxpesanan_id\":\"C902CC8F-86DD-4177-A2D8-B1399F494963\",\"trxpesanan_qty_realisasi\":null}},{\"after\":{\"trxpesanan_id\":\"C902CC8F-86DD-4177-A2D8-B1399F494963\",\"trxpesanan_produkid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_qty_realisasi\":\"4\"}}]', '1');
INSERT INTO `histories` VALUES ('7f97278c-aff3-11e7-a09e-54650c0f9c19', '[{\"before\":{\"trxpesanan_id\":\"C902CC8F-86DD-4177-A2D8-B1399F494963\",\"trxpesanan_qty_realisasi\":null}},{\"after\":{\"trxpesanan_id\":\"C902CC8F-86DD-4177-A2D8-B1399F494963\",\"trxpesanan_produkid\":\"1ddf36b7-a630-11e7-bee2-54650c0f9c19\",\"trxpesanan_qty_realisasi\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('7fb088d3-aff3-11e7-a09e-54650c0f9c19', '[{\"before\":{\"trxpesanan_id\":\"C902CC8F-86DD-4177-A2D8-B1399F494963\",\"trxpesanan_qty_realisasi\":null}},{\"after\":{\"trxpesanan_id\":\"C902CC8F-86DD-4177-A2D8-B1399F494963\",\"trxpesanan_produkid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_qty_realisasi\":\"4\"}}]', '1');
INSERT INTO `histories` VALUES ('ba647723-aff3-11e7-a09e-54650c0f9c19', '[{\"before\":{\"trxpesanan_id\":\"C902CC8F-86DD-4177-A2D8-B1399F494963\",\"trxpesanan_qty_realisasi\":null}},{\"after\":{\"trxpesanan_id\":\"C902CC8F-86DD-4177-A2D8-B1399F494963\",\"trxpesanan_produkid\":\"1ddf36b7-a630-11e7-bee2-54650c0f9c19\",\"trxpesanan_qty_realisasi\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('ba989ba5-aff3-11e7-a09e-54650c0f9c19', '[{\"before\":{\"trxpesanan_id\":\"C902CC8F-86DD-4177-A2D8-B1399F494963\",\"trxpesanan_qty_realisasi\":null}},{\"after\":{\"trxpesanan_id\":\"C902CC8F-86DD-4177-A2D8-B1399F494963\",\"trxpesanan_produkid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"trxpesanan_qty_realisasi\":\"4\"}}]', '1');
INSERT INTO `histories` VALUES ('45310823-aff5-11e7-a09e-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"2b6cd002-a48e-11e7-8af8-54650c0f9c19\",\"logged_in\":true}}] ', '1');

-- ----------------------------
-- Table structure for icon
-- ----------------------------
DROP TABLE IF EXISTS `icon`;
CREATE TABLE `icon` (
  `icon_name` varchar(100) NOT NULL,
  `icon_group` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`icon_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of icon
-- ----------------------------
INSERT INTO `icon` VALUES ('glyphicon glyphicon-adjust', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-alert', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-align-center', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-align-justify', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-align-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-align-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-apple', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-arrow-down', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-arrow-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-arrow-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-arrow-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-asterisk', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-baby-formula', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-backward', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-ban-circle', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-barcode', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-bed', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-bell', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-bishop', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-bitcoin', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-blackboard', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-bold', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-book', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-bookmark', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-briefcase', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-btc', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-bullhorn', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-calendar', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-camera', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-cd', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-certificate', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-check', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-chevron-down', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-chevron-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-chevron-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-chevron-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-circle-arrow-down', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-circle-arrow-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-circle-arrow-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-circle-arrow-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-cloud', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-cloud-download', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-cloud-upload', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-cog', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-collapse-down', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-collapse-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-comment', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-compressed', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-console', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-copy', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-copyright-mark', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-credit-card', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-cutlery', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-dashboard', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-download', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-download-alt', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-duplicate', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-earphone', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-edit', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-education', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-eject', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-envelope', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-equalizer', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-erase', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-eur', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-euro', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-exclamation-sign', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-expand', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-export', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-eye-close', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-eye-open', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-facetime-video', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-fast-backward', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-fast-forward', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-file', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-film', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-filter', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-fire', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-flag', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-flash', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-floppy-disk', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-floppy-open', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-floppy-remove', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-floppy-save', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-floppy-saved', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-folder-close', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-folder-open', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-font', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-forward', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-fullscreen', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-gbp', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-gift', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-glass', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-globe', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-grain', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-hand-down', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-hand-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-hand-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-hand-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-hd-video', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-hdd', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-header', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-headphones', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-heart', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-heart-empty', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-home', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-hourglass', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-ice-lolly', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-ice-lolly-tasted', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-import', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-inbox', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-indent-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-indent-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-info-sign', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-italic', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-jpy', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-king', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-knight', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-lamp', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-leaf', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-level-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-link', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-list', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-list-alt', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-lock', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-log-in', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-log-out', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-magnet', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-map-marker', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-menu-down', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-menu-hamburger', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-menu-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-menu-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-menu-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-minus', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-minus-sign', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-modal-window', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-move', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-music', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-new-window', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-object-align-bottom', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-object-align-horizontal', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-object-align-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-object-align-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-object-align-top', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-object-align-vertical', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-off', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-oil', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-ok', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-ok-circle', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-ok-sign', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-open', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-open-file', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-option-horizontal', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-option-vertical', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-paperclip', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-paste', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-pause', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-pawn', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-pencil', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-phone', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-phone-alt', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-picture', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-piggy-bank', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-plane', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-play', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-play-circle', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-plus', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-plus-sign', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-print', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-pushpin', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-qrcode', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-queen', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-question-sign', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-random', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-record', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-refresh', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-registration-mark', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-remove', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-remove-circle', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-remove-sign', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-repeat', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-resize-full', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-resize-horizontal', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-resize-small', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-resize-vertical', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-retweet', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-road', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-rub', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-ruble', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-save', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-save-file', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-saved', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-scale', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-scissors', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-screenshot', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sd-video', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-search', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-send', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-share', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-share-alt', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-shopping-cart', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-signal', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sort', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sort-by-alphabet', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sort-by-alphabet-alt', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sort-by-attributes', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sort-by-attributes-alt', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sort-by-order', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sort-by-order-alt', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sound-5-1', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sound-6-1', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sound-7-1', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sound-dolby', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sound-stereo', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-star', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-star-empty', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-stats', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-step-backward', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-step-forward', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-stop', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-subscript', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-subtitles', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sunglasses', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-superscript', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-tag', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-tags', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-tasks', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-tent', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-text-background', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-text-color', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-text-height', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-text-size', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-text-width', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-th', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-th-large', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-th-list', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-thumbs-down', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-thumbs-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-time', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-tint', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-tower', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-transfer', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-trash', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-tree-conifer', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-tree-deciduous', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-triangle-bottom', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-triangle-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-triangle-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-triangle-top', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-unchecked', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-upload', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-usd', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-user', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-volume-down', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-volume-off', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-volume-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-warning-sign', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-wrench', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-xbt', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-yen', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-zoom-in', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-zoom-out', 'glyphicon');

-- ----------------------------
-- Table structure for languages
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `language_id` char(4) NOT NULL,
  `language_name` varchar(50) NOT NULL,
  `language_active` tinyint(4) NOT NULL,
  `language_default` tinyint(4) NOT NULL,
  PRIMARY KEY (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of languages
-- ----------------------------
INSERT INTO `languages` VALUES ('id', 'Indonesia', '1', '1');

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(50) NOT NULL,
  `menu_url` varchar(50) NOT NULL,
  `menu_icon` varchar(50) NOT NULL,
  `menu_parent` int(11) NOT NULL,
  `menu_order` smallint(6) NOT NULL,
  `menu_active` tinyint(4) NOT NULL,
  `menu_create_module` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES ('1', 'settings', '#', 'glyphicon glyphicon-cog', '0', '1', '1', '0');
INSERT INTO `menus` VALUES ('2', 'users', 'users', 'glyphicon glyphicon-user', '1', '1', '1', '0');
INSERT INTO `menus` VALUES ('3', 'roles', 'roles', 'glyphicon glyphicon-cog', '1', '2', '1', '0');
INSERT INTO `menus` VALUES ('4', 'menus', 'menus', 'glyphicon glyphicon-equalizer', '1', '3', '1', '0');
INSERT INTO `menus` VALUES ('5', 'products', 'products', 'glyphicon glyphicon-equalizer', '0', '2', '1', '0');
INSERT INTO `menus` VALUES ('6', 'keanggotaans', 'keanggotaans', 'glyphicon glyphicon-cog', '0', '3', '1', '0');
INSERT INTO `menus` VALUES ('7', 'anggotas', 'anggotas', 'glyphicon glyphicon-cog', '6', '1', '1', '0');
INSERT INTO `menus` VALUES ('8', 'reqanggotas', 'reqanggotas', 'glyphicon glyphicon-cog', '6', '2', '1', '0');
INSERT INTO `menus` VALUES ('9', 'saldos', 'saldos', 'glyphicon glyphicon-cog', '0', '4', '1', '0');
INSERT INTO `menus` VALUES ('10', 'saldos', 'saldos', 'glyphicon glyphicon-cog', '9', '1', '1', '0');
INSERT INTO `menus` VALUES ('11', 'reqsaldos', 'reqsaldos', 'glyphicon glyphicon-cog', '9', '2', '1', '0');
INSERT INTO `menus` VALUES ('12', 'trxpesanans', 'trxpesanans', 'glyphicon glyphicon-cog', '0', '5', '1', '0');
INSERT INTO `menus` VALUES ('13', 'trxpesanans', 'trxpesanans', 'glyphicon glyphicon-cog', '12', '1', '1', '0');
INSERT INTO `menus` VALUES ('14', 'trxpembayaranverify', 'trxpembayaranverify', 'glyphicon glyphicon-cog', '12', '2', '1', '0');
INSERT INTO `menus` VALUES ('15', 'trxpesananverify', 'trxpesananverify', 'glyphicon glyphicon-cog', '12', '3', '1', '0');
INSERT INTO `menus` VALUES ('16', 'trxpengirimans', 'trxpengirimans', 'glyphicon glyphicon-cog', '12', '4', '1', '0');
INSERT INTO `menus` VALUES ('17', 'trackings', 'trackings', 'glyphicon glyphicon-cog', '0', '6', '1', '0');

-- ----------------------------
-- Table structure for produk
-- ----------------------------
DROP TABLE IF EXISTS `produk`;
CREATE TABLE `produk` (
  `produk_id` varchar(36) NOT NULL,
  `produk_identity` varchar(5) NOT NULL,
  `produk_nama` varchar(100) NOT NULL,
  `produk_harga` decimal(10,2) NOT NULL,
  `produk_stok` int(11) NOT NULL,
  `produk_satuan` varchar(25) NOT NULL,
  `produk_status` bit(1) NOT NULL,
  `produk_photo` text,
  `produk_description` text,
  `produk_pointorder` int(11) DEFAULT NULL,
  PRIMARY KEY (`produk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of produk
-- ----------------------------
INSERT INTO `produk` VALUES ('08ace22d-a63b-11e7-bee2-54650c0f9c19', 'jjkl', 'klj', '9.00', '9', 'jnk', '', '15414_IMG_7206_(2).JPG', 'jjk', '2');
INSERT INTO `produk` VALUES ('1ddf36b7-a630-11e7-bee2-54650c0f9c19', 'P890', 'Silver Queen', '18000.00', '100', 'pcs', '', '25973_Desert.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.', '1');
INSERT INTO `produk` VALUES ('20650133-a63d-11e7-bee2-54650c0f9c19', 'pppp', 'pppp', '8888.00', '8', 'jkj', '\0', '', 'jk', '1');
INSERT INTO `produk` VALUES ('2b6cd002-a48e-11e7-8af8-54650c0f9c19', 'P001', 'beras', '100000.00', '5', 'karung', '', null, 'testing', '1');
INSERT INTO `produk` VALUES ('652c096d-a63a-11e7-bee2-54650c0f9c19', 'sa', 'sd', '90.00', '0', 'kkj', '', '', 'df', '1');
INSERT INTO `produk` VALUES ('6fd9a04e-a63e-11e7-bee2-54650c0f9c19', 'ret', 'ert', '3.00', '34', 'f', '', '3967_KPR.jpg', 'sdf', '1');
INSERT INTO `produk` VALUES ('95696fcd-a63e-11e7-bee2-54650c0f9c19', 'g001', 'gula edit', '8000.00', '9', 'pcs', '', '46392_237600_kn_(2).jpg', 'sadfsd', '1');
INSERT INTO `produk` VALUES ('983ebfc6-a63d-11e7-bee2-54650c0f9c19', 'jkl', 'kjk', '0.00', '0', 'k', '', '', 'kj', '2');
INSERT INTO `produk` VALUES ('b131680a-acbf-11e7-814e-54650c0f9c19', 'P003', 'Polo Shirt', '100000.00', '10', 'pcs', '', '3271_Koala.jpg', 'ini adalah fitur sebagai percontohan untuk module produk', '1');
INSERT INTO `produk` VALUES ('babc039e-a62f-11e7-bee2-54650c0f9c19', 'sda', 'sdf', '98.00', '8', 'jh', '', '', '980', '1');
INSERT INTO `produk` VALUES ('bcdf4d4e-a63a-11e7-bee2-54650c0f9c19', 'j', 'kl', '9.00', '0', 'kl', '', '', 'klj', '1');
INSERT INTO `produk` VALUES ('be469fbc-a63e-11e7-bee2-54650c0f9c19', 'asdf0', 'ddd', '45.00', '34', 'sdf', '\0', '91018_Hydrangeas.jpg', 'sdfasdf', '1');
INSERT INTO `produk` VALUES ('e2bffe8e-a5e8-11e7-bee2-54650c0f9c19', 'P002', 'telor edit', '5000.00', '5', 'kilo', '', 'asdf', 'telor ayam', '1');
INSERT INTO `produk` VALUES ('fa998123-ace0-11e7-814e-54650c0f9c19', 'p0043', 'meja belajar', '600000.00', '4', 'box', '', '16632_Tulips.jpg', 'Hey i am totally new in Codeigniter.In the Controllers folder I created a file named caller.php and I created a file home1.php in Views. In root directory i created a image folder name Images and also created a css folder named css.In images Folder there are 6 picture. In css folder style.css file exist.', '1');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(50) NOT NULL,
  `role_level` int(11) NOT NULL,
  `role_active` tinyint(4) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'SUPER ADMIN', '99', '1');
INSERT INTO `roles` VALUES ('2', 'MEMBER', '1', '1');
INSERT INTO `roles` VALUES ('3', 'VISITOR', '2', '1');

-- ----------------------------
-- Table structure for role_menus
-- ----------------------------
DROP TABLE IF EXISTS `role_menus`;
CREATE TABLE `role_menus` (
  `role_menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`role_menu_id`),
  KEY `role_id` (`role_id`),
  KEY `role_menus_ibfk_1` (`menu_id`),
  CONSTRAINT `role_menus_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`menu_id`),
  CONSTRAINT `role_menus_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of role_menus
-- ----------------------------
INSERT INTO `role_menus` VALUES ('115', '1', '1');
INSERT INTO `role_menus` VALUES ('116', '1', '2');
INSERT INTO `role_menus` VALUES ('117', '1', '3');
INSERT INTO `role_menus` VALUES ('118', '1', '4');
INSERT INTO `role_menus` VALUES ('119', '1', '5');
INSERT INTO `role_menus` VALUES ('120', '1', '6');
INSERT INTO `role_menus` VALUES ('121', '1', '7');
INSERT INTO `role_menus` VALUES ('122', '1', '8');
INSERT INTO `role_menus` VALUES ('123', '1', '9');
INSERT INTO `role_menus` VALUES ('124', '1', '10');
INSERT INTO `role_menus` VALUES ('125', '1', '11');
INSERT INTO `role_menus` VALUES ('126', '1', '12');
INSERT INTO `role_menus` VALUES ('127', '1', '13');
INSERT INTO `role_menus` VALUES ('128', '1', '14');
INSERT INTO `role_menus` VALUES ('129', '1', '15');
INSERT INTO `role_menus` VALUES ('130', '1', '16');
INSERT INTO `role_menus` VALUES ('131', '1', '17');
INSERT INTO `role_menus` VALUES ('132', '2', '15');
INSERT INTO `role_menus` VALUES ('133', '2', '16');

-- ----------------------------
-- Table structure for saldo
-- ----------------------------
DROP TABLE IF EXISTS `saldo`;
CREATE TABLE `saldo` (
  `user_id` varchar(36) NOT NULL,
  `saldo_saldo` decimal(18,2) DEFAULT NULL,
  `saldo_lastupdate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of saldo
-- ----------------------------
INSERT INTO `saldo` VALUES ('610eaee3-ad68-11e7-96d5-54650c0f9c19', '350000.00', '2017-10-11 05:27:51');
INSERT INTO `saldo` VALUES ('d2afa5b9-ad67-11e7-96d5-54650c0f9c19', '231000.00', '2017-10-11 05:29:33');

-- ----------------------------
-- Table structure for temp_anggota
-- ----------------------------
DROP TABLE IF EXISTS `temp_anggota`;
CREATE TABLE `temp_anggota` (
  `user_id` varchar(36) NOT NULL,
  `user_identity` varchar(4) DEFAULT NULL,
  `anggota_nama` varchar(100) DEFAULT NULL,
  `anggota_tmplahir` varchar(100) DEFAULT NULL,
  `anggota_tgllahir` date DEFAULT NULL,
  `anggota_alamat` text,
  `anggota_jnskelamin` varchar(1) DEFAULT NULL,
  `anggota_tglregistrasi` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `anggota_notlp` varchar(50) DEFAULT NULL,
  `anggota_email` varchar(50) DEFAULT NULL,
  `anggota_noidentitas` varchar(50) DEFAULT NULL,
  `anggota_jnsidentitas` varchar(36) DEFAULT NULL,
  `anggota_scanidentitas` text,
  `anggota_approvedby` varchar(36) DEFAULT NULL,
  `anggota_tglapprove` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `anggota_persetujuan` bit(1) DEFAULT NULL,
  `anggota_photo` text,
  `user_registerdate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `user_status` smallint(1) DEFAULT NULL,
  `anggota_note` text,
  `user_password` text,
  `anggota_areaid` int(2) DEFAULT NULL,
  `user_salt` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of temp_anggota
-- ----------------------------
INSERT INTO `temp_anggota` VALUES ('20e8af85-ad3a-11e7-96d5-54650c0f9c19', 'A002', 'jkh', 'jjklj', '2017-10-30', '9898', 'L', '2017-10-10 04:38:03', '8789', 'w@m.m', '0987', '12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '10778_Chrysanthemum.jpg', null, null, '', '43814_Desert.jpg', '2017-10-10 04:38:03', '0', null, '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', null, null);
INSERT INTO `temp_anggota` VALUES ('2f8afe30-ad60-11e7-96d5-54650c0f9c19', 'A007', 'Tantan', 'bandung', '2017-10-13', 'jatihandap bandung', 'L', '2017-10-10 09:39:13', '0987654321', 'tan@mail.com', '1234567890', '12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '65197_Chrysanthemum.jpg', null, '2017-10-10 09:39:13', '', '12979_Desert.jpg', '2017-10-10 09:39:13', '2', 'tolong diulangi', 'b03ddf3ca2e714a6548e7495e2a03f5e824eaac9837cd7f159c67b90fb4b7342', '1', null);
INSERT INTO `temp_anggota` VALUES ('5164e61a-ad68-11e7-96d5-54650c0f9c19', 'A009', 'T Suryana', 'Bandung', '2017-10-16', 'The Green', 'L', '2017-10-10 10:09:08', '098765432', 'tan@mail.com', '1234567890', '12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '9042_Koala.jpg', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '2017-10-10 10:09:08', '', '81103_Penguins.jpg', '2017-10-10 10:09:08', '1', null, '6f705103c782bb4de4d0bbcb51faa0bb98fccbbf5530d7ed97e30c28b02008b1', '2', '58637');
INSERT INTO `temp_anggota` VALUES ('62d77e15-ad64-11e7-96d5-54650c0f9c19', 'A008', 'Tantan Suryana', 'Bandung', '2017-10-06', 'Green City View', 'L', '2017-10-10 10:05:09', '086543222', 'tan@mail.com', '123456789', '12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '28280_Hydrangeas.jpg', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '2017-10-10 10:05:09', '', '28076_Jellyfish.jpg', '2017-10-10 10:05:09', '1', null, '98e1201a4236fa48192f5e279113949658dbbe970661bd99281953a172aaed4f', '2', null);
INSERT INTO `temp_anggota` VALUES ('9b9633c1-ad39-11e7-96d5-54650c0f9c19', 'A001', 'poiu', 'jjh', '2017-10-10', 'kjklj', 'L', '2017-10-10 13:55:58', '0987', 't@m.m', '09876', '12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '5560_Chrysanthemum.jpg', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '2017-10-10 13:55:57', '', '71871_Desert.jpg', '2017-10-10 13:55:58', '1', null, null, null, null);
INSERT INTO `temp_anggota` VALUES ('bb7a6b78-ad87-11e7-bf38-54650c0f9c19', 'A111', 'ppp', 'pppp', '2017-10-04', 'asf', 'P', '2017-10-10 13:53:42', '987', '6@m.m', '789', '12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '2691_Desert.jpg', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '2017-10-10 13:53:42', '', '77938_Hydrangeas.jpg', '2017-10-10 13:53:42', '1', null, '032688908ac08732935afd2d3d8f71425ac27d47bc7e5ba52c0e8d4d5e9fdfab', '2', '98550');
INSERT INTO `temp_anggota` VALUES ('c18f5f3f-ad82-11e7-bf38-54650c0f9c19', 'A010', 'tes', 'bandung', '2017-10-12', 'tes', 'P', '2017-10-10 13:18:06', '09876543', 't@m.m', '2345', '13EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '74084_Penguins.jpg', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '2017-10-10 13:18:06', '', '31631_Tulips.jpg', '2017-10-10 13:18:06', '1', null, '317f24673140b837003a2a3fe3dda7ac308d602d613c0f98379aa08b8756a1ff', '1', '55994');
INSERT INTO `temp_anggota` VALUES ('ea861c0f-a7e9-11e7-bbff-54650c0f9c19', 'tes2', 'testing 2', 'sdf', '2017-10-11', 'sadf', 'L', '2017-10-03 14:21:55', '098', 'lkjl', '0987', '12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '3121_Jellyfish.jpg', null, '2017-10-03 14:21:55', '', '17028_Hydrangeas.jpg', '2017-10-03 14:21:55', '2', 'tidak layak', null, null, null);

-- ----------------------------
-- Table structure for trxpengiriman
-- ----------------------------
DROP TABLE IF EXISTS `trxpengiriman`;
CREATE TABLE `trxpengiriman` (
  `trxpesanan_id` varchar(36) NOT NULL,
  `trxpengiriman_tglkirim` date DEFAULT NULL,
  `trxpengiriman_ekspedisi` int(11) DEFAULT NULL,
  `trxpengiriman_noresi` varchar(100) DEFAULT NULL,
  `trxpengiriman_status` smallint(1) DEFAULT NULL,
  `trxpengiriman_verifiedby` varchar(36) DEFAULT NULL,
  `trxpengiriman_verifieddate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `trxpengiriman_note` text,
  PRIMARY KEY (`trxpesanan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of trxpengiriman
-- ----------------------------
INSERT INTO `trxpengiriman` VALUES ('7F039353-7DC2-459E-95D9-B65193B70CAB', '2017-10-02', '1', '987654321', '0', null, '2017-10-08 08:02:04', null);

-- ----------------------------
-- Table structure for trxpesanan
-- ----------------------------
DROP TABLE IF EXISTS `trxpesanan`;
CREATE TABLE `trxpesanan` (
  `trxpesanan_id` varchar(36) NOT NULL,
  `trxpesanan_tgl` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `trxpesanan_userid` varchar(36) DEFAULT NULL,
  `trxpesanan_status` tinyint(1) DEFAULT NULL,
  `trxpesanan_invoiceid` varchar(10) DEFAULT NULL,
  `trxpesanan_pointorder` int(11) DEFAULT NULL,
  `trxpesanan_biayakirim` decimal(18,2) DEFAULT NULL,
  PRIMARY KEY (`trxpesanan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of trxpesanan
-- ----------------------------
INSERT INTO `trxpesanan` VALUES ('C902CC8F-86DD-4177-A2D8-B1399F494963', '2017-10-13 15:51:47', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '4', '1710120610', '5', '20000.00');

-- ----------------------------
-- Table structure for trxpesanan_detail
-- ----------------------------
DROP TABLE IF EXISTS `trxpesanan_detail`;
CREATE TABLE `trxpesanan_detail` (
  `trxpesanan_id` varchar(36) NOT NULL,
  `trxpesanan_produkid` varchar(36) NOT NULL,
  `trxpesanan_qty` int(11) DEFAULT NULL,
  `trxpesanan_qty_realisasi` int(11) DEFAULT NULL,
  `trxpesanan_verifiedby` varchar(36) DEFAULT NULL,
  `trxpesanan_verifieddate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`trxpesanan_id`,`trxpesanan_produkid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of trxpesanan_detail
-- ----------------------------
INSERT INTO `trxpesanan_detail` VALUES ('C902CC8F-86DD-4177-A2D8-B1399F494963', '1ddf36b7-a630-11e7-bee2-54650c0f9c19', '1', '1', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '2017-10-13 15:51:47');
INSERT INTO `trxpesanan_detail` VALUES ('C902CC8F-86DD-4177-A2D8-B1399F494963', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '4', '4', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '2017-10-13 15:51:48');

-- ----------------------------
-- Table structure for trxpesanan_pembayaran
-- ----------------------------
DROP TABLE IF EXISTS `trxpesanan_pembayaran`;
CREATE TABLE `trxpesanan_pembayaran` (
  `trxpesanan_id` varchar(36) NOT NULL,
  `trxpembayaran_metode` varchar(36) DEFAULT NULL,
  `trxpembayaran_status` smallint(1) DEFAULT NULL,
  `trxpembayaran_tgl` date DEFAULT NULL,
  `trxpembayaran_nominal` decimal(18,2) DEFAULT NULL,
  `trxpembayaran_kdbank` varchar(3) DEFAULT NULL,
  `trxpembayaran_noref` varchar(100) DEFAULT NULL,
  `trxpembayaran_verifiedby` varchar(36) DEFAULT NULL,
  `trxpembayaran_verifieddate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `trxpembayaran_note` text,
  PRIMARY KEY (`trxpesanan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of trxpesanan_pembayaran
-- ----------------------------
INSERT INTO `trxpesanan_pembayaran` VALUES ('C902CC8F-86DD-4177-A2D8-B1399F494963', '1', '1', '2017-10-12', '438000.00', '1', '1234567890', '2b6cd002-a48e-11e7-8af8-54650c0f9c19', '2017-10-13 11:24:09', 'tolong dilengkapi');

-- ----------------------------
-- Table structure for trxsaldo
-- ----------------------------
DROP TABLE IF EXISTS `trxsaldo`;
CREATE TABLE `trxsaldo` (
  `trxsaldo_id` varchar(36) NOT NULL,
  `trxsaldo_user_id` varchar(36) DEFAULT NULL,
  `trxsaldo_tgl` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `trxsaldo_nominal` decimal(18,2) DEFAULT NULL,
  `trxsaldo_kdbank` varchar(3) DEFAULT NULL,
  `trxsaldo_noref` varchar(100) DEFAULT NULL,
  `trxsaldo_status` tinyint(1) DEFAULT NULL,
  `trxsaldo_verifiedby` varchar(36) DEFAULT NULL,
  `trxsaldo_verifieddate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `trxsaldo_note` text,
  `trxsaldo_methode` int(1) DEFAULT NULL,
  PRIMARY KEY (`trxsaldo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of trxsaldo
-- ----------------------------
INSERT INTO `trxsaldo` VALUES ('04df1435-ad9d-11e7-bf38-54650c0f9c19', '610eaee3-ad68-11e7-96d5-54650c0f9c19', '2017-10-10 17:01:40', '100000.00', '', '10102017162546', '1', 'adm1', '2017-10-10 17:01:40', '', '2');
INSERT INTO `trxsaldo` VALUES ('3b1f40f0-ae0a-11e7-b4b4-54650c0f9c19', '610eaee3-ad68-11e7-96d5-54650c0f9c19', '2017-10-11 05:27:51', '250000.00', '', '1110201752734', '1', 'adm1', '2017-10-11 05:27:51', '', '2');
INSERT INTO `trxsaldo` VALUES ('5fce8e29-ae0a-11e7-b4b4-54650c0f9c19', 'd2afa5b9-ad67-11e7-96d5-54650c0f9c19', '2017-10-11 05:29:33', '231000.00', '1', '123123123', '1', 'adm1', '2017-10-11 05:29:33', '', '1');
INSERT INTO `trxsaldo` VALUES ('6c5418a5-ae0a-11e7-b4b4-54650c0f9c19', 'd2afa5b9-ad67-11e7-96d5-54650c0f9c19', '2017-10-11 05:30:31', '100000.00', '', '1110201752857', '2', 'adm1', '2017-10-11 05:30:31', 'dibatalkan', '2');
INSERT INTO `trxsaldo` VALUES ('9e43774b-ae09-11e7-b4b4-54650c0f9c19', '610eaee3-ad68-11e7-96d5-54650c0f9c19', '2017-10-11 05:25:27', '100000.00', '1', '123456789', '1', 'adm1', '2017-10-11 05:25:27', '', '1');
INSERT INTO `trxsaldo` VALUES ('b3ee47b3-ad9c-11e7-bf38-54650c0f9c19', '610eaee3-ad68-11e7-96d5-54650c0f9c19', '2017-10-10 16:59:05', '900000.00', '1', '987654321', '2', 'adm1', '2017-10-10 16:59:05', 'asdf', '1');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` varchar(36) NOT NULL,
  `user_identity` varchar(25) DEFAULT NULL,
  `user_role_id` int(11) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_registerdate` timestamp NULL DEFAULT NULL,
  `user_salt` varchar(5) DEFAULT NULL,
  `user_password` text,
  `user_status` bit(1) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `role_id` (`user_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1169f43c-ad88-11e7-bf38-54650c0f9c19', 'A001', '2', 'poiu', '2017-10-10 13:55:57', null, null, '');
INSERT INTO `users` VALUES ('277d788c-aca0-11e7-814e-54650c0f9c19', 'tantan2', '2', 'Surya', '2017-10-09 10:15:52', '32967', 'b03ddf3ca2e714a6548e7495e2a03f5e824eaac9837cd7f159c67b90fb4b7342', '');
INSERT INTO `users` VALUES ('2b6cd002-a48e-11e7-8af8-54650c0f9c19', 'adm1', '1', 'administrator', '2017-09-29 03:46:57', '79760', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', '');
INSERT INTO `users` VALUES ('396440f8-ac5f-11e7-902b-54650c0f9c19', 'testinguserpanjang2', '2', 'Suryana Tantan', '2017-10-09 02:31:04', '39301', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', '');
INSERT INTO `users` VALUES ('5a7c3a86-ac9c-11e7-814e-54650c0f9c19', 'tanz117', '1', 'tantan suryana ST', '2017-10-09 09:48:39', '98958', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', '');
INSERT INTO `users` VALUES ('610eaee3-ad68-11e7-96d5-54650c0f9c19', 'A009', '2', 'T Suryana', '2017-10-10 10:09:08', '79760', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', '');
INSERT INTO `users` VALUES ('b489c5a5-abf3-11e7-ac4a-54650c0f9c19', 'tes9', '1', 'testing 9', '2017-10-08 13:41:26', '55075', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', '\0');
INSERT INTO `users` VALUES ('c0bedf89-ad87-11e7-bf38-54650c0f9c19', 'A111', '2', 'ppp', '2017-10-10 13:53:42', '95705', '4575f7a0acd773850cc300fff7ab2ef8420138e4a0e98a51425afaf4125a0be7', '');
INSERT INTO `users` VALUES ('c7ae2667-ad82-11e7-bf38-54650c0f9c19', 'A010', '2', 'tes', '2017-10-10 13:18:06', '08944', '33b06187c914a054c2c507fc70b1bf03594eac0ff71107c9025d71c99f671a76', '');
INSERT INTO `users` VALUES ('cd329b31-abf3-11e7-ac4a-54650c0f9c19', '1234', '1', '12345654321', '2017-10-08 13:42:07', '55212', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', '\0');
INSERT INTO `users` VALUES ('d2afa5b9-ad67-11e7-96d5-54650c0f9c19', 'A008', '2', 'Tantan Suryana', '2017-10-10 10:05:09', null, '98e1201a4236fa48192f5e279113949658dbbe970661bd99281953a172aaed4f', '');
INSERT INTO `users` VALUES ('f9c996cc-ac5e-11e7-902b-54650c0f9c19', 'testingdenganidpanjang4', '1', 'Tantan Suryana', '2017-10-09 02:29:17', '50004', '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5', '');

-- ----------------------------
-- View structure for view_menu_list
-- ----------------------------
DROP VIEW IF EXISTS `view_menu_list`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `view_menu_list` AS SELECT a.menu_name,(CASE WHEN menu_active = '1' THEN 'active' ELSE 'inactive' END) AS menu_active_label,
	(CASE WHEN menu_parent = 0 THEN
		'PARENT'
	ELSE
		(SELECT menu_name FROM menus WHERE menu_id = a.menu_parent)
	END) AS menus
	FROM menus a ;

-- ----------------------------
-- View structure for view_saldo_summary
-- ----------------------------
DROP VIEW IF EXISTS `view_saldo_summary`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `view_saldo_summary` AS SELECT
		a.user_id,
		c.user_identity,
		b.anggota_nama,
		a.saldo_lastupdate,
		a.saldo_saldo,
		(SELECT SUM(trxsaldo_nominal) FROM trxsaldo WHERE trxsaldo_user_id = a.user_id AND trxsaldo_status = 1 AND trxsaldo_tgl > '2017-10-11') AS total_trxsaldo,
		(SELECT SUM(trxsaldo_nominal) FROM trxsaldo) AS total_trxpemesanan
	FROM
		saldo a
	LEFT JOIN anggota b ON a.user_id = b.user_id
	LEFT JOIN users c ON a.user_id = c.user_id ;

-- ----------------------------
-- View structure for view_trxpembayaran
-- ----------------------------
DROP VIEW IF EXISTS `view_trxpembayaran`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `view_trxpembayaran` AS SELECT
	a.*,c.anggota_nama,d.configuration_value AS metodepembayaran_label,e.bank_nama
FROM
	trxpesanan_pembayaran a
INNER JOIN trxpesanan b ON a.trxpesanan_id = b.trxpesanan_id
INNER JOIN anggota c ON b.trxpesanan_userid = c.user_id
LEFT JOIN configurations d ON a.trxpembayaran_metode = d.configuration_id
LEFT JOIN bank e ON a.trxpembayaran_kdbank = e.bank_id ;

-- ----------------------------
-- View structure for view_trxsaldo
-- ----------------------------
DROP VIEW IF EXISTS `view_trxsaldo`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `view_trxsaldo` AS SELECT
	a.*,c.user_identity,b.anggota_nama,d.bank_nama,e.configuration_value AS trxsaldo_status_label,(CASE WHEN trxsaldo_methode = 1 THEN 'Transfer' ELSE 'Tunai' END) AS trxsaldo_methode_label
FROM
	trxsaldo a
LEFT JOIN anggota b ON a.trxsaldo_user_id = b.user_id
LEFT JOIN users c ON a.trxsaldo_user_id = c.user_id
LEFT JOIN bank d ON a.trxsaldo_kdbank = d.bank_id
LEFT JOIN configurations e ON a.trxsaldo_status = e.configuration_key
WHERE
	e.configurationgroup_id = '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6H' ;

-- ----------------------------
-- View structure for view_trxsaldo_1
-- ----------------------------
DROP VIEW IF EXISTS `view_trxsaldo_1`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `view_trxsaldo_1` AS SELECT
	a.*,c.user_identity,b.anggota_nama,d.bank_nama,e.configuration_value AS trxsaldo_status_label
FROM
	trxsaldo a
LEFT JOIN anggota b ON a.trxsaldo_user_id = b.user_id
LEFT JOIN users c ON a.trxsaldo_user_id = c.user_id
LEFT JOIN bank d ON a.trxsaldo_kdbank = d.bank_id
LEFT JOIN configurations e ON a.trxsaldo_status = e.configuration_key
WHERE
	e.configurationgroup_id = '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6H' ;

-- ----------------------------
-- View structure for view_trxsaldo_2
-- ----------------------------
DROP VIEW IF EXISTS `view_trxsaldo_2`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `view_trxsaldo_2` AS SELECT
	a.*,c.user_identity,b.anggota_nama,d.bank_nama,e.configuration_value AS trxsaldo_status_label,(CASE WHEN trxsaldo_methode = 1 THEN 'Transfer' ELSE 'Saldo' END) AS trxsaldo_mthode_label
FROM
	trxsaldo a
LEFT JOIN anggota b ON a.trxsaldo_user_id = b.user_id
LEFT JOIN users c ON a.trxsaldo_user_id = c.user_id
LEFT JOIN bank d ON a.trxsaldo_kdbank = d.bank_id
LEFT JOIN configurations e ON a.trxsaldo_status = e.configuration_key
WHERE
	e.configurationgroup_id = '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6H' ;

-- ----------------------------
-- Procedure structure for log_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `log_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `log_insert`(IN id_status varchar(20), IN user_id varchar(36),IN ip_address varchar(16),IN browser varchar(255),IN p_status varchar(4),IN method_name varchar(25),IN module_name varchar(32),IN history_detail TEXT)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @is_success = @success;
			SET @history_id = @last_history_id;

				IF @is_success = 1 THEN
					CALL sp_audit_trail_insert (user_id,ip_address,browser,@history_id,p_status,method_name,module_name,id_status,@success);
					SET @is_success = @success;
					
					IF @is_success = 1 THEN
						COMMIT;
					END IF;
				END IF;
		SELECT @is_success AS jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_anggota_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_anggota_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_anggota_insert`(IN p_user_identity varchar(4),  IN p_anggota_nama varchar(100),  IN p_anggota_tmplahir varchar(100),  IN p_anggota_tgllahir date,  IN p_anggota_alamat text,  IN p_anggota_jnskelamin varchar(1),  IN p_anggota_notlp varchar(50),  IN p_anggota_email varchar(50),  IN p_anggota_jnsidentitas varchar(36),  IN p_anggota_noidentitas varchar(50),  IN p_anggota_persetujuan BIT,  IN p_anggota_scanidentitas text,  IN p_anggota_photo text,IN p_anggota_areaid INT,IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text,IN user_salt varchar(5),IN user_password text,IN user_status BIT)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @user_id = (SELECT UUID());
			SET @currentdate = (SELECT NOW());

			INSERT INTO anggota(user_id,anggota_nama,anggota_tmplahir,anggota_tgllahir,anggota_alamat,anggota_jnskelamin,anggota_tglregistrasi,anggota_notlp,anggota_email,anggota_noidentitas,anggota_jnsidentitas,anggota_scanidentitas,anggota_persetujuan,anggota_photo,anggota_approvedby,anggota_tglapprove,anggota_areaid)
				VALUES(@user_id,p_anggota_nama,p_anggota_tmplahir,p_anggota_tgllahir,p_anggota_alamat,p_anggota_jnskelamin,@currentdate,p_anggota_notlp,p_anggota_email,p_anggota_noidentitas,p_anggota_jnsidentitas,p_anggota_scanidentitas,p_anggota_persetujuan,p_anggota_photo,userid,@currentdate,p_anggota_areaid);

			INSERT INTO users(user_id,user_identity, user_role_id,user_name,user_registerdate,user_salt,user_password,user_status)
				VALUES(@user_id,p_user_identity,'2',p_anggota_nama,@currentdate,user_salt,user_password,user_status);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,@user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_anggota_resetpassword
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_anggota_resetpassword`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_anggota_resetpassword`(IN p_user_id varchar(36),IN p_user_password text,IN p_user_salt varchar(5),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			UPDATE users
			SET
				user_password = p_user_password,
				user_salt = p_user_salt
			WHERE
				user_id = p_user_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_anggota_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_anggota_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_anggota_update`(IN p_user_id varchar(36),IN p_user_identity varchar(4),  IN p_anggota_nama varchar(100),  IN p_anggota_tmplahir varchar(100),  IN p_anggota_tgllahir date,  IN p_anggota_alamat text,  IN p_anggota_jnskelamin varchar(1),  IN p_anggota_notlp varchar(50),  IN p_anggota_email varchar(50),  IN p_anggota_jnsidentitas varchar(36),  IN p_anggota_noidentitas varchar(50),  IN p_anggota_persetujuan BIT,  IN p_anggota_scanidentitas text,  IN p_anggota_photo text,IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			UPDATE anggota
			SET
				anggota_nama = p_anggota_nama,
				anggota_tmplahir = p_anggota_tmplahir,
				anggota_tgllahir = p_anggota_tgllahir,
				anggota_alamat = p_anggota_alamat,
				anggota_jnskelamin = p_anggota_jnskelamin,
				anggota_notlp = p_anggota_notlp,
				anggota_email = p_anggota_email,
				anggota_jnsidentitas = p_anggota_jnsidentitas,
				anggota_noidentitas = p_anggota_noidentitas,
				anggota_persetujuan = p_anggota_persetujuan,
				anggota_scanidentitas = p_anggota_scanidentitas,
				anggota_photo = p_anggota_photo
			WHERE
				user_id = p_user_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_audit_trail_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_audit_trail_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_audit_trail_insert`(IN user_id varchar(36), IN ip_address varchar(16),
 IN browser varchar(255), IN history_id VARCHAR(36), IN p_status varchar(4), IN action_id varchar(25), IN module_id varchar(32),IN record_id varchar(36), OUT is_success BIT)
BEGIN 
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @audit_id = (SELECT UUID());

			INSERT INTO audit_trails
				VALUES(@audit_id, CURRENT_TIMESTAMP, user_id, ip_address, browser, history_id, p_status, action_id, module_id, record_id);

			SET is_success = 1;
		COMMIT;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_get_saldo_summary
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_get_saldo_summary`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_saldo_summary`(IN p_user_id varchar(36))
BEGIN
	#Routine body goes here...
	SELECT
		a.user_id,
		c.user_identity,
		b.anggota_nama,
		a.saldo_lastupdate,
		a.saldo_saldo,
		(SELECT SUM(trxsaldo_nominal) FROM trxsaldo) AS total_trxsaldo,
		(SELECT SUM(trxsaldo_nominal) FROM trxsaldo) AS total_trxpemesanan
	FROM
		saldo a
	LEFT JOIN anggota b ON a.user_id = b.user_id
	LEFT JOIN users c ON a.user_id = c.user_id
	WHERE
		a.user_id = p_user_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_histories_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_histories_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_histories_insert`(IN history_detail text, IN history_active INT, OUT is_success BIT,OUT history_id varchar(36))
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @history_id = (SELECT UUID());

			INSERT INTO histories(history_id,history_detail, history_active)
				VALUES(@history_id,history_detail, history_active);

			SET is_success = 1;
			SET history_id = @history_id;
		COMMIT;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_icon_list
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_icon_list`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_icon_list`()
BEGIN
	#Routine body goes here...
	SELECT icon_name FROM icon ORDER BY icon_name ASC;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_menu_list
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_menu_list`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_menu_list`()
BEGIN
	#Routine body goes here...
	SELECT a.*,(CASE WHEN menu_active = '1' THEN 'active' ELSE 'inactive' END) AS menu_active_label,
	(CASE WHEN menu_parent = 0 THEN
		'PARENT'
	ELSE
		(SELECT menu_name FROM menus WHERE menu_id = a.menu_parent)
	END) AS menus
	FROM menus a;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_menu_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_menu_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_menu_update`(IN p_menu_id INT,IN p_menu_icon varchar(50),IN p_menu_active tinyint,IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

	START TRANSACTION;
		UPDATE menus
		SET	
			menu_icon = p_menu_icon,
			menu_active = p_menu_active
		WHERE menu_id = p_menu_id;

		CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
		SET @histories_success = @success;
		SET @history_id = @last_history_id;

		IF @histories_success = 1 THEN
			CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_menu_id,@success);
			SET @is_success = @success;
		END IF;

		SET @is_success = 1;
	COMMIT;
	SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_produk_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_produk_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_produk_insert`(IN p_produk_identity varchar(5),IN p_produk_nama varchar(100),IN p_produk_harga decimal(10,2),IN p_produk_stok INT,IN p_produk_satuan varchar(25),IN p_produk_photo text,IN p_produk_description text,IN p_produk_status BIT,IN p_produk_pointorder INT,IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @produk_id = (SELECT UUID());

			INSERT INTO produk(produk_id,produk_identity,produk_nama,produk_harga,produk_stok,produk_satuan,produk_status,produk_photo,produk_description,produk_pointorder)
				VALUES(@produk_id,p_produk_identity,p_produk_nama,p_produk_harga,p_produk_stok,p_produk_satuan,p_produk_status,p_produk_photo,p_produk_description,p_produk_pointorder);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,@produk_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_produk_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_produk_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_produk_update`(IN p_produk_id varchar(36),IN p_produk_identity varchar(5),IN p_produk_nama varchar(100),IN p_produk_harga decimal(10,2),IN p_produk_stok INT,IN p_produk_satuan varchar(25),IN p_produk_photo text,IN p_produk_description text,IN p_produk_status BIT,IN p_produk_pointorder INT,IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			UPDATE produk
			SET
				produk_identity = p_produk_identity,
				produk_nama = p_produk_nama,
				produk_harga = p_produk_harga,
				produk_stok = p_produk_stok,
				produk_satuan = p_produk_satuan,
				produk_photo = p_produk_photo,
				produk_description = p_produk_description,
				produk_status = p_produk_status,
				produk_pointorder = p_produk_pointorder
			WHERE
				produk_id = p_produk_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_produk_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_reqsaldo_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_reqsaldo_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reqsaldo_insert`(IN p_trxsaldo_noref varchar(100),  IN p_trxsaldo_kdbank varchar(3),  IN p_trxsaldo_nominal decimal(18,2),IN p_trxsaldo_user_id varchar(36),IN p_trxsaldo_methode INT,IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @trxsaldo_id = (SELECT UUID());
			SET @currentdate = (SELECT NOW());

			INSERT INTO trxsaldo(trxsaldo_id,trxsaldo_user_id,trxsaldo_tgl,trxsaldo_nominal,trxsaldo_kdbank,trxsaldo_noref,trxsaldo_status,trxsaldo_methode)
				VALUES(@trxsaldo_id,p_trxsaldo_user_id,@currentdate,p_trxsaldo_nominal,p_trxsaldo_kdbank,p_trxsaldo_noref,0,p_trxsaldo_methode);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,@trxsaldo_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_role_list
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_role_list`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_role_list`()
BEGIN
	#Routine body goes here...
	SELECT 
		c.role_id,
		c.role_name,
		role_level,
		role_active,
		CASE WHEN role_active = '1' THEN 'active' ELSE 'inactive' END AS role_active_label,
		GROUP_CONCAT(b.menu_name SEPARATOR ', ') AS roles
	FROM role_menus a
	LEFT JOIN menus b ON a.menu_id = b.menu_id
	RIGHT JOIN roles c ON a.role_id = c.role_id
	GROUP BY role_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_role_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_role_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_role_update`(IN p_role_id INT,IN p_menu_id varchar(255),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

	START TRANSACTION;
		DELETE FROM role_menus WHERE role_id = p_role_id;

		iterator:
		LOOP
			IF LENGTH(TRIM(p_menu_id)) = 0 OR p_menu_id IS NULL THEN
				LEAVE iterator;
			END IF;

			SET @next = SUBSTRING_INDEX(p_menu_id,',',1);
			SET @nextlen = LENGTH(@next);
			SET @value = TRIM(@next);
			INSERT INTO role_menus (role_id,menu_id) VALUES (p_role_id,@next);
			SET p_menu_id = INSERT(p_menu_id,1,@nextlen + 1,'');

		END LOOP;

		CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
		SET @histories_success = @success;
		SET @history_id = @last_history_id;

		IF @histories_success = 1 THEN
			CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_role_id,@success);
			SET @is_success = @success;
		END IF;

		SET @is_success = 1;
	COMMIT;
	SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_tempanggota_approval
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_tempanggota_approval`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_tempanggota_approval`(IN p_user_id varchar(36),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @currentdate = (SELECT NOW());

			SELECT 
				user_identity,anggota_nama,anggota_tmplahir,anggota_tgllahir,anggota_alamat,anggota_jnskelamin,anggota_notlp,
				anggota_email,anggota_jnsidentitas,anggota_noidentitas,anggota_persetujuan,anggota_scanidentitas,anggota_photo,anggota_areaid,
				user_salt,user_password
			INTO
				@user_identity,@anggota_nama,@anggota_tmplahir,@anggota_tgllahir,@anggota_alamat,@anggota_jnskelamin,@anggota_notlp,
				@anggota_email,@anggota_jnsidentitas,@anggota_noidentitas,@anggota_persetujuan,@anggota_scanidentitas,@anggota_photo,@anggota_areaid,
				@user_salt,@user_password
			FROM 
				temp_anggota
			WHERE
				user_id = p_user_id;

			CALL sp_anggota_insert(@user_identity,@anggota_nama,@anggota_tmplahir,@anggota_tgllahir,@anggota_alamat,@anggota_jnskelamin,
				@anggota_notlp,@anggota_email,@anggota_jnsidentitas,@anggota_noidentitas,@anggota_persetujuan,@anggota_scanidentitas,
				@anggota_photo,@anggota_areaid,userid,ip_address,browser,p_status,method_name,module_name,history_detail ,@user_salt,@user_password,1);
			
			UPDATE 
				temp_anggota 
			SET 
				user_status = 1,
				anggota_approvedby = userid,
				anggota_tglapprove = @currentdate
			WHERE 
				user_id = p_user_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_tempanggota_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_tempanggota_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_tempanggota_insert`(IN p_user_identity varchar(4),  IN p_anggota_nama varchar(100),  IN p_anggota_tmplahir varchar(100),  IN p_anggota_tgllahir date,  IN p_anggota_alamat text,  IN p_anggota_jnskelamin varchar(1),  IN p_anggota_notlp varchar(50),  IN p_anggota_email varchar(50),  IN p_anggota_jnsidentitas varchar(36),  IN p_anggota_noidentitas varchar(50),  IN p_anggota_persetujuan BIT,  IN p_anggota_scanidentitas text,  IN p_anggota_photo text,IN p_user_password text,IN p_anggota_areaid INT,IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text,IN user_salt varchar(5),IN user_password text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @user_id = (SELECT UUID());
			SET @currentdate = (SELECT NOW());

			INSERT INTO temp_anggota(user_id,user_identity,anggota_nama,anggota_tmplahir,anggota_tgllahir,anggota_alamat,anggota_jnskelamin,anggota_tglregistrasi,anggota_notlp,anggota_email,anggota_noidentitas,anggota_jnsidentitas,anggota_scanidentitas,anggota_persetujuan,anggota_photo,user_registerdate,user_status,user_password,anggota_areaid,user_salt)
				VALUES(@user_id,p_user_identity,p_anggota_nama,p_anggota_tmplahir,p_anggota_tgllahir,p_anggota_alamat,p_anggota_jnskelamin,@currentdate,p_anggota_notlp,p_anggota_email,p_anggota_noidentitas,p_anggota_jnsidentitas,p_anggota_scanidentitas,p_anggota_persetujuan,p_anggota_photo,@currentdate,'0',p_user_password,p_anggota_areaid,user_salt);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,@user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_tempanggota_reject
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_tempanggota_reject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_tempanggota_reject`(IN p_user_id varchar(36),IN p_anggota_note text,IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
					
			UPDATE temp_anggota SET user_status = 2,anggota_note = p_anggota_note WHERE user_id = p_user_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trxpengiriman_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trxpengiriman_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trxpengiriman_insert`(IN p_trxpesanan_id varchar(36),IN p_trxpengiriman_ekspedisi INT, IN p_trxpengiriman_tglkirim date,  IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;

			INSERT INTO trxpengiriman(trxpesanan_id,trxpengiriman_tglkirim,trxpengiriman_ekspedisi,trxpengiriman_status)
				VALUES(p_trxpesanan_id,p_trxpengiriman_tglkirim,p_trxpengiriman_ekspedisi,0);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_trxpesanan_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trxpengiriman_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trxpengiriman_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trxpengiriman_update`(IN p_trxpesanan_id varchar(36),IN p_trxpengiriman_noresi varchar(100),  IN userid varchar(4),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;

			UPDATE
				trxpengiriman
			SET
				trxpengiriman_noresi = p_trxpengiriman_noresi
			WHERE
				trxpesanan_id = p_trxpesanan_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_trxpesanan_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trxpesanandetail_delete
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trxpesanandetail_delete`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trxpesanandetail_delete`(IN p_trxpesanan_id varchar(36), IN p_trxpesanan_produkid varchar(36),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			DELETE FROM
				trxpesanan_detail
			WHERE
				trxpesanan_id = p_trxpesanan_id
				AND trxpesanan_produkid = p_trxpesanan_produkid;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_trxpesanan_produkid,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trxpesanandetail_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trxpesanandetail_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trxpesanandetail_insert`(IN p_trxpesanan_id varchar(36), IN p_produk_id varchar(36),  IN p_produk_qty INT,IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;

			INSERT INTO trxpesanan_detail(trxpesanan_id,trxpesanan_produkid,trxpesanan_qty)
				VALUES(p_trxpesanan_id,p_produk_id, p_produk_qty);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_produk_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trxpesanandetail_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trxpesanandetail_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trxpesanandetail_update`(IN p_trxpesanan_id varchar(36), IN p_produk_id varchar(36),  IN p_produk_qty INT,IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			/*SET @totalQty = (SELECT
												trxpesanan_qty
											FROM
												trxpesanan_detail
											WHERE
												trxpesanan_id = p_trxpesanan_id
												AND trxpesanan_produkid = p_produk_id) + p_produk_qty;*/

			UPDATE trxpesanan_detail
			SET
				trxpesanan_qty = p_produk_qty
			WHERE
				trxpesanan_id = p_trxpesanan_id
				AND trxpesanan_produkid = p_produk_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_produk_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trxpesananpembayaran_approve
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trxpesananpembayaran_approve`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trxpesananpembayaran_approve`(IN p_trxpesanan_id varchar(36),IN p_trxpembayaran_verifiedby varchar(36),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @currentdate = (SELECT NOW());
			
			UPDATE 
				trxpesanan_pembayaran 
			SET 
				trxpembayaran_status = 1,
				trxpembayaran_verifiedby = p_trxpembayaran_verifiedby,
				trxpembayaran_verifieddate = @currentdate
			WHERE 
				trxpesanan_id = p_trxpesanan_id;

			UPDATE
				trxpesanan
			SET
				trxpesanan_status = 3
			WHERE 
				trxpesanan_id = p_trxpesanan_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_trxpesanan_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trxpesananpembayaran_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trxpesananpembayaran_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trxpesananpembayaran_insert`(IN p_trxpembayaran_metode varchar(36),IN p_trxpembayaran_nominal decimal(18,2),IN p_trxpembayaran_kdbank varchar(3),IN p_trxpembayaran_noref varchar(100),IN p_trxpesanan_id varchar(36),IN p_trxpesanan_pointorder INT,IN p_trxpesanan_biayakirim decimal(18,2),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			/*SET @trxpesananpembayaran_id = (SELECT UUID());*/
			SET @currentdate = (SELECT NOW());

			INSERT INTO trxpesanan_pembayaran(trxpesanan_id,trxpembayaran_metode,trxpembayaran_status,trxpembayaran_tgl,trxpembayaran_nominal,trxpembayaran_kdbank,trxpembayaran_noref)
				VALUES(p_trxpesanan_id,p_trxpembayaran_metode,0,@currentdate,p_trxpembayaran_nominal,p_trxpembayaran_kdbank,p_trxpembayaran_noref);

			UPDATE 
				trxpesanan
			SET
				trxpesanan_status = 1,
				trxpesanan_pointorder = p_trxpesanan_pointorder,
				trxpesanan_biayakirim = p_trxpesanan_biayakirim
			WHERE
				trxpesanan_id = p_trxpesanan_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_trxpesanan_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trxpesananpembayaran_reject
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trxpesananpembayaran_reject`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trxpesananpembayaran_reject`(IN p_trxpesanan_id varchar(36),IN p_trxpembayaran_verifiedby varchar(36),IN p_trxpembayaran_note text,IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @currentdate = (SELECT NOW());
			
			UPDATE 
				trxpesanan_pembayaran 
			SET 
				trxpembayaran_status = 2,
				trxpembayaran_verifiedby = p_trxpembayaran_verifiedby,
				trxpembayaran_note = p_trxpembayaran_note,
				trxpembayaran_verifieddate = @currentdate
			WHERE 
				trxpesanan_id = p_trxpesanan_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_trxpesanan_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trxpesananverify_realisasi
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trxpesananverify_realisasi`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trxpesananverify_realisasi`(IN p_trxpesanan_id varchar(36),IN p_trxpesanan_produkid varchar(36),IN p_trxpesanan_qty_realisasi INT,IN p_trxpembayaran_verifiedby varchar(36),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @currentdate = (SELECT NOW());
			
			UPDATE 
				trxpesanan 
			SET 
				trxpesanan_status = 4
			WHERE 
				trxpesanan_id = p_trxpesanan_id;

			UPDATE
				trxpesanan_detail
			SET
				trxpesanan_qty_realisasi = p_trxpesanan_qty_realisasi,
				trxpesanan_verifiedby = p_trxpembayaran_verifiedby,
				trxpesanan_verifieddate = @currentdate
			WHERE 
				trxpesanan_id = p_trxpesanan_id
				AND trxpesanan_produkid = p_trxpesanan_produkid;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_trxpesanan_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trxpesanan_delete
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trxpesanan_delete`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trxpesanan_delete`(IN p_trxpesanan_id varchar(36),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;

			DELETE a
			FROM
				trxpesanan_detail a
			INNER JOIN trxpesanan b ON a.trxpesanan_id = b.trxpesanan_id
			WHERE
				a.trxpesanan_id = p_trxpesanan_id
				AND b.trxpesanan_status = 0;

			DELETE FROM
				trxpesanan
			WHERE
				trxpesanan_id = p_trxpesanan_id
				AND trxpesanan_status = 0;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_trxpesanan_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trxpesanan_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trxpesanan_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trxpesanan_insert`(IN p_trxpesanan_id varchar(36), IN p_trxpesanan_userid varchar(36),  IN p_trxpesanan_invoiceid varchar(10),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @currentdate = (SELECT NOW());

			INSERT INTO trxpesanan(trxpesanan_id,trxpesanan_tgl,trxpesanan_userid,trxpesanan_status,trxpesanan_invoiceid)
				VALUES(p_trxpesanan_id,@currentdate,p_trxpesanan_userid,0,p_trxpesanan_invoiceid);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_trxpesanan_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_trxsaldo_response
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_trxsaldo_response`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_trxsaldo_response`(IN p_trxsaldo_id varchar(36),IN p_trxsaldo_status tinyint,IN p_trxsaldo_verifiedby varchar(36),IN p_trxsaldo_note text,IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;	
			SET @currentdate = (SELECT NOW());

			UPDATE trxsaldo 
			SET 
				trxsaldo_status = p_trxsaldo_status,
				trxsaldo_verifiedby = p_trxsaldo_verifiedby,
				trxsaldo_verifieddate = @currentdate,
				trxsaldo_note = p_trxsaldo_note
			WHERE 
				trxsaldo_id = p_trxsaldo_id;

			IF p_trxsaldo_status = 1 THEN
				SET @user_id = (SELECT trxsaldo_user_id FROM trxsaldo WHERE trxsaldo_id = p_trxsaldo_id);
				SET @current_account = (SELECT COUNT(*) FROM saldo WHERE user_id = @user_id);
				SET @nominal_penambahan = (SELECT trxsaldo_nominal FROM trxsaldo WHERE trxsaldo_id = p_trxsaldo_id);

				IF @current_account > 0 THEN					
					SET @saldo_awal = (SELECT saldo_saldo FROM saldo WHERE user_id = @user_id);
					UPDATE 
						saldo 
					SET 
						saldo_saldo = @saldo_awal + @nominal_penambahan,
						saldo_lastupdate = @currentdate
					WHERE
						user_id = @user_id;
				ELSE
					INSERT INTO saldo (user_id,saldo_saldo,saldo_lastupdate) VALUES	
						(@user_id,@nominal_penambahan,@currentdate);
				END IF;
			END IF;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_trxsaldo_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_users_delete
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_users_delete`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_users_delete`(IN p_user_id varchar(36),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			DELETE FROM users
			WHERE
				user_id = p_user_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_users_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_users_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_users_insert`(IN user_identity varchar(25),IN user_name varchar(100),IN role_id INT,IN user_salt varchar(5),IN user_password text,IN user_status BIT,IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @user_id = (SELECT UUID());
			SET @currentdate = (SELECT NOW());

			INSERT INTO users(user_id,user_identity, user_role_id,user_name,user_registerdate,user_salt,user_password,user_status)
				VALUES(@user_id,user_identity,role_id,user_name,@currentdate,user_salt,user_password,user_status);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,@user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_users_resetpassword
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_users_resetpassword`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_users_resetpassword`(IN p_user_id varchar(36),IN p_user_password text,IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			UPDATE users
			SET
				user_password = p_user_password
			WHERE
				user_id = p_user_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_users_status
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_users_status`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_users_status`(IN p_user_id varchar(36),IN p_user_status BIT,IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			UPDATE users
			SET
				user_status = p_user_status
			WHERE
				user_id = p_user_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_users_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_users_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_users_update`(IN p_user_id varchar(36),IN user_identity varchar(25),IN user_name varchar(100),IN role_id INT,IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			UPDATE users
			SET
				user_identity = user_identity,
				user_role_id = role_id,
				user_name = user_name
			WHERE
				user_id = p_user_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;
