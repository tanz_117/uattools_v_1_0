/*
Navicat MySQL Data Transfer

Source Server         : localhost_mySQL
Source Server Version : 50626
Source Host           : localhost:3306
Source Database       : compro_1

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2017-09-12 16:30:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for articles
-- ----------------------------
DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `article_id` varchar(36) NOT NULL,
  `article_created` datetime DEFAULT NULL,
  `article_createdby` varchar(50) DEFAULT NULL,
  `article_title` varchar(255) DEFAULT NULL,
  `article_content` text,
  `article_status` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of articles
-- ----------------------------

-- ----------------------------
-- Table structure for configurations
-- ----------------------------
DROP TABLE IF EXISTS `configurations`;
CREATE TABLE `configurations` (
  `configuration_id` varchar(32) NOT NULL,
  `configuration_key` varchar(80) DEFAULT NULL,
  `configuration_value` longtext,
  `configuration_status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`configuration_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of configurations
-- ----------------------------
INSERT INTO `configurations` VALUES ('CF371C9618AB459299790F9A4A120F49', 'copyright', '2016. All rights reserved.', '1');
INSERT INTO `configurations` VALUES ('EB43051CAFAD49BDBACDC922F47D904C', 'created_by', 'HOA', '1');

-- ----------------------------
-- Table structure for dictionaries
-- ----------------------------
DROP TABLE IF EXISTS `dictionaries`;
CREATE TABLE `dictionaries` (
  `dictionary_id` int(11) NOT NULL,
  `dictionary_lang_id` char(4) NOT NULL,
  `dictionary_key` varchar(255) NOT NULL,
  `dictionary_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`dictionary_id`),
  KEY `dictionary_lang_id` (`dictionary_lang_id`),
  CONSTRAINT `dictionaries_ibfk_1` FOREIGN KEY (`dictionary_lang_id`) REFERENCES `languages` (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dictionaries
-- ----------------------------
INSERT INTO `dictionaries` VALUES ('586', 'en', 'base.nav.title.settings', 'Setting');
INSERT INTO `dictionaries` VALUES ('587', 'id', 'base.nav.title.settings', 'Pengaturan');
INSERT INTO `dictionaries` VALUES ('588', 'en', 'base.nav.title.users', 'User');
INSERT INTO `dictionaries` VALUES ('589', 'id', 'base.nav.title.users', 'Pengguna');
INSERT INTO `dictionaries` VALUES ('590', 'en', 'base.nav.title.menus', 'Menu');
INSERT INTO `dictionaries` VALUES ('591', 'id', 'base.nav.title.menus', 'Menu');
INSERT INTO `dictionaries` VALUES ('592', 'en', 'base.nav.title.roles', 'Roles');
INSERT INTO `dictionaries` VALUES ('593', 'id', 'base.nav.title.roles', 'Hak Akses');
INSERT INTO `dictionaries` VALUES ('594', 'en', 'base.nav.title.languages', 'Language');
INSERT INTO `dictionaries` VALUES ('595', 'id', 'base.nav.title.languages', 'Bahasa');
INSERT INTO `dictionaries` VALUES ('596', 'en', 'base.nav.title.dictionaries', 'Dictionary');
INSERT INTO `dictionaries` VALUES ('597', 'id', 'base.nav.title.dictionaries', 'Kamus Kata');
INSERT INTO `dictionaries` VALUES ('598', 'en', 'dictionaries.dictionary_key', 'Dictionary Key');
INSERT INTO `dictionaries` VALUES ('599', 'id', 'dictionaries.dictionary_key', 'Kata Kunci');
INSERT INTO `dictionaries` VALUES ('600', 'en', 'dictionaries.page_title.dictionaries', 'Dictionaries');
INSERT INTO `dictionaries` VALUES ('601', 'id', 'dictionaries.page_title.dictionaries', 'Kamus Kata');
INSERT INTO `dictionaries` VALUES ('602', 'en', 'dictionaries.dictionary_value', 'Dictionary Value');
INSERT INTO `dictionaries` VALUES ('603', 'id', 'dictionaries.dictionary_value', 'Arti Kamus Kata');
INSERT INTO `dictionaries` VALUES ('604', 'en', 'languages.page_title.languages', 'Language');
INSERT INTO `dictionaries` VALUES ('605', 'id', 'languages.page_title.languages', 'Bahasa');
INSERT INTO `dictionaries` VALUES ('606', 'en', 'languages.language_id', 'Language ID');
INSERT INTO `dictionaries` VALUES ('607', 'id', 'languages.language_id', 'ID Bahasa');
INSERT INTO `dictionaries` VALUES ('608', 'en', 'languages.language_name', 'Language Name');
INSERT INTO `dictionaries` VALUES ('609', 'id', 'languages.language_name', 'Nama Bahasa');
INSERT INTO `dictionaries` VALUES ('610', 'en', 'languages.language_default', 'Language Default');
INSERT INTO `dictionaries` VALUES ('611', 'id', 'languages.language_default', 'Bahasa Utama');
INSERT INTO `dictionaries` VALUES ('612', 'en', 'languages.language_active', 'Language Status');
INSERT INTO `dictionaries` VALUES ('613', 'id', 'languages.language_active', 'Status Bahasa');
INSERT INTO `dictionaries` VALUES ('614', 'en', 'roles.page_title.roles', 'Roles');
INSERT INTO `dictionaries` VALUES ('615', 'id', 'roles.page_title.roles', 'Hak Akses');
INSERT INTO `dictionaries` VALUES ('616', 'en', 'roles.role_name', 'Role Name');
INSERT INTO `dictionaries` VALUES ('617', 'id', 'roles.role_name', 'Nama Hak Akses');
INSERT INTO `dictionaries` VALUES ('618', 'en', 'roles.role_menu', 'Role Menu');
INSERT INTO `dictionaries` VALUES ('619', 'id', 'roles.role_menu', 'Menu Hak Akses');
INSERT INTO `dictionaries` VALUES ('620', 'en', 'roles.role_level', 'Role Level');
INSERT INTO `dictionaries` VALUES ('621', 'id', 'roles.role_level', 'Level Hak Akses');
INSERT INTO `dictionaries` VALUES ('622', 'en', 'roles.role_active', 'Role Status');
INSERT INTO `dictionaries` VALUES ('623', 'id', 'roles.role_active', 'Status Hak Akses');
INSERT INTO `dictionaries` VALUES ('643', 'en', 'menus.page_title.menus', 'Menu');
INSERT INTO `dictionaries` VALUES ('644', 'id', 'menus.page_title.menus', 'Menu');
INSERT INTO `dictionaries` VALUES ('645', 'en', 'menus.menu_name', 'Menu Name');
INSERT INTO `dictionaries` VALUES ('646', 'id', 'menus.menu_name', 'Nama Menu');
INSERT INTO `dictionaries` VALUES ('647', 'en', 'menus.menu_url', 'URL');
INSERT INTO `dictionaries` VALUES ('648', 'id', 'menus.menu_url', 'URL');
INSERT INTO `dictionaries` VALUES ('649', 'en', 'menus.menu_icon', 'Icon');
INSERT INTO `dictionaries` VALUES ('650', 'id', 'menus.menu_icon', 'Icon');
INSERT INTO `dictionaries` VALUES ('651', 'en', 'menus.menu_parent', 'Parent Menu');
INSERT INTO `dictionaries` VALUES ('652', 'id', 'menus.menu_parent', 'Menu Utama');
INSERT INTO `dictionaries` VALUES ('653', 'en', 'menus.menu_order', 'Menu Order');
INSERT INTO `dictionaries` VALUES ('654', 'id', 'menus.menu_order', 'Urutan');
INSERT INTO `dictionaries` VALUES ('655', 'en', 'menus.menu_active', 'Status');
INSERT INTO `dictionaries` VALUES ('656', 'id', 'menus.menu_active', 'Status');
INSERT INTO `dictionaries` VALUES ('657', 'en', 'base.page_title.tes4', 'tes4');
INSERT INTO `dictionaries` VALUES ('658', 'id', 'base.page_title.tes4', 'tes4');
INSERT INTO `dictionaries` VALUES ('659', 'en', 'base.page_title.tes4', 'tes4');
INSERT INTO `dictionaries` VALUES ('660', 'id', 'base.page_title.tes4', 'tes4');
INSERT INTO `dictionaries` VALUES ('661', 'en', 'base.page_title.tes4', 'tes4');
INSERT INTO `dictionaries` VALUES ('662', 'id', 'base.page_title.tes4', 'tes4');
INSERT INTO `dictionaries` VALUES ('663', 'en', 'base.page_title.1234', '1234');
INSERT INTO `dictionaries` VALUES ('664', 'id', 'base.page_title.1234', '1234');
INSERT INTO `dictionaries` VALUES ('665', 'en', 'users.page_title.users', 'User');
INSERT INTO `dictionaries` VALUES ('666', 'id', 'users.page_title.users', 'Pengguna');
INSERT INTO `dictionaries` VALUES ('667', 'en', 'users.identity', 'ID');
INSERT INTO `dictionaries` VALUES ('668', 'id', 'users.identity', 'ID');
INSERT INTO `dictionaries` VALUES ('669', 'en', 'users.role_name', 'Role Name');
INSERT INTO `dictionaries` VALUES ('670', 'id', 'users.role_name', 'Nama Hak Akses');
INSERT INTO `dictionaries` VALUES ('671', 'en', 'users.user_name', 'User Name');
INSERT INTO `dictionaries` VALUES ('672', 'id', 'users.user_name', 'Nama User');
INSERT INTO `dictionaries` VALUES ('673', 'en', 'users.user_registerdate', 'Register Date');
INSERT INTO `dictionaries` VALUES ('674', 'id', 'users.user_registerdate', 'Tgl Register');
INSERT INTO `dictionaries` VALUES ('675', 'en', 'users.user_company', 'Company');
INSERT INTO `dictionaries` VALUES ('676', 'id', 'users.user_company', 'Perusahaan');
INSERT INTO `dictionaries` VALUES ('677', 'en', 'users.role_id', 'Role');
INSERT INTO `dictionaries` VALUES ('678', 'id', 'users.role_id', 'Hak Akses');
INSERT INTO `dictionaries` VALUES ('801', 'en', 'base.page_title.mails', 'Surat');
INSERT INTO `dictionaries` VALUES ('802', 'id', 'base.page_title.mails', 'Surat');
INSERT INTO `dictionaries` VALUES ('807', 'en', 'base.page_title.masterdata', 'masterdata');
INSERT INTO `dictionaries` VALUES ('808', 'id', 'base.page_title.masterdata', 'masterdata');
INSERT INTO `dictionaries` VALUES ('809', 'en', 'base.page_title.divisions', 'Divisions');
INSERT INTO `dictionaries` VALUES ('810', 'id', 'base.page_title.divisions', 'Divisions');
INSERT INTO `dictionaries` VALUES ('811', 'en', 'base.page_title.unitkerjas', 'unitkerjas');
INSERT INTO `dictionaries` VALUES ('812', 'id', 'base.page_title.unitkerjas', 'unitkerjas');
INSERT INTO `dictionaries` VALUES ('813', 'en', 'base.page_title.inbox', 'inbox');
INSERT INTO `dictionaries` VALUES ('814', 'id', 'base.page_title.inbox', 'inbox');
INSERT INTO `dictionaries` VALUES ('815', 'en', 'base.page_title.inbox', 'inbox');
INSERT INTO `dictionaries` VALUES ('816', 'id', 'base.page_title.inbox', 'inbox');
INSERT INTO `dictionaries` VALUES ('817', 'en', 'base.page_title.employees', 'employees');
INSERT INTO `dictionaries` VALUES ('818', 'id', 'base.page_title.employees', 'employees');
INSERT INTO `dictionaries` VALUES ('819', 'en', 'base.page_title.pymt', 'pymt');
INSERT INTO `dictionaries` VALUES ('820', 'id', 'base.page_title.pymt', 'pymt');
INSERT INTO `dictionaries` VALUES ('821', 'en', 'base.page_title.pymt_unit_kerjas', 'pymt_unit_kerjas');
INSERT INTO `dictionaries` VALUES ('822', 'id', 'base.page_title.pymt_unit_kerjas', 'pymt_unit_kerjas');
INSERT INTO `dictionaries` VALUES ('823', 'en', 'base.page_title.histories', 'Histories');
INSERT INTO `dictionaries` VALUES ('824', 'id', 'base.page_title.histories', 'Histories');
INSERT INTO `dictionaries` VALUES ('825', 'en', 'base.page_title.inbox_pymts', 'inbox_pymts');
INSERT INTO `dictionaries` VALUES ('826', 'id', 'base.page_title.inbox_pymts', 'inbox_pymts');
INSERT INTO `dictionaries` VALUES ('829', 'id', 'base.nav.title.mails', 'Surat');
INSERT INTO `dictionaries` VALUES ('830', 'en', 'base.nav.title.mails', 'Surat');
INSERT INTO `dictionaries` VALUES ('831', 'id', 'mails.surat_nomor', 'Nomor Surat');
INSERT INTO `dictionaries` VALUES ('832', 'en', 'mails.surat_nomor', 'Nomor Surat');
INSERT INTO `dictionaries` VALUES ('833', 'id', 'mails.surat_received', 'Tgl Diterima');
INSERT INTO `dictionaries` VALUES ('834', 'en', 'mails.surat_received', 'Tgl Diterima');
INSERT INTO `dictionaries` VALUES ('835', 'id', 'mails.surat_tgl', 'Tgl Surat');
INSERT INTO `dictionaries` VALUES ('836', 'en', 'mails.surat_tgl', 'Tgl Surat');
INSERT INTO `dictionaries` VALUES ('837', 'id', 'mails.surat_jenis', 'Jenis Surat');
INSERT INTO `dictionaries` VALUES ('838', 'en', 'mails.surat_jenis', 'Jenis Surat');
INSERT INTO `dictionaries` VALUES ('839', 'id', 'mails.surat_pengirim', 'Pengirim');
INSERT INTO `dictionaries` VALUES ('840', 'en', 'mails.surat_pengirim', 'Pengirim');
INSERT INTO `dictionaries` VALUES ('841', 'id', 'mails.surat_tujuan', 'Tujuan');
INSERT INTO `dictionaries` VALUES ('842', 'en', 'mails.surat_tujuan', 'Tujuan');
INSERT INTO `dictionaries` VALUES ('843', 'id', 'mails.surat_receivedby', 'Diterima Oleh');
INSERT INTO `dictionaries` VALUES ('844', 'en', 'mails.surat_receivedby', 'Surat Diterima Oleh');
INSERT INTO `dictionaries` VALUES ('845', 'id', 'mails.surat_perihal', 'Perihal');
INSERT INTO `dictionaries` VALUES ('846', 'en', 'mails.surat_perihal', 'Perihal');
INSERT INTO `dictionaries` VALUES ('847', 'id', 'mails.surat_type', 'Tipe Surat');
INSERT INTO `dictionaries` VALUES ('848', 'en', 'mails.surat_type', 'Tipe Surat');
INSERT INTO `dictionaries` VALUES ('851', 'id', 'mails.surat_keterangan', 'Keterangan');
INSERT INTO `dictionaries` VALUES ('852', 'en', 'mails.surat_keterangan', 'Keterangan');
INSERT INTO `dictionaries` VALUES ('853', 'id', 'mails.surat_pengirim_masuk', 'Tipe Pengirim');
INSERT INTO `dictionaries` VALUES ('854', 'en', 'mails.surat_pengirim_masuk', 'Tipe Pengirim');
INSERT INTO `dictionaries` VALUES ('855', 'id', 'mails.surat_pengirim_masuk_internal', 'Pengirim');
INSERT INTO `dictionaries` VALUES ('856', 'en', 'mails.surat_pengirim_masuk_internal', 'Pengirim');
INSERT INTO `dictionaries` VALUES ('857', 'id', 'mails.surat_type_tujuan_masuk', 'Tujuan');
INSERT INTO `dictionaries` VALUES ('858', 'en', 'mails.surat_type_tujuan_masuk', 'Tujuan');
INSERT INTO `dictionaries` VALUES ('859', 'id', 'mails.surat_pengirim_masuk_external', 'Pengirim');
INSERT INTO `dictionaries` VALUES ('860', 'en', 'mails.surat_pengirim_masuk_external', 'Pengirim');
INSERT INTO `dictionaries` VALUES ('861', 'id', 'mails.surat_tujuan_masuk_internal', 'Tujuan');
INSERT INTO `dictionaries` VALUES ('862', 'en', 'mails.surat_tujuan_masuk_internal', 'Tujuan');
INSERT INTO `dictionaries` VALUES ('863', 'id', 'mails.surat_pengirim_keluar', 'Tipe Pengirim');
INSERT INTO `dictionaries` VALUES ('864', 'en', 'mails.surat_pengirim_keluar', 'Tipe Pengirim');
INSERT INTO `dictionaries` VALUES ('865', 'id', 'mails.surat_pengirim_keluar_internal', 'Pengirim');
INSERT INTO `dictionaries` VALUES ('866', 'en', 'mails.surat_pengirim_keluar_internal', 'Pengirim');
INSERT INTO `dictionaries` VALUES ('867', 'id', 'mails.surat_type_tujuan_keluar', 'Tipe Tujuan');
INSERT INTO `dictionaries` VALUES ('868', 'en', 'mails.surat_type_tujuan_keluar', 'Tipe Tujuan');
INSERT INTO `dictionaries` VALUES ('869', 'id', 'mails.surat_tujuan_keluar_internal', 'Tujuan');
INSERT INTO `dictionaries` VALUES ('870', 'en', 'mails.surat_tujuan_keluar_internal', 'Tujuan');
INSERT INTO `dictionaries` VALUES ('871', 'id', 'mails.surat_tujuan_keluar_external', 'Tujuan');
INSERT INTO `dictionaries` VALUES ('872', 'en', 'mails.surat_tujuan_keluar_external', 'Tujuan');
INSERT INTO `dictionaries` VALUES ('873', 'id', 'base.nav.title.masterdata', 'Master Data');
INSERT INTO `dictionaries` VALUES ('874', 'en', 'base.nav.title.masterdata', 'Master Data');
INSERT INTO `dictionaries` VALUES ('875', 'id', 'base.nav.title.divisions', 'Divisi');
INSERT INTO `dictionaries` VALUES ('876', 'en', 'base.nav.title.divisions', 'Divisi');
INSERT INTO `dictionaries` VALUES ('877', 'id', 'divisions.divisi_id', 'ID Divisi');
INSERT INTO `dictionaries` VALUES ('878', 'en', 'divisions.divisi_id', 'ID Divisi');
INSERT INTO `dictionaries` VALUES ('879', 'id', 'divisions.divisi_nama', 'Nama Divisi');
INSERT INTO `dictionaries` VALUES ('880', 'en', 'divisions.divisi_nama', 'Nama Divisi');
INSERT INTO `dictionaries` VALUES ('881', 'id', 'base.nav.title.unitkerjas', 'Unit Kerja');
INSERT INTO `dictionaries` VALUES ('882', 'en', 'base.nav.title.unitkerjas', 'Unit Kerja');
INSERT INTO `dictionaries` VALUES ('883', 'id', 'unitkerjas.unitkerja_id', 'Id Unit Kerja');
INSERT INTO `dictionaries` VALUES ('884', 'en', 'unitkerjas.unitkerja_id', 'Id Unit Kerja');
INSERT INTO `dictionaries` VALUES ('885', 'id', 'unitkerjas.unitkerja_nama', 'Nama Unit Kerja');
INSERT INTO `dictionaries` VALUES ('886', 'en', 'unitkerjas.unitkerja_nama', 'Nama Unit Kerja');
INSERT INTO `dictionaries` VALUES ('887', 'id', 'unitkerjas.divisi_nama', 'Nama Divisi');
INSERT INTO `dictionaries` VALUES ('888', 'en', 'unitkerjas.divisi_nama', 'Nama Divisi');
INSERT INTO `dictionaries` VALUES ('889', 'id', 'base.nav.title.employees', 'Pegawai');
INSERT INTO `dictionaries` VALUES ('890', 'en', 'base.nav.title.employees', 'Pegawai');
INSERT INTO `dictionaries` VALUES ('891', 'id', 'employees.pegawai_id', 'Pegawai Id');
INSERT INTO `dictionaries` VALUES ('892', 'en', 'employees.pegawai_id', 'Pegawai Id');
INSERT INTO `dictionaries` VALUES ('893', 'id', 'employees.pegawai_nama', 'Pegawai Nama');
INSERT INTO `dictionaries` VALUES ('894', 'en', 'employees.pegawai_nama', 'Pegawai Nama');
INSERT INTO `dictionaries` VALUES ('895', 'id', 'employees.pegawai_id_divisi', 'Divisi Id');
INSERT INTO `dictionaries` VALUES ('896', 'en', 'employees.pegawai_id_divisi', 'Divisi Id');
INSERT INTO `dictionaries` VALUES ('897', 'id', 'employees.pegawai_id_unitkerja', 'Unit Kerja');
INSERT INTO `dictionaries` VALUES ('898', 'en', 'employees.pegawai_id_unitkerja', 'Unit Kerja');
INSERT INTO `dictionaries` VALUES ('899', 'id', 'base.nav.title.inbox', 'Inbox');
INSERT INTO `dictionaries` VALUES ('900', 'en', 'base.nav.title.inbox', 'Inbox');
INSERT INTO `dictionaries` VALUES ('901', 'id', 'inbox.surat_jenis', 'Surat Jenis');
INSERT INTO `dictionaries` VALUES ('902', 'en', 'inbox.surat_jenis', 'Surat Jenis');
INSERT INTO `dictionaries` VALUES ('903', 'id', 'inbox.surat_nomor', 'Surat Nomor');
INSERT INTO `dictionaries` VALUES ('904', 'en', 'inbox.surat_nomor', 'Surat Nomor');
INSERT INTO `dictionaries` VALUES ('907', 'id', 'inbox.surat_perihal', 'Subject');
INSERT INTO `dictionaries` VALUES ('908', 'en', 'inbox.surat_perihal', 'Perihal');
INSERT INTO `dictionaries` VALUES ('909', 'id', 'inbox.jml_disposisi', 'Jumlah Disposisi');
INSERT INTO `dictionaries` VALUES ('910', 'en', 'inbox.jml_disposisi', 'Jumlah Disposisi');
INSERT INTO `dictionaries` VALUES ('911', 'id', 'base.nav.title.pymt_divisis', 'PYMT Divisi');
INSERT INTO `dictionaries` VALUES ('912', 'en', 'base.nav.title.pymt_divisis', 'PYMT Divisi');
INSERT INTO `dictionaries` VALUES ('913', 'id', 'pymt_divisis.keterangan', 'Keterangan');
INSERT INTO `dictionaries` VALUES ('914', 'en', 'pymt_divisis.keterangan', 'Keterangan');
INSERT INTO `dictionaries` VALUES ('915', 'id', 'pymt_divisis.tanggal', 'Tanggal');
INSERT INTO `dictionaries` VALUES ('916', 'en', 'pymt_divisis.tanggal', 'Tanggal');
INSERT INTO `dictionaries` VALUES ('917', 'id', 'pymt_divisis.div_uk', 'Divisi / Unitkerja');
INSERT INTO `dictionaries` VALUES ('918', 'en', 'pymt_divisis.div_uk', 'Divisi / Unitkerja');
INSERT INTO `dictionaries` VALUES ('919', 'id', 'pymt_divisis.pymt1', 'PYMT');
INSERT INTO `dictionaries` VALUES ('920', 'en', 'pymt_divisis.pymt1', 'PYMT 1');
INSERT INTO `dictionaries` VALUES ('921', 'id', 'pymt_divisis.pymt2', 'PYMT 2');
INSERT INTO `dictionaries` VALUES ('922', 'en', 'pymt_divisis.pymt2', 'PYMT 2');
INSERT INTO `dictionaries` VALUES ('923', 'id', 'base.page_title.pymt_divisis', 'PYMT Divisi');
INSERT INTO `dictionaries` VALUES ('924', 'en', 'base.page_title.pymt_divisis', 'PYMT Divisi');
INSERT INTO `dictionaries` VALUES ('925', 'id', 'base.page_title.pymt_unit_kerjas', 'PYMT Unit Kerja');
INSERT INTO `dictionaries` VALUES ('926', 'en', 'base.page_title.pymt_unit_kerjas', 'PYMT Unit Kerja');
INSERT INTO `dictionaries` VALUES ('927', 'id', 'base.nav.title.pymt_unit_kerjas', 'PYMT Unit kerja');
INSERT INTO `dictionaries` VALUES ('928', 'en', 'base.nav.title.pymt_unit_kerjas', 'PYMT Unit kerja');
INSERT INTO `dictionaries` VALUES ('929', 'id', 'pymt_unit_kerjas.keterangan', 'Keterangan');
INSERT INTO `dictionaries` VALUES ('930', 'en', 'pymt_unit_kerjas.keterangan', 'Keterangan');
INSERT INTO `dictionaries` VALUES ('931', 'id', 'pymt_unit_kerjas.tanggal', 'Tanggal');
INSERT INTO `dictionaries` VALUES ('932', 'en', 'pymt_unit_kerjas.tanggal', 'Tanggal');
INSERT INTO `dictionaries` VALUES ('933', 'id', 'pymt_unit_kerjas.div_uk', 'Divisi / Unitkerja');
INSERT INTO `dictionaries` VALUES ('934', 'en', 'pymt_unit_kerjas.div_uk', 'Divisi / Unitkerja');
INSERT INTO `dictionaries` VALUES ('935', 'id', 'pymt_unit_kerjas.pymt1', 'PYMT 1');
INSERT INTO `dictionaries` VALUES ('936', 'en', 'pymt_unit_kerjas.pymt1', 'PYMT 1');
INSERT INTO `dictionaries` VALUES ('937', 'id', 'base.nav.title.histories', 'Histories');
INSERT INTO `dictionaries` VALUES ('938', 'en', 'base.nav.title.histories', 'Histories');
INSERT INTO `dictionaries` VALUES ('939', 'id', 'histories.surat_nomor', 'Nomor Surat');
INSERT INTO `dictionaries` VALUES ('940', 'en', 'histories.surat_nomor', 'Nomor Surat');
INSERT INTO `dictionaries` VALUES ('941', 'id', 'histories.surat_received', 'Tgl Diterima');
INSERT INTO `dictionaries` VALUES ('942', 'en', 'histories.surat_received', 'Tgl Diterima');
INSERT INTO `dictionaries` VALUES ('943', 'id', 'histories.surat_tgl', 'Tgl Surat');
INSERT INTO `dictionaries` VALUES ('944', 'en', 'histories.surat_tgl', 'Tgl Surat');
INSERT INTO `dictionaries` VALUES ('945', 'id', 'histories.surat_jenis', 'Jenis Surat');
INSERT INTO `dictionaries` VALUES ('946', 'en', 'histories.surat_jenis', 'Jenis Surat');
INSERT INTO `dictionaries` VALUES ('947', 'id', 'histories.surat_pengirim', 'Pengirim');
INSERT INTO `dictionaries` VALUES ('948', 'en', 'histories.surat_pengirim', 'Pengirim');
INSERT INTO `dictionaries` VALUES ('949', 'id', 'histories.surat_tujuan', 'Tujuan');
INSERT INTO `dictionaries` VALUES ('950', 'en', 'histories.surat_tujuan', 'Tujuan');
INSERT INTO `dictionaries` VALUES ('951', 'en', 'base.nav.title.inbox_pymts', 'Inbox PYMT');
INSERT INTO `dictionaries` VALUES ('952', 'id', 'base.nav.title.inbox_pymts', 'Inbox PYMT');
INSERT INTO `dictionaries` VALUES ('953', 'en', 'mails.lampiran', 'Attachments');
INSERT INTO `dictionaries` VALUES ('954', 'id', 'mails.lampiran', 'Lampiran');
INSERT INTO `dictionaries` VALUES ('955', 'en', 'mails.surat_created', 'Created');
INSERT INTO `dictionaries` VALUES ('956', 'id', 'mails.surat_created', 'Tgl Input');
INSERT INTO `dictionaries` VALUES ('957', 'en', 'mails.surat_createdby', 'Created By');
INSERT INTO `dictionaries` VALUES ('958', 'id', 'mails.surat_createdby', 'Diinput oleh');
INSERT INTO `dictionaries` VALUES ('959', 'en', 'mails.surat_type_pengirim', 'Sender Type');
INSERT INTO `dictionaries` VALUES ('960', 'id', 'mails.surat_type_pengirim', 'Type Pengirim');
INSERT INTO `dictionaries` VALUES ('961', 'en', 'inbox.disposition.receivedby', 'Received By');
INSERT INTO `dictionaries` VALUES ('962', 'id', 'inbox.disposition.receivedby', 'Telah diterima oleh');
INSERT INTO `dictionaries` VALUES ('963', 'en', 'inbox.disposition.attachment', 'Attachment');
INSERT INTO `dictionaries` VALUES ('964', 'id', 'inbox.disposition.attachment', 'Lampiran');
INSERT INTO `dictionaries` VALUES ('965', 'en', 'inbox.surat_pengirim_label', 'Sender');
INSERT INTO `dictionaries` VALUES ('966', 'id', 'inbox.surat_pengirim_label', 'Pengirim');
INSERT INTO `dictionaries` VALUES ('967', 'en', 'inbox.surat_keterangan', 'Description');
INSERT INTO `dictionaries` VALUES ('968', 'id', 'inbox.surat_keterangan', 'Keterangan');
INSERT INTO `dictionaries` VALUES ('969', 'en', 'inbox.disposisi_instruksi', 'Instruction');
INSERT INTO `dictionaries` VALUES ('970', 'id', 'inbox.disposisi_instruksi', 'Instruksi');
INSERT INTO `dictionaries` VALUES ('971', 'en', 'inbox.surat_tgl', 'Mail Date');
INSERT INTO `dictionaries` VALUES ('972', 'id', 'inbox.surat_tgl', 'Tanggal Surat');
INSERT INTO `dictionaries` VALUES ('973', 'en', 'inbox.disposition.instruction.label', 'Fill receiver and instruction');
INSERT INTO `dictionaries` VALUES ('974', 'id', 'inbox.disposition.instruction.label', 'Isi kolom penerima disposisi sesuai dengan level user berserta instruksi');
INSERT INTO `dictionaries` VALUES ('975', 'en', 'inbox.disposition_receiver', 'Disposition Receiver');
INSERT INTO `dictionaries` VALUES ('976', 'id', 'inbox.disposition_receiver', 'Penerima Disposisi');
INSERT INTO `dictionaries` VALUES ('977', 'en', 'mails.input_mails', 'Input Mail');
INSERT INTO `dictionaries` VALUES ('978', 'id', 'mails.input_mails', 'Input Surat');
INSERT INTO `dictionaries` VALUES ('979', 'en', 'mails.mails_notice', 'mail dan memo form');
INSERT INTO `dictionaries` VALUES ('980', 'id', 'mails.mails_notice', 'Form input surat dan memo');
INSERT INTO `dictionaries` VALUES ('981', 'en', 'pymt_divisis.unit_kerja_divisi', 'Divisi');
INSERT INTO `dictionaries` VALUES ('982', 'id', 'pymt_divisis.unit_kerja_divisi', 'Divisi');
INSERT INTO `dictionaries` VALUES ('983', 'en', 'pymt_divisis.tgl_dari', 'Start Date');
INSERT INTO `dictionaries` VALUES ('984', 'id', 'pymt_divisis.tgl_dari', 'Tgl Awal');
INSERT INTO `dictionaries` VALUES ('985', 'en', 'pymt_divisis.tgl_sampai', 'End Date');
INSERT INTO `dictionaries` VALUES ('986', 'id', 'pymt_divisis.tgl_sampai', 'Tgl Akhir');
INSERT INTO `dictionaries` VALUES ('987', 'en', 'histories.surat_perihal', 'Subject');
INSERT INTO `dictionaries` VALUES ('988', 'id', 'histories.surat_perihal', 'Perihal');
INSERT INTO `dictionaries` VALUES ('989', 'en', 'histories.pindiv', 'Head Of Division');
INSERT INTO `dictionaries` VALUES ('990', 'id', 'histories.pindiv', 'Pemimpin Divisi');
INSERT INTO `dictionaries` VALUES ('991', 'en', 'histories.group_head', 'Head of Team');
INSERT INTO `dictionaries` VALUES ('992', 'id', 'histories.group_head', 'Pemimpin Bagian');
INSERT INTO `dictionaries` VALUES ('993', 'en', 'histories.staff', 'Staff');
INSERT INTO `dictionaries` VALUES ('994', 'id', 'histories.staff', 'Staff');
INSERT INTO `dictionaries` VALUES ('995', 'en', 'histories.disposisi_tujuan', 'To');
INSERT INTO `dictionaries` VALUES ('996', 'id', 'histories.disposisi_tujuan', 'Untuk');
INSERT INTO `dictionaries` VALUES ('997', 'en', 'histories.dari', 'From');
INSERT INTO `dictionaries` VALUES ('998', 'id', 'histories.dari', 'Dari');
INSERT INTO `dictionaries` VALUES ('999', 'en', 'inbox_pymts.surat_jenis', 'Mail Type');
INSERT INTO `dictionaries` VALUES ('1000', 'id', 'inbox_pymts.surat_jenis', 'Jenis Surat');
INSERT INTO `dictionaries` VALUES ('1001', 'en', 'inbox_pymts.surat_nomor', 'Mail Number');
INSERT INTO `dictionaries` VALUES ('1002', 'id', 'inbox_pymts.surat_nomor', 'Nomor Surat');
INSERT INTO `dictionaries` VALUES ('1003', 'en', 'inbox_pymts.surat_tgl', 'Mail Date');
INSERT INTO `dictionaries` VALUES ('1004', 'id', 'inbox_pymts.surat_tgl', 'Tgl Surat');
INSERT INTO `dictionaries` VALUES ('1005', 'en', 'inbox_pymts.surat_perihal', 'Subject');
INSERT INTO `dictionaries` VALUES ('1006', 'id', 'inbox_pymts.surat_perihal', 'Perihal');
INSERT INTO `dictionaries` VALUES ('1007', 'en', 'inbox_pymts.page_title.inbox_pymts', 'PYMT Inbox');
INSERT INTO `dictionaries` VALUES ('1008', 'id', 'inbox_pymts.page_title.inbox_pymts', 'Inbox PYMT');
INSERT INTO `dictionaries` VALUES ('1009', 'en', 'pymt_unit_kerjas.pymt_div_uk', 'Group Name');
INSERT INTO `dictionaries` VALUES ('1010', 'id', 'pymt_unit_kerjas.pymt_div_uk', 'Unit Kerja');
INSERT INTO `dictionaries` VALUES ('1011', 'en', 'pymt_unit_kerjas.pymt_tgl_dari', 'From');
INSERT INTO `dictionaries` VALUES ('1012', 'id', 'pymt_unit_kerjas.pymt_tgl_dari', 'Dari');
INSERT INTO `dictionaries` VALUES ('1013', 'en', 'pymt_unit_kerjas.pymt_tgl_sampai', 'Until');
INSERT INTO `dictionaries` VALUES ('1014', 'id', 'pymt_unit_kerjas.pymt_tgl_sampai', 'Sampai');
INSERT INTO `dictionaries` VALUES ('1015', 'en', 'pymt_unit_kerjas.pymt_user_id1', 'PYMT User');
INSERT INTO `dictionaries` VALUES ('1016', 'id', 'pymt_unit_kerjas.pymt_user_id1', 'User PYMT');
INSERT INTO `dictionaries` VALUES ('1017', 'en', 'pymt_unit_kerjas.pymt_keterangan', 'Description');
INSERT INTO `dictionaries` VALUES ('1018', 'id', 'pymt_unit_kerjas.pymt_keterangan', 'Keterangan');
INSERT INTO `dictionaries` VALUES ('1019', 'en', 'pymt_divisis.pymt_tgl_dari', 'From');
INSERT INTO `dictionaries` VALUES ('1020', 'id', 'pymt_divisis.pymt_tgl_dari', 'Dari');
INSERT INTO `dictionaries` VALUES ('1021', 'en', 'pymt_divisis.pymt_tgl_sampai', 'Until');
INSERT INTO `dictionaries` VALUES ('1022', 'id', 'pymt_divisis.pymt_tgl_sampai', 'Sampai');
INSERT INTO `dictionaries` VALUES ('1023', 'en', 'pymt_unit_kerjas.unit_kerja_divisi', 'Group');
INSERT INTO `dictionaries` VALUES ('1024', 'id', 'pymt_unit_kerjas.unit_kerja_divisi', 'Unit Kerja');
INSERT INTO `dictionaries` VALUES ('1025', 'en', 'base.page_title.feedbacks', 'feedbacks');
INSERT INTO `dictionaries` VALUES ('1026', 'id', 'base.page_title.feedbacks', 'feedbacks');
INSERT INTO `dictionaries` VALUES ('1029', 'en', 'feedbacks.feedback_subject', 'Subject');
INSERT INTO `dictionaries` VALUES ('1030', 'id', 'feedbacks.feedback_subject', 'Perihal');
INSERT INTO `dictionaries` VALUES ('1031', 'en', 'feedbacks.feedback_desc', 'Message');
INSERT INTO `dictionaries` VALUES ('1032', 'id', 'feedbacks.feedback_desc', 'Pesan');
INSERT INTO `dictionaries` VALUES ('1033', 'en', 'base.nav.title.feedbacks', 'Feedback');
INSERT INTO `dictionaries` VALUES ('1034', 'id', 'base.nav.title.feedbacks', 'Feedback');
INSERT INTO `dictionaries` VALUES ('1035', 'en', 'inbox_pymts.jml_disposisi', 'Count Disposition');
INSERT INTO `dictionaries` VALUES ('1036', 'id', 'inbox_pymts.jml_disposisi', 'Jml Disposisi');
INSERT INTO `dictionaries` VALUES ('1037', 'en', 'base.page_title.feedback_readers', 'feedback_reader');
INSERT INTO `dictionaries` VALUES ('1038', 'id', 'base.page_title.feedback_readers', 'feedback_reader');
INSERT INTO `dictionaries` VALUES ('1039', 'en', 'base.nav.title.feedback_readers', 'Feedback Reader');
INSERT INTO `dictionaries` VALUES ('1040', 'id', 'base.nav.title.feedback_readers', 'Feedback Reader');
INSERT INTO `dictionaries` VALUES ('1041', 'en', 'feedback_readers.page_title.feedback_readers', 'Feedback Reader');
INSERT INTO `dictionaries` VALUES ('1042', 'id', 'feedback_readers.page_title.feedback_readers', 'Feedback Reader');
INSERT INTO `dictionaries` VALUES ('1043', 'en', 'feedback_readers.feedback_created', 'Created');
INSERT INTO `dictionaries` VALUES ('1044', 'id', 'feedback_readers.feedback_created', 'Tgl Input');
INSERT INTO `dictionaries` VALUES ('1045', 'en', 'feedback_readers.feedback_createdby', 'Created By');
INSERT INTO `dictionaries` VALUES ('1046', 'id', 'feedback_readers.feedback_createdby', 'Diinput Oleh');
INSERT INTO `dictionaries` VALUES ('1047', 'en', 'feedback_readers.feedback_subject', 'Subject');
INSERT INTO `dictionaries` VALUES ('1048', 'id', 'feedback_readers.feedback_subject', 'Perihal');
INSERT INTO `dictionaries` VALUES ('1049', 'en', 'feedback_readers.feedback_desc', 'Description');
INSERT INTO `dictionaries` VALUES ('1050', 'id', 'feedback_readers.feedback_desc', 'Deskripsi');
INSERT INTO `dictionaries` VALUES ('1051', 'en', 'feedbacks.page_title.feedbacks', 'Feedback');
INSERT INTO `dictionaries` VALUES ('1052', 'id', 'feedbacks.page_title.feedbacks', 'Feedback');
INSERT INTO `dictionaries` VALUES ('1053', 'en', 'base.page_title.groupings', 'Employee Grouping');
INSERT INTO `dictionaries` VALUES ('1054', 'id', 'base.page_title.groupings', 'Anggota Unitkerja');
INSERT INTO `dictionaries` VALUES ('1055', 'en', 'base.nav.title.groupings', 'Employee Grouping');
INSERT INTO `dictionaries` VALUES ('1056', 'id', 'base.nav.title.groupings', 'Anggota Unit Kerja');
INSERT INTO `dictionaries` VALUES ('1057', 'en', 'groupings.pegawai_id', 'Employee ID');
INSERT INTO `dictionaries` VALUES ('1058', 'id', 'groupings.pegawai_id', 'ID Pegawai');
INSERT INTO `dictionaries` VALUES ('1059', 'en', 'groupings.pegawai_nama', 'Employee Name');
INSERT INTO `dictionaries` VALUES ('1060', 'id', 'groupings.pegawai_nama', 'Nama Pegawai');
INSERT INTO `dictionaries` VALUES ('1061', 'en', 'inbox.disposisi_tgl', 'Disposition Date');
INSERT INTO `dictionaries` VALUES ('1062', 'id', 'inbox.disposisi_tgl', 'Tgl Disposisi');
INSERT INTO `dictionaries` VALUES ('1063', 'en', 'inbox.disposisi_createdby_label', 'Disposisi Creator');
INSERT INTO `dictionaries` VALUES ('1064', 'id', 'inbox.disposisi_createdby_label', 'Didisposisikan oleh');
INSERT INTO `dictionaries` VALUES ('1065', 'en', 'inbox.disposisi_createdby_level', 'Disposisi Creator Level');
INSERT INTO `dictionaries` VALUES ('1066', 'id', 'inbox.disposisi_createdby_level', 'Level Pengirim Disposisi');
INSERT INTO `dictionaries` VALUES ('1067', 'en', 'inbox_pymts.surat_pengirim_label', 'Sender');
INSERT INTO `dictionaries` VALUES ('1068', 'id', 'inbox_pymts.surat_pengirim_label', 'Pengirim Surat');
INSERT INTO `dictionaries` VALUES ('1069', 'en', 'inbox_pymts.surat_keterangan', 'Mail Description');
INSERT INTO `dictionaries` VALUES ('1070', 'id', 'inbox_pymts.surat_keterangan', 'Keterangan');
INSERT INTO `dictionaries` VALUES ('1071', 'en', 'inbox_pymts.disposisi_tgl', 'Disposition Date');
INSERT INTO `dictionaries` VALUES ('1072', 'id', 'inbox_pymts.disposisi_tgl', 'Tgl Disposisi');
INSERT INTO `dictionaries` VALUES ('1073', 'en', 'inbox_pymts.disposisi_createdby_label', 'Disposition Creator');
INSERT INTO `dictionaries` VALUES ('1074', 'id', 'inbox_pymts.disposisi_createdby_label', 'Didisposisikan oleh');
INSERT INTO `dictionaries` VALUES ('1075', 'en', 'inbox_pymts.disposisi_createdby_level', 'Disposition Creator Level');
INSERT INTO `dictionaries` VALUES ('1076', 'id', 'inbox_pymts.disposisi_createdby_level', 'Level Pengirim Disposisi');
INSERT INTO `dictionaries` VALUES ('1077', 'en', 'inbox_pymts.disposisi_instruksi', 'Instruction');
INSERT INTO `dictionaries` VALUES ('1078', 'id', 'inbox_pymts.disposisi_instruksi', 'Instruksi');
INSERT INTO `dictionaries` VALUES ('1249', 'en', 'feedbacks.feedback_created', 'Created');
INSERT INTO `dictionaries` VALUES ('1250', 'id', 'feedbacks.feedback_created', 'Tgl Input');
INSERT INTO `dictionaries` VALUES ('1252', 'en', 'mails.surat_active', 'Mail Status');
INSERT INTO `dictionaries` VALUES ('1253', 'id', 'mails.surat_active', 'Status Surat');
INSERT INTO `dictionaries` VALUES ('1255', 'en', 'inbox_pymts.disposition.receivedby', 'Received By');
INSERT INTO `dictionaries` VALUES ('1256', 'id', 'inbox_pymts.disposition.receivedby', 'Telah diterima oleh');
INSERT INTO `dictionaries` VALUES ('1258', 'en', 'inbox_pymts.disposition.attachment', 'Attachment');
INSERT INTO `dictionaries` VALUES ('1259', 'id', 'inbox_pymts.disposition.attachment', 'Lampiran');
INSERT INTO `dictionaries` VALUES ('1261', 'en', 'base.page_title.applications', 'applications');
INSERT INTO `dictionaries` VALUES ('1262', 'id', 'base.page_title.applications', 'Aplikasi');
INSERT INTO `dictionaries` VALUES ('1264', 'en', 'applications.app_name', 'Application Name');
INSERT INTO `dictionaries` VALUES ('1265', 'id', 'applications.app_name', 'Nama Aplikasi');
INSERT INTO `dictionaries` VALUES ('1266', 'en', 'applications.app_appserver', 'Application Server');
INSERT INTO `dictionaries` VALUES ('1267', 'id', 'applications.app_appserver', 'Server Aplikasi');
INSERT INTO `dictionaries` VALUES ('1268', 'en', 'applications.app_webserver', 'Web Server');
INSERT INTO `dictionaries` VALUES ('1269', 'id', 'applications.app_webserver', 'Server Web');
INSERT INTO `dictionaries` VALUES ('1270', 'en', 'applications.app_dbserver', 'Database Server');
INSERT INTO `dictionaries` VALUES ('1271', 'id', 'applications.app_dbserver', 'Server Database');
INSERT INTO `dictionaries` VALUES ('1272', 'en', 'applications.app_level', 'Application Level');
INSERT INTO `dictionaries` VALUES ('1273', 'id', 'applications.app_level', 'Level Aplikasi');
INSERT INTO `dictionaries` VALUES ('1274', 'en', 'applications.app_status', 'Application Status');
INSERT INTO `dictionaries` VALUES ('1275', 'id', 'applications.app_status', 'Status Aplikasi');
INSERT INTO `dictionaries` VALUES ('1276', 'en', 'applications.app_description', 'Description');
INSERT INTO `dictionaries` VALUES ('1277', 'id', 'applications.app_description', 'Deskripsi');
INSERT INTO `dictionaries` VALUES ('1278', 'en', 'applications.app_type', 'Application Type');
INSERT INTO `dictionaries` VALUES ('1279', 'id', 'applications.app_type', 'Tipe Aplikasi');
INSERT INTO `dictionaries` VALUES ('1280', 'en', 'applications.app_url', 'URL');
INSERT INTO `dictionaries` VALUES ('1281', 'id', 'applications.app_url', 'URL');
INSERT INTO `dictionaries` VALUES ('1282', 'en', 'applications.app_user', 'User');
INSERT INTO `dictionaries` VALUES ('1283', 'id', 'applications.app_user', 'User');
INSERT INTO `dictionaries` VALUES ('1284', 'en', 'applications.app_tutorial', 'Tutorial');
INSERT INTO `dictionaries` VALUES ('1285', 'id', 'applications.app_tutorial', 'Tutorial');
INSERT INTO `dictionaries` VALUES ('1286', 'en', 'base.nav.title.applications', 'Aplications');
INSERT INTO `dictionaries` VALUES ('1287', 'id', 'base.nav.title.applications', 'Aplikasi');
INSERT INTO `dictionaries` VALUES ('1288', 'en', 'base.page_title.faqs', 'FAQ');
INSERT INTO `dictionaries` VALUES ('1289', 'id', 'base.page_title.faqs', 'FAQ');
INSERT INTO `dictionaries` VALUES ('1290', 'en', 'base.page_title.complains', 'complains');
INSERT INTO `dictionaries` VALUES ('1291', 'id', 'base.page_title.complains', 'complains');
INSERT INTO `dictionaries` VALUES ('1292', 'en', 'base.page_title.db_generators', 'DB Generator');
INSERT INTO `dictionaries` VALUES ('1293', 'id', 'base.page_title.db_generators', 'DB Generator');
INSERT INTO `dictionaries` VALUES ('1294', 'en', 'faqs.app_name', 'Application Name');
INSERT INTO `dictionaries` VALUES ('1295', 'id', 'faqs.app_name', 'Nama Aplikasi');
INSERT INTO `dictionaries` VALUES ('1296', 'en', 'faqs.app_description', 'App Description');
INSERT INTO `dictionaries` VALUES ('1297', 'id', 'faqs.app_description', 'Deskripsi');
INSERT INTO `dictionaries` VALUES ('1298', 'en', 'faqs.faq_app_id', 'Application ID');
INSERT INTO `dictionaries` VALUES ('1299', 'id', 'faqs.faq_app_id', 'ID Aplikasi');
INSERT INTO `dictionaries` VALUES ('1300', 'en', 'base.nav.title.faqs', 'FAQ');
INSERT INTO `dictionaries` VALUES ('1301', 'id', 'base.nav.title.faqs', 'FAQ');
INSERT INTO `dictionaries` VALUES ('1302', 'en', 'faqs.faq_question', 'Question');
INSERT INTO `dictionaries` VALUES ('1303', 'id', 'faqs.faq_question', 'Pertanyaan');
INSERT INTO `dictionaries` VALUES ('1304', 'en', 'faqs.faq_answer', 'Answer');
INSERT INTO `dictionaries` VALUES ('1305', 'id', 'faqs.faq_answer', 'Jawaban');
INSERT INTO `dictionaries` VALUES ('1306', 'en', 'faqs.faq_status', 'Status');
INSERT INTO `dictionaries` VALUES ('1307', 'id', 'faqs.faq_status', 'Status');
INSERT INTO `dictionaries` VALUES ('1308', 'en', 'faqs.faq_attachment', 'Attachment');
INSERT INTO `dictionaries` VALUES ('1309', 'id', 'faqs.faq_attachment', 'Lampiran');
INSERT INTO `dictionaries` VALUES ('1310', 'en', 'base.page_title.hoas_member', 'hoas_member');
INSERT INTO `dictionaries` VALUES ('1311', 'id', 'base.page_title.hoas_member', 'hoas_member');
INSERT INTO `dictionaries` VALUES ('1312', 'en', 'base.page_title.ramadhan', 'ramadhan');
INSERT INTO `dictionaries` VALUES ('1313', 'id', 'base.page_title.ramadhan', 'ramadhan');
INSERT INTO `dictionaries` VALUES ('1314', 'en', 'faqs.faq_count', 'Count Asked');
INSERT INTO `dictionaries` VALUES ('1315', 'id', 'faqs.faq_count', 'Jumlah Pertanyaan');
INSERT INTO `dictionaries` VALUES ('1316', 'en', 'hoas_members.pegawai_nama', 'Employee Name');
INSERT INTO `dictionaries` VALUES ('1317', 'id', 'hoas_members.pegawai_nama', 'Nama Pegawai');
INSERT INTO `dictionaries` VALUES ('1318', 'en', 'base.page_title.hoas_members', 'HOA Member');
INSERT INTO `dictionaries` VALUES ('1319', 'id', 'base.page_title.hoas_members', 'Sahabat HOA');
INSERT INTO `dictionaries` VALUES ('1320', 'en', 'base.nav.title.hoas_members', 'HOA Member');
INSERT INTO `dictionaries` VALUES ('1321', 'id', 'base.nav.title.hoas_members', 'Sahabat HOA');
INSERT INTO `dictionaries` VALUES ('1322', 'id', 'base.nav.title.db_generators', 'DB Generator');
INSERT INTO `dictionaries` VALUES ('1323', 'en', 'base.nav.title.db_generators', 'DB Generator');
INSERT INTO `dictionaries` VALUES ('1324', 'en', 'db_generators.app_name', 'Application Name');
INSERT INTO `dictionaries` VALUES ('1325', 'id', 'db_generators.app_name', 'Nama Aplikasi');
INSERT INTO `dictionaries` VALUES ('1326', 'en', 'db_generators.app_appserver', 'Application Server');
INSERT INTO `dictionaries` VALUES ('1327', 'id', 'db_generators.app_appserver', 'Server Aplikasi');
INSERT INTO `dictionaries` VALUES ('1328', 'en', 'db_generators.app_webserver', 'Web Server');
INSERT INTO `dictionaries` VALUES ('1329', 'id', 'db_generators.app_webserver', 'Server Web');
INSERT INTO `dictionaries` VALUES ('1330', 'en', 'db_generators.app_dbserver', 'Database Server');
INSERT INTO `dictionaries` VALUES ('1331', 'id', 'db_generators.app_dbserver', 'Server Database');
INSERT INTO `dictionaries` VALUES ('1332', 'en', 'db_generators.app_level', 'Application Level');
INSERT INTO `dictionaries` VALUES ('1333', 'id', 'db_generators.app_level', 'Level Aplikasi');
INSERT INTO `dictionaries` VALUES ('1334', 'en', 'db_generators.app_status', 'Application Status');
INSERT INTO `dictionaries` VALUES ('1335', 'id', 'db_generators.app_status', 'Status Aplikasi');
INSERT INTO `dictionaries` VALUES ('1336', 'en', 'db_generators.app_description', 'Description');
INSERT INTO `dictionaries` VALUES ('1337', 'id', 'db_generators.app_description', 'Deskripsi');
INSERT INTO `dictionaries` VALUES ('1338', 'en', 'db_generators.app_user', 'User');
INSERT INTO `dictionaries` VALUES ('1339', 'id', 'db_generators.app_user', 'User');
INSERT INTO `dictionaries` VALUES ('1340', 'en', 'db_generators.app_tutorial', 'Tutorial');
INSERT INTO `dictionaries` VALUES ('1341', 'id', 'db_generators.app_tutorial', 'Tutorial');
INSERT INTO `dictionaries` VALUES ('1342', 'en', 'db_generators.table_name', 'Nama Table');
INSERT INTO `dictionaries` VALUES ('1343', 'id', 'db_generators.table_name', 'Table Name');
INSERT INTO `dictionaries` VALUES ('1344', 'en', 'db_generators.table_desc', 'Deskripsi Table');
INSERT INTO `dictionaries` VALUES ('1345', 'id', 'db_generators.table_desc', 'Table Description');
INSERT INTO `dictionaries` VALUES ('1346', 'id', 'db_generators.field_name', 'Field Name');
INSERT INTO `dictionaries` VALUES ('1347', 'en', 'db_generators.field_name', 'Nama Field');
INSERT INTO `dictionaries` VALUES ('1348', 'en', 'db_generators.field_type', 'Type');
INSERT INTO `dictionaries` VALUES ('1349', 'id', 'db_generators.field_type', 'Type');
INSERT INTO `dictionaries` VALUES ('1350', 'id', 'db_generators.field_length', 'Length');
INSERT INTO `dictionaries` VALUES ('1351', 'en', 'db_generators.field_length', 'Length');
INSERT INTO `dictionaries` VALUES ('1352', 'id', 'db_generators.field_scale', 'Scale');
INSERT INTO `dictionaries` VALUES ('1353', 'en', 'db_generators.field_scale', 'Scale');
INSERT INTO `dictionaries` VALUES ('1354', 'id', 'db_generators.field_not_null', 'Not Null');
INSERT INTO `dictionaries` VALUES ('1355', 'en', 'db_generators.field_not_null', 'Not Null');
INSERT INTO `dictionaries` VALUES ('1356', 'id', 'db_generators.field_is_primary_key', 'Is Primary Key');
INSERT INTO `dictionaries` VALUES ('1357', 'en', 'db_generators.field_is_primary_key', 'Is Primary Key');
INSERT INTO `dictionaries` VALUES ('1358', 'en', 'base.page_title.projects', 'Project');
INSERT INTO `dictionaries` VALUES ('1359', 'id', 'base.page_title.projects', 'Project');
INSERT INTO `dictionaries` VALUES ('1360', 'en', 'base.nav.title.projects', 'Projects');
INSERT INTO `dictionaries` VALUES ('1361', 'id', 'base.nav.title.projects', 'Projek');
INSERT INTO `dictionaries` VALUES ('1362', 'en', 'projects.app_name', 'Application Name');
INSERT INTO `dictionaries` VALUES ('1363', 'id', 'projects.app_name', 'Nama Aplikasi');
INSERT INTO `dictionaries` VALUES ('1364', 'en', 'projects.app_level', 'Application Level');
INSERT INTO `dictionaries` VALUES ('1365', 'id', 'projects.app_level', 'Level Aplikasi');
INSERT INTO `dictionaries` VALUES ('1366', 'en', 'projects.app_status', 'Application Step Status');
INSERT INTO `dictionaries` VALUES ('1367', 'id', 'projects.app_status', 'Tahap Aplikasi');
INSERT INTO `dictionaries` VALUES ('1368', 'en', 'projects.app_is_active', 'Application Status');
INSERT INTO `dictionaries` VALUES ('1369', 'id', 'projects.app_is_active', 'Status Aplikasi');
INSERT INTO `dictionaries` VALUES ('1370', 'en', 'projects.app_user', 'User');
INSERT INTO `dictionaries` VALUES ('1371', 'id', 'projects.app_user', 'User');
INSERT INTO `dictionaries` VALUES ('1372', 'en', 'projects.app_type', 'Application Type');
INSERT INTO `dictionaries` VALUES ('1373', 'id', 'projects.app_type', 'Tipe Aplikasi');
INSERT INTO `dictionaries` VALUES ('1374', 'en', 'projects.app_created', 'Created');
INSERT INTO `dictionaries` VALUES ('1375', 'id', 'projects.app_created', 'Tgl Input');
INSERT INTO `dictionaries` VALUES ('1376', 'en', 'projects.app_createdby', 'Created By');
INSERT INTO `dictionaries` VALUES ('1377', 'id', 'projects.app_createdby', 'Diinput oleh');
INSERT INTO `dictionaries` VALUES ('1378', 'en', 'projects.app_project_type_label', 'Project Type');
INSERT INTO `dictionaries` VALUES ('1379', 'id', 'projects.app_project_type_label', 'Tipe Project');
INSERT INTO `dictionaries` VALUES ('1380', 'en', 'projects.app_target_live_label', 'Live Target');
INSERT INTO `dictionaries` VALUES ('1381', 'id', 'projects.app_target_live_label', 'Target Live');
INSERT INTO `dictionaries` VALUES ('1382', 'en', 'projects.app_rbb_nrbb_label', 'RBB or Non RBB');
INSERT INTO `dictionaries` VALUES ('1383', 'id', 'projects.app_rbb_nrbb_label', 'RBB atau Non RBB');
INSERT INTO `dictionaries` VALUES ('1384', 'en', 'projects.assigned_employee', 'Person In Charge');
INSERT INTO `dictionaries` VALUES ('1385', 'id', 'projects.assigned_employee', 'Pegawai ditugaskan');
INSERT INTO `dictionaries` VALUES ('1386', 'en', 'projects.appmatrix_role_id', 'Role ID');
INSERT INTO `dictionaries` VALUES ('1387', 'id', 'projects.appmatrix_role_id', 'Role ID');
INSERT INTO `dictionaries` VALUES ('1388', 'en', 'projects.appmatrix_name', 'Role Name');
INSERT INTO `dictionaries` VALUES ('1389', 'id', 'projects.appmatrix_name', 'Nama Role');
INSERT INTO `dictionaries` VALUES ('1390', 'en', 'projects.appmatrix_description', 'Role Description');
INSERT INTO `dictionaries` VALUES ('1391', 'id', 'projects.appmatrix_description', 'Deskripsi Role');
INSERT INTO `dictionaries` VALUES ('1392', 'en', 'projects.appmodules_name', 'Module Name');
INSERT INTO `dictionaries` VALUES ('1393', 'id', 'projects.appmodules_name', 'Nama Modul');
INSERT INTO `dictionaries` VALUES ('1394', 'en', 'projects.appmodules_appprogrammers_id', 'Programmers');
INSERT INTO `dictionaries` VALUES ('1395', 'id', 'projects.appmodules_appprogrammers_id', 'Programmer');
INSERT INTO `dictionaries` VALUES ('1396', 'en', 'projects.appmodules_description', 'Module Description');
INSERT INTO `dictionaries` VALUES ('1397', 'id', 'projects.appmodules_description', 'Deskripsi Modul');
INSERT INTO `dictionaries` VALUES ('1398', 'en', 'projects.app_year', 'Year');
INSERT INTO `dictionaries` VALUES ('1399', 'id', 'projects.app_year', 'Tahun');
INSERT INTO `dictionaries` VALUES ('1400', 'en', 'projects.app_description', 'Description');
INSERT INTO `dictionaries` VALUES ('1401', 'id', 'projects.app_description', 'Deskripsi');
INSERT INTO `dictionaries` VALUES ('1402', 'en', 'base.page_title.parameters', 'Parameter');
INSERT INTO `dictionaries` VALUES ('1403', 'id', 'base.page_title.parameters', 'Parameter');
INSERT INTO `dictionaries` VALUES ('1404', 'en', 'base.page_title.parameter_groups', 'Parameter Group');
INSERT INTO `dictionaries` VALUES ('1405', 'id', 'base.page_title.parameter_groups', 'Parameter Group');
INSERT INTO `dictionaries` VALUES ('1406', 'en', 'base.nav.title.parameters', 'Parameter');
INSERT INTO `dictionaries` VALUES ('1407', 'id', 'base.nav.title.parameters', 'Parameter');
INSERT INTO `dictionaries` VALUES ('1408', 'en', 'base.nav.title.parameter_groups', 'Parameter Group');
INSERT INTO `dictionaries` VALUES ('1409', 'id', 'base.nav.title.parameter_groups', 'Parameter Group');
INSERT INTO `dictionaries` VALUES ('1410', 'en', 'parameters.parameter_key', 'Parameter Key');
INSERT INTO `dictionaries` VALUES ('1411', 'id', 'parameters.parameter_key', 'Parameter Key');
INSERT INTO `dictionaries` VALUES ('1412', 'en', 'parameters.parameter_value', 'Parameter Value');
INSERT INTO `dictionaries` VALUES ('1413', 'id', 'parameters.parameter_value', 'Parameter Value');
INSERT INTO `dictionaries` VALUES ('1414', 'en', 'parameters.parameter_status', 'Parameter Status');
INSERT INTO `dictionaries` VALUES ('1415', 'id', 'parameters.parameter_status', 'Parameter Status');
INSERT INTO `dictionaries` VALUES ('1418', 'en', 'parameters.parameter_group_parameter_id', 'Parameter id');
INSERT INTO `dictionaries` VALUES ('1419', 'id', 'parameters.parameter_group_parameter_id', 'Parameter id');
INSERT INTO `dictionaries` VALUES ('1420', 'id', 'parameters.parameter_order', 'Parameter Order');
INSERT INTO `dictionaries` VALUES ('1421', 'en', 'parameters.parameter_order', 'Parameter Order');
INSERT INTO `dictionaries` VALUES ('1422', 'en', 'parameter_groups.group_parameter_id', 'Id');
INSERT INTO `dictionaries` VALUES ('1423', 'id', 'parameter_groups.group_parameter_id', 'Id');
INSERT INTO `dictionaries` VALUES ('1424', 'en', 'parameter_groups.group_parameter_name', 'Name');
INSERT INTO `dictionaries` VALUES ('1425', 'id', 'parameter_groups.group_parameter_name', 'Name');
INSERT INTO `dictionaries` VALUES ('1426', 'en', 'parameter_groups.group_parameter_status', 'Status');
INSERT INTO `dictionaries` VALUES ('1427', 'id', 'parameter_groups.group_parameter_status', 'Status');

-- ----------------------------
-- Table structure for parameters
-- ----------------------------
DROP TABLE IF EXISTS `parameters`;
CREATE TABLE `parameters` (
  `parameter_key` varchar(10) NOT NULL,
  `parameter_value` varchar(255) DEFAULT NULL,
  `parameter_status` smallint(6) DEFAULT NULL,
  `parameter_desc` text,
  PRIMARY KEY (`parameter_key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of parameters
-- ----------------------------

-- ----------------------------
-- Table structure for productdetails
-- ----------------------------
DROP TABLE IF EXISTS `productdetails`;
CREATE TABLE `productdetails` (
  `productdetail_id` varchar(36) DEFAULT NULL,
  `productdetail_product_id` varchar(36) DEFAULT NULL,
  `productdetail_description` varchar(255) DEFAULT NULL,
  `productdetail_status` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of productdetails
-- ----------------------------

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `product_id` varchar(36) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_description` text,
  `product_price` varchar(25) DEFAULT NULL,
  `product_status` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of products
-- ----------------------------
