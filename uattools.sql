/*
Navicat MySQL Data Transfer

Source Server         : localhost_mySQL
Source Server Version : 50626
Source Host           : localhost:3306
Source Database       : uattools

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2018-09-13 15:04:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for audit_trails
-- ----------------------------
DROP TABLE IF EXISTS `audit_trails`;
CREATE TABLE `audit_trails` (
  `audit_trail_id` varchar(36) DEFAULT NULL,
  `audit_trail_action_date` datetime DEFAULT NULL,
  `audit_trail_user_id` varchar(36) DEFAULT NULL,
  `audit_trail_ip_address` varchar(16) DEFAULT NULL,
  `audit_trail_browser_ua` text,
  `audit_trail_history_id` varchar(36) DEFAULT NULL,
  `audit_trail_status` varchar(4) DEFAULT NULL,
  `audit_trail_action_id` varchar(25) DEFAULT NULL,
  `audit_trail_module_id` varchar(32) DEFAULT NULL,
  `audit_trail_record_id` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of audit_trails
-- ----------------------------
INSERT INTO `audit_trails` VALUES ('25c97946-b4d0-11e8-b773-54650c0f9c19', '2018-09-10 15:04:31', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '25bee72b-b4d0-11e8-b773-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('3f242c3c-b4d6-11e8-b773-54650c0f9c19', '2018-09-10 15:48:11', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '3f113b14-b4d6-11e8-b773-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('5ccaba41-b4d6-11e8-b773-54650c0f9c19', '2018-09-10 15:49:01', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '5cba2ec7-b4d6-11e8-b773-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('82995f03-b4d6-11e8-b773-54650c0f9c19', '2018-09-10 15:50:04', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '828a6c15-b4d6-11e8-b773-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('21b63531-b4d9-11e8-b773-54650c0f9c19', '2018-09-10 16:08:50', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '21a8afcd-b4d9-11e8-b773-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('87e5a92d-b4dc-11e8-b773-54650c0f9c19', '2018-09-10 16:33:10', '', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '87d0629d-b4dc-11e8-b773-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('0d43a1f7-b4e8-11e8-b773-54650c0f9c19', '2018-09-10 17:55:38', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '0d1f24ec-b4e8-11e8-b773-54650c0f9c19', '0000', 'add', 'companies', 'tes');
INSERT INTO `audit_trails` VALUES ('2ff88a8d-b4ec-11e8-b773-54650c0f9c19', '2018-09-10 18:25:15', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '2fe92685-b4ec-11e8-b773-54650c0f9c19', '0000', 'edit', 'companies', '0');
INSERT INTO `audit_trails` VALUES ('8190a09b-b4ec-11e8-b773-54650c0f9c19', '2018-09-10 18:27:31', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '81874dbe-b4ec-11e8-b773-54650c0f9c19', '0000', 'edit', 'companies', '2');
INSERT INTO `audit_trails` VALUES ('e9ea6f1e-b4ed-11e8-b773-54650c0f9c19', '2018-09-10 18:37:36', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 'e9dc093a-b4ed-11e8-b773-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('f9405b57-b586-11e8-b40f-54650c0f9c19', '2018-09-11 12:53:14', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 'f906192b-b586-11e8-b40f-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('d49bb248-b58d-11e8-b40f-54650c0f9c19', '2018-09-11 13:42:19', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 'd48f94f3-b58d-11e8-b40f-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('0e63b745-b629-11e8-ad1f-54650c0f9c19', '2018-09-12 08:13:28', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '0e4978d8-b629-11e8-ad1f-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('53a19b86-b637-11e8-ad1f-54650c0f9c19', '2018-09-12 09:55:37', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '5394b512-b637-11e8-ad1f-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('65bcb9aa-b637-11e8-ad1f-54650c0f9c19', '2018-09-12 09:56:08', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '65ade376-b637-11e8-ad1f-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('e29b91a7-b6bd-11e8-89fd-54650c0f9c19', '2018-09-13 01:58:50', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 'e259af87-b6bd-11e8-89fd-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('3bf83f44-b6be-11e8-89fd-54650c0f9c19', '2018-09-13 02:01:20', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '3beb5857-b6be-11e8-89fd-54650c0f9c19', '0000', 'add', 'companies', 'tes 3');
INSERT INTO `audit_trails` VALUES ('455b8c97-b6be-11e8-89fd-54650c0f9c19', '2018-09-13 02:01:35', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '454c75fc-b6be-11e8-89fd-54650c0f9c19', '0000', 'edit', 'companies', '3');
INSERT INTO `audit_trails` VALUES ('91309089-b6be-11e8-89fd-54650c0f9c19', '2018-09-13 02:03:43', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '91251cce-b6be-11e8-89fd-54650c0f9c19', '0000', 'add', 'sitegroups', 'tes');
INSERT INTO `audit_trails` VALUES ('bbdbd1d7-b6be-11e8-89fd-54650c0f9c19', '2018-09-13 02:04:54', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 'bbd417bf-b6be-11e8-89fd-54650c0f9c19', '0000', 'edit', 'sitegroups', '2');
INSERT INTO `audit_trails` VALUES ('ebde30c3-b6be-11e8-89fd-54650c0f9c19', '2018-09-13 02:06:15', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 'ebd55427-b6be-11e8-89fd-54650c0f9c19', '0000', 'add', 'units', 'tes');
INSERT INTO `audit_trails` VALUES ('f42c9628-b6be-11e8-89fd-54650c0f9c19', '2018-09-13 02:06:29', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 'f4211e7c-b6be-11e8-89fd-54650c0f9c19', '0000', 'edit', 'units', '2');
INSERT INTO `audit_trails` VALUES ('2b26117d-b6c0-11e8-89fd-54650c0f9c19', '2018-09-13 02:15:10', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '2b1d1014-b6c0-11e8-89fd-54650c0f9c19', '0000', 'add', 'hwtypes', 'hw1');
INSERT INTO `audit_trails` VALUES ('34052deb-b6c0-11e8-89fd-54650c0f9c19', '2018-09-13 02:15:25', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '3401719a-b6c0-11e8-89fd-54650c0f9c19', '0000', 'edit', 'hwtypes', '1');
INSERT INTO `audit_trails` VALUES ('6377e959-b6c0-11e8-89fd-54650c0f9c19', '2018-09-13 02:16:45', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '636f3ad1-b6c0-11e8-89fd-54650c0f9c19', '0000', 'add', 'hostnames', 'host1');
INSERT INTO `audit_trails` VALUES ('6b61ce9f-b6c0-11e8-89fd-54650c0f9c19', '2018-09-13 02:16:58', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '6b569891-b6c0-11e8-89fd-54650c0f9c19', '0000', 'edit', 'hostnames', '1');
INSERT INTO `audit_trails` VALUES ('1d3b5e78-b6c1-11e8-89fd-54650c0f9c19', '2018-09-13 02:21:57', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '1d33cf9e-b6c1-11e8-89fd-54650c0f9c19', '0000', 'add', 'groupinformations', 'group1');
INSERT INTO `audit_trails` VALUES ('26236909-b6c1-11e8-89fd-54650c0f9c19', '2018-09-13 02:22:11', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '261a70c7-b6c1-11e8-89fd-54650c0f9c19', '0000', 'edit', 'groupinformations', '1');
INSERT INTO `audit_trails` VALUES ('77072631-b6f1-11e8-89fd-54650c0f9c19', '2018-09-13 08:08:05', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '76f9097e-b6f1-11e8-89fd-54650c0f9c19', '0000', 'login', 'main', 'login');
INSERT INTO `audit_trails` VALUES ('3025bb3a-b6f9-11e8-89fd-54650c0f9c19', '2018-09-13 09:03:22', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '30221473-b6f9-11e8-89fd-54650c0f9c19', '0000', 'add', 'users', 'tes');
INSERT INTO `audit_trails` VALUES ('cd698a97-b6fb-11e8-89fd-54650c0f9c19', '2018-09-13 09:22:05', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 'cd5e2a73-b6fb-11e8-89fd-54650c0f9c19', '0000', 'edit', 'users', 'd651');
INSERT INTO `audit_trails` VALUES ('18ebe941-b6fc-11e8-89fd-54650c0f9c19', '2018-09-13 09:24:12', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '18ddc9bd-b6fc-11e8-89fd-54650c0f9c19', '0000', 'edit', 'users', 'd651');
INSERT INTO `audit_trails` VALUES ('3ed278bb-b6fc-11e8-89fd-54650c0f9c19', '2018-09-13 09:25:15', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '3ebf4f66-b6fc-11e8-89fd-54650c0f9c19', '0000', 'edit', 'companies', '3');
INSERT INTO `audit_trails` VALUES ('4cecbabf-b6fc-11e8-89fd-54650c0f9c19', '2018-09-13 09:25:39', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '4ce77ae1-b6fc-11e8-89fd-54650c0f9c19', '0000', 'edit', 'sitegroups', '2');
INSERT INTO `audit_trails` VALUES ('58ef428b-b6fc-11e8-89fd-54650c0f9c19', '2018-09-13 09:25:59', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '58e2e0cb-b6fc-11e8-89fd-54650c0f9c19', '0000', 'edit', 'units', '2');
INSERT INTO `audit_trails` VALUES ('676a2118-b6fc-11e8-89fd-54650c0f9c19', '2018-09-13 09:26:24', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '675c0015-b6fc-11e8-89fd-54650c0f9c19', '0000', 'edit', 'hwtypes', '1');
INSERT INTO `audit_trails` VALUES ('78346de8-b6fc-11e8-89fd-54650c0f9c19', '2018-09-13 09:26:52', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '7827672f-b6fc-11e8-89fd-54650c0f9c19', '0000', 'edit', 'hostnames', '1');
INSERT INTO `audit_trails` VALUES ('92812340-b701-11e8-89fd-54650c0f9c19', '2018-09-13 10:03:23', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '927d7612-b701-11e8-89fd-54650c0f9c19', '0000', 'add', 'hardwares', 'tes');
INSERT INTO `audit_trails` VALUES ('20a86dd3-b70b-11e8-89fd-54650c0f9c19', '2018-09-13 11:11:47', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '209ef0b6-b70b-11e8-89fd-54650c0f9c19', '0000', 'edit', 'hardwares', '1');
INSERT INTO `audit_trails` VALUES ('bfc727ad-b70c-11e8-89fd-54650c0f9c19', '2018-09-13 11:23:24', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 'bfc2089e-b70c-11e8-89fd-54650c0f9c19', '0000', 'edit', 'groupinformations', '1');
INSERT INTO `audit_trails` VALUES ('622714e7-b71d-11e8-89fd-54650c0f9c19', '2018-09-13 13:22:29', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '621c349a-b71d-11e8-89fd-54650c0f9c19', '0000', 'add', 'informations', 'tes');
INSERT INTO `audit_trails` VALUES ('c78d9250-b71e-11e8-89fd-54650c0f9c19', '2018-09-13 13:32:28', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 'c77e19ec-b71e-11e8-89fd-54650c0f9c19', '0000', 'edit', 'informations', '4');
INSERT INTO `audit_trails` VALUES ('baa3a396-b720-11e8-89fd-54650c0f9c19', '2018-09-13 13:46:25', 'd650', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 'ba973466-b720-11e8-89fd-54650c0f9c19', '0000', 'edit', 'informations', '4');

-- ----------------------------
-- Table structure for companies
-- ----------------------------
DROP TABLE IF EXISTS `companies`;
CREATE TABLE `companies` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of companies
-- ----------------------------
INSERT INTO `companies` VALUES ('1', 'Company A', null, null, null);
INSERT INTO `companies` VALUES ('2', 'tes2', null, null, null);
INSERT INTO `companies` VALUES ('3', 'tes 5', null, null, null);

-- ----------------------------
-- Table structure for configurationgroup
-- ----------------------------
DROP TABLE IF EXISTS `configurationgroup`;
CREATE TABLE `configurationgroup` (
  `configurationgroup_id` varchar(36) NOT NULL,
  `configurationgroup_name` varchar(100) NOT NULL,
  `configurationgroup_status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`configurationgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of configurationgroup
-- ----------------------------
INSERT INTO `configurationgroup` VALUES ('11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6E', 'parameter system', '1');
INSERT INTO `configurationgroup` VALUES ('11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'Jenis Identitas', '1');
INSERT INTO `configurationgroup` VALUES ('11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6G', 'Status Anggota', '1');
INSERT INTO `configurationgroup` VALUES ('11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6H', 'Status trxsaldo', '1');
INSERT INTO `configurationgroup` VALUES ('11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6I', 'Metode Pembayaran', '1');
INSERT INTO `configurationgroup` VALUES ('11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6J', 'Pembayaran status', '1');
INSERT INTO `configurationgroup` VALUES ('11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6K', 'Pesanan Status', '1');

-- ----------------------------
-- Table structure for configurations
-- ----------------------------
DROP TABLE IF EXISTS `configurations`;
CREATE TABLE `configurations` (
  `configuration_id` varchar(36) NOT NULL,
  `configuration_key` varchar(80) DEFAULT NULL,
  `configuration_value` longtext,
  `configuration_status` tinyint(4) DEFAULT NULL,
  `configurationgroup_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`configuration_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of configurations
-- ----------------------------
INSERT INTO `configurations` VALUES ('12EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '1', 'KTP', null, '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F');
INSERT INTO `configurations` VALUES ('13EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '2', 'SIM', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F');
INSERT INTO `configurations` VALUES ('14EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'statusanggota.waiting', 'waiting', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6G');
INSERT INTO `configurations` VALUES ('15EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'statusanggota.active', 'active', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6G');
INSERT INTO `configurations` VALUES ('16EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'statusanggota.inactive', 'inactive', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6G');
INSERT INTO `configurations` VALUES ('17EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'statusanggota.rejected', 'rejected', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6G');
INSERT INTO `configurations` VALUES ('18EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '0', 'waiting', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6H');
INSERT INTO `configurations` VALUES ('19EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '1', 'approve', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6H');
INSERT INTO `configurations` VALUES ('20EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '2', 'reject', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6H');
INSERT INTO `configurations` VALUES ('21EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '1', 'Transfer', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6I');
INSERT INTO `configurations` VALUES ('22EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '2', 'Tunai', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6I');
INSERT INTO `configurations` VALUES ('23EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '0', 'waiting', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6J');
INSERT INTO `configurations` VALUES ('24EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '1', 'Approve', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6J');
INSERT INTO `configurations` VALUES ('25EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '2', 'Reject', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6J');
INSERT INTO `configurations` VALUES ('26EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', 'MIN_POINT_ORDER', '9', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6E');
INSERT INTO `configurations` VALUES ('27EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '0', 'waiting', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6K');
INSERT INTO `configurations` VALUES ('28EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '1', 'approve', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6K');
INSERT INTO `configurations` VALUES ('29EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '2', 'reject', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6K');
INSERT INTO `configurations` VALUES ('30EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '3', 'packing', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6K');
INSERT INTO `configurations` VALUES ('31EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '4', 'ready_to_send', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6K');
INSERT INTO `configurations` VALUES ('32EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '5', 'sent', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6K');
INSERT INTO `configurations` VALUES ('33EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6F', '3', 'Saldo', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6I');
INSERT INTO `configurations` VALUES ('CF371C9618AB459299790F9A4A120F49', 'copyright', '2016. All rights reserved. andrimuhammad', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6E');
INSERT INTO `configurations` VALUES ('EB43051CAFAD49BDBACDC922F47D904C', 'created_by', 'templatelab', '1', '11EE2D0F-9420-4DD8-BE8C-A94FCE9BAD6E');

-- ----------------------------
-- Table structure for detail_informations
-- ----------------------------
DROP TABLE IF EXISTS `detail_informations`;
CREATE TABLE `detail_informations` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `status_id` int(10) NOT NULL,
  `group_detail_information_id` int(10) NOT NULL,
  `parent` int(10) DEFAULT NULL,
  `role_id` int(10) NOT NULL,
  `order` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKdetail_inf797436` (`group_detail_information_id`),
  CONSTRAINT `FKdetail_inf797436` FOREIGN KEY (`group_detail_information_id`) REFERENCES `group_detail_informations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of detail_informations
-- ----------------------------
INSERT INTO `detail_informations` VALUES ('4', 'tes2', '1', '1', '0', '1', '1', null, null, null);

-- ----------------------------
-- Table structure for dictionaries
-- ----------------------------
DROP TABLE IF EXISTS `dictionaries`;
CREATE TABLE `dictionaries` (
  `dictionary_id` int(11) NOT NULL,
  `dictionary_lang_id` char(4) NOT NULL,
  `dictionary_key` varchar(255) NOT NULL,
  `dictionary_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`dictionary_id`),
  KEY `dictionary_lang_id` (`dictionary_lang_id`),
  CONSTRAINT `dictionaries_ibfk_1` FOREIGN KEY (`dictionary_lang_id`) REFERENCES `languages` (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dictionaries
-- ----------------------------
INSERT INTO `dictionaries` VALUES ('0', 'id', 'base.page_title.products', 'Produk');
INSERT INTO `dictionaries` VALUES ('1', 'en', 'base.page_title.settings', 'Setting');
INSERT INTO `dictionaries` VALUES ('2', 'id', 'base.page_title.settings', 'Pengaturan');
INSERT INTO `dictionaries` VALUES ('3', 'en', 'base.page_title.menus', 'Menus');
INSERT INTO `dictionaries` VALUES ('4', 'id', 'base.page_title.menus', 'Menu');
INSERT INTO `dictionaries` VALUES ('5', 'en', 'base.page_title.users', 'Users');
INSERT INTO `dictionaries` VALUES ('6', 'id', 'base.page_title.users', 'User');
INSERT INTO `dictionaries` VALUES ('7', 'en', 'base.page_title.roles', 'Roles');
INSERT INTO `dictionaries` VALUES ('8', 'id', 'base.page_title.roles', 'Hak Akses');
INSERT INTO `dictionaries` VALUES ('9', 'en', 'base.page_title.languages', 'Languages');
INSERT INTO `dictionaries` VALUES ('10', 'id', 'base.page_title.languages', 'Bahasa');
INSERT INTO `dictionaries` VALUES ('11', 'en', 'base.page_title.dictionaries', 'Dictionaries');
INSERT INTO `dictionaries` VALUES ('12', 'id', 'base.page_title.dictionaries', 'Kamus Kata');
INSERT INTO `dictionaries` VALUES ('13', 'en', 'base.page_title.system_parameters', 'System Parameters');
INSERT INTO `dictionaries` VALUES ('14', 'id', 'base.page_title.system_parameters', 'Sistem Parameter');
INSERT INTO `dictionaries` VALUES ('15', 'en', 'base.page_title.group_parameters', 'Group Parameters');
INSERT INTO `dictionaries` VALUES ('16', 'id', 'base.page_title.group_parameters', 'Grup Parameter');
INSERT INTO `dictionaries` VALUES ('17', 'en', 'base.page_title.parameters', 'Parameters');
INSERT INTO `dictionaries` VALUES ('18', 'id', 'base.page_title.parameters', 'Parameter');
INSERT INTO `dictionaries` VALUES ('19', 'en', 'base.page_title.uids', 'UID');
INSERT INTO `dictionaries` VALUES ('20', 'id', 'base.page_title.uids', 'UID');
INSERT INTO `dictionaries` VALUES ('21', 'en', 'base.page_title.offices', 'Offices');
INSERT INTO `dictionaries` VALUES ('22', 'id', 'base.page_title.offices', 'Kantor');
INSERT INTO `dictionaries` VALUES ('23', 'en', 'base.page_title.parent_offices', 'Parent Offices');
INSERT INTO `dictionaries` VALUES ('24', 'id', 'base.page_title.parent_offices', 'Kantor Induk');
INSERT INTO `dictionaries` VALUES ('25', 'en', 'base.page_title.branch_offices', 'Branch Offices');
INSERT INTO `dictionaries` VALUES ('26', 'id', 'base.page_title.branch_offices', 'Kantor Cabang');
INSERT INTO `dictionaries` VALUES ('27', 'en', 'base.page_title.mapping', 'Mapping');
INSERT INTO `dictionaries` VALUES ('28', 'id', 'base.page_title.mapping', 'Mapping');
INSERT INTO `dictionaries` VALUES ('29', 'en', 'base.page_title.applications', 'Applications');
INSERT INTO `dictionaries` VALUES ('30', 'id', 'base.page_title.applications', 'Aplikasi');
INSERT INTO `dictionaries` VALUES ('31', 'en', 'base.page_title.functions', 'Functions');
INSERT INTO `dictionaries` VALUES ('32', 'id', 'base.page_title.functions', 'Fungsi');
INSERT INTO `dictionaries` VALUES ('33', 'en', 'base.page_title.mapping_access', 'UID Access');
INSERT INTO `dictionaries` VALUES ('34', 'id', 'base.page_title.mapping_access', 'Akses UID');
INSERT INTO `dictionaries` VALUES ('35', 'en', 'base.page_title.logs', 'Logs');
INSERT INTO `dictionaries` VALUES ('36', 'id', 'base.page_title.logs', 'Log');
INSERT INTO `dictionaries` VALUES ('39', 'en', 'base.page_title.servers', 'Servers');
INSERT INTO `dictionaries` VALUES ('40', 'id', 'base.page_title.servers', 'Server');
INSERT INTO `dictionaries` VALUES ('41', 'en', 'base.page_title.simulators', 'Simulator');
INSERT INTO `dictionaries` VALUES ('42', 'id', 'base.page_title.simulators', 'Simulator');
INSERT INTO `dictionaries` VALUES ('43', 'en', 'base.page_title.pc_address', 'PC IP Address');
INSERT INTO `dictionaries` VALUES ('44', 'id', 'base.page_title.pc_address', 'IP Address PC');
INSERT INTO `dictionaries` VALUES ('45', 'en', 'base.page_title.login_simulation', 'Login Simulation');
INSERT INTO `dictionaries` VALUES ('46', 'id', 'base.page_title.login_simulation', 'Simulasi Login');
INSERT INTO `dictionaries` VALUES ('47', 'en', 'menus.menu_name', 'Menu Name');
INSERT INTO `dictionaries` VALUES ('48', 'id', 'menus.menu_name', 'Nama Menu');
INSERT INTO `dictionaries` VALUES ('49', 'en', 'menus.menu_url', 'URL');
INSERT INTO `dictionaries` VALUES ('50', 'id', 'menus.menu_url', 'URL');
INSERT INTO `dictionaries` VALUES ('51', 'en', 'menus.menu_icon', 'Icon');
INSERT INTO `dictionaries` VALUES ('52', 'id', 'menus.menu_icon', 'Icon');
INSERT INTO `dictionaries` VALUES ('53', 'en', 'menus.menu_parent', 'Parent Menu');
INSERT INTO `dictionaries` VALUES ('54', 'id', 'menus.menu_parent', 'Menu Parent');
INSERT INTO `dictionaries` VALUES ('55', 'en', 'menus.menu_order', 'Order');
INSERT INTO `dictionaries` VALUES ('56', 'id', 'menus.menu_order', 'Urutan');
INSERT INTO `dictionaries` VALUES ('57', 'en', 'menus.menu_active', 'Status');
INSERT INTO `dictionaries` VALUES ('58', 'id', 'menus.menu_active', 'Menu');
INSERT INTO `dictionaries` VALUES ('59', 'en', 'menus.menu_create_module', 'Create Module File');
INSERT INTO `dictionaries` VALUES ('60', 'id', 'menus.menu_create_module', 'Buat File Module');
INSERT INTO `dictionaries` VALUES ('61', 'en', 'roles.role_name', 'Role Name');
INSERT INTO `dictionaries` VALUES ('62', 'id', 'roles.role_name', 'Nama Hak Akses');
INSERT INTO `dictionaries` VALUES ('63', 'en', 'roles.role_menu', 'Menus');
INSERT INTO `dictionaries` VALUES ('64', 'id', 'roles.role_menu', 'Menu');
INSERT INTO `dictionaries` VALUES ('65', 'en', 'roles.role_level', 'Level');
INSERT INTO `dictionaries` VALUES ('66', 'id', 'roles.role_level', 'Level');
INSERT INTO `dictionaries` VALUES ('67', 'en', 'roles.role_active', 'Status');
INSERT INTO `dictionaries` VALUES ('68', 'id', 'roles.role_active', 'Status');
INSERT INTO `dictionaries` VALUES ('69', 'en', 'roles.menu_id', 'Menu');
INSERT INTO `dictionaries` VALUES ('70', 'id', 'roles.menu_id', 'Menu');
INSERT INTO `dictionaries` VALUES ('71', 'en', 'users.identity', 'Identity');
INSERT INTO `dictionaries` VALUES ('72', 'id', 'users.identity', 'Identitas');
INSERT INTO `dictionaries` VALUES ('73', 'en', 'users.role_name', 'Role');
INSERT INTO `dictionaries` VALUES ('74', 'id', 'users.role_name', 'Hak Akses');
INSERT INTO `dictionaries` VALUES ('75', 'en', 'languages.language_id', 'Language');
INSERT INTO `dictionaries` VALUES ('76', 'id', 'languages.language_id', 'Bahasa');
INSERT INTO `dictionaries` VALUES ('77', 'en', 'languages.language_name', 'Language Name');
INSERT INTO `dictionaries` VALUES ('78', 'id', 'languages.language_name', 'Nama Bahasa');
INSERT INTO `dictionaries` VALUES ('79', 'en', 'languages.language_default', 'Default Language');
INSERT INTO `dictionaries` VALUES ('80', 'id', 'languages.language_default', 'Bahasa Utama');
INSERT INTO `dictionaries` VALUES ('81', 'en', 'languages.language_active', 'Status');
INSERT INTO `dictionaries` VALUES ('82', 'id', 'languages.language_active', 'Status');
INSERT INTO `dictionaries` VALUES ('83', 'en', 'dictionaries.dictionary_key', 'Dictionary Key');
INSERT INTO `dictionaries` VALUES ('84', 'id', 'dictionaries.dictionary_key', 'Kunci Kamus Kata');
INSERT INTO `dictionaries` VALUES ('85', 'en', 'dictionaries.dictionary_value', 'Dictionary Value');
INSERT INTO `dictionaries` VALUES ('86', 'id', 'dictionaries.dictionary_value', 'Isi Kamus Kata');
INSERT INTO `dictionaries` VALUES ('87', 'en', 'dictionaries.kamus', 'Dictionaries');
INSERT INTO `dictionaries` VALUES ('88', 'id', 'dictionaries.kamus', 'Kamus');
INSERT INTO `dictionaries` VALUES ('89', 'en', 'group_parameters.group_parameter_name', 'Group Parameter Name');
INSERT INTO `dictionaries` VALUES ('90', 'id', 'group_parameters.group_parameter_name', 'Nama Parameter Grup');
INSERT INTO `dictionaries` VALUES ('91', 'en', 'group_parameters.group_parameter_status', 'Status');
INSERT INTO `dictionaries` VALUES ('92', 'id', 'group_parameters.group_parameter_status', 'Status');
INSERT INTO `dictionaries` VALUES ('93', 'en', 'parameters.parameter_group_id', 'Group Parameter');
INSERT INTO `dictionaries` VALUES ('94', 'id', 'parameters.parameter_group_id', 'Parameter Grup');
INSERT INTO `dictionaries` VALUES ('95', 'en', 'parameters.parameter_name', 'Parameter');
INSERT INTO `dictionaries` VALUES ('96', 'id', 'parameters.parameter_name', 'Parameter');
INSERT INTO `dictionaries` VALUES ('97', 'en', 'parameters.parameter_status', 'Status');
INSERT INTO `dictionaries` VALUES ('98', 'id', 'parameters.parameter_status', 'Status');
INSERT INTO `dictionaries` VALUES ('99', 'en', 'uids.uid', 'UID');
INSERT INTO `dictionaries` VALUES ('100', 'id', 'uids.uid', 'UID');
INSERT INTO `dictionaries` VALUES ('101', 'en', 'uids.uid_name', 'Name');
INSERT INTO `dictionaries` VALUES ('102', 'id', 'uids.uid_name', 'Nama');
INSERT INTO `dictionaries` VALUES ('109', 'en', 'uids.uid_status', 'Status');
INSERT INTO `dictionaries` VALUES ('110', 'id', 'uids.uid_status', 'Status');
INSERT INTO `dictionaries` VALUES ('115', 'en', 'uids.uid_position', 'Position');
INSERT INTO `dictionaries` VALUES ('116', 'id', 'uids.uid_position', 'Posisi');
INSERT INTO `dictionaries` VALUES ('117', 'en', 'uids.uid_is_active', 'Active');
INSERT INTO `dictionaries` VALUES ('118', 'id', 'uids.uid_is_active', 'Aktif');
INSERT INTO `dictionaries` VALUES ('119', 'en', 'uids.uid_pass_is_expired', 'Password is Expired');
INSERT INTO `dictionaries` VALUES ('120', 'id', 'uids.uid_pass_is_expired', 'Password Kadaluarsa');
INSERT INTO `dictionaries` VALUES ('121', 'en', 'uids.uid_is_locked', 'Locked');
INSERT INTO `dictionaries` VALUES ('122', 'id', 'uids.uid_is_locked', 'Terkunci');
INSERT INTO `dictionaries` VALUES ('123', 'en', 'parent_offices.office_parent', 'Parent Office');
INSERT INTO `dictionaries` VALUES ('124', 'id', 'parent_offices.office_parent', 'Kantor Induk');
INSERT INTO `dictionaries` VALUES ('125', 'en', 'parent_offices.office_region', 'Office Region');
INSERT INTO `dictionaries` VALUES ('126', 'id', 'parent_offices.office_region', 'Kantor Wilayah');
INSERT INTO `dictionaries` VALUES ('127', 'en', 'parent_offices.office_parent_name', 'Parent Office Name');
INSERT INTO `dictionaries` VALUES ('128', 'id', 'parent_offices.office_parent_name', 'Nama Kantor Induk');
INSERT INTO `dictionaries` VALUES ('129', 'en', 'parent_offices.office_parent_code', 'Parent Office Code');
INSERT INTO `dictionaries` VALUES ('130', 'id', 'parent_offices.office_parent_code', 'Kode Kantor Induk');
INSERT INTO `dictionaries` VALUES ('131', 'en', 'branch_offices.office_branch', 'Branch Office');
INSERT INTO `dictionaries` VALUES ('132', 'id', 'branch_offices.office_branch', 'Kantor Cabang');
INSERT INTO `dictionaries` VALUES ('133', 'en', 'branch_offices.office_branch_parent', 'Parent Office');
INSERT INTO `dictionaries` VALUES ('134', 'id', 'branch_offices.office_branch_parent', 'Kantor Induk');
INSERT INTO `dictionaries` VALUES ('135', 'en', 'branch_offices.office_region', 'Office Region');
INSERT INTO `dictionaries` VALUES ('136', 'id', 'branch_offices.office_region', 'Kantor Wilayah');
INSERT INTO `dictionaries` VALUES ('137', 'en', 'branch_offices.office_parent_code', 'Parent Office Code');
INSERT INTO `dictionaries` VALUES ('138', 'id', 'branch_offices.office_parent_code', 'Kode Kantor Induk');
INSERT INTO `dictionaries` VALUES ('139', 'en', 'branch_offices.office_branch_name', 'Office Branch Name');
INSERT INTO `dictionaries` VALUES ('140', 'id', 'branch_offices.office_branch_name', 'Nama Kantor Cabang');
INSERT INTO `dictionaries` VALUES ('141', 'en', 'branch_offices.office_branch_code', 'Office Branch Code');
INSERT INTO `dictionaries` VALUES ('142', 'id', 'branch_offices.office_branch_code', 'Kode Kantor Cabang');
INSERT INTO `dictionaries` VALUES ('143', 'en', 'applications.application_name', 'Application Name');
INSERT INTO `dictionaries` VALUES ('144', 'id', 'applications.application_name', 'Nama Aplikasi');
INSERT INTO `dictionaries` VALUES ('145', 'en', 'applications.application_code', 'Application Code');
INSERT INTO `dictionaries` VALUES ('146', 'id', 'applications.application_code', 'Kode Aplikasi');
INSERT INTO `dictionaries` VALUES ('147', 'en', 'applications.application_status', 'Status');
INSERT INTO `dictionaries` VALUES ('148', 'id', 'applications.application_status', 'Status');
INSERT INTO `dictionaries` VALUES ('149', 'en', 'functions.function_name', 'Function Name');
INSERT INTO `dictionaries` VALUES ('150', 'id', 'functions.function_name', 'Nama Fungsi');
INSERT INTO `dictionaries` VALUES ('151', 'en', 'functions.function_level', 'Function Level');
INSERT INTO `dictionaries` VALUES ('152', 'id', 'functions.function_level', 'Level Fungsi');
INSERT INTO `dictionaries` VALUES ('153', 'en', 'functions.function_status', 'Status');
INSERT INTO `dictionaries` VALUES ('154', 'id', 'functions.function_status', 'Status');
INSERT INTO `dictionaries` VALUES ('155', 'en', 'functions.application_name', 'Application Name');
INSERT INTO `dictionaries` VALUES ('156', 'id', 'functions.application_name', 'Nama Aplikasi');
INSERT INTO `dictionaries` VALUES ('157', 'en', 'functions.function_application_id', 'Application ID');
INSERT INTO `dictionaries` VALUES ('158', 'id', 'functions.function_application_id', 'ID Aplikasi');
INSERT INTO `dictionaries` VALUES ('159', 'en', 'functions.function_application_detail', 'Function Detail');
INSERT INTO `dictionaries` VALUES ('160', 'id', 'functions.function_application_detail', 'Detil Fungsi');
INSERT INTO `dictionaries` VALUES ('161', 'en', 'mapping_access.access_uid', 'UID');
INSERT INTO `dictionaries` VALUES ('162', 'id', 'mapping_access.access_uid', 'UID');
INSERT INTO `dictionaries` VALUES ('163', 'en', 'mapping_access.access_office', 'Office');
INSERT INTO `dictionaries` VALUES ('164', 'id', 'mapping_access.access_office', 'Kantor');
INSERT INTO `dictionaries` VALUES ('165', 'en', 'mapping_access.access_application', 'Application');
INSERT INTO `dictionaries` VALUES ('166', 'id', 'mapping_access.access_application', 'Aplikasi');
INSERT INTO `dictionaries` VALUES ('167', 'en', 'mapping_access.access_function', 'Function');
INSERT INTO `dictionaries` VALUES ('168', 'id', 'mapping_access.access_function', 'Fungsi');
INSERT INTO `dictionaries` VALUES ('169', 'en', 'simulators.simulator_name', 'Simulator Name');
INSERT INTO `dictionaries` VALUES ('170', 'id', 'simulators.simulator_name', 'Nama Simulator');
INSERT INTO `dictionaries` VALUES ('171', 'en', 'simulators.simulator_ip', 'IP Address');
INSERT INTO `dictionaries` VALUES ('172', 'id', 'simulators.simulator_ip', 'IP Address');
INSERT INTO `dictionaries` VALUES ('173', 'en', 'simulators.simulator_port', 'Port');
INSERT INTO `dictionaries` VALUES ('174', 'id', 'simulators.simulator_port', 'Port');
INSERT INTO `dictionaries` VALUES ('175', 'en', 'simulators.simulator_ssh', 'SSH');
INSERT INTO `dictionaries` VALUES ('176', 'id', 'simulators.simulator_ssh', 'SSH');
INSERT INTO `dictionaries` VALUES ('177', 'en', 'simulators.simulator_running', 'Status');
INSERT INTO `dictionaries` VALUES ('178', 'id', 'simulators.simulator_running', 'Status');
INSERT INTO `dictionaries` VALUES ('179', 'en', 'simulators.simulator_ssh_username', 'SSH Username');
INSERT INTO `dictionaries` VALUES ('180', 'id', 'simulators.simulator_ssh_username', 'SSH Username');
INSERT INTO `dictionaries` VALUES ('181', 'en', 'simulators.simulator_ssh_password', 'SSH Password');
INSERT INTO `dictionaries` VALUES ('182', 'id', 'simulators.simulator_ssh_password', 'SSH Password');
INSERT INTO `dictionaries` VALUES ('183', 'en', 'simulators.simulator_ssh_port', 'SSH Port');
INSERT INTO `dictionaries` VALUES ('184', 'id', 'simulators.simulator_ssh_port', 'SSH Port');
INSERT INTO `dictionaries` VALUES ('185', 'en', 'pc_address.pc_address_ip', 'IP Address');
INSERT INTO `dictionaries` VALUES ('186', 'id', 'pc_address.pc_address_ip', 'IP Address');
INSERT INTO `dictionaries` VALUES ('187', 'en', 'pc_address.pc_address_name', 'PC Name');
INSERT INTO `dictionaries` VALUES ('188', 'id', 'pc_address.pc_address_name', 'Nama PC');
INSERT INTO `dictionaries` VALUES ('189', 'en', 'login_simulation.simulator_service', 'Service');
INSERT INTO `dictionaries` VALUES ('190', 'id', 'login_simulation.simulator_service', 'Service');
INSERT INTO `dictionaries` VALUES ('191', 'en', 'login_simulation.simulator_application', 'Application');
INSERT INTO `dictionaries` VALUES ('192', 'id', 'login_simulation.simulator_application', 'Aplikasi');
INSERT INTO `dictionaries` VALUES ('193', 'en', 'login_simulation.simulator_uid', 'UID');
INSERT INTO `dictionaries` VALUES ('194', 'id', 'login_simulation.simulator_uid', 'UID');
INSERT INTO `dictionaries` VALUES ('195', 'en', 'login_simulation.simulator_pass', 'Password');
INSERT INTO `dictionaries` VALUES ('196', 'id', 'login_simulation.simulator_pass', 'Password');
INSERT INTO `dictionaries` VALUES ('197', 'en', 'logs.log_request_time', 'Request Time');
INSERT INTO `dictionaries` VALUES ('198', 'id', 'logs.log_request_time', 'Waktu Request');
INSERT INTO `dictionaries` VALUES ('199', 'en', 'logs.log_ip_address', 'IP Address');
INSERT INTO `dictionaries` VALUES ('200', 'id', 'logs.log_ip_address', 'IP Address');
INSERT INTO `dictionaries` VALUES ('201', 'en', 'logs.log_uid', 'UID');
INSERT INTO `dictionaries` VALUES ('202', 'id', 'logs.log_uid', 'UID');
INSERT INTO `dictionaries` VALUES ('203', 'en', 'logs.log_application_code', 'Application');
INSERT INTO `dictionaries` VALUES ('204', 'id', 'logs.log_application_code', 'Aplikasi');
INSERT INTO `dictionaries` VALUES ('205', 'en', 'logs.log_request_message', 'Request Message');
INSERT INTO `dictionaries` VALUES ('206', 'id', 'logs.log_request_message', 'Request Message');
INSERT INTO `dictionaries` VALUES ('207', 'en', 'logs.log_response_message', 'Response Message');
INSERT INTO `dictionaries` VALUES ('208', 'id', 'logs.log_response_message', 'Response Message');
INSERT INTO `dictionaries` VALUES ('209', 'en', 'login_simulation.simulator_ip', 'IP Address');
INSERT INTO `dictionaries` VALUES ('210', 'id', 'login_simulation.simulator_ip', 'IP Address');
INSERT INTO `dictionaries` VALUES ('211', 'en', 'login_simulation.simulator_port', 'Port');
INSERT INTO `dictionaries` VALUES ('212', 'id', 'login_simulation.simulator_port', 'Port');
INSERT INTO `dictionaries` VALUES ('213', 'id', 'base.page_title.module_creators', 'Modul Creator');
INSERT INTO `dictionaries` VALUES ('216', 'id', 'base.page_title.module_creator_fields', 'Field Modul');
INSERT INTO `dictionaries` VALUES ('217', 'id', 'base.page_title.module_creator_setting', 'Pengaturan Modul');
INSERT INTO `dictionaries` VALUES ('218', 'id', 'base.page_title.module_creator_parent', 'Module Creator');
INSERT INTO `dictionaries` VALUES ('219', 'id', 'module_creators.module_name', 'Nama Modul');
INSERT INTO `dictionaries` VALUES ('220', 'id', 'module_creators.module_modelname', 'Nama Model');
INSERT INTO `dictionaries` VALUES ('221', 'id', 'module_creators.module_primarytablename', 'Tabel Utama');
INSERT INTO `dictionaries` VALUES ('222', 'id', 'module_creators.module_pkey', 'Primary Key');
INSERT INTO `dictionaries` VALUES ('223', 'id', 'module_creators.module_noticelabel', 'Notice Label');
INSERT INTO `dictionaries` VALUES ('224', 'id', 'module_creators.field_name', 'Nama Field');
INSERT INTO `dictionaries` VALUES ('225', 'id', 'module_creators.field_rules', 'Rules');
INSERT INTO `dictionaries` VALUES ('226', 'id', 'module_creator_fields.field_module_id', 'ID Modul');
INSERT INTO `dictionaries` VALUES ('227', 'id', 'module_creator_fields.field_name', 'Nama Field');
INSERT INTO `dictionaries` VALUES ('228', 'id', 'module_creator_fields.field_rules', 'Rules');
INSERT INTO `dictionaries` VALUES ('229', 'id', 'module_creator_fields.module_name', 'Nama Modul');
INSERT INTO `dictionaries` VALUES ('230', 'id', 'base.page_title.module_creator_master_rules', 'Master Rule');
INSERT INTO `dictionaries` VALUES ('231', 'id', 'base.page_title.module_creators', 'Module Creator');
INSERT INTO `dictionaries` VALUES ('232', 'id', 'module_creators.module_creators_title', 'Module Creator');
INSERT INTO `dictionaries` VALUES ('233', 'id', 'module_creators.module_created', 'Created');
INSERT INTO `dictionaries` VALUES ('234', 'id', 'module_creators.module_createdby', 'Created By');
INSERT INTO `dictionaries` VALUES ('235', 'id', 'module_creator_fields.module_creator_fields_title', 'Module Creator Fields');
INSERT INTO `dictionaries` VALUES ('236', 'id', 'module_creator_fields.module_id', 'ID Modul');
INSERT INTO `dictionaries` VALUES ('237', 'id', 'module_creator_fields.field_is_search', 'Field Pencarian');
INSERT INTO `dictionaries` VALUES ('238', 'id', 'module_creator_fields.field_is_on_grid', 'Ditampilkan di grid');
INSERT INTO `dictionaries` VALUES ('239', 'id', 'module_creator_fields.field_is_on_form', 'Field isian dlm form');
INSERT INTO `dictionaries` VALUES ('240', 'id', 'module_projects.module_projects_title', 'Projek');
INSERT INTO `dictionaries` VALUES ('241', 'id', 'base.page_title.module_projects', 'Daftar Projek');
INSERT INTO `dictionaries` VALUES ('242', 'id', 'module_projects.project_createdby', 'Dibuat oleh');
INSERT INTO `dictionaries` VALUES ('243', 'id', 'module_projects.project_created', 'Tgl. Pembuatan');
INSERT INTO `dictionaries` VALUES ('244', 'id', 'module_projects.project_name', 'Nama Projek');
INSERT INTO `dictionaries` VALUES ('245', 'id', 'module_projects.project_description', 'Deskripsi Projek');
INSERT INTO `dictionaries` VALUES ('246', 'id', 'module_creators.web_server', 'Web Server');
INSERT INTO `dictionaries` VALUES ('247', 'id', 'module_creators.app_server', 'Application Server');
INSERT INTO `dictionaries` VALUES ('248', 'id', 'module_creators.db_server', 'Database Server');
INSERT INTO `dictionaries` VALUES ('249', 'id', 'module_creators.project_list', 'Daftar Project');
INSERT INTO `dictionaries` VALUES ('250', 'id', 'module_creators.about_this_project', 'Tentang Project');
INSERT INTO `dictionaries` VALUES ('251', 'id', 'module_creators.project_description', 'Deskripsi Project');
INSERT INTO `dictionaries` VALUES ('252', 'id', 'base.page_title.menu_header', 'Menu Anda');
INSERT INTO `dictionaries` VALUES ('253', 'id', 'base.page_title.close_module_projects', 'Tutup Project');
INSERT INTO `dictionaries` VALUES ('254', 'id', 'module_projects.project_level', 'Level');
INSERT INTO `dictionaries` VALUES ('255', 'id', 'module_projects.project_status', 'Status');
INSERT INTO `dictionaries` VALUES ('256', 'id', 'module_projects.project_url', 'URL');
INSERT INTO `dictionaries` VALUES ('257', 'id', 'module_projects.project_user', 'User');
INSERT INTO `dictionaries` VALUES ('258', 'id', 'module_projects.project_appip', 'IP App Server');
INSERT INTO `dictionaries` VALUES ('259', 'id', 'module_projects.project_apptype', 'Type App Server');
INSERT INTO `dictionaries` VALUES ('260', 'id', 'module_projects.project_appname', 'Nama App Server');
INSERT INTO `dictionaries` VALUES ('261', 'id', 'module_projects.project_webip', 'IP Web Server');
INSERT INTO `dictionaries` VALUES ('262', 'id', 'module_projects.project_webtype', 'Type Web Server');
INSERT INTO `dictionaries` VALUES ('263', 'id', 'module_projects.project_webname', 'Nama Web Server');
INSERT INTO `dictionaries` VALUES ('264', 'id', 'module_projects.project_dbip', 'IP Database');
INSERT INTO `dictionaries` VALUES ('265', 'id', 'module_projects.project_dbtype', 'Type Database');
INSERT INTO `dictionaries` VALUES ('266', 'id', 'module_projects.project_dbname', 'Nama Database');
INSERT INTO `dictionaries` VALUES ('267', 'id', 'base.page_title.hoa_catalogues', 'HOA Catalogue');
INSERT INTO `dictionaries` VALUES ('268', 'id', 'base.page_title.meeting_rooms', 'Rang Rapat');
INSERT INTO `dictionaries` VALUES ('269', 'id', 'base.page_title.module_creator_properties', 'Properties');
INSERT INTO `dictionaries` VALUES ('270', 'id', 'module_creators.module_defaultsortfield', 'Urut berdasarkan');
INSERT INTO `dictionaries` VALUES ('271', 'id', 'module_creators.module_defaultsortmethod', 'Type Urutan');
INSERT INTO `dictionaries` VALUES ('272', 'id', 'module_creator_fields.field_datatype', 'Type Data');
INSERT INTO `dictionaries` VALUES ('273', 'id', 'module_creator_fields.field_length', 'Panjang karakter');
INSERT INTO `dictionaries` VALUES ('274', 'id', 'base.page_title.data_guru', 'Data Pegawai');
INSERT INTO `dictionaries` VALUES ('275', 'id', 'base.page_title.jabatan', 'Jabatan');
INSERT INTO `dictionaries` VALUES ('276', 'id', 'base.page_title.daftarhadir', 'Daftar Hadir');
INSERT INTO `dictionaries` VALUES ('277', 'id', 'base.page_title.mutasi', 'Mutasi');
INSERT INTO `dictionaries` VALUES ('278', 'id', 'base.page_title.payroll', 'Penggajian');
INSERT INTO `dictionaries` VALUES ('279', 'id', 'base.page_title.promosi', 'Promosi');
INSERT INTO `dictionaries` VALUES ('280', 'id', 'base.page_title.parameter_system', 'Parameter System');
INSERT INTO `dictionaries` VALUES ('281', 'id', 'base.page_title.master_data', 'Data Master');
INSERT INTO `dictionaries` VALUES ('282', 'id', 'base.page_title.g_komp_pks', 'Grup Komponen Penilaian');
INSERT INTO `dictionaries` VALUES ('283', 'id', 'base.page_title.komponen_penilaians', 'Komponen Penilaian');
INSERT INTO `dictionaries` VALUES ('284', 'id', 'base.page_title.penilaian', 'Penilaian Kinerja');
INSERT INTO `dictionaries` VALUES ('285', 'id', 'base.page_title.penilaiankinerjas', 'Penilaian Kinerja');
INSERT INTO `dictionaries` VALUES ('286', 'id', 'base.page_title.pk_details', 'Detail Penilaian Kerja');
INSERT INTO `dictionaries` VALUES ('287', 'id', 'base.page_title.anggota', 'Anggota');
INSERT INTO `dictionaries` VALUES ('288', 'id', 'base.page_title.keanggotaans', 'Keanggotaan');
INSERT INTO `dictionaries` VALUES ('289', 'id', 'base.page_title.anggotas', 'Anggota');
INSERT INTO `dictionaries` VALUES ('290', 'id', 'base.page_title.reqanggotas', 'Request Anggota');
INSERT INTO `dictionaries` VALUES ('291', 'id', 'base.page_title.saldos', 'Saldo');
INSERT INTO `dictionaries` VALUES ('292', 'id', 'base.page_title.reqsaldos', 'Request Saldo');
INSERT INTO `dictionaries` VALUES ('293', 'id', 'base.page_title.trxpesanans', 'Pemesanan');
INSERT INTO `dictionaries` VALUES ('294', 'id', 'base.page_title.trxpembayaranverify', 'Verif. Pembayaran');
INSERT INTO `dictionaries` VALUES ('295', 'id', 'base.page_title.trxpesananverify', 'Verif. Pesanan');
INSERT INTO `dictionaries` VALUES ('296', 'id', 'base.page_title.trxpengirimans', 'Pengiriman');
INSERT INTO `dictionaries` VALUES ('297', 'id', 'users.user_identity', 'User ID');
INSERT INTO `dictionaries` VALUES ('298', 'id', 'users.user_name', 'Nama User');
INSERT INTO `dictionaries` VALUES ('299', 'id', 'users.user_registerdate', 'Tgl Daftar');
INSERT INTO `dictionaries` VALUES ('300', 'id', 'users.user_status', 'Status');
INSERT INTO `dictionaries` VALUES ('301', 'id', 'users.user_password', 'Password');
INSERT INTO `dictionaries` VALUES ('302', 'id', 'users.user_password_confirm', 'Confirm Password');
INSERT INTO `dictionaries` VALUES ('303', 'id', 'roles.role_active_label', 'Status');
INSERT INTO `dictionaries` VALUES ('304', 'id', 'menus.menu_active_label', 'Status');
INSERT INTO `dictionaries` VALUES ('305', 'id', 'products.produk_identity', 'ID Produk');
INSERT INTO `dictionaries` VALUES ('306', 'id', 'products.produk_nama', 'Nama Produk');
INSERT INTO `dictionaries` VALUES ('307', 'id', 'products.produk_harga', 'Harga');
INSERT INTO `dictionaries` VALUES ('308', 'id', 'products.produk_stok', 'Stok');
INSERT INTO `dictionaries` VALUES ('309', 'id', 'products.produk_status_label', 'Status');
INSERT INTO `dictionaries` VALUES ('310', 'id', 'products.produk_satuan', 'Satuan');
INSERT INTO `dictionaries` VALUES ('311', 'id', 'products.produk_photo', 'Photo');
INSERT INTO `dictionaries` VALUES ('312', 'id', 'products.produk_description', 'Deskripsi');
INSERT INTO `dictionaries` VALUES ('313', 'id', 'products.produk_status', 'Status');
INSERT INTO `dictionaries` VALUES ('314', 'id', 'anggotas.anggota_nama', 'Nama');
INSERT INTO `dictionaries` VALUES ('315', 'id', 'anggotas.user_identity', 'User ID');
INSERT INTO `dictionaries` VALUES ('316', 'id', 'anggotas.anggota_jnskelamin_label', 'Jns. Kelamin');
INSERT INTO `dictionaries` VALUES ('317', 'id', 'anggotas.anggota_email', 'Email');
INSERT INTO `dictionaries` VALUES ('318', 'id', 'anggotas.anggota_noidentitas', 'No. Identitas');
INSERT INTO `dictionaries` VALUES ('319', 'id', 'anggotas.user_status_label', 'Status');
INSERT INTO `dictionaries` VALUES ('320', 'id', 'anggotas.anggota_tmplahir', 'Tmp. Lahir');
INSERT INTO `dictionaries` VALUES ('321', 'id', 'anggotas.anggota_tgllahir', 'Tgl. Lahir');
INSERT INTO `dictionaries` VALUES ('322', 'id', 'anggotas.anggota_alamat', 'Alamat');
INSERT INTO `dictionaries` VALUES ('323', 'id', 'anggotas.anggota_jnskelamin', 'Jns. Kelamin');
INSERT INTO `dictionaries` VALUES ('324', 'id', 'anggotas.anggota_notlp', 'No. Tlp');
INSERT INTO `dictionaries` VALUES ('325', 'id', 'anggotas.anggota_jnsidentitas', 'Jns. Identitas');
INSERT INTO `dictionaries` VALUES ('326', 'id', 'anggotas.anggota_scanidentitas', 'Scan Identitas');
INSERT INTO `dictionaries` VALUES ('327', 'id', 'anggotas.anggota_photo', 'Photo');
INSERT INTO `dictionaries` VALUES ('328', 'id', 'reqanggotas.anggota_nama', 'Nama');
INSERT INTO `dictionaries` VALUES ('329', 'id', 'reqanggotas.user_identity', 'User ID');
INSERT INTO `dictionaries` VALUES ('330', 'id', 'reqanggotas.anggota_jnskelamin_label', 'Jns. Kelamin');
INSERT INTO `dictionaries` VALUES ('331', 'id', 'reqanggotas.anggota_email', 'Email');
INSERT INTO `dictionaries` VALUES ('332', 'id', 'reqanggotas.anggota_noidentitas', 'No.Identitas');
INSERT INTO `dictionaries` VALUES ('333', 'id', 'reqanggotas.user_status_label', 'Status');
INSERT INTO `dictionaries` VALUES ('334', 'id', 'reqanggotas.anggota_tmplahir', 'Tmp. Lahir');
INSERT INTO `dictionaries` VALUES ('335', 'id', 'reqanggotas.anggota_tgllahir', 'Tgl. Lahir');
INSERT INTO `dictionaries` VALUES ('336', 'id', 'reqanggotas.anggota_alamat', 'Alamat');
INSERT INTO `dictionaries` VALUES ('337', 'id', 'reqanggotas.anggota_jnskelamin', 'Jns.Kelamin');
INSERT INTO `dictionaries` VALUES ('338', 'id', 'reqanggotas.anggota_notlp', 'No. Tlp');
INSERT INTO `dictionaries` VALUES ('339', 'id', 'reqanggotas.anggota_jnsidentitas', 'Jns. Identitas');
INSERT INTO `dictionaries` VALUES ('340', 'id', 'reqanggotas.anggota_scanidentitas', 'Scan Identitas');
INSERT INTO `dictionaries` VALUES ('341', 'id', 'reqanggotas.anggota_photo', 'Photo');
INSERT INTO `dictionaries` VALUES ('342', 'id', 'reqanggotas.user_password', 'Password');
INSERT INTO `dictionaries` VALUES ('343', 'id', 'reqanggotas.user_password_confirm', 'Confirm Password');
INSERT INTO `dictionaries` VALUES ('344', 'id', 'reqanggotas.anggota_persetujuan', 'Persetujuan Keanggotaan');
INSERT INTO `dictionaries` VALUES ('345', 'id', 'reqanggotas.anggota_area', 'Area');
INSERT INTO `dictionaries` VALUES ('346', 'id', 'reqanggotas.anggota_note', 'Catatan');
INSERT INTO `dictionaries` VALUES ('347', 'id', 'reqanggotas.anggota_areaid', 'Area');
INSERT INTO `dictionaries` VALUES ('348', 'id', 'anggotas.old_password', 'Password lama');
INSERT INTO `dictionaries` VALUES ('349', 'id', 'anggotas.new_password', 'Password baru');
INSERT INTO `dictionaries` VALUES ('350', 'id', 'anggotas.confirm_new_password', 'Confirm password baru');
INSERT INTO `dictionaries` VALUES ('351', 'id', 'anggotas.anggota_areaid', 'Area');
INSERT INTO `dictionaries` VALUES ('352', 'id', 'reqsaldos.anggota_nama', 'Nama');
INSERT INTO `dictionaries` VALUES ('353', 'id', 'reqsaldos.user_identity', 'User ID');
INSERT INTO `dictionaries` VALUES ('354', 'id', 'reqsaldos.trxsaldo_noref', 'No. Refferensi');
INSERT INTO `dictionaries` VALUES ('355', 'id', 'reqsaldos.trxsaldo_tgl', 'Tanggal');
INSERT INTO `dictionaries` VALUES ('356', 'id', 'reqsaldos.bank_nama', 'Nama Bank');
INSERT INTO `dictionaries` VALUES ('357', 'id', 'reqsaldos.trxsaldo_nominal', 'Nominal');
INSERT INTO `dictionaries` VALUES ('358', 'id', 'reqsaldos.trxsaldo_status_label', 'Status');
INSERT INTO `dictionaries` VALUES ('359', 'id', 'reqsaldos.trxsaldo_kdbank', 'Bank');
INSERT INTO `dictionaries` VALUES ('360', 'id', 'reqsaldos.trxsaldo_methode', 'Metode');
INSERT INTO `dictionaries` VALUES ('361', 'id', 'reqsaldos.trxsaldo_user_id', 'Anggota');
INSERT INTO `dictionaries` VALUES ('362', 'id', 'reqsaldos.trxsaldo_methode_label', 'Metode');
INSERT INTO `dictionaries` VALUES ('363', 'id', 'reqsaldos.trxsaldo_note', 'Catatan');
INSERT INTO `dictionaries` VALUES ('364', 'id', 'saldos.anggota_nama', 'Nama');
INSERT INTO `dictionaries` VALUES ('365', 'id', 'saldos.user_identity', 'User ID');
INSERT INTO `dictionaries` VALUES ('366', 'id', 'saldos.saldo_lastupdate', 'Update terakhir');
INSERT INTO `dictionaries` VALUES ('367', 'id', 'saldos.saldo_saldo', 'Saldo awal');
INSERT INTO `dictionaries` VALUES ('368', 'id', 'saldos.total_trxsaldo', 'Tambah saldo berjalan');
INSERT INTO `dictionaries` VALUES ('369', 'id', 'saldos.total_trxpemesanan', 'Pemesanan berjalan');
INSERT INTO `dictionaries` VALUES ('370', 'id', 'trxpesanans.trxpemesanans_invoiceid', 'ID Pesanan');
INSERT INTO `dictionaries` VALUES ('371', 'id', 'trxpesanans.trxpemesanans_itemcount', 'Jml Produk');
INSERT INTO `dictionaries` VALUES ('372', 'id', 'trxpesanans.produk_identity', 'Produk ID');
INSERT INTO `dictionaries` VALUES ('373', 'id', 'trxpesanans.produk_nama', 'Nama Produk');
INSERT INTO `dictionaries` VALUES ('374', 'id', 'trxpesanans.produk_harga', 'Harga');
INSERT INTO `dictionaries` VALUES ('375', 'id', 'trxpesanans.produk_stok', 'Stok');
INSERT INTO `dictionaries` VALUES ('376', 'id', 'trxpesanans.produk_satuan', 'Satuan');
INSERT INTO `dictionaries` VALUES ('377', 'id', 'trxpesanans.produk_status', 'Status');
INSERT INTO `dictionaries` VALUES ('378', 'id', 'trxpesanans.produk_qty', 'Jml dipesan');
INSERT INTO `dictionaries` VALUES ('379', 'id', 'trxpesanans.produk_pointorder', 'Point Order');
INSERT INTO `dictionaries` VALUES ('380', 'id', 'trxpesanans.trxpemesanans_total', 'Subtotal');
INSERT INTO `dictionaries` VALUES ('381', 'id', 'trxpesanans.trxpemesanans_pointorder', 'PointOrder');
INSERT INTO `dictionaries` VALUES ('382', 'id', 'trxpesanans.trxpembayaran_metode', 'Metode Pembayaran');
INSERT INTO `dictionaries` VALUES ('383', 'id', 'trxpesanans.trxpembayaran_nominal', 'Nominal');
INSERT INTO `dictionaries` VALUES ('384', 'id', 'trxpesanans.trxpembayaran_kdbank', 'Bank');
INSERT INTO `dictionaries` VALUES ('385', 'id', 'trxpesanans.trxpembayaran_noref', 'No. Refferensi');
INSERT INTO `dictionaries` VALUES ('386', 'id', 'trxpesanans.trxpesanan_qty', 'Qty');
INSERT INTO `dictionaries` VALUES ('389', 'id', 'trxpembayaranverify.anggota_nama', 'Nama Anggota');
INSERT INTO `dictionaries` VALUES ('390', 'id', 'trxpembayaranverify.user_identity', 'User ID');
INSERT INTO `dictionaries` VALUES ('391', 'id', 'trxpembayaranverify.trxpembayaran_noref', 'No. Refferensi');
INSERT INTO `dictionaries` VALUES ('392', 'id', 'trxpembayaranverify.trxpembayaran_tgl', 'Tanggal');
INSERT INTO `dictionaries` VALUES ('393', 'id', 'trxpembayaranverify.trxpembayaran_nominal', 'Nominal');
INSERT INTO `dictionaries` VALUES ('394', 'id', 'trxpembayaranverify.trxpembayaran_status_label', 'Status');
INSERT INTO `dictionaries` VALUES ('395', 'id', 'trxpesanans.anggota_nama', 'Nama');
INSERT INTO `dictionaries` VALUES ('396', 'id', 'trxpesanans.anggota_alamat', 'Alamat');
INSERT INTO `dictionaries` VALUES ('397', 'id', 'trxpesanans.anggota_notlp', 'No.Tlp');
INSERT INTO `dictionaries` VALUES ('398', 'id', 'trxpesanans.anggota_email', 'Email');
INSERT INTO `dictionaries` VALUES ('399', 'id', 'trxpesanans.area_nama', 'Area');
INSERT INTO `dictionaries` VALUES ('400', 'id', 'trxpesanans.area_biayakirim', 'Biaya Kirim');
INSERT INTO `dictionaries` VALUES ('401', 'id', 'trxpesanans.trxpemesanans_totalbayar', 'Total Bayar');
INSERT INTO `dictionaries` VALUES ('402', 'id', 'trxpembayaranverify.trxpesanan_invoiceid', 'ID Pesanan');
INSERT INTO `dictionaries` VALUES ('403', 'id', 'trxpembayaranverify.trxpembayaran_methode', 'Metode');
INSERT INTO `dictionaries` VALUES ('404', 'id', 'trxpembayaranverify.bank_nama', 'Nama Bank');
INSERT INTO `dictionaries` VALUES ('405', 'id', 'trxpembayaranverify.anggota_alamat', 'Alamat');
INSERT INTO `dictionaries` VALUES ('406', 'id', 'trxpembayaranverify.anggota_notlp', 'No.Tlp');
INSERT INTO `dictionaries` VALUES ('407', 'id', 'trxpembayaranverify.anggota_email', 'Email');
INSERT INTO `dictionaries` VALUES ('408', 'id', 'trxpembayaranverify.trxpesanan_pointorder', 'Point Order');
INSERT INTO `dictionaries` VALUES ('409', 'id', 'trxpembayaranverify.trxpesanan_biayakirim', 'Biaya Kirim');
INSERT INTO `dictionaries` VALUES ('410', 'id', 'trxpembayaranverify.trxpembayaran_metode', 'Metode');
INSERT INTO `dictionaries` VALUES ('411', 'id', 'trxpembayaranverify.trxpembayaran_status', 'Status');
INSERT INTO `dictionaries` VALUES ('412', 'id', 'trxpembayaranverify.trxpembayaran_kdbank', 'Bank');
INSERT INTO `dictionaries` VALUES ('413', 'id', 'trxpembayaranverify.trxpesanan_subtotal', 'Subtotal');
INSERT INTO `dictionaries` VALUES ('414', 'id', 'trxpembayaranverify.trxpesanan_totalbayar', 'Total');
INSERT INTO `dictionaries` VALUES ('415', 'id', 'trxpembayaranverify.trxpembayaran_note', 'Catatan');
INSERT INTO `dictionaries` VALUES ('416', 'id', 'trxpesananverify.trxpesanan_invoiceid', 'ID Pesanan');
INSERT INTO `dictionaries` VALUES ('417', 'id', 'trxpesananverify.trxpesanan_tgl', 'Tanggal');
INSERT INTO `dictionaries` VALUES ('418', 'id', 'trxpesananverify.user_identity', 'User ID');
INSERT INTO `dictionaries` VALUES ('419', 'id', 'trxpesananverify.anggota_nama', 'Nama Anggota');
INSERT INTO `dictionaries` VALUES ('420', 'id', 'trxpesananverify.trxpesanan_status', 'Status');
INSERT INTO `dictionaries` VALUES ('421', 'id', 'trxpesananverify.trxpesanan_produkid', 'Produk');
INSERT INTO `dictionaries` VALUES ('422', 'id', 'trxpesananverify.trxpesanan_qty', 'Jumlah');
INSERT INTO `dictionaries` VALUES ('423', 'id', 'trxpesananverify.trxpesanan_qty_realisasi', 'Realisasi');
INSERT INTO `dictionaries` VALUES ('424', 'id', 'trxpengirimans.trxpesanan_invoiceid', 'ID Pesanan');
INSERT INTO `dictionaries` VALUES ('425', 'id', 'trxpengirimans.user_identity', 'User ID');
INSERT INTO `dictionaries` VALUES ('426', 'id', 'trxpengirimans.trxpesanan_tgl', 'Tanggal');
INSERT INTO `dictionaries` VALUES ('427', 'id', 'trxpengirimans.user_identity', 'Anggota ID');
INSERT INTO `dictionaries` VALUES ('428', 'id', 'trxpengirimans.anggota_nama', 'Nama');
INSERT INTO `dictionaries` VALUES ('429', 'id', 'trxpengirimans.trxpesanan_status', 'Status');
INSERT INTO `dictionaries` VALUES ('430', 'id', 'trxpengirimans.anggota_notlp', 'No.Tlp');
INSERT INTO `dictionaries` VALUES ('431', 'id', 'trxpengirimans.anggota_email', 'Email');
INSERT INTO `dictionaries` VALUES ('432', 'id', 'trxpengirimans.anggota_alamat', 'Alamat');
INSERT INTO `dictionaries` VALUES ('433', 'id', 'trxpengirimans.trxpesanan_biayakirim', 'Biaya Kirim');
INSERT INTO `dictionaries` VALUES ('434', 'id', 'trxpengirimans.trxpengiriman_tglkirim', 'Tgl.Kirim');
INSERT INTO `dictionaries` VALUES ('435', 'id', 'trxpengirimans.trxpengiriman_ekspedisi', 'Ekspedisi');
INSERT INTO `dictionaries` VALUES ('436', 'id', 'trxpengirimans.trxpengiriman_noresi', 'No.Resi');
INSERT INTO `dictionaries` VALUES ('437', 'id', 'base.page_title.trackings', 'Cek Pesanan');
INSERT INTO `dictionaries` VALUES ('438', 'id', 'trackings.user_identity', 'User ID');
INSERT INTO `dictionaries` VALUES ('439', 'id', 'base.user_identity', 'User ID');
INSERT INTO `dictionaries` VALUES ('440', 'id', 'base.anggota_nama', 'Nama');
INSERT INTO `dictionaries` VALUES ('441', 'id', 'base.anggota_tmplahir', 'Tempat lahir');
INSERT INTO `dictionaries` VALUES ('442', 'id', 'base.anggota_tgllahir', 'Tgl. Lahir');
INSERT INTO `dictionaries` VALUES ('443', 'id', 'base.anggota_alamat', 'Alamat');
INSERT INTO `dictionaries` VALUES ('444', 'id', 'base.anggota_area', 'Area');
INSERT INTO `dictionaries` VALUES ('445', 'id', 'base.anggota_jnskelamin', 'Jns.Kelamin');
INSERT INTO `dictionaries` VALUES ('446', 'id', 'base.anggota_notlp', 'No.Tlp');
INSERT INTO `dictionaries` VALUES ('447', 'id', 'base.anggota_email', 'Email');
INSERT INTO `dictionaries` VALUES ('448', 'id', 'base.anggota_jnsidentitas', 'Jns. Identitas');
INSERT INTO `dictionaries` VALUES ('449', 'id', 'base.anggota_noidentitas', 'No.Identitas');
INSERT INTO `dictionaries` VALUES ('450', 'id', 'base.anggota_scanidentitas', 'Scan Identitas');
INSERT INTO `dictionaries` VALUES ('451', 'id', 'base.anggota_photo', 'Photo');
INSERT INTO `dictionaries` VALUES ('452', 'id', 'base.user_password', 'Password');
INSERT INTO `dictionaries` VALUES ('453', 'id', 'base.user_password_confirm', 'Konfirmasi password');
INSERT INTO `dictionaries` VALUES ('454', 'id', 'base.anggota_persetujuan', 'Konfirmasi persetujuan pendaftaran');
INSERT INTO `dictionaries` VALUES ('455', 'id', 'base.page_title.banks', 'Bank');
INSERT INTO `dictionaries` VALUES ('456', 'id', 'base.page_title.ekspedisis', 'Ekspedisi');
INSERT INTO `dictionaries` VALUES ('457', 'id', 'banks.bank_nama', 'Nama Bank');
INSERT INTO `dictionaries` VALUES ('458', 'id', 'base.page_title.areas', 'Area');
INSERT INTO `dictionaries` VALUES ('459', 'id', 'base.page_title.masters', 'Data Master');
INSERT INTO `dictionaries` VALUES ('460', 'id', 'ekspedisis.bank_nama', 'Nama Bank');
INSERT INTO `dictionaries` VALUES ('461', 'id', 'ekspedisis.ekspedisi_nama', 'Nama Ekspedisi');
INSERT INTO `dictionaries` VALUES ('462', 'id', 'areas.area_nama', 'Nama Area');
INSERT INTO `dictionaries` VALUES ('463', 'id', 'areas.area_biayakirim', 'Biaya Kirim');
INSERT INTO `dictionaries` VALUES ('464', 'id', 'parameters.configuration_key', 'Configuration Key');
INSERT INTO `dictionaries` VALUES ('465', 'id', 'parameters.configuration_value', 'Configuration Value');
INSERT INTO `dictionaries` VALUES ('466', 'id', 'base.page_title.Fsaldos', 'Saldo');
INSERT INTO `dictionaries` VALUES ('467', 'id', 'base.page_title.Freqsaldos', 'Request Saldo');
INSERT INTO `dictionaries` VALUES ('468', 'id', 'base.page_title.Fproducts', 'Produk');
INSERT INTO `dictionaries` VALUES ('469', 'id', 'base.page_title.Fcharts', 'Chart');
INSERT INTO `dictionaries` VALUES ('470', 'id', 'Fsaldos.saldo_saldo', 'Saldo');
INSERT INTO `dictionaries` VALUES ('471', 'id', 'Fsaldos.total_trxsaldo', 'Request Saldo');
INSERT INTO `dictionaries` VALUES ('472', 'id', 'Fsaldos.total_trxpemesanan', 'Total Pesanan');
INSERT INTO `dictionaries` VALUES ('473', 'id', 'Fsaldos.total_biaya_kirim', 'Total Biaya Kirim');
INSERT INTO `dictionaries` VALUES ('474', 'id', 'Fsaldos.saldo_akhir', 'Saldo Akhir');
INSERT INTO `dictionaries` VALUES ('475', 'id', 'Fsaldos.saldo_lastupdate', 'Tgl Update');
INSERT INTO `dictionaries` VALUES ('476', 'id', 'Freqsaldos.trxsaldo_noref', 'No. Refferensi');
INSERT INTO `dictionaries` VALUES ('477', 'id', 'Freqsaldos.trxsaldo_tgl', 'Tanggal Transaksi');
INSERT INTO `dictionaries` VALUES ('478', 'id', 'Freqsaldos.trxsaldo_methode_label', 'Methode');
INSERT INTO `dictionaries` VALUES ('479', 'id', 'Freqsaldos.bank_nama', 'Nama Bank');
INSERT INTO `dictionaries` VALUES ('480', 'id', 'Freqsaldos.trxsaldo_nominal', 'Nominal');
INSERT INTO `dictionaries` VALUES ('481', 'id', 'Freqsaldos.trxsaldo_status_label', 'Status');
INSERT INTO `dictionaries` VALUES ('482', 'id', 'Freqsaldos.trxsaldo_methode', 'Metode');
INSERT INTO `dictionaries` VALUES ('483', 'id', 'Fcharts.trxpesanan_invoiceid', 'ID Pesanan');
INSERT INTO `dictionaries` VALUES ('484', 'id', 'Fcharts.trxpesanan_tgl', 'Tgl. Transaksi');
INSERT INTO `dictionaries` VALUES ('485', 'id', 'Fcharts.count_item', 'Jumlah Item');
INSERT INTO `dictionaries` VALUES ('486', 'id', 'Fcharts.trxpesanan_pointorder', 'Point Order');
INSERT INTO `dictionaries` VALUES ('487', 'id', 'Fcharts.trxpesanan_biayakirim', 'Biaya Kirim');
INSERT INTO `dictionaries` VALUES ('488', 'id', 'base.page_title.Fprofiles', 'Profile');
INSERT INTO `dictionaries` VALUES ('490', 'id', 'Fprofiles.anggota_tglregistrasi', 'Tgl. Registrasi');
INSERT INTO `dictionaries` VALUES ('491', 'id', 'Fprofiles.anggota_areaid', 'Area');
INSERT INTO `dictionaries` VALUES ('492', 'id', 'Fprofiles.anggota_tglapprove', 'Tgl Approve');
INSERT INTO `dictionaries` VALUES ('493', 'id', 'base.page_title.Faboutus', 'Tentang kami');
INSERT INTO `dictionaries` VALUES ('494', 'id', 'base.page_title.Fhowto', 'Panduan');
INSERT INTO `dictionaries` VALUES ('496', 'id', 'Fproducts.produk_qty', 'Jml Pesan');
INSERT INTO `dictionaries` VALUES ('497', 'id', 'Fproducts.produk_identity', 'Produk ID');
INSERT INTO `dictionaries` VALUES ('498', 'id', 'Fproducts.produk_nama', 'Nama Produk');
INSERT INTO `dictionaries` VALUES ('499', 'id', 'Fproducts.produk_harga', 'Harga');
INSERT INTO `dictionaries` VALUES ('500', 'id', 'Fproducts.produk_stok', 'Stok');
INSERT INTO `dictionaries` VALUES ('501', 'id', 'Fproducts.produk_satuan', 'Satuan');
INSERT INTO `dictionaries` VALUES ('502', 'id', 'Fproducts.produk_status', 'Status');
INSERT INTO `dictionaries` VALUES ('503', 'id', 'Fproducts.produk_pointorder', 'Jml pembelian utk 1 Point Order');
INSERT INTO `dictionaries` VALUES ('504', 'id', 'base.advance_search', 'Cari Pesanan');
INSERT INTO `dictionaries` VALUES ('505', 'id', 'base.produk_nama', 'Nama Produk');
INSERT INTO `dictionaries` VALUES ('506', 'id', 'products.produk_pointorder', 'Point Order');
INSERT INTO `dictionaries` VALUES ('507', 'id', 'Fcharts.trxpemesanans_invoiceid', 'ID Pesanan');
INSERT INTO `dictionaries` VALUES ('508', 'id', 'Fcharts.trxpemesanans_itemcount', 'Jml Pesanan');
INSERT INTO `dictionaries` VALUES ('509', 'id', 'Fcharts.trxpemesanans_total', 'Subtotal');
INSERT INTO `dictionaries` VALUES ('510', 'id', 'Fcharts.trxpemesanans_pointorder', 'Point Order');
INSERT INTO `dictionaries` VALUES ('511', 'id', 'Fcharts.trxpemesanans_totalbayar', 'Total');
INSERT INTO `dictionaries` VALUES ('512', 'id', 'Fcharts.trxpembayaran_metode', 'Metode Pembayaran');
INSERT INTO `dictionaries` VALUES ('513', 'id', 'Fcharts.trxpembayaran_nominal', 'Nominal');
INSERT INTO `dictionaries` VALUES ('514', 'id', 'Fcharts.trxpembayaran_kdbank', 'Bank');
INSERT INTO `dictionaries` VALUES ('515', 'id', 'Fcharts.trxpembayaran_noref', 'No Refferensi');
INSERT INTO `dictionaries` VALUES ('516', 'id', 'Fcharts.produk_identity', 'ID Produk');
INSERT INTO `dictionaries` VALUES ('517', 'id', 'Fcharts.produk_harga', 'Harga Satuan');
INSERT INTO `dictionaries` VALUES ('518', 'id', 'Fcharts.trxpesanan_qty', 'Jml');
INSERT INTO `dictionaries` VALUES ('519', 'id', 'Fcharts.area_biayakirim', 'Biaya Kirim');
INSERT INTO `dictionaries` VALUES ('520', 'id', 'trackings.trxpesanan_userid', 'User ID');
INSERT INTO `dictionaries` VALUES ('521', 'id', 'trackings.trxpesanan_invoiceid', 'ID Pesanan');
INSERT INTO `dictionaries` VALUES ('522', 'id', 'trackings.trxpesanan_tgl', 'Tgl');
INSERT INTO `dictionaries` VALUES ('523', 'id', 'trackings.trxpesanan_status', 'Status');
INSERT INTO `dictionaries` VALUES ('524', 'id', 'trackings.trxpesanan_pointorder', 'Point Order');
INSERT INTO `dictionaries` VALUES ('525', 'id', 'Fcharts.trxpesanan_status_label\r\n', 'Status');
INSERT INTO `dictionaries` VALUES ('526', 'id', 'base.page_title.reports', 'Laporan');
INSERT INTO `dictionaries` VALUES ('527', 'id', 'reports.jns_report', 'Jenis Laporan');
INSERT INTO `dictionaries` VALUES ('528', 'id', 'reports.tgl_awal', 'Tgl Awal');
INSERT INTO `dictionaries` VALUES ('529', 'id', 'reports.tgl_akhir', 'Tgl Akhir');
INSERT INTO `dictionaries` VALUES ('530', 'id', 'reports.trxpesanan_invoiceid', 'ID Pesanan');
INSERT INTO `dictionaries` VALUES ('531', 'id', 'reports.trxpesanan_tgl', 'Tanggal');
INSERT INTO `dictionaries` VALUES ('532', 'id', 'reports.anggota_nama', 'Nama Anggota');
INSERT INTO `dictionaries` VALUES ('533', 'id', 'reports.area_biayakirim', 'Biaya Kirim');
INSERT INTO `dictionaries` VALUES ('534', 'id', 'reports.totalbayar', 'Total pesanan');
INSERT INTO `dictionaries` VALUES ('535', 'id', 'reports.totalpointorder', 'Total Point Order');
INSERT INTO `dictionaries` VALUES ('536', 'id', 'reports.grandtotal', 'Grand Total');
INSERT INTO `dictionaries` VALUES ('537', 'id', 'base.page_title.Laporan_Pemesanan', 'Laporan Pemesanan');
INSERT INTO `dictionaries` VALUES ('538', 'id', 'base.page_title.Laporan_Detail_Pemesanan', 'Laporan Detail Pesanan');
INSERT INTO `dictionaries` VALUES ('539', 'id', 'reports.produk_identity', 'ID Produk');
INSERT INTO `dictionaries` VALUES ('540', 'id', 'reports.trxpesanan_qty', 'Qty');
INSERT INTO `dictionaries` VALUES ('541', 'id', 'reports.produk_harga', 'Harga');
INSERT INTO `dictionaries` VALUES ('542', 'id', 'reports.subtotal\r', 'Subtotal');
INSERT INTO `dictionaries` VALUES ('543', 'id', 'base.page_title.companies', 'Company');
INSERT INTO `dictionaries` VALUES ('544', 'id', 'base.page_title.masterusers', 'Master User');
INSERT INTO `dictionaries` VALUES ('545', 'id', 'base.page_title.sitegroups', 'Site Group');
INSERT INTO `dictionaries` VALUES ('546', 'id', 'base.page_title.units', 'Unit');
INSERT INTO `dictionaries` VALUES ('547', 'id', 'base.page_title.masterhardwares', 'Master Hardware');
INSERT INTO `dictionaries` VALUES ('548', 'id', 'base.page_title.hardwares', 'Hardware');
INSERT INTO `dictionaries` VALUES ('549', 'id', 'base.page_title.hwtypes', 'Hardware Type');
INSERT INTO `dictionaries` VALUES ('550', 'id', 'base.page_title.hostnames', 'Hostname');
INSERT INTO `dictionaries` VALUES ('551', 'id', 'base.page_title.masterinformations', 'Master Information');
INSERT INTO `dictionaries` VALUES ('552', 'id', 'base.page_title.informations', 'Information');
INSERT INTO `dictionaries` VALUES ('553', 'id', 'base.page_title.groupinformations', 'Group Information');
INSERT INTO `dictionaries` VALUES ('554', 'id', 'base.page_title.UAT', 'UAT');
INSERT INTO `dictionaries` VALUES ('555', 'id', 'base.page_title.migrations', 'Migration');
INSERT INTO `dictionaries` VALUES ('556', 'id', 'base.page_title.reportmigrations', 'Report Migration');

-- ----------------------------
-- Table structure for group_detail_informations
-- ----------------------------
DROP TABLE IF EXISTS `group_detail_informations`;
CREATE TABLE `group_detail_informations` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of group_detail_informations
-- ----------------------------
INSERT INTO `group_detail_informations` VALUES ('1', 'group3', null, null, null);

-- ----------------------------
-- Table structure for hardwares
-- ----------------------------
DROP TABLE IF EXISTS `hardwares`;
CREATE TABLE `hardwares` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `hostname_id` int(10) NOT NULL,
  `ipaddress` varchar(32) NOT NULL,
  `asset_tag` varchar(32) NOT NULL,
  `serial_number` varchar(32) NOT NULL,
  `hw_type_id` int(10) NOT NULL,
  `memory` varchar(16) NOT NULL,
  `hdd` varchar(16) NOT NULL,
  `status_id` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `asset_tag` (`asset_tag`),
  UNIQUE KEY `serial_number` (`serial_number`) USING BTREE,
  KEY `FKhardwares868669` (`hw_type_id`),
  KEY `FKhardwares368047` (`hostname_id`),
  CONSTRAINT `FKhardwares368047` FOREIGN KEY (`hostname_id`) REFERENCES `hostnames` (`id`),
  CONSTRAINT `FKhardwares868669` FOREIGN KEY (`hw_type_id`) REFERENCES `hw_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hardwares
-- ----------------------------
INSERT INTO `hardwares` VALUES ('1', 'tes1', '1', '67890', '098765', '567890', '1', '12', '222', '1', null, null, null);

-- ----------------------------
-- Table structure for histories
-- ----------------------------
DROP TABLE IF EXISTS `histories`;
CREATE TABLE `histories` (
  `history_id` varchar(36) NOT NULL,
  `history_detail` text NOT NULL,
  `history_active` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of histories
-- ----------------------------
INSERT INTO `histories` VALUES ('25bee72b-b4d0-11e8-b773-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":null,\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('3f113b14-b4d6-11e8-b773-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('5cba2ec7-b4d6-11e8-b773-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('828a6c15-b4d6-11e8-b773-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('21a8afcd-b4d9-11e8-b773-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('87d0629d-b4dc-11e8-b773-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('0d1f24ec-b4e8-11e8-b773-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"name\":\"tes\"}}]', '1');
INSERT INTO `histories` VALUES ('2fe92685-b4ec-11e8-b773-54650c0f9c19', '[{\"before\":{\"id\":\"2\",\"name\":\"tes\"}},{\"after\":{\"bank_id\":\"\",\"name\":\"tes2\"}}]', '1');
INSERT INTO `histories` VALUES ('81874dbe-b4ec-11e8-b773-54650c0f9c19', '[{\"before\":{\"id\":\"2\",\"name\":\"tes\"}},{\"after\":{\"id\":\"2\",\"name\":\"tes2\"}}]', '1');
INSERT INTO `histories` VALUES ('e9dc093a-b4ed-11e8-b773-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('f906192b-b586-11e8-b40f-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('d48f94f3-b58d-11e8-b40f-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('0e4978d8-b629-11e8-ad1f-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('5394b512-b637-11e8-ad1f-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('65ade376-b637-11e8-ad1f-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('e259af87-b6bd-11e8-89fd-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('3beb5857-b6be-11e8-89fd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"name\":\"tes 3\"}}]', '1');
INSERT INTO `histories` VALUES ('454c75fc-b6be-11e8-89fd-54650c0f9c19', '[{\"before\":{\"id\":\"3\",\"name\":\"tes 3\"}},{\"after\":{\"id\":\"3\",\"name\":\"tes 4\"}}]', '1');
INSERT INTO `histories` VALUES ('91251cce-b6be-11e8-89fd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"name\":\"tes\"}}]', '1');
INSERT INTO `histories` VALUES ('bbd417bf-b6be-11e8-89fd-54650c0f9c19', '[{\"before\":{\"id\":\"2\",\"name\":\"tes\"}},{\"after\":{\"id\":\"2\",\"name\":\"tes1\"}}]', '1');
INSERT INTO `histories` VALUES ('ebd55427-b6be-11e8-89fd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"name\":\"tes\"}}]', '1');
INSERT INTO `histories` VALUES ('f4211e7c-b6be-11e8-89fd-54650c0f9c19', '[{\"before\":{\"id\":\"2\",\"name\":\"tes\"}},{\"after\":{\"id\":\"2\",\"name\":\"tes1\"}}]', '1');
INSERT INTO `histories` VALUES ('2b1d1014-b6c0-11e8-89fd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"name\":\"hw1\"}}]', '1');
INSERT INTO `histories` VALUES ('3401719a-b6c0-11e8-89fd-54650c0f9c19', '[{\"before\":{\"id\":\"1\",\"name\":\"hw1\"}},{\"after\":{\"id\":\"1\",\"name\":\"hw2\"}}]', '1');
INSERT INTO `histories` VALUES ('636f3ad1-b6c0-11e8-89fd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"name\":\"host1\"}}]', '1');
INSERT INTO `histories` VALUES ('6b569891-b6c0-11e8-89fd-54650c0f9c19', '[{\"before\":{\"id\":\"1\",\"name\":\"host1\"}},{\"after\":{\"id\":\"1\",\"name\":\"host2\"}}]', '1');
INSERT INTO `histories` VALUES ('1d33cf9e-b6c1-11e8-89fd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"name\":\"group1\"}}]', '1');
INSERT INTO `histories` VALUES ('261a70c7-b6c1-11e8-89fd-54650c0f9c19', '[{\"before\":{\"id\":\"1\",\"name\":\"group1\"}},{\"after\":{\"id\":\"1\",\"name\":\"group2\"}}]', '1');
INSERT INTO `histories` VALUES ('76f9097e-b6f1-11e8-89fd-54650c0f9c19', '[{\"before\":{\"uid\":null,\"logged_in\":false}},{\"after\":{\"uid\":\"d650\",\"logged_in\":true}}] ', '1');
INSERT INTO `histories` VALUES ('30221473-b6f9-11e8-89fd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"id\":\"d651\",\"name\":\"tes\",\"role_id\":\"2\",\"company_id\":\"1\",\"status_id\":\"2\",\"site_group_id\":\"2\",\"unit_id\":\"2\",\"phone\":\"0987654321\"}}]', '1');
INSERT INTO `histories` VALUES ('cd5e2a73-b6fb-11e8-89fd-54650c0f9c19', '[{\"before\":{\"id\":\"d651\",\"name\":\"tes\",\"role_id\":\"2\",\"company_id\":\"1\",\"phone\":\"0987654321\",\"status_id\":\"2\",\"site_group_id\":\"2\",\"unit_id\":\"2\"}},{\"after\":{\"id\":\"d651\",\"name\":\"tes10\",\"role_id\":\"2\",\"company_id\":\"1\",\"status_id\":\"2\",\"site_group_id\":\"2\",\"unit_id\":\"2\",\"phone\":\"0987654321\"}}]', '1');
INSERT INTO `histories` VALUES ('18ddc9bd-b6fc-11e8-89fd-54650c0f9c19', '[{\"before\":{\"id\":\"d651\",\"name\":\"\",\"role_id\":\"2\",\"company_id\":\"1\",\"phone\":\"0987654321\",\"status_id\":\"2\",\"site_group_id\":\"2\",\"unit_id\":\"2\"}},{\"after\":{\"id\":\"d651\",\"name\":\"tes10\",\"role_id\":\"2\",\"company_id\":\"1\",\"status_id\":\"2\",\"site_group_id\":\"2\",\"unit_id\":\"2\",\"phone\":\"0987654321\"}}]', '1');
INSERT INTO `histories` VALUES ('3ebf4f66-b6fc-11e8-89fd-54650c0f9c19', '[{\"before\":{\"id\":\"3\",\"name\":\"tes 4\"}},{\"after\":{\"id\":\"3\",\"name\":\"tes 5\"}}]', '1');
INSERT INTO `histories` VALUES ('4ce77ae1-b6fc-11e8-89fd-54650c0f9c19', '[{\"before\":{\"id\":\"2\",\"name\":\"tes1\"}},{\"after\":{\"id\":\"2\",\"name\":\"tes10\"}}]', '1');
INSERT INTO `histories` VALUES ('58e2e0cb-b6fc-11e8-89fd-54650c0f9c19', '[{\"before\":{\"id\":\"2\",\"name\":\"tes1\"}},{\"after\":{\"id\":\"2\",\"name\":\"tes10\"}}]', '1');
INSERT INTO `histories` VALUES ('675c0015-b6fc-11e8-89fd-54650c0f9c19', '[{\"before\":{\"id\":\"1\",\"name\":\"hw2\"}},{\"after\":{\"id\":\"1\",\"name\":\"hw20\"}}]', '1');
INSERT INTO `histories` VALUES ('7827672f-b6fc-11e8-89fd-54650c0f9c19', '[{\"before\":{\"id\":\"1\",\"name\":\"host2\"}},{\"after\":{\"id\":\"1\",\"name\":\"host20\"}}]', '1');
INSERT INTO `histories` VALUES ('927d7612-b701-11e8-89fd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"name\":\"tes\",\"hostname_id\":\"1\",\"ipaddress\":\"67890\",\"asset_tag\":\"098765\",\"serial_number\":\"567890\",\"hw_type_id\":\"1\",\"memory\":\"12\",\"hdd\":\"222\",\"status_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('209ef0b6-b70b-11e8-89fd-54650c0f9c19', '[{\"before\":{\"id\":\"1\",\"name\":\"tes\"}},{\"after\":{\"id\":\"1\",\"name\":\"tes1\",\"hostname_id\":\"1\",\"ipaddress\":\"67890\",\"asset_tag\":\"098765\",\"serial_number\":\"567890\",\"hw_type_id\":\"1\",\"memory\":\"12\",\"hdd\":\"222\",\"status_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('bfc2089e-b70c-11e8-89fd-54650c0f9c19', '[{\"before\":{\"id\":\"1\",\"name\":\"group2\"}},{\"after\":{\"id\":\"1\",\"name\":\"group3\"}}]', '1');
INSERT INTO `histories` VALUES ('9fc0a665-b71c-11e8-89fd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"label\":\"tes\",\"group_detail_information_id\":\"1\",\"parent\":\"0\",\"role_id\":\"2\",\"order\":\"1\",\"status_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('621c349a-b71d-11e8-89fd-54650c0f9c19', '[{\"before\":[]},{\"after\":{\"label\":\"tes\",\"group_detail_information_id\":\"1\",\"parent\":\"0\",\"role_id\":\"1\",\"order\":\"1\",\"status_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('c77e19ec-b71e-11e8-89fd-54650c0f9c19', '[{\"before\":{\"id\":\"4\",\"name\":null}},{\"after\":{\"id\":\"4\",\"label\":\"tes1\",\"group_detail_information_id\":\"1\",\"parent\":\"\",\"role_id\":\"1\",\"order\":\"1\",\"status_id\":\"1\"}}]', '1');
INSERT INTO `histories` VALUES ('ba973466-b720-11e8-89fd-54650c0f9c19', '[{\"before\":{\"id\":\"4\",\"label\":\"tes1\",\"group_detail_information_id\":\"1\",\"parent\":\"0\",\"role_id\":\"1\",\"order\":\"1\",\"status_id\":\"1\"}},{\"after\":{\"id\":\"4\",\"label\":\"tes2\",\"group_detail_information_id\":\"1\",\"parent\":\"\",\"role_id\":\"1\",\"order\":\"1\",\"status_id\":\"1\"}}]', '1');

-- ----------------------------
-- Table structure for hostnames
-- ----------------------------
DROP TABLE IF EXISTS `hostnames`;
CREATE TABLE `hostnames` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hostnames
-- ----------------------------
INSERT INTO `hostnames` VALUES ('1', 'host20', null, null, null);

-- ----------------------------
-- Table structure for hw_types
-- ----------------------------
DROP TABLE IF EXISTS `hw_types`;
CREATE TABLE `hw_types` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hw_types
-- ----------------------------
INSERT INTO `hw_types` VALUES ('1', 'hw20', null, null, null);

-- ----------------------------
-- Table structure for icon
-- ----------------------------
DROP TABLE IF EXISTS `icon`;
CREATE TABLE `icon` (
  `icon_name` varchar(100) NOT NULL,
  `icon_group` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`icon_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of icon
-- ----------------------------
INSERT INTO `icon` VALUES ('glyphicon glyphicon-adjust', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-alert', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-align-center', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-align-justify', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-align-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-align-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-apple', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-arrow-down', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-arrow-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-arrow-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-arrow-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-asterisk', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-baby-formula', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-backward', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-ban-circle', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-barcode', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-bed', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-bell', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-bishop', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-bitcoin', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-blackboard', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-bold', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-book', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-bookmark', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-briefcase', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-btc', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-bullhorn', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-calendar', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-camera', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-cd', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-certificate', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-check', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-chevron-down', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-chevron-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-chevron-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-chevron-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-circle-arrow-down', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-circle-arrow-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-circle-arrow-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-circle-arrow-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-cloud', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-cloud-download', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-cloud-upload', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-cog', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-collapse-down', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-collapse-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-comment', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-compressed', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-console', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-copy', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-copyright-mark', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-credit-card', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-cutlery', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-dashboard', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-download', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-download-alt', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-duplicate', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-earphone', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-edit', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-education', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-eject', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-envelope', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-equalizer', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-erase', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-eur', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-euro', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-exclamation-sign', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-expand', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-export', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-eye-close', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-eye-open', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-facetime-video', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-fast-backward', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-fast-forward', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-file', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-film', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-filter', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-fire', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-flag', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-flash', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-floppy-disk', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-floppy-open', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-floppy-remove', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-floppy-save', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-floppy-saved', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-folder-close', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-folder-open', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-font', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-forward', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-fullscreen', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-gbp', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-gift', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-glass', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-globe', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-grain', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-hand-down', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-hand-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-hand-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-hand-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-hd-video', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-hdd', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-header', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-headphones', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-heart', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-heart-empty', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-home', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-hourglass', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-ice-lolly', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-ice-lolly-tasted', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-import', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-inbox', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-indent-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-indent-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-info-sign', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-italic', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-jpy', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-king', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-knight', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-lamp', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-leaf', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-level-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-link', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-list', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-list-alt', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-lock', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-log-in', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-log-out', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-magnet', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-map-marker', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-menu-down', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-menu-hamburger', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-menu-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-menu-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-menu-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-minus', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-minus-sign', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-modal-window', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-move', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-music', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-new-window', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-object-align-bottom', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-object-align-horizontal', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-object-align-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-object-align-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-object-align-top', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-object-align-vertical', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-off', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-oil', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-ok', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-ok-circle', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-ok-sign', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-open', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-open-file', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-option-horizontal', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-option-vertical', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-paperclip', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-paste', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-pause', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-pawn', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-pencil', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-phone', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-phone-alt', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-picture', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-piggy-bank', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-plane', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-play', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-play-circle', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-plus', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-plus-sign', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-print', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-pushpin', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-qrcode', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-queen', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-question-sign', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-random', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-record', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-refresh', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-registration-mark', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-remove', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-remove-circle', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-remove-sign', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-repeat', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-resize-full', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-resize-horizontal', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-resize-small', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-resize-vertical', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-retweet', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-road', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-rub', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-ruble', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-save', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-save-file', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-saved', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-scale', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-scissors', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-screenshot', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sd-video', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-search', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-send', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-share', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-share-alt', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-shopping-cart', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-signal', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sort', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sort-by-alphabet', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sort-by-alphabet-alt', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sort-by-attributes', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sort-by-attributes-alt', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sort-by-order', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sort-by-order-alt', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sound-5-1', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sound-6-1', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sound-7-1', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sound-dolby', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sound-stereo', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-star', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-star-empty', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-stats', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-step-backward', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-step-forward', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-stop', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-subscript', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-subtitles', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-sunglasses', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-superscript', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-tag', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-tags', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-tasks', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-tent', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-text-background', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-text-color', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-text-height', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-text-size', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-text-width', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-th', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-th-large', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-th-list', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-thumbs-down', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-thumbs-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-time', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-tint', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-tower', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-transfer', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-trash', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-tree-conifer', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-tree-deciduous', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-triangle-bottom', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-triangle-left', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-triangle-right', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-triangle-top', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-unchecked', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-upload', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-usd', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-user', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-volume-down', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-volume-off', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-volume-up', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-warning-sign', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-wrench', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-xbt', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-yen', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-zoom-in', 'glyphicon');
INSERT INTO `icon` VALUES ('glyphicon glyphicon-zoom-out', 'glyphicon');

-- ----------------------------
-- Table structure for languages
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `language_id` char(4) NOT NULL,
  `language_name` varchar(50) NOT NULL,
  `language_active` tinyint(4) NOT NULL,
  `language_default` tinyint(4) NOT NULL,
  PRIMARY KEY (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of languages
-- ----------------------------
INSERT INTO `languages` VALUES ('id', 'Indonesia', '1', '1');

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(50) NOT NULL,
  `menu_url` varchar(50) NOT NULL,
  `menu_icon` varchar(50) NOT NULL,
  `menu_parent` int(11) NOT NULL,
  `menu_order` smallint(6) NOT NULL,
  `menu_active` tinyint(4) NOT NULL,
  `menu_create_module` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES ('1', 'settings', '#', 'glyphicon glyphicon-cog', '0', '1', '1', '0');
INSERT INTO `menus` VALUES ('2', 'users', 'users', 'glyphicon glyphicon-user', '32', '1', '1', '0');
INSERT INTO `menus` VALUES ('3', 'roles', 'roles', 'glyphicon glyphicon-cog', '1', '2', '1', '0');
INSERT INTO `menus` VALUES ('4', 'menus', 'menus', 'glyphicon glyphicon-equalizer', '1', '3', '1', '0');
INSERT INTO `menus` VALUES ('32', 'masterusers', '#', 'glyphicon glyphicon-equalizer', '0', '2', '1', '0');
INSERT INTO `menus` VALUES ('33', 'companies', 'companies', 'glyphicon glyphicon-equalizer', '32', '2', '1', '0');
INSERT INTO `menus` VALUES ('34', 'sitegroups', 'sitegroups', 'glyphicon glyphicon-equalizer', '32', '3', '1', '0');
INSERT INTO `menus` VALUES ('35', 'units', 'units', 'glyphicon glyphicon-equalizer', '32', '4', '1', '0');
INSERT INTO `menus` VALUES ('36', 'masterhardwares', '#', 'glyphicon glyphicon-equalizer', '0', '3', '1', '0');
INSERT INTO `menus` VALUES ('37', 'hardwares', 'hardwares', 'glyphicon glyphicon-equalizer', '36', '1', '1', '0');
INSERT INTO `menus` VALUES ('38', 'hwtypes', 'hwtypes', 'glyphicon glyphicon-equalizer', '36', '2', '1', '0');
INSERT INTO `menus` VALUES ('39', 'hostnames', 'hostnames', 'glyphicon glyphicon-equalizer', '36', '3', '1', '0');
INSERT INTO `menus` VALUES ('40', 'masterinformations', '#', 'glyphicon glyphicon-equalizer', '0', '4', '1', '0');
INSERT INTO `menus` VALUES ('41', 'informations', 'informations', 'glyphicon glyphicon-equalizer', '40', '1', '1', '0');
INSERT INTO `menus` VALUES ('42', 'groupinformations', 'groupinformations', 'glyphicon glyphicon-equalizer', '40', '2', '1', '0');
INSERT INTO `menus` VALUES ('43', 'UAT', '#', 'glyphicon glyphicon-equalizer', '0', '5', '1', '0');
INSERT INTO `menus` VALUES ('44', 'migrations', 'migrations', 'glyphicon glyphicon-equalizer', '43', '1', '1', '0');
INSERT INTO `menus` VALUES ('45', 'reports', '#', 'glyphicon glyphicon-equalizer', '0', '6', '1', '0');
INSERT INTO `menus` VALUES ('46', 'reportmigrations', 'reportmigrations', 'glyphicon glyphicon-equalizer', '45', '1', '1', '0');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `hardware_id` int(10) NOT NULL,
  `user_id` varchar(5) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `user_approve_at` timestamp NULL DEFAULT NULL,
  `it_approve_at` timestamp NULL DEFAULT NULL,
  `status_id` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKMigrations683892` (`hardware_id`),
  KEY `FKMigrations59032` (`user_id`),
  CONSTRAINT `FKMigrations59032` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FKMigrations683892` FOREIGN KEY (`hardware_id`) REFERENCES `hardwares` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of migrations
-- ----------------------------

-- ----------------------------
-- Table structure for migration_details
-- ----------------------------
DROP TABLE IF EXISTS `migration_details`;
CREATE TABLE `migration_details` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `detail_information_id` int(10) NOT NULL,
  `migration_id` int(10) NOT NULL,
  `value_before` smallint(1) NOT NULL,
  `value_after` smallint(1) NOT NULL,
  `note_before` varchar(255) DEFAULT NULL,
  `note_after` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKmigration_214371` (`detail_information_id`),
  KEY `FKmigration_716156` (`migration_id`),
  CONSTRAINT `FKmigration_214371` FOREIGN KEY (`detail_information_id`) REFERENCES `detail_informations` (`id`),
  CONSTRAINT `FKmigration_716156` FOREIGN KEY (`migration_id`) REFERENCES `migrations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of migration_details
-- ----------------------------

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(50) NOT NULL,
  `role_level` int(11) NOT NULL,
  `role_active` tinyint(4) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'SUPER ADMIN', '99', '1');
INSERT INTO `roles` VALUES ('2', 'MEMBER', '1', '1');
INSERT INTO `roles` VALUES ('3', 'VISITOR', '2', '1');
INSERT INTO `roles` VALUES ('4', 'OPERATOR', '3', '1');

-- ----------------------------
-- Table structure for role_menus
-- ----------------------------
DROP TABLE IF EXISTS `role_menus`;
CREATE TABLE `role_menus` (
  `role_menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`role_menu_id`),
  KEY `role_id` (`role_id`),
  KEY `role_menus_ibfk_1` (`menu_id`),
  CONSTRAINT `role_menus_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`menu_id`),
  CONSTRAINT `role_menus_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=301 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of role_menus
-- ----------------------------
INSERT INTO `role_menus` VALUES ('263', '1', '1');
INSERT INTO `role_menus` VALUES ('264', '1', '2');
INSERT INTO `role_menus` VALUES ('265', '1', '3');
INSERT INTO `role_menus` VALUES ('266', '1', '4');
INSERT INTO `role_menus` VALUES ('286', '1', '32');
INSERT INTO `role_menus` VALUES ('287', '1', '33');
INSERT INTO `role_menus` VALUES ('288', '1', '34');
INSERT INTO `role_menus` VALUES ('289', '1', '35');
INSERT INTO `role_menus` VALUES ('290', '1', '36');
INSERT INTO `role_menus` VALUES ('291', '1', '37');
INSERT INTO `role_menus` VALUES ('292', '1', '38');
INSERT INTO `role_menus` VALUES ('293', '1', '39');
INSERT INTO `role_menus` VALUES ('294', '1', '40');
INSERT INTO `role_menus` VALUES ('295', '1', '41');
INSERT INTO `role_menus` VALUES ('296', '1', '42');
INSERT INTO `role_menus` VALUES ('297', '1', '43');
INSERT INTO `role_menus` VALUES ('298', '1', '44');
INSERT INTO `role_menus` VALUES ('299', '1', '45');
INSERT INTO `role_menus` VALUES ('300', '1', '46');

-- ----------------------------
-- Table structure for site_groups
-- ----------------------------
DROP TABLE IF EXISTS `site_groups`;
CREATE TABLE `site_groups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of site_groups
-- ----------------------------
INSERT INTO `site_groups` VALUES ('1', 'site A', null, null, null);
INSERT INTO `site_groups` VALUES ('2', 'tes10', null, null, null);

-- ----------------------------
-- Table structure for units
-- ----------------------------
DROP TABLE IF EXISTS `units`;
CREATE TABLE `units` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of units
-- ----------------------------
INSERT INTO `units` VALUES ('1', 'unit A', null, null, null);
INSERT INTO `units` VALUES ('2', 'tes10', null, null, null);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` varchar(5) NOT NULL,
  `name` varchar(128) NOT NULL,
  `company_id` int(10) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `status_id` smallint(10) NOT NULL COMMENT '1:active;2:inactive',
  `site_group_id` int(10) NOT NULL,
  `unit_id` int(10) NOT NULL,
  `role_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKusers509495` (`unit_id`),
  KEY `FKusers467682` (`site_group_id`),
  KEY `FKusers45309` (`company_id`),
  KEY `FKuserroles` (`role_id`),
  CONSTRAINT `FKuserroles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
  CONSTRAINT `FKusers45309` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`),
  CONSTRAINT `FKusers467682` FOREIGN KEY (`site_group_id`) REFERENCES `site_groups` (`id`),
  CONSTRAINT `FKusers509495` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('d650', 'Tantan', '1', '0987654321', '1', '1', '1', '1');
INSERT INTO `users` VALUES ('d651', 'tes10', '1', '0987654321', '2', '2', '2', '2');

-- ----------------------------
-- Procedure structure for log_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `log_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `log_insert`(IN id_status varchar(20), IN user_id varchar(36),IN ip_address varchar(16),IN browser varchar(255),IN p_status varchar(4),IN method_name varchar(25),IN module_name varchar(32),IN history_detail TEXT)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @is_success = @success;
			SET @history_id = @last_history_id;

				IF @is_success = 1 THEN
					CALL sp_audit_trail_insert (user_id,ip_address,browser,@history_id,p_status,method_name,module_name,id_status,@success);
					SET @is_success = @success;
					
					IF @is_success = 1 THEN
						COMMIT;
					END IF;
				END IF;
		SELECT @is_success AS jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_audit_trail_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_audit_trail_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_audit_trail_insert`(IN user_id varchar(36), IN ip_address varchar(16),
 IN browser varchar(255), IN history_id VARCHAR(36), IN p_status varchar(4), IN action_id varchar(25), IN module_id varchar(32),IN record_id varchar(36), OUT is_success BIT)
BEGIN 
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @audit_id = (SELECT UUID());

			INSERT INTO audit_trails
				VALUES(@audit_id, CURRENT_TIMESTAMP, user_id, ip_address, browser, history_id, p_status, action_id, module_id, record_id);

			SET is_success = 1;
		COMMIT;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_company_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_company_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_company_insert`(IN p_name varchar(64),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			INSERT INTO companies(`name`)
				VALUES(p_name);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_name,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_company_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_company_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_company_update`(IN p_id INT,IN p_name varchar(50),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			UPDATE companies
			SET `name` = p_name
			WHERE id = p_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_configuration_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_configuration_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_configuration_update`(IN p_configuration_id varchar(36),IN p_configuration_value longtext,IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			UPDATE configurations
			SET configuration_value = p_configuration_value
			WHERE configuration_id = p_configuration_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_configuration_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_groupinformation_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_groupinformation_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_groupinformation_insert`(IN p_name varchar(64),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			INSERT INTO group_detail_informations(`name`)
				VALUES(p_name);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_name,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_groupinformation_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_groupinformation_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_groupinformation_update`(IN p_id INT,IN p_name varchar(50),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			UPDATE group_detail_informations
			SET `name` = p_name
			WHERE id = p_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_hardware_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_hardware_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hardware_insert`(IN p_name varchar(64), IN p_hostname_id INT, IN p_ipaddress varchar(32), IN p_asset_tag varchar(32), IN p_serial_number varchar(32), IN p_hw_type_id INT, IN p_memory varchar(16), IN p_hdd varchar(16), IN p_status_id INT, IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			INSERT INTO hardwares(`name`,hostname_id,ipaddress,asset_tag,serial_number,hw_type_id,memory,hdd,status_id)
				VALUES(p_name, p_hostname_id, p_ipaddress, p_asset_tag, p_serial_number, p_hw_type_id, p_memory, p_hdd, p_status_id);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_name,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_hardware_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_hardware_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hardware_update`(IN p_id INT, IN p_name varchar(64), IN p_hostname_id INT, IN p_ipaddress varchar(32), IN p_asset_tag varchar(32), IN p_serial_number varchar(32), IN p_hw_type_id INT, IN p_memory varchar(16), IN p_hdd varchar(16), IN p_status_id INT, IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			UPDATE hardwares
			SET 
				`name` = p_name,
				hostname_id = p_hostname_id, 
				ipaddress = p_ipaddress, 
				asset_tag = p_asset_tag, 
				serial_number = p_serial_number, 
				hw_type_id = p_hw_type_id, 
				memory = p_memory, 
				hdd = p_hdd, 
				status_id = p_status_id
			WHERE id = p_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_histories_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_histories_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_histories_insert`(IN history_detail text, IN history_active INT, OUT is_success BIT,OUT history_id varchar(36))
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @history_id = (SELECT UUID());

			INSERT INTO histories(history_id,history_detail, history_active)
				VALUES(@history_id,history_detail, history_active);

			SET is_success = 1;
			SET history_id = @history_id;
		COMMIT;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_hostname_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_hostname_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hostname_insert`(IN p_name varchar(64),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			INSERT INTO hostnames(`name`)
				VALUES(p_name);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_name,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_hostname_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_hostname_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hostname_update`(IN p_id INT,IN p_name varchar(50),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			UPDATE hostnames
			SET `name` = p_name
			WHERE id = p_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_hwtypes_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_hwtypes_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hwtypes_insert`(IN p_name varchar(64),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			INSERT INTO hw_types(`name`)
				VALUES(p_name);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_name,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_hwtypes_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_hwtypes_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_hwtypes_update`(IN p_id INT,IN p_name varchar(50),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			UPDATE hw_types
			SET `name` = p_name
			WHERE id = p_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_icon_list
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_icon_list`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_icon_list`()
BEGIN
	#Routine body goes here...
	SELECT icon_name FROM icon ORDER BY icon_name ASC;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_information_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_information_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_information_insert`(IN p_label varchar(255), IN p_status_id INT, IN p_group_detail_information_id INT, IN p_parent INT, IN p_role_id INT, IN p_order INT, IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			INSERT INTO detail_informations(label,status_id,group_detail_information_id,parent,role_id,`order`)
				VALUES(p_label, p_status_id, p_group_detail_information_id, p_parent, p_role_id, p_order);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_label,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_information_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_information_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_information_update`(IN p_id INT, IN p_label varchar(255), IN p_status_id INT, IN p_group_detail_information_id INT, IN p_parent INT, IN p_role_id INT, IN p_order INT, IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			UPDATE detail_informations
			SET 
				label = p_label, 
				status_id = p_status_id, 
				group_detail_information_id = p_group_detail_information_id, 
				parent = p_parent, 
				role_id = p_role_id, 
				`order` = p_order
			WHERE id = p_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_menu_list
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_menu_list`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_menu_list`()
BEGIN
	#Routine body goes here...
	SELECT a.*,(CASE WHEN menu_active = '1' THEN 'active' ELSE 'inactive' END) AS menu_active_label,
	(CASE WHEN menu_parent = 0 THEN
		'PARENT'
	ELSE
		(SELECT menu_name FROM menus WHERE menu_id = a.menu_parent)
	END) AS menus
	FROM menus a;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_menu_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_menu_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_menu_update`(IN p_menu_id INT,IN p_menu_icon varchar(50),IN p_menu_active tinyint,IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

	START TRANSACTION;
		UPDATE menus
		SET	
			menu_icon = p_menu_icon,
			menu_active = p_menu_active
		WHERE menu_id = p_menu_id;

		CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
		SET @histories_success = @success;
		SET @history_id = @last_history_id;

		IF @histories_success = 1 THEN
			CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_menu_id,@success);
			SET @is_success = @success;
		END IF;

		SET @is_success = 1;
	COMMIT;
	SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_role_list
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_role_list`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_role_list`()
BEGIN
	#Routine body goes here...
	SELECT 
		c.role_id,
		c.role_name,
		role_level,
		role_active,
		CASE WHEN role_active = '1' THEN 'active' ELSE 'inactive' END AS role_active_label,
		GROUP_CONCAT(b.menu_name SEPARATOR ', ') AS roles
	FROM role_menus a
	LEFT JOIN menus b ON a.menu_id = b.menu_id
	RIGHT JOIN roles c ON a.role_id = c.role_id
	GROUP BY role_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_role_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_role_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_role_update`(IN p_role_id INT,IN p_menu_id varchar(255),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

	START TRANSACTION;
		DELETE FROM role_menus WHERE role_id = p_role_id;

		iterator:
		LOOP
			IF LENGTH(TRIM(p_menu_id)) = 0 OR p_menu_id IS NULL THEN
				LEAVE iterator;
			END IF;

			SET @next = SUBSTRING_INDEX(p_menu_id,',',1);
			SET @nextlen = LENGTH(@next);
			SET @value = TRIM(@next);
			INSERT INTO role_menus (role_id,menu_id) VALUES (p_role_id,@next);
			SET p_menu_id = INSERT(p_menu_id,1,@nextlen + 1,'');

		END LOOP;

		CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
		SET @histories_success = @success;
		SET @history_id = @last_history_id;

		IF @histories_success = 1 THEN
			CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_role_id,@success);
			SET @is_success = @success;
		END IF;

		SET @is_success = 1;
	COMMIT;
	SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_sitegroup_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_sitegroup_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_sitegroup_insert`(IN p_name varchar(64),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			INSERT INTO site_groups(`name`)
				VALUES(p_name);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_name,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_sitegroup_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_sitegroup_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_sitegroup_update`(IN p_id INT,IN p_name varchar(50),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			UPDATE site_groups
			SET `name` = p_name
			WHERE id = p_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_unit_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_unit_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_unit_insert`(IN p_name varchar(64),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			INSERT INTO units(`name`)
				VALUES(p_name);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_name,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_unit_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_unit_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_unit_update`(IN p_id INT,IN p_name varchar(50),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			UPDATE units
			SET `name` = p_name
			WHERE id = p_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_users_delete
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_users_delete`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_users_delete`(IN p_user_id varchar(36),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			DELETE FROM users
			WHERE
				user_id = p_user_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_users_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_users_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_users_insert`(IN user_identity varchar(25),IN user_name varchar(100),IN role_id INT,IN user_salt varchar(5),IN user_password text,IN user_status BIT,IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			SET @user_id = (SELECT UUID());
			SET @currentdate = (SELECT NOW());

			INSERT INTO users(user_id,user_identity, user_role_id,user_name,user_registerdate,user_salt,user_password,user_status)
				VALUES(@user_id,user_identity,role_id,user_name,@currentdate,user_salt,user_password,user_status);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,@user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_users_resetpassword
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_users_resetpassword`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_users_resetpassword`(IN p_user_id varchar(36),IN p_user_password text,IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			UPDATE users
			SET
				user_password = p_user_password
			WHERE
				user_id = p_user_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_users_status
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_users_status`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_users_status`(IN p_user_id varchar(36),IN p_user_status BIT,IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			UPDATE users
			SET
				user_status = p_user_status
			WHERE
				user_id = p_user_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_users_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_users_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_users_update`(IN p_user_id varchar(36),IN user_identity varchar(25),IN user_name varchar(100),IN role_id INT,IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			UPDATE users
			SET
				user_identity = user_identity,
				user_role_id = role_id,
				user_name = user_name
			WHERE
				user_id = p_user_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_user_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_user_insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_user_insert`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_insert`(IN p_id varchar(5), IN p_name varchar(128), IN p_company_id INT(10), IN p_phone varchar(32), IN p_status_id smallint, IN p_site_group_id  INT(10), IN p_unit_id  INT(10), IN p_role_id  INT(10), IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			INSERT INTO users(`id`,`name`,`company_id`,`phone`,`status_id`,`site_group_id`,`unit_id`,`role_id`)
				VALUES(p_id, p_name, p_company_id, p_phone, p_status_id, p_site_group_id, p_unit_id, p_role_id);

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_name,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_user_update
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_user_update`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_update`(IN p_id varchar(5), IN p_name varchar(128), IN p_company_id INT(10), IN p_phone varchar(32), IN p_status_id smallint, IN p_site_group_id  INT(10), IN p_unit_id  INT(10), IN p_role_id  INT(10),IN userid varchar(36),IN ip_address varchar(50),IN browser text,IN p_status varchar(4),IN method_name varchar(255),IN module_name varchar(255),IN history_detail text)
BEGIN
	#Routine body goes here...
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
		BEGIN
			SET @is_success = 0;
			ROLLBACK;
    END;

    START TRANSACTION;
			UPDATE users
				SET 
					`name` = p_name,
					company_id = p_company_id, 
					phone = p_phone, 
					status_id = p_status_id, 
					site_group_id = p_site_group_id, 
					unit_id = p_unit_id, 
					role_id = p_role_id
				WHERE 
					id = p_id;

			CALL sp_histories_insert (history_detail,1,@success, @last_history_id);
			SET @histories_success = @success;
			SET @history_id = @last_history_id;

			IF @histories_success = 1 THEN
				CALL sp_audit_trail_insert (userid,ip_address,browser,@history_id,p_status,method_name,module_name,p_id,@success);
				SET @is_success = @success;
			END IF;

			SET @is_success = 1;
		COMMIT;
		SELECT @is_success as jml;
END
;;
DELIMITER ;
